<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]
namespace think;

// 加载基础文件
require __DIR__ . '/../thinkphp/base.php';

// 修改admin端控制器层目录
if(strpos($_SERVER['HTTP_HOST'], 'admin') === 0 ) {
    // 定义站点名
    define('SITE_NAME', 'admin');
    // 定义控制器层目录，若不定义则默认使用站点名作为目录名
    define('URL_CONTROLLER_LAYER', 'admin');
}else if(strpos($_SERVER['HTTP_HOST'], 'api') === 0 ) { // 修改api端控制器层目录
    // 定义站点名
    define('SITE_NAME', 'api');
    // 定义控制器层目录，若不定义则默认使用站点名作为目录名
    define('URL_CONTROLLER_LAYER', 'api');
}else {
    // 定义站点名
    define('SITE_NAME', 'default');
    // 定义控制器层目录，若不定义则默认使用站点名作为目录名
    define('URL_CONTROLLER_LAYER', 'controller');
}
//支持跨域
header("Access-Control-Allow-Origin:*");
header('Access-Control-Allow-Methods:*');
header('Access-Control-Allow-Headers:*');

// 获取header中的语言参数, 并注入到Get中
if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
    preg_match('/^([a-z\d]+)/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $matches);
    // 由于header中accept-language中的语言标识是用语言与地区组装的, 如(zh-CN, en-CN), 因此修改header语言标识, 去掉地区段落, 方便框架自动识别语言
    $_SERVER['HTTP_ACCEPT_LANGUAGE'] = strtolower($matches[1]);
}
/** @var App $app */
$app = Container::get('app');
$app->lang->setAcceptLanguage([
    'zh' => 'zh-cn'
]);

// 支持事先使用静态方法设置Request对象和Config对象

// 执行应用并响应
Container::get('app')->run()->send();
