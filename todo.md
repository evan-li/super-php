## 框架

- [x] 构建器
- [x] 模块扩展
- [x] system模块
    - [x] 权限/用户/角色管理
    - [x] 模块管理
    - [x] 附件管理-文件存储
        - [x] 本地
        - [x] 七牛
        - [x] 阿里云
        - [ ] 腾讯云
        - [x] AWS S3

## 模块市场

### 基础模块
- [ ] 示例模块
- [ ] 数据模块 - 地区/国家数据
- [ ] 微信模块 - 公众号/小程序/微信开放平台
- [ ] 支付模块 - 微信支付/支付宝/applePay/paypal
- [ ] 消息模块
    - [ ] 系统内消息
    - [ ] 短信
    - [ ] push
    - [ ] 邮件

### 业务模块
- [ ] 用户模块
- [ ] 商城模块

### Saas 业务模块
- [ ] 用户模块
- [ ] 商城模块
- [ ] 项目方模块