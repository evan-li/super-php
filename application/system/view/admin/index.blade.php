<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>后台管理</title>
    @foreach(mix('index.html')['css'] as $css)
        <link rel="stylesheet" href="{{$css}}">
    @endforeach
    @foreach($icons as $icon)
    <link rel="stylesheet" href="{{$icon['css_url']}}">
    @endforeach
</head>
<body>

<div id="app"></div>

<script>
    var captcha_url = '{{captcha_src()}}'
    var systemConfigUrl = '{{$config["system_config_url"]}}'
    const SITE_NAME = '{{SITE_NAME}}'
    const INDEX_PAGE_PATH = '{{$config["index_page_path"]}}'
</script>
<script type="module"  src="{{mix('index.html')['file']}}"></script>
</body>
</html>
