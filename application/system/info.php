<?php

return [
    // ------------- 模块基本信息
    // 模块名
    'name' => 'system',
    // 模块中文名
    'title' => '系统模块',
    // 作者
    'author' => 'EvanLee',
    // 模块描述
    'desc' => '系统核心模块',
    // 模块主页地址
    'homepage' => 'https://www.kancloud.cn/evanlee/sphp',
    // 模块版本号
    'version' => '1.0.0',

    // 模块初始化sql配置
    'sql' => [
        // 模块安装sql, 默认为 sql/install.sql
        'install' => ['sql/install.sql', 'sql/areas.sql', 'sql/coutrys.sql'],
        // 卸载时执行的sql, 默认为 sql目录下的uninstall.sql
        'uninstall' => 'uninstall.sql',
    ],
    // 配置钩子, 用于安装及卸载模块时的操作, 非必须, 默认为当前模块下与模块名相同的类, 如: \app\system\System
    // 两种配置方式:
    //       1. 直接配置类名, 会自动在适当时机执行 install / uninstall / enable / disable 方法
    //       1.1. 可配置为数组, install与enable钩子顺序执行, uninstall与disable倒序执行
    //       2. 配置具体的钩子, 分别为  install / uninstall / enable / disable , 每个钩子指向具体的方法, 不配置方法时默认执行与钩子相同的方法名, 如install时执行install方法
    //       2.2 同样的每个钩子可以配置为数组, install/enable正序执行, uninstall/disable 倒序执行
    'hook' => '',

    // 除核心模块外依赖的其他模块列表, 核心模块包括系统模块与common模块, 这两个基础模块不需要在这里声明
    'require-modules' => [
//        'xxx' => '1.0.0'
    ],
    // composer 依赖的模块列表
    'require' => [
//        'xxx' => '1.0.0'
    ],
    // composer dev 依赖列表
    'require-dev' => [],
];