<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 * Date: 2019/5/3
 * Time: 21:38
 */

namespace app\system\api;


use app\system\service\AttachmentService;
use evan\sbuilder\controller\Base;
use Exception;
use think\helper\Str;

class Attachment extends Base
{

    /**
     * 文件上传
     * 图片存储地址 public/uploads/images
     * 图片存储地址 public/uploads/videos
     * 其他文件存储地址 public/uploads/files
     * 文件类型, 根据传入的dir区分
     * @param string $dir 存储目录 images-图片 files-其他文件
     * @return \think\Response|\think\response\Json
     * @throws Exception
     */
    public function upload($dir = '')
    {
        if(empty($dir)) return res_error('没有指定上传目录');

        $file = request()->file('file');
        // 判断附件格式是否符合
        $fileName = input('filename') ?: $file->getInfo('name');
        log_info('上传的文件名: ' . $fileName, $file->getInfo());
        $fileExt  = strtolower(substr($fileName, strrpos($fileName, '.')+1));
        $extLimit = $dir == 'images' ? config('filesystem.upload_image_ext') : ($dir == 'videos' ? config('filesystem.upload_video_ext') : config('filesystem.upload_file_ext'));
        $extLimit = explode(',', $extLimit);

        // 判断是否非法文件
        if ($file->getMime() == 'text/x-php' || $file->getMime() == 'text/html') {
            return res_error('禁止上传非法文件！');
        }
        if (preg_grep("/php/i", $extLimit)) {
            return res_error('禁止上传非法文件！');
        }
        // 判断文件后缀类型是否合法
        if (!preg_grep("/$fileExt/i", $extLimit)) {
            log_error('文件类型不正确', compact('fileExt', 'extLimit'));
            return res_error('文件类型不正确！');
        }

        $attachment = AttachmentService::upload($file, $dir);

        return res_ok([
            'id' => $attachment->id,
            'url' => $attachment->url,
            'thumb' => $attachment->thumb,
            'width' => $attachment->width,
            'height' => $attachment->height,
        ]);
    }

    /**
     * 附件下载
     */
    public function download()
    {
        $external = request()->param('external', false);
        if(!$external){
            // 如果是内部附件
            $res = $this->validate($this->request->param(), [
                'id' => 'require'
            ]);
            if($res !== true){
                return admin_error($res);
            }
            $id = $this->request->param('id');
            $url = get_file_path($id);
            if(Str::startsWith($url, '/uploads')){
                $path = public_path($url);
            }else {
                $path = $url;
            }
        }else {
            $res = $this->validate($this->request->param(), [
                'url' => 'require'
            ]);
            if($res !== true){
                return admin_error($res);
            }
            $path = $this->request->param('url');
        }
        return download($path);
    }

}