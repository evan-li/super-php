<?php


namespace app\system\model;


use app\common\model\BaseModel;
use think\Collection;

/**
 * 图标库
 * Class Icon
 * @package app\system\admin
 *
 * @property int id
 * @property string name
 * @property int type
 * @property string font_family
 * @property string css_url
 * @property string js_url
 * @property string icons
 * @property int status
 * @property int create_time
 * @property int update_time
 */
class Icon extends BaseModel
{
    protected $name = 'system_icon';

    /**
     * 获取可用的图标库列表
     * @return array|Collection
     */
    public static function getEnableIcons()
    {
        return self::where('status', 1)->cache(true, 5)->select();
    }

}