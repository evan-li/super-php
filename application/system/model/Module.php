<?php

namespace app\system\model;

use app\common\model\BaseModel;

/**
 * 已安装模块列表
 *
 * @property int id
 * @property string name
 * @property string title
 * @property string author
 * @property string desc
 * @property string cover
 * @property string pics
 * @property int type
 * @property string version
 * @property int status
 */
class Module extends BaseModel
{
    protected $name = 'system_module';

}