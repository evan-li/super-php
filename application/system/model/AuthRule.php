<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 * Date: 2019/5/2
 * Time: 16:40
 */

namespace app\system\model;


use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\DbException;
use think\Model;

/**
 * Class AuthRule
 * @package app\system\model
 *
 * @property int id
 * @property int pid
 * @property string name
 * @property string url
 * @property string icon
 * @property int is_menu
 * @property int sort
 * @property int status
 * @property int is_dev
 * @property int create_time
 * @property int update_time
 */
class AuthRule extends Model
{
    protected $name = 'system_auth_rule';

    /**
     * 以树形方式获取所有的权限列表
     * @param bool $withDev
     * @param null|string|array $fields
     * @return array
     */
    public static function getAllTree($withDev = false, $fields = null)
    {
        if(!empty($fields)){
            if (is_string($fields)){
                $fields = explode(',', $fields);
            }
            // id与pid为必须项
            if (!in_array('id', $fields)) $fields[] = 'id';
            if (!in_array('pid', $fields)) $fields[] = 'pid';
        }
        $query = self::field($fields);
        if (!$withDev) {
            $query->where('is_dev', 0);
        }
        $list = $query->order('sort', 'desc')->select();
        $tree = convert_list_to_tree($list);
        return $tree;
    }

    /**
     * 获取所有可用的权限列表
     * @param bool $withDev
     * @param null $fields
     * @return array
     */
    public static function getAllEnableTree($withDev = false, $fields = null)
    {
        if(!empty($fields)){
            if (is_string($fields)){
                $fields = explode(',', $fields);
            }
            // id与pid为必须项
            if (!in_array('id', $fields)) $fields[] = 'id';
            if (!in_array('pid', $fields)) $fields[] = 'pid';
        }
        $query = self::field($fields);
        if (!$withDev) {
            $query->where('is_dev', 0);
        }
        $list = $query->where('status', 1)->order('sort', 'desc')->select();
        $tree = convert_list_to_tree($list);
        return $tree;
    }

    /**
     * 获取所有层级的父ID列表
     * @param $ruleIds
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function getAllTierPids($ruleIds)
    {
        if(!is_array($ruleIds)) $ruleIds = explode(',', $ruleIds);
        $pids = self::whereIn('id', $ruleIds)->column('pid');
        // 去除为0的pid
        $pids = array_filter(array_unique($pids), function($item){return $item > 0;});
        if(empty($pids)) return [];
        $ppids = self::getAllTierPids($pids);
        return array_unique(array_merge($ppids, $pids));
    }

    /**
     * 根据权限ID获取权限所有的父ID
     * @param $pid
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function getPidsById($pid)
    {
        /** @var AuthRule $rule */
        $parent = AuthRule::field('id,pid')->get($pid);
        $ppids = [];
        if($parent->pid > 0){
            $ppids = self::getPidsById($parent->pid);
        }
        return array_merge([$pid], $ppids);
    }

    /**
     * 从权限树中获取权限列表
     * @param $tree
     * @param $list
     * @return array
     */
    private static function getRuleListFromTree($tree, &$list)
    {
        foreach ($tree as &$item) {
            $list[] = &$item;
            if(isset($item['sub']) && is_array($item['sub']) && sizeof($item['sub']) > 0){
                self::getRuleListFromTree($item['sub'], $list);
            }
        }
        return $list;
    }

}