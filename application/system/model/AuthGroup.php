<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 * Date: 2019/5/4
 * Time: 11:55
 */
namespace app\system\model;


use app\common\model\BaseModel;

/**
 * 角色
 * Class AuthGroup
 * @package app\system\model
 *
 * @property int id
 * @property string name
 * @property string rule_ids
 * @property int status
 * @property int create_time
 * @property int update_time
 */
class AuthGroup extends BaseModel
{
    protected $name = 'system_auth_group';

}