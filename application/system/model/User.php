<?php


namespace app\system\model;


use app\common\model\BaseModel;
use think\Collection;

/**
 * Class User
 * @package app\system\model
 *
 * @property int id
 * @property string username
 * @property string nickname
 * @property string password
 * @property string salt
 * @property string avatar
 * @property string email
 * @property int status
 * @property int group_id
 * @property int is_super
 * @property int is_dev
 * @property int create_time
 * @property int update_time
 *
 * @property AuthGroup auth_group
 */
class User extends BaseModel
{
    protected $name = 'system_user';

    /**
     * 生成加密的密码
     * @param $password
     * @param $salt
     * @return string
     */
    public static function hashPassword($password, $salt)
    {
        return md5(md5($password) . $salt);
    }

    /**
     * 获取用户的权限列表, 默认获取后存入session,后续会从session中拿出
     * @param User $currentUser
     * @param bool $force 强制重新获取
     * @return array
     */
    public static function getAuthRules($currentUser, $force = false)
    {
        if($force || empty($rules = session('user_rules'))){
            if($currentUser->is_super == 1){
                $query = AuthRule::where('status', 1)->order('sort', 'desc');
                if (!$currentUser->is_dev) {
                    $query->where('is_dev', 0);
                }
                $rules = $query->select();
            }else {
                $ruleIds = AuthGroup::where('id', $currentUser->group_id)->value('rule_ids');
                $query = AuthRule::where('id', 'in', explode(',', $ruleIds))
                    ->where('status', 1)
                    ->order('sort', 'desc');
                if (!$currentUser->is_dev) {
                    $query->where('is_dev', 0);
                }
                $rules = $query->select();
            }
            session('user_rules', $rules);
        }
        return $rules;
    }

    /**
     * 获取用户菜单, 树形结构, 默认获取后存入session,后续会从session中拿出
     * @param $currentUser
     * @param bool $force 是否强制重新获取
     * @return array
     */
    public static function getMenus($currentUser, $force = false)
    {
        if($force || empty($menus = session('user_menus'))){
            $rules = self::getAuthRules($currentUser, $force);
            if($rules instanceof Collection) $rules = $rules->all();
            $menus = array_filter($rules, function($item){return $item->is_menu; });
            $menus = convert_list_to_tree(array_values($menus));
            array_unshift($menus, ['id' => -1, 'name' => '控制台', 'icon' => 'fa fa-dashboard', 'url' => '/dashboard']);
            session('user_menus', $menus);
        }

        return $menus;
    }

    public function authGroup()
    {
        return $this->hasOne(AuthGroup::class, 'id', 'group_id');
    }

}