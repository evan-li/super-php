<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 * Date: 2019/5/4
 * Time: 11:29
 */

namespace app\system\model;


use app\common\model\BaseModel;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\DbException;

/**
 * Class Attachment
 * @package app\system\model
 *
 * @property int id
 * @property string name
 * @property string url
 * @property string thumb
 * @property int width
 * @property int height
 */
class Attachment extends BaseModel
{
    protected $name = 'system_attachment';

    /**
     * 根据ID获取文件路径
     * @param string|array $id
     * @return array|bool|string
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public static function getFilePath($id = '')
    {
        if (is_array($id)) {
            /*$data_list = self::where('id', 'in', $id)->cache(true, 3600)->select();
            $paths = [];
            foreach ($data_list as $key => $value) {
                $paths[$key] = $value['url'];
            }*/
            $paths = [];
            foreach ($id as $item) {
                $paths[] = is_numeric($item) ? self::getFilePath($item) : $item;
            }
            return $paths;
        } else if(!empty($id)) {
            if(is_numeric($id)) {
                $data = self::where('id', $id)->cache(true, 86400 * 30)->find();
                if ($data) {
                    return $data['url'];
                } else {
                    return false;
                }
            }else {
                return $id;
            }
        }else {
            return $id;
        }
    }

    /**
     * 根据ID获取缩略图(目前仅支持视频)
     * @param string|array $id
     * @return array|bool|string
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public static function getThumb($id = '')
    {
        if (is_array($id)) {
            /*$data_list = self::where('id', 'in', $id)->cache(true, 3600)->select();
            $paths = [];
            foreach ($data_list as $key => $value) {
                $paths[$key] = $value['url'];
            }*/
            $paths = [];
            foreach ($id as $item) {
                $paths[] = is_numeric($item) ? self::getFilePath($item) : $item;
            }
            return $paths;
        } else if(!empty($id)) {
            if(is_numeric($id)) {
                $data = self::where('id', $id)->cache(true, 86400 * 30)->find();
                if ($data) {
                    return $data['thumb'];
                } else {
                    return false;
                }
            }else {
                $data = self::where('url', $id)->cache(true, 86400 * 30)->find();
                if ($data) {
                    return $data['thumb'];
                }else {
                    return $id;
                }
            }
        }else {
            return $id;
        }
    }

    /**
     * 根据ID获取文件路径
     * @param int|array $id
     * @return Attachment[]|Attachment|false
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public static function getById($id = '')
    {
        if (is_array($id)) {
            /*$data_list = self::where('id', 'in', $id)->cache(true, 3600)->select();
            $paths = [];
            foreach ($data_list as $key => $value) {
                $paths[$key] = $value['url'];
            }*/
            $paths = [];
            foreach ($id as $item) {
                $paths[] = is_numeric($item) ? self::getFilePath($item) : $item;
            }
            return $paths;
        } else if(!empty($id)) {
            return self::where('id', $id)->cache(true, 86400 * 30)->find();
        }else {
            return false;
        }
    }
}