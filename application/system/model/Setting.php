<?php
/**
 * Create By Evan.
 * Date: 2019/7/26
 * Time: 23:57
 */

namespace app\system\model;


use app\common\model\BaseModel;
use think\facade\Cache;

/**
 * 系统配置
 * Class Setting
 * @package app\system\model
 *
 * @property int id
 * @property string key
 * @property string value
 * @property string module
 * @property int create_time
 * @property int update_time
 */
class Setting extends BaseModel {

    protected $name = 'system_setting';

    protected static function init()
    {
        self::afterWrite(function($setting){
            // 配置更新后清空当前模块配置缓存
            Cache::rm('system:setting:' . $setting->module);
        });
    }


}