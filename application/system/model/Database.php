<?php


namespace app\system\model;


use app\common\exception\CommonResponseException;
use app\common\model\BaseModel;
use app\common\ResCode;
use think\Db;
use think\db\Query;

class Database extends BaseModel
{
    protected $name = 'system_database';

    /**
     * @var array 数据库查询对象池, 数据库连接使用单例实现
     */
    static $querys = [];

    /**
     * 获取表格类型列表
     * @return array
     */
    public static function getTableTypes()
    {
        return [
            'text' => 'text',
            'status' => 'status',
            'yesno' => 'yesno',
            'datetime' => 'datetime',
            'image' => 'image',
            'images' => 'images',
            'link' => 'link',
            'text.edit' => 'text.edit',
            'select' => 'select',
        ];
    }

    /**
     * 获取表单类型列表
     * @return array
     */
    public static function getFormTypes()
    {
        return [
            'text' => '文本框',
            'textarea' => '多行文本框',
            'number' => '数字输入框',
            'password' => '密码框',
            'radio' => '单选框',
            'status' => '状态选择器',
            'checkbox' => '复选框',
            'select' => '下拉框',
            'cascader' => '级联选择器',
            'switch' => '开关',
            'date' => '日期选择器',
            'datetime' => '日期时间选择器',
            'time' => '时间选择器',
            'daterange' => '日期范围选择器',
            'datetimerange' => '日期时间范围选择器',
            'timerange' => '时间范围选择器',
            'icon' => '图标选择器',
            'color' => '颜色选择器',
            'image' => '单图上传',
            'images' => '多图上传',
            'file' => '单文件上传',
            'files' => '多文件上传',
            'editor' => '多文件上传',
            'static' => '静态文本',
        ];
    }

    /**
     * 获取数据库的表列表
     * @param $database
     * @return array
     * @throws
     */
    public static function getTables($database)
    {
        $query = self::getDatabaseQuery($database);
        $result = $query->query("SELECT TABLE_NAME, ENGINE, TABLE_ROWS, DATA_LENGTH, INDEX_LENGTH, TABLE_COLLATION, TABLE_COMMENT FROM information_schema.TABLES WHERE table_schema='super-php-v3'");
//        $result = $query->query('show tables');

        $list = [];
        foreach ($result as $item) {
            $list[] = [
                'name' => $item['TABLE_NAME'],
                'engine' => $item['ENGINE'],
                'table_rows' => $item['TABLE_ROWS'],
                'data_length' => $item['DATA_LENGTH'],
                'index_length' => $item['INDEX_LENGTH'],
                'table_collation' => $item['TABLE_COLLATION'],
                'table_comment' => $item['TABLE_COMMENT'],
            ];
        }
        return $list;
    }

    /**
     * @param $database
     * @param $table
     * @return Query
     */
    public static function getTableQuery($database, $table)
    {
        return self::getDatabaseQuery($database)->table($table);
    }

    /**
     * @param $database
     * @return Query
     * @throws
     */
    public static function getDatabaseQuery($database)
    {
        if(!isset(self::$querys[$database])) {
            $config = self::getDatabaseConfig($database);
            self::$querys[$database] = Db::connect($config);
        }
        return self::$querys[$database];
    }

    /**
     * 根据数据库名获取数据库连接配置
     * @param $database
     * @return array
     * @throws
     */
    public static function getDatabaseConfig($database)
    {
        $config = config('database.');
        if($database != 'default') {
            $config = Database::where('name', $database)->find();
            if(empty($config)) {
                throw new CommonResponseException(ResCode::data_not_exist, '数据库不存在');
            }
            $config = $config->toArray();
        }
        return $config;
    }
}