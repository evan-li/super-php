<?php

namespace app\system\model;

use app\common\model\BaseModel;

/**
 * OSS配置白哦
 * @property string driver
 * @property string access_key
 * @property string secret_key
 * @property string bucket
 * @property string prefix
 * @property string endpoint
 * @property string domain
 * @property string region
 * @property string schema
 * @property int status
 * @property string remark
 */
class OssConfig extends BaseModel
{
    protected $table = 'system_oss_config';

    const status_1 = 1;
    const status_2 = 2;

    public static $drivers = [
        'local' => '本地存储',
        'qiniu' => '七牛云',
        'aliyun' => '阿里云',
        'tencent' => '腾讯云',
        'aws' => 'AWS S3',
    ];

}