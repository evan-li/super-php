

-- 导出  表 system_database 结构
CREATE TABLE IF NOT EXISTS `__PREFIX__system_database`(
	`id` int(11) NOT NULL  auto_increment ,
	`name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' COMMENT '连接名' ,
	`remark` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' COMMENT '备注' ,
	`type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT 'mysql' ,
	`hostname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' ,
	`database` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' ,
	`username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' ,
	`password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' ,
	`hostport` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' ,
	`create_time` int(11) NOT NULL  DEFAULT 0 ,
	`update_time` int(11) NOT NULL  DEFAULT 0 ,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_unicode_ci' COMMENT='数据库管理';


-- 导出  表 system_icon 结构
CREATE TABLE IF NOT EXISTS `__PREFIX__system_icon`(
	`id` int(11) NOT NULL  auto_increment ,
	`name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' COMMENT '图标库名称' ,
	`type` tinyint(4) NOT NULL  DEFAULT 1 COMMENT '图标库类型 1-阿里iconfont图标库' ,
	`font_family` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' COMMENT 'font-family, 为兼容elementUI对图标的处理,font-family需要以el-icon-开头' ,
	`css_url` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' COMMENT 'css地址,用于前端显示图标' ,
	`icons` text COLLATE utf8mb4_unicode_ci NOT NULL  ,
	`status` tinyint(4) NOT NULL  DEFAULT 1 COMMENT '状态' ,
	`create_time` int(11) NOT NULL  DEFAULT 0 ,
	`update_time` int(11) NOT NULL  DEFAULT 0 ,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_unicode_ci' COMMENT='图标扩展库';


-- 导出  表 system_setting 结构
CREATE TABLE IF NOT EXISTS `__PREFIX__system_setting`(
	`id` int(10) unsigned NOT NULL  auto_increment ,
	`module` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' COMMENT '模块' ,
	`key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL  COMMENT '配置key' ,
	`value` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' COMMENT '配置值' ,
	`desc` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' COMMENT '配置项说明' ,
	`create_time` int(11) NOT NULL  DEFAULT 0 COMMENT '创建时间' ,
	`update_time` int(11) NOT NULL  DEFAULT 0 COMMENT '更新时间' ,
	PRIMARY KEY (`id`) ,
	KEY `key`(`key`) ,
	KEY `module`(`module`)
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_unicode_ci' COMMENT='淘宝客配置';


-- 导出  表 system_table_field_type 结构
CREATE TABLE IF NOT EXISTS `__PREFIX__system_table_field_type`(
	`id` int(11) NOT NULL  auto_increment ,
	`database` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' ,
	`table` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' ,
	`field` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' ,
	`type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' ,
	`table_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' ,
	`form_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  DEFAULT '' ,
	`create_time` int(11) NOT NULL  DEFAULT 0 ,
	`update_time` int(11) NOT NULL  DEFAULT 0 ,
	PRIMARY KEY (`id`) ,
	KEY `database`(`database`) ,
	KEY `table`(`table`) ,
	KEY `field`(`field`)
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_unicode_ci' COMMENT='表字段类型映射';

-- 导出  表 system_attachment 结构
CREATE TABLE IF NOT EXISTS `__PREFIX__system_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文件名',
  `module` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '模块名，由哪个模块上传的',
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '缩略图路径',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文件链接',
  `mime` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文件mime类型',
  `ext` char(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文件类型',
  `size` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `md5` char(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'sha1 散列值',
  `driver` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'local' COMMENT '上传驱动',
  `download` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '下载次数',
  `sort` int(11) NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  `width` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '图片宽度',
  `height` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '图片高度',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='附件表';

-- 导出  表 system_auth_group 结构
CREATE TABLE IF NOT EXISTS `__PREFIX__system_auth_group` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '分组名',
    `rule_ids` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '权限ID列表，逗号分隔',
    `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0-禁用 1-启用',
    `create_time` int(11) NOT NULL DEFAULT '0',
    `update_time` int(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='后台分组表';

-- 导出  表 system_auth_rule 结构
CREATE TABLE IF NOT EXISTS `__PREFIX__system_auth_rule` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父权限ID',
    `module` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '模块名' COLLATE 'utf8mb4_unicode_ci',
    `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '权限名',
    `url` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '节点链接',
    `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单图标',
    `is_menu` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否是菜单',
    `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
    `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态: 1-有效  2-无效',
    `is_dev` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '是否开发者菜单',
    `create_time` int(11) DEFAULT NULL,
    `update_time` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `is_menu` (`is_menu`),
    KEY `status` (`status`),
    KEY `pid` (`pid`),
    KEY `sort` (`sort`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='权限节点表';

INSERT INTO `__PREFIX__system_auth_rule` (`id`, `pid`, `module`, `name`, `url`, `icon`, `is_menu`, `sort`, `status`, `is_dev`, `create_time`, `update_time`) VALUES
    (1, 0, 'system', '系统管理', 'system', 'el-icon-Setting', 1, 0, 1, 0, 1682671804, 1682672036),
    (2, 1, 'system', '菜单规则', '/system/auth_rule/index', 'el-icon-Grid', 1, 0, 1, 0, 1682671876, 1682672343),
    (3, 2, 'system', '数据列表', '/system/auth_rule/getlist', '', 0, 0, 1, 0, 1682672343, 1682672343),
    (4, 2, 'system', '新增', '/system/auth_rule/add', '', 0, 0, 1, 0, 1682672343, 1682672343),
    (5, 2, 'system', '编辑', '/system/auth_rule/edit', '', 0, 0, 1, 0, 1682672343, 1682672343),
    (6, 2, 'system', '删除', '/system/auth_rule/delete', '', 0, 0, 1, 0, 1682672343, 1682672343),
    (7, 2, 'system', '启用', '/system/auth_rule/enable', '', 0, 0, 1, 0, 1682672343, 1682672343),
    (8, 2, 'system', '禁用', '/system/auth_rule/disable', '', 0, 0, 1, 0, 1682672343, 1682672343),
    (9, 2, 'system', '快速编辑', '/system/auth_rule/quickedit', '', 0, 0, 1, 0, 1682672343, 1682672343),
    (10, 2, 'system', '批量操作', '/system/auth_rule/batch', '', 0, 0, 1, 0, 1682672343, 1682672343),
    (11, 1, 'system', '角色管理', '/system/auth_group/index', 'fa fa-group', 1, 0, 1, 0, 1682672395, 1682672395),
    (12, 11, 'system', '数据列表', '/system/auth_group/getlist', '', 0, 0, 1, 0, 1682672395, 1682672395),
    (13, 11, 'system', '新增', '/system/auth_group/add', '', 0, 0, 1, 0, 1682672395, 1682672395),
    (14, 11, 'system', '编辑', '/system/auth_group/edit', '', 0, 0, 1, 0, 1682672395, 1682672395),
    (15, 11, 'system', '删除', '/system/auth_group/delete', '', 0, 0, 1, 0, 1682672395, 1682672395),
    (16, 11, 'system', '启用', '/system/auth_group/enable', '', 0, 0, 1, 0, 1682672395, 1682672395),
    (17, 11, 'system', '禁用', '/system/auth_group/disable', '', 0, 0, 1, 0, 1682672395, 1682672395),
    (18, 11, 'system', '快速编辑', '/system/auth_group/quickedit', '', 0, 0, 1, 0, 1682672395, 1682672395),
    (19, 11, 'system', '批量操作', '/system/auth_group/batch', '', 0, 0, 1, 0, 1682672395, 1682672395),
    (20, 1, 'system', '用户管理', '/system/user/index', 'fa fa-user', 1, 0, 1, 0, 1682672423, 1682672423),
    (21, 20, 'system', '数据列表', '/system/user/getlist', '', 0, 0, 1, 0, 1682672423, 1682672423),
    (22, 20, 'system', '新增', '/system/user/add', '', 0, 0, 1, 0, 1682672424, 1682672424),
    (23, 20, 'system', '编辑', '/system/user/edit', '', 0, 0, 1, 0, 1682672424, 1682672424),
    (24, 20, 'system', '删除', '/system/user/delete', '', 0, 0, 1, 0, 1682672424, 1682672424),
    (25, 20, 'system', '启用', '/system/user/enable', '', 0, 0, 1, 0, 1682672424, 1682672424),
    (26, 20, 'system', '禁用', '/system/user/disable', '', 0, 0, 1, 0, 1682672424, 1682672424),
    (27, 20, 'system', '快速编辑', '/system/user/quickedit', '', 0, 0, 1, 0, 1682672424, 1682672424),
    (28, 20, 'system', '批量操作', '/system/user/batch', '', 0, 0, 1, 0, 1682672424, 1682672424),
    (29, 1, 'system', '附件管理', '/system/attachment/index', 'el-icon-Files', 1, 0, 1, 0, 1682672601, 1682672737),
    (30, 29, 'system', '数据列表', '/system/attachment/getlist', '', 0, 0, 1, 0, 1682672601, 1682672601),
    (31, 29, 'system', '新增', '/system/attachment/add', '', 0, 0, 1, 0, 1682672601, 1682672601),
    (32, 29, 'system', '编辑', '/system/attachment/edit', '', 0, 0, 1, 0, 1682672601, 1682672601),
    (33, 29, 'system', '删除', '/system/attachment/delete', '', 0, 0, 1, 0, 1682672601, 1682672601),
    (34, 29, 'system', '启用', '/system/attachment/enable', '', 0, 0, 1, 0, 1682672601, 1682672601),
    (35, 29, 'system', '禁用', '/system/attachment/disable', '', 0, 0, 1, 0, 1682672601, 1682672601),
    (36, 29, 'system', '快速编辑', '/system/attachment/quickedit', '', 0, 0, 1, 0, 1682672601, 1682672601),
    (37, 29, 'system', '批量操作', '/system/attachment/batch', '', 0, 0, 1, 0, 1682672601, 1682672601),
    (38, 1, 'system', '模块扩展', '/system/module/index', 'fa fa-plug', 1, 0, 1, 0, 1682672714, 1683355412),
    (39, 38, 'system', '数据列表', '/system/module/getlist', '', 0, 0, 1, 0, 1682672714, 1682672714),
    (40, 38, 'system', '新增', '/system/module/add', '', 0, 0, 1, 0, 1682672714, 1682672714),
    (41, 38, 'system', '编辑', '/system/module/edit', '', 0, 0, 1, 0, 1682672714, 1682672714),
    (42, 38, 'system', '删除', '/system/module/delete', '', 0, 0, 1, 0, 1682672714, 1682672714),
    (43, 38, 'system', '启用', '/system/module/enable', '', 0, 0, 1, 0, 1682672714, 1682672714),
    (44, 38, 'system', '禁用', '/system/module/disable', '', 0, 0, 1, 0, 1682672714, 1682672714),
    (45, 38, 'system', '快速编辑', '/system/module/quickedit', '', 0, 0, 1, 0, 1682672714, 1682672714),
    (46, 38, 'system', '批量操作', '/system/module/batch', '', 0, 0, 1, 0, 1682672714, 1682672714),
    (47, 1, 'system', '数据库管理', '/system/database/index', 'fa fa-database', 1, 0, 1, 0, 1596785135, 1682406038),
    (48, 47, 'system', '数据列表', '/system/database/getlist', '', 0, 0, 1, 0, 1596785457, 1596785457),
    (49, 47, 'system', '新增', '/system/database/add', '', 0, 0, 1, 0, 1596785457, 1596785457),
    (50, 47, 'system', '编辑', '/system/database/edit', '', 0, 0, 1, 0, 1596785457, 1596785457),
    (51, 47, 'system', '删除', '/system/database/delete', '', 0, 0, 1, 0, 1596785457, 1596785457),
    (52, 47, 'system', '启用', '/system/database/enable', '', 0, 0, 1, 0, 1596785457, 1596785457),
    (53, 47, 'system', '禁用', '/system/database/disable', '', 0, 0, 1, 0, 1596785457, 1596785457),
    (54, 47, 'system', '批量操作', '/system/database/batch', '', 0, 0, 1, 0, 1596785457, 1596785457),
    (55, 47, 'system', '快速编辑', '/system/database/quickedit', '', 0, 0, 1, 0, 1596785457, 1596785457),
    (56, 47, 'system', '数据源管理', '/system/database/connections', 'el-icon-Connection', 1, 0, 1, 0, 1597841298, 1682405995);


-- 导出  表 system_user 结构
CREATE TABLE IF NOT EXISTS `__PREFIX__system_user` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
    `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
    `nickname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '昵称',
    `password` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '密码',
    `salt` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '盐值',
    `avatar` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '头像',
    `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '邮箱',
    `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态 0-禁用 1-启用',
    `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '角色ID',
    `is_super` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '超级管理员, 跳过权限验证',
    `is_dev` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '开发人员',
    `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
    `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
    PRIMARY KEY (`id`),
    KEY `username` (`username`),
    KEY `group_id` (`group_id`),
    KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='后台用户表';

INSERT INTO `__PREFIX__system_user` (`id`, `username`, `nickname`, `password`, `salt`, `avatar`, `email`, `status`, `group_id`, `is_super`, `create_time`, `update_time`) VALUES
	(1, 'admin', 'admin', '46f65f037b2762b7d60d68a34d653848', 'y5fc8TkBv5IGYJPn', '', '', 1, 0, 1, 1558508873, 1558508873);

CREATE TABLE IF NOT EXISTS `__PREFIX__failed_job` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `queue` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '队列名',
  `payload` LONGTEXT NULL DEFAULT NULL COMMENT '任务数据, 包含job,id,data,attempts等信息',
  `exception` LONGTEXT NULL DEFAULT NULL COMMENT '错误信息',
  `create_time` INT(11) NOT NULL DEFAULT '0',
  `update_time` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) COMMENT='失败任务表' COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;

CREATE TABLE `__PREFIX__system_oss_config` (
 `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '配置ID',
 `driver` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '文件存储驱动, 支持 local, qiniu, aliyun, aws, tencent',
 `access_key` VARCHAR(100) NOT NULL DEFAULT '' COMMENT 'accessKey',
 `secret_key` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '秘钥',
 `bucket` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '桶名称',
 `prefix` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '前缀, local时表示相对于public目录的存储路径',
 `endpoint` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '访问站点 (aliyun)',
 `domain` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '自定义域名',
 `region` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '地域 (aws/tencent)',
 `scheme` VARCHAR(10) NOT NULL DEFAULT 'https' COMMENT '协议 (tencent) https或http',
 `status` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '装态 1-启用 2-禁用',
 `remark` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '备注',
 `create_time` INT(11) NOT NULL DEFAULT '0',
 `update_time` INT(11) NOT NULL DEFAULT '0',
 PRIMARY KEY (`id`) USING BTREE
) COMMENT='oss配置表' COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;

CREATE TABLE `__PREFIX__system_module` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '扩展名' COLLATE 'utf8mb4_unicode_ci',
 `title` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '模块中文名' COLLATE 'utf8mb4_unicode_ci',
 `author` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '作者' COLLATE 'utf8mb4_unicode_ci',
 `desc` TEXT NULL DEFAULT NULL COMMENT '模块描述' COLLATE 'utf8mb4_unicode_ci',
 `cover` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '封面图' COLLATE 'utf8mb4_unicode_ci',
 `pics` VARCHAR(2000) NOT NULL DEFAULT '' COMMENT '图片列表' COLLATE 'utf8mb4_unicode_ci',
 `type` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '类型 1-公开模块 2-内部模块',
 `version` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '版本号' COLLATE 'utf8mb4_unicode_ci',
 `status` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '状态 1-启用 2-禁用',
 `create_time` INT(11) NOT NULL DEFAULT '0',
 `update_time` INT(11) NOT NULL DEFAULT '0',
 PRIMARY KEY (`id`) USING BTREE
) COMMENT='已安装模块表' COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;


