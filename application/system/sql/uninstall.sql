
-- 删除  表 system_database
DROP TABLE IF EXISTS `__PREFIX__system_database`;
-- 删除  表 system_icon
DROP TABLE IF EXISTS `__PREFIX__system_icon`;
-- 删除  表 system_setting
DROP TABLE IF EXISTS `__PREFIX__system_setting`;
-- 删除  表 system_table_field_type
DROP TABLE IF EXISTS `__PREFIX__system_table_field_type`;

-- 删除  表 system_attachment
DROP TABLE IF EXISTS `__PREFIX__system_attachment`;

-- 删除  表 system_auth_group
DROP TABLE IF EXISTS `__PREFIX__system_auth_group`;

-- 删除  表 system_auth_rule
DROP TABLE IF EXISTS `__PREFIX__system_auth_rule`;

-- 删除  表 system_user
DROP TABLE IF EXISTS `__PREFIX__system_user`;
-- 删除  表 failed_job
DROP TABLE IF EXISTS `__PREFIX__failed_job`;
-- 删除 表 system_module
DROP TABLE IF EXISTS `__PREFIX__system_module`;