<?php


namespace app\system\admin;


use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;
use evan\sbuilder\module\Manager;
use evan\sbuilder\module\ModuleException;
use evan\sbuilder\module\Store;
use think\facade\App;

class Module extends Admin
{

    /**
     * 模块市场
     */
    public function index()
    {
        if (is_page()) {
            return SBuilder::makeCardList()
                ->setPageTitle('模块管理')
                ->setNavTab([
                    ['market', '模块市场', url('index', '', false)],
                    ['local', '本地模块', url('local', '', false)],
                ])
                ->setNavTabCurrent('market')
                ->setAjaxDataUrl(url(''))
                ->setDetailLink('pop:' . url('detail'))
                ->setProps(['name' => 'title'])
                ->fetch();
        }else {
            $data = Store::getList(input('page'), input('page_size'));

            $names = [];
            foreach ($data['list'] as $item) {
                $names[] = $item['name'];
            }
            // 是否已安装
            $installedList = Store::getInstalledListByNames($names);
            foreach ($installedList as $installed) {
                foreach ($data['list'] as &$item) {
                    if ($installed['name'] == $item['name']) {
                        $item['installed'] = true;
                        $item['show_btn'] = true;
                        $item['btn'] = '已安装';
                        $item['show_price'] = false;
                        $item['status'] = $installed->status;
                    }
                }
            }
            return admin_data($data);
        }
    }

    /**
     * 本地已安装模块
     */
    public function local()
    {
        $localModules = Store::getLocalModules();
        $names = [];
        foreach ($localModules as $localModule) {
            $names[] = $localModule['name'];
        }
        /** @var \app\system\model\Module[] $installedList */
        $installedList = Store::getInstalledList();
        $storeModules = Store::getListByNames($names);
        foreach ($localModules as &$localModule) {
            $localModule['installed'] = false;
            $localModule['is_local'] = true;
            foreach ($installedList as $installed) {
                if ($installed->name == $localModule['name']) {
                    $localModule['installed'] = true;
                    $localModule['status'] = $installed->status;
                }
            }
            foreach ($storeModules as $storeModule) {
                // 补充本地模块的属性
                if($localModule['name'] == $storeModule['name']) {
                    if(!$localModule['installed']) {
                        // 同步本地模块到已安装模块中
                        \app\system\model\Module::create($localModule);
                        $localModule['installed'] = true;
                    }
                    $localModule['title'] = $storeModule['title'];
                    $localModule['author'] = $storeModule['author'];
                    $localModule['desc'] = $storeModule['desc'];
                    $localModule['cover'] = $storeModule['cover'];
                    $localModule['pics'] = $storeModule['pics'];
                    $localModule['type'] = $storeModule['type'];
                    $localModule['tags'] = $storeModule['tags'];
                    $localModule['price'] = $storeModule['price'];
                    $localModule['crossed_price'] = $storeModule['crossed_price'];
                    $localModule['download_count'] = $storeModule['download_count'];
                    $localModule['last_version'] = $storeModule['last_version'];
                    $localModule['last_version_time'] = $storeModule['last_version_time'];
                    $localModule['create_time'] = $storeModule['create_time'];
                    $localModule['homepage'] = $storeModule['homepage'];
                }
            }
        }

        if (is_page()) {
            return SBuilder::makeCardList()
                ->setPageTitle('模块管理')
                ->setNavTab([
                    ['market', '模块市场', url('index', '', false)],
                    ['local', '本地模块', url('local', '', false)],
                ])
                ->setNavTabCurrent('local')
                ->setData($localModules)
                ->setDetailLink('pop:' . url('detail'))
                ->setProps(['name' => 'title'])
                ->fetch();
        }else {
            return admin_data(['list' => $localModules]);
        }
    }

    /**
     * 打包发布本地模块
     * @throws ModuleException
     * @throws \Exception
     */
    public function publish()
    {
        $module = input('module');
        $version = input('version');
        $title = input('title');
        $desc = input('desc');

        Manager::publish($module, $version, $title, $desc);

        return admin_success('操作成功', '__relist__');
    }

    /**
     * 启用模块
     */
    public function enable()
    {
        $module = input('module');
        Manager::enable($module);
        return admin_success('操作成功', '__relist__');
    }

    /**
     * 禁用模块
     */
    public function disable()
    {
        $module = input('module');
        Manager::disable($module);
        return admin_success('操作成功', '__relist__');
    }

    /**
     * 安装模块
     */
    public function install()
    {
        $module = input('module');
        $version = input('version');
        Manager::install($module, $version);
        return admin_success('操作成功', '__relist__');
    }

    /**
     * 卸载模块
     */
    public function uninstall()
    {
        $module = input('module');
        Manager::uninstall($module);
        return admin_success('操作成功', '__relist__');
    }

    public function reconfig() {
        $name = $this->request->param('name');
        if(empty($name)){
            return admin_error('模块名不能为空');
        }
        $path = App::getAppPath() . $name;
        if(!is_dir($path)){
            return admin_error('模块不存在');
        }
        $config = require $path . '/config.php';

        foreach ($config as $key => $value) {
            if(is_array($value)){
                $value = json_encode($value, JSON_UNESCAPED_UNICODE);
            }
            module_config($name, $key, $value);
        }
        return admin_success('初始化模块配置成功', '');
    }
}