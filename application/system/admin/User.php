<?php


namespace app\system\admin;


use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;
use app\system\model\User as UserModel;
use think\Exception;
use think\helper\Str;

class User extends Admin
{

    /**
     * @return mixed
     * @throws Exception
     */
    public function index()
    {
        $groups = \app\system\model\AuthGroup::where('status', 1)->column('name', 'id');
        return SBuilder::make('table')
            ->setPageTitle('用户管理')
            ->addColumns([
                ['id', 'ID'],
                ['nickname', '昵称'],
                ['username', '用户名', 'link', url('detail', ['id' => '__id__'])],
                ['group_name', '角色', ''],
                ['avatar', '头像', 'image'],
                ['status', '状态', 'status'],
                ['email', '邮箱', '', '未填值'],
                ['create_time', '创建时间', 'datetime'],
                ['__btn__'],
            ])
            ->addTopButton('add', '', '', 'pop')
            ->addTopButton('enable')
            ->addTopButton('disable')
            ->addTopButton('delete')
            ->autoEdit([
                ['username', '用户名', 'static'],
                ['group_id', '角色', 'select', $groups],
                ['nickname', '昵称', ],
                ['email', '邮箱', ],
                ['avatar', '头像', 'image'],
                ['status', '状态', 'status'],
            ], '', true)
            ->addRightButton('enable')
            ->addRightButton('disable')
            ->addRightButton('delete')
            ->addRightButton('detail', '查看详情', url('detail'), 'link', ['icon' => 'el-icon-Grid'])
            ->addRightButton('reset', '重置密码', url('resetPassword'), 'pop', ['icon' => 'el-icon-Unlock'])
            ->addRightButton('setSuper', '设为超管', url('setSuper', ['is_super' => 1]), 'ajax', ['text' => true, 'show' => ['is_super' => 0]])
            ->addRightButton('setSuper', '取消超管', url('setSuper', ['is_super' => 0]), 'ajax', ['text' => true, 'hide' => ['is_super' => 0, 'id' => 1]])
            ->addTimeFilter([
                ['create_time', '', 'datetime'],
            ])
            ->setSearch(['username|用户名', 'nickname|昵称', 'id|ID|='])
            ->showCheckbox()
            ->setColumnWidth('__btn__', 260)
            ->fetch();
    }

    public function getList()
    {
        $query = UserModel::where($this->getWhere())
            ->order($this->getOrder())
            ->with(['authGroup' => function($query) {
                $query->field('id, name');
            }]);
        if (!$this->currentUser->is_dev) {
            $query->where('is_dev', 0);
        }
        $data = $query->paginate();
        foreach ($data as $item) {
            if($item->is_super) {
                $item->group_name = '超级管理员';
            }else if($item->authGroup) {
                $item->group_name = $item->authGroup->name;
            }
        }
        return admin_data($data);
    }

    /**
     * @return mixed
     */
    public function detail()
    {
        $this->validate($this->request->param(), [
            'id' => 'require|integer',
        ]);
        $id = $this->request->param('id');
        $user = UserModel::with('authGroup')->getOrFail($id);
        $user->group_name = $user->is_super ? '超级管理员' : ($user->auth_group ? $user->auth_group->name : '');
        return SBuilder::makeForm()
            ->setPageTitle('用户详情')
            ->addFormItems([
                ['id', 'ID', 'static'],
                ['username', '用户名', 'static'],
                ['group_name', '角色', 'static'],
                ['nickname', '昵称', 'static'],
                ['email', '邮箱', 'static'],
                ['avatar', '头像', 'images', '', 'https://jmhp520.com.static.niucha.ren/goods_share_picture/60087d6c7a68bc5ec8822e2ca3593cee.png', ['disabled' => true]],
                ['status', '状态', 'status'],
            ])
            ->addImage()
            ->setFormData($user)
            ->fetch();
    }

    /**
     * 添加用户信息
     * @return mixed
     * @throws Exception
     */
    public function add()
    {
        if(is_page()){
            $groups = \app\system\model\AuthGroup::where('status', 1)->column('name', 'id');
            return SBuilder::make('form')
                ->setPageTitle('新增用户')
                ->addFormItems([
                    ['username', '用户名', '', '', '', ['require' => true]],
                    ['group_id', '角色', 'select', $groups],
                    ['nickname', '昵称', ],
                    ['email', '邮箱', ],
                    ['avatar', '头像', 'image'],
                    ['password', '密码', 'password'],
                    ['password_confirm', '确认密码|请重新输入密码', 'password'],
                    ['status', '状态', 'status'],
                ])
                ->fetch();
        }
        $result = $this->validate($this->request->param(), [
            'username|用户名' => 'require',
            'password|密码' => 'require|length:6,32|confirm',
            'password_confirm|确认密码' => 'require',
        ]);
        if($result !== true){
            return admin_error($result);
        }
        $username = $this->request->param('username');
        $user = UserModel::where('username', $username)->find();
        if(!empty($user)){
            return admin_error('用户名已存在');
        }
        $salt = Str::random();
        $password = $this->request->param('password');
        $data = [
            'username' => $username,
            'nickname' => $this->request->param('nickname'),
            'email' => $this->request->param('email', ''),
            'avatar' => $this->request->param('avatar', ''),
            'group_id' => $this->request->param('group_id', 0),
        ];
        $data['salt'] = $salt;
        $data['password'] = UserModel::hashPassword($password, $salt);
        UserModel::create($data);
        return admin_success('添加用户成功', '__relist__');
    }

    /**
     * 重置密码
     */
    public function resetPassword()
    {
        $this->checkParam([
            'id' => 'require|integer',
        ]);
        $id = $this->request->param('id');
        $user = UserModel::field('id, username')->find($id);
        if(empty($user)) {
            return admin_error('用户不存在');
        }
        if(is_page()) {
            return SBuilder::makeForm()
                ->setPageTitle('重置密码')
                ->addFormItems([
                    ['id', 'id', 'hidden'],
                    ['username', '用户名', 'static'],
                    ['password', '密码', 'password'],
                    ['rePassword', '确认密码', 'password'],
                ])
                ->setFormData($user)
                ->fetch();
        }else {
            $this->checkParam([
                'password' => 'require|min:6',
                'rePassword' => 'require|confirm:password'
            ]);
            $salt = Str::random();
            $user->password = UserModel::hashPassword(input('password'), $salt);
            $user->salt = $salt;
            $user->save();
            return admin_success('密码修改成功');
        }
    }

    public function setSuper()
    {
        $this->checkParam([
            'id' => 'require|integer',
            'is_super' => 'require|integer|in:0,1'
        ]);
        $id = $this->request->param('id');
        $user = UserModel::field('id, username')->find($id);
        if(empty($user)) {
            return admin_error('用户不存在');
        }
        $user->is_super = input('is_super');
        $user->save();
        return admin_success('设置成功', '__relist__');
    }

}