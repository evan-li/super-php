<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 * Date: 2019/5/2
 * Time: 15:25
 */

namespace app\system\admin;


use app\system\model\AuthRule as AuthRuleModel;
use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;
use think\Exception;
use think\helper\Str;

class AuthRule extends Admin
{

    /**
     * 节点列表
     * @throws Exception
     */
    public function index()
    {
        return SBuilder::make('table')
            ->setPageTitle('节点管理')
            ->addTopButton('add', '添加权限', '', 'pop')
            ->addTopButton('enable')
            ->addTopButton('disable')
            ->addTopButton('delete')
            ->addTopButton('clearCache', '清除菜单缓存', url('index/session', ['opt' => 'rm', 'key' => 'user_menus']), 'ajax', ['type' => 'warning', 'batch' => false])
            ->addColumn()
            ->addColumns([
//                ['__index__'],
                ['name', '名称'],
                ['icon', '图标', 'icon'],
                ['id', 'ID'],
                ['sort', '排序', ],
                ['url', '权限地址'],
                ['is_menu', '是否菜单', 'yesno'],
                ['status', '状态', 'status'],
                $this->currentUser->is_dev ? ['is_dev', '开发者权限', 'yesno'] : [],
                ['create_time', '创建时间', 'datetime'],
                ['__btn__']
            ])
            ->addRightButton('custom', '添加子节点', url('add'), 'pop', ['icon' => 'el-icon-Plus', 'extra_data' => ['pid' => '__id__']])
            ->addRightButton('edit', '', '', 'pop')
            ->addRightButton('enable')
            ->addRightButton('disable')
            ->addRightButton('setIsDev', '开发者', url('setIsDev'), 'ajax', ['hide' => !$this->currentUser->is_dev])
            ->addRightButton('delete', '', '', '', [
                'confirm' => [
                    'title' => '确定删除此权限及下属所有节点么? ',
                    'tips' => '删除后不可恢复!',
                    'type' => 'error'
                ]
            ])
            ->setColumnWidth('icon,id', 60)
            ->setColumnWidth('__btn__', 300)
            ->showCheckbox()
            ->fetch();
    }

    public function getList()
    {
        $list = AuthRuleModel::getAllTree($this->currentUser->is_dev);
        return admin_data($list);
    }


    /**
     * @return mixed
     * @throws Exception
     */
    public function add()
    {
        if(is_page()){
            $pid = $this->request->param('pid', 0);

            $ruleTree = AuthRuleModel::getAllTree($this->currentUser->is_dev, 'id,pid,name');

            return SBuilder::make('form')
                ->setPageTitle('新增权限节点')
                ->addFormItems([
                    ['pid', '所属权限|顶级权限', 'cascader', $ruleTree, '', $pid, [
                        'select_all_levels' => true,
                        'props' => [
                            'value' => 'id',
                            'label' => 'name',
                            'children' => 'children',
                            'disabled' => 'disabled',
                        ]
                    ]],
                    ['name', '权限名'],
                    ['is_menu', '是否展示为菜单', 'switch', '', 1, ],
                    ['url', '菜单链接', '', 'url格式为: <span class="c-warning">/模块名/控制器名/操作名</span>, 如: <span class="c-warning">/system/auth_rule/index</span>'],
                    ['icon', '菜单图标', 'icon'],
                    ['gen_sub', '自动生成子节点', 'switch', '', '自动生成子节点时, url不能为空'],
                    ['subs', '子节点', 'checkbox', [
                        'getlist' => '数据列表',
                        'add' => '新增',
                        'edit' => '编辑',
                        'delete' => '删除',
                        'enable' => '启用',
                        'disable' => '禁用',
                        'quickedit' => '快速编辑',
                        'batch' => '批量操作',
                    ], '', ['getlist', 'add', 'edit', 'delete', 'enable', 'disable', 'quickedit', 'batch'], ],
                    ['sort', '排序', 'number', '排序提示',],
                    ['status', '状态', 'radio', ['禁用', '启用'], '', 1],
                ])
                ->setLabelWith(120)
                ->setTrigger('subs', 'gen_sub', '1')
                ->fetch();
        }else {
            $result = $this->validate($this->request->param(), [
                'name|名称' => 'require',
                'url|链接' => 'requireIf:gen_sub,1',
            ]);
            if($result !== true){
                return admin_error($result);
            }

            $params = $this->request->param();
            // 验证URL格式
            $module = '';
            $controller = '';
            if(!empty($params['url'])){
                if(mb_strrpos($params['url'], '/') !== false) {
                    $url = explode('/', trim($params['url'], '/'));
                    $module = Str::snake($url[0] ?? 'index');
                    $controller = Str::snake($url[1] ?? 'index');
                    $action = strtolower($url[2] ?? 'index');

                    $params['url'] = "/$module/$controller/$action";
                }
            }

            $genSub = $params['gen_sub'];
            $subs = $params['subs'];
            unset($params['gen_sub']);
            unset($params['subs']);
            $rule = AuthRuleModel::create($params);
            if($genSub){
                if(is_string($subs)) $subs = explode(',', $subs);
                $subNames = [ 'getlist' => '数据列表', 'add' => '新增', 'edit' => '编辑', 'delete' => '删除', 'enable' => '启用', 'disable' => '禁用', 'quickedit' => '快速编辑', 'batch' => '批量操作',];
                foreach ($subs as $sub) {
                    $subData = [
                        'pid' => $rule['id'],
                        'name' => $subNames[$sub],
                        'is_menu' => 0,
                        'url' => "/$module/$controller/$sub",
                    ];
                    AuthRuleModel::create($subData);
                }
            }
            $this->refreshSession();
            return admin_success('权限添加成功', '__relist__');
        }
    }

    public function edit()
    {
        if(is_page()){
            $id = $this->request->param('id');
            if (empty($id)) return admin_error('缺少主键');
            $rule = AuthRuleModel::get($id);
            // 过滤并回显已经自动生成过的子菜单
            $subs = AuthRuleModel::where('pid', $id)->select();
            $autoSubs = [];
            foreach ($subs as $item) {
                $key = substr($item->url, strrpos($item->url, '/') + 1);
                if(in_array($key, ['getlist', 'add', 'edit', 'delete', 'enable', 'disable', 'quickedit', 'batch'])){
                    $autoSubs[] = $key;
                }
            }
            $rule->subs = $autoSubs;
            $ruleTree = AuthRuleModel::getAllTree($this->currentUser->is_dev, 'id,pid,name');
            return SBuilder::make('form')
                ->setPageTitle('编辑权限节点')
                ->addFormItems([
                    ['pid', '所属权限|顶级权限', 'cascader', $ruleTree, '', '', [
                        'select_all_levels' => true,
                        'props' => [
                            'value' => 'id',
                            'label' => 'name',
                            'children' => 'children',
                            'disabled' => 'disabled',
                        ]
                    ]],
                    ['name', '权限名'],
                    ['is_menu', '是否展示为菜单', 'switch', '', 1, ],
                    ['url', '菜单链接', '', 'url格式为: <span class="c-warning">/模块名/控制器名/操作名</span>, 如: <span class="c-warning">/system/auth_rule/index</span>'],
                    ['icon', '菜单图标', 'icon'],
                    ['gen_sub', '自动生成子节点', 'switch', '', '自动生成子节点时, url不能为空'],
                    ['subs', '子节点', 'checkbox', [
                        'getlist' => '数据列表',
                        'add' => '新增',
                        'edit' => '编辑',
                        'delete' => '删除',
                        'enable' => '启用',
                        'disable' => '禁用',
                        'quickedit' => '快速编辑',
                        'batch' => '批量操作',
                    ]],
                    ['sort', '排序', 'number', '排序提示',],
                    ['status', '状态', 'radio', ['禁用', '启用'], '', 1],
                ])
                ->setLabelWith(120)
                ->setFormData($rule)
                ->setTrigger('subs', 'gen_sub', '1')
                ->fetch();
        }else {
            $result = $this->validate($this->request->param(), [
                'name|名称' => 'require',
                'url|链接' => 'requireIf:gen_sub,1',
            ]);
            if($result !== true){
                return admin_error($result);
            }

            $params = $this->request->param();
            // 验证URL格式
            $module = '';
            $controller = '';
            if(!empty($params['url'])){
                if(mb_strrpos($params['url'], '/') !== false) {
                    $url = explode('/', trim($params['url'], '/'));
                    $module = Str::snake($url[0] ?? 'index');
                    $controller = Str::snake($url[1] ?? 'index');
                    $action = strtolower($url[2] ?? 'index');

                    $params['url'] = "/$module/$controller/$action";
                }
            }

            $id = $this->request->param('id');
            if (empty($id)) return admin_error('缺少主键');

            $genSub = $params['gen_sub'];
            $subs = $params['subs'];
            unset($params['gen_sub']);
            unset($params['subs']);
            $rule = AuthRuleModel::get($id);
            $rule->save($params);


            if($genSub){
                if(is_string($subs)) $subs = explode(',', $subs);

                // 查询已经生成的子菜单, 并顾虑掉, 防止重复生成
                $subRules = AuthRuleModel::where('pid', $id)->select();
                $autoSubs = [];
                foreach ($subRules as $item) {
                    $key = substr($item->url, strrpos($item->url, '/') + 1);
                    if(in_array($key, ['getlist', 'add', 'edit', 'delete', 'enable', 'disable', 'quickedit', 'batch'])){
                        $autoSubs[] = $key;
                    }
                }
                $subs = array_diff($subs, $autoSubs);
                $subNames = [ 'getlist' => '数据列表', 'add' => '新增', 'edit' => '编辑', 'delete' => '删除', 'enable' => '启用', 'disable' => '禁用', 'quickedit' => '快速编辑', 'batch' => '批量操作',];
                foreach ($subs as $sub) {
                    // 验证子菜单是否已存在
                    $url = "/$module/$controller/$sub";
                    $subData = [
                        'pid' => $rule['id'],
                        'name' => $subNames[$sub],
                        'is_menu' => 0,
                        'url' => $url,
                    ];
                    AuthRuleModel::create($subData);
                }
            }
            $this->refreshSession();
            return admin_success('权限编辑成功', '__relist__');
        }
    }

    /**
     * 删除方法, 需要检测
     * @throws \Exception
     */
    public function delete()
    {
        $this->validate($this->request->param(), [
            'id' => 'require|integer|min:1',
        ]);
        $id = $this->request->param('id');
//        $obj = AuthRuleModel::get($id);
        $ids = $this->getAllSubRuleIds($id);
        array_push($ids, $id);
        AuthRuleModel::where('id', 'in', $ids)->delete();
        $this->refreshSession();
        return admin_success('删除成功', '__relist__');
    }

    protected function getAllSubRuleIds($id)
    {
        $subIds = AuthRuleModel::where('pid', $id)
            ->column('id');
        if(count($subIds) > 0){
            foreach ($subIds as $subId) {
                $subIds = array_merge($subIds, $this->getAllSubRuleIds($subId));
            }
        }
        return $subIds;
    }

    /**
     * 刷新用户权限相关session, 需要在权限更新及添加后调用
     */
    protected function refreshSession(){
        session('user_rules', null);
        session('user_menus', null);
    }

    public function setIsDev()
    {
        $this->validate($this->request->param(), [
            'id' => 'require|integer|min:1',
        ]);
        $id = $this->request->param('id');
        $rule = AuthRuleModel::get($id);
        $rule->is_dev = $rule->is_dev == 1 ? 0 : 1;
        $rule->save();
        return admin_success('', '__relist__');
    }
}