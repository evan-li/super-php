<?php

namespace app\system\admin;

use app\common\exception\CommonResponseException;
use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;

class OssConfig extends Admin
{
    public function index()
    {
        return SBuilder::makeTable()
            ->setPageTitle('文件存储配置管理')
            ->addColumns([
                ['id', 'ID'],
                ['driver', '存储驱动', '', \app\system\model\OssConfig::$drivers],
                ['name', '配置名'],
                ['remark', '备注'],
                ['domain', '自定义域名'],
                ['bucket', '桶名称'],
                ['endpoint', '访问站点'],
                ['prefix', '前缀'],
                ['domain', '地域'],
                ['status', '装态', 'status', ],
                ['__btn__']
            ])
            ->addTopButton('add')
            ->addRightButton('edit')
            ->addRightButton('delete')
            ->fetch();
    }

    public function getList()
    {
        $data = \app\system\model\OssConfig::where($this->getWhere())
            ->order($this->getOrder())
            ->paginate(input('page_size'));
        return admin_data($data);
    }

    public function add()
    {
        if (is_page()) {
            return SBuilder::makeForm()
                ->setPageTitle('添加配置')
                ->addFormItems($this->getFormItems())
                ->setTrigger('local_prefix', 'driver', 'local')
                ->setTrigger('qiniu_access_key,qiniu_secret_key,qiniu_bucket,qiniu_domain', 'driver', 'qiniu')
                ->setTrigger('aliyun_oss_id,aliyun_oss_secret,aliyun_oss_endpoint,aliyun_oss_bucket,aliyun_oss_domain', 'driver', 'aliyun')
                ->setTrigger('tencent_secret_id,tencent_secret_key,tencent_cos_region,tencent_cos_schema,tencent_cos_bucket,tencent_cos_domain', 'driver', 'tencent')
                ->setTrigger('aws_s3_key,aws_s3_secret,aws_s3_region,aws_s3_bucket,aws_s3_domain', 'driver', 'aws')
                ->fetch();
        }else {
            $params = input('param.');
            $driver = input('driver');
            $data = $this->convertParamsToModelData($driver, $params);
            if ($data['status'] == \app\system\model\OssConfig::status_1) { // 如果是启用状态, 将其他启用的禁用
                \app\system\model\OssConfig::where('status', \app\system\model\OssConfig::status_1)->update(['status' => \app\system\model\OssConfig::status_2]);
            }else{
                $enableConfig = \app\system\model\OssConfig::where('status', \app\system\model\OssConfig::status_1)->find();
                if (empty($enableConfig)) {
                    throw new CommonResponseException('当前没有已启用的配置, 请启用此配置');
                }
            }
            $obj = \app\system\model\OssConfig::create($data);
            return admin_success('操作成功', '__relist__');
        }
    }

    public function edit()
    {
        $id = input('id');
        if (empty($id)) return admin_error('主键为空');
        $obj = \app\system\model\OssConfig::get($id);
        if (empty($obj)) {
            return admin_error('数据不存在');
        }
        if (is_page()) {
            return SBuilder::makeForm()
                ->setPageTitle('修改配置')
                ->addFormItems($this->getFormItems())
                ->setTrigger('local_prefix', 'driver', 'local')
                ->setTrigger('qiniu_access_key,qiniu_secret_key,qiniu_bucket,qiniu_domain', 'driver', 'qiniu')
                ->setTrigger('aliyun_oss_id,aliyun_oss_secret,aliyun_oss_endpoint,aliyun_oss_bucket,aliyun_oss_domain', 'driver', 'aliyun')
                ->setTrigger('tencent_secret_id,tencent_secret_key,tencent_cos_region,tencent_cos_schema,tencent_cos_bucket,tencent_cos_domain', 'driver', 'tencent')
                ->setTrigger('aws_s3_key,aws_s3_secret,aws_s3_region,aws_s3_bucket,aws_s3_domain', 'driver', 'aws')
                ->setFormData($this->convertModelDataToParams($obj->driver, $obj))
                ->fetch();
        }else {
            $params = input('param.');
            $driver = input('driver');
            $data = $this->convertParamsToModelData($driver, $params);
            if ($data['status'] == \app\system\model\OssConfig::status_1) { // 如果是启用状态, 将其他启用的禁用
                \app\system\model\OssConfig::where('status', \app\system\model\OssConfig::status_1)->update(['status' => \app\system\model\OssConfig::status_2]);
            }else{
                $enableConfig = \app\system\model\OssConfig::where('status', \app\system\model\OssConfig::status_1)->find();
                if (empty($enableConfig)) {
                    throw new CommonResponseException('当前没有已启用的配置, 请启用此配置');
                }
            }
            $obj->data($data)->save();
            return admin_success('操作成功', '__relist__');
        }
    }

    private $fieldsMappings = [
        'local' => [
            'prefix' => 'local_prefix',
        ],
        'qiniu' => [
            'access_key' => 'qiniu_access_key',
            'secret_key' => 'qiniu_secret_key',
            'bucket' => 'qiniu_bucket',
            'domain' => 'qiniu_domain',
        ],
        'aliyun' => [
            'access_key' => 'aliyun_oss_id',
            'secret_key' => 'aliyun_oss_secret',
            'endpoint' => 'aliyun_oss_endpoint',
            'bucket' => 'aliyun_oss_bucket',
            'domain' => 'aliyun_oss_domain',
        ],
        'tencent' => [
            'access_key' => 'tencent_secret_id',
            'secret_key' => 'tencent_secret_key',
            'region' => 'tencent_cos_region',
            'schema' => 'tencent_cos_schema',
            'bucket' => 'tencent_cos_bucket',
            'domain' => 'tencent_cos_domain',
        ],
        'aws' => [
            'access_key' => 'aws_s3_key',
            'secret_key' => 'aws_s3_secret',
            'region' => 'aws_s3_region',
            'bucket' => 'aws_s3_bucket',
            'domain' => 'aws_s3_domain',
        ],
    ];
    private function convertParamsToModelData($driver, $params){
        $data = ['driver' => $driver];
        $mapping = $this->fieldsMappings[$driver];
        foreach ($mapping as $key => $value) {
            $data[$key] = $params[$value];
        }
        $data['status'] = $params['status'];
        $data['remark'] = $params['remark'];
        return $data;
    }

    private function convertModelDataToParams($driver, $model){
        $params = ['driver' => $driver];
        $mapping = $this->fieldsMappings[$driver];
        foreach ($mapping as $key => $value) {
            $params[$value] = $model[$key];
        }
        $params['status'] = $model['status'];
        $params['remark'] = $model['remark'];
        $params['id'] = $model['id'];
        return $params;
    }

    private function getFormItems()
    {
        return [
            ['driver:require', '存储驱动', 'radio', \app\system\model\OssConfig::$drivers, '', 'local'],
            ['name', '配置名'],
            ['local_prefix:require', '存储路径', '', '相对于public目录的路径, 不需要用<code>/</code>开头, 如<code>uploads</code>'],

            ['qiniu_access_key:require', 'AccessKey'],
            ['qiniu_secret_key:require', '秘钥'],
            ['qiniu_bucket:require', '存储桶'],
            ['qiniu_domain:require', '自定义域名', '', '自定义域名, 若没有请填写七牛云分配的域名, 此域名用于上传后生成文件地址'],

            ['aliyun_oss_id:require', '阿里云OssID'],
            ['aliyun_oss_secret:require', '秘钥'],
            ['aliyun_oss_endpoint:require', '访问地址'],
            ['aliyun_oss_bucket:require', '存储桶'],
            ['aliyun_oss_domain:require', '自定义域名', '', '自定义域名, 若没有请填写阿里云分配的域名, 此域名用于上传后生成文件地址'],

            ['tencent_secret_id:require', '腾讯云SecretID'],
            ['tencent_secret_key:require', '秘钥'],
            ['tencent_cos_region:require', '区域', '', '如: ap-guangzhou'],
            ['tencent_cos_schema:require', '协议', 'radio', ['http' => 'http', 'https' => 'https'], '', 'https'],
            ['tencent_cos_bucket:require', '存储桶'],
            ['tencent_cos_domain:require', '自定义域名', '', '自定义域名, 若没有请填写腾讯云分配的域名, 此域名用于上传后生成文件地址'],

            ['aws_s3_key:require', 'AWS S3 KEY'],
            ['aws_s3_secret:require', '秘钥'],
            ['aws_s3_region:require', '区域', '', '如: us-east-2'],
            ['aws_s3_bucket:require', '存储桶'],
            ['aws_s3_domain', '自定义域名'],

            ['status', '状态', 'switch', '', '2', ['active_text' => '启用', 'inactive_text' => '禁用']],

            ['remark', '备注'],
        ];
    }


}