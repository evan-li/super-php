<?php


namespace app\system\admin;


use app\system\model\Database as DatabaseModel;
use app\system\model\TableFieldType;
use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;
use think\Db;

class Database extends Admin
{

    private function getTabs(){

        $databases = DatabaseModel::field('id, name, remark')->select();

        $navTabs = [
            ['default', '默认', url('index', ['current' => 'default'])],
        ];
        foreach ($databases as $database) {
            $navTabs[] = [$database->name, $database->remark, url('index', ['current' => $database->name])];
        }
        $navTabs[] = ['add_database', '+', 'pop:' . url('addDatabase')];
        return $navTabs;
    }

    /**
     * 获取数据库连接列表
     */
    public function connections()
    {
        $databases = DatabaseModel::order('id desc')->select();
        $databases->each(function (&$item) {
            $item['password'] = '';
        });
        $default = config('database.');
        $default['name'] = 'default';
        $default['remark'] = 'default';
        $default['id'] = '0';
        unset($default['password']);
        $databases->push($default);

        return SBuilder::makeTable()
            ->setPageTitle('数据源管理')
            ->addColumns([
                ['id', 'ID'],
                ['name', '连接名'],
                ['remark', '备注名'],
                ['type', '连接类型'],
                ['hostname', '主机名'],
                ['database', '数据库名'],
                ['username', '用户名'],
                ['hostport', '端口'],
                ['create_time', '创建时间', 'datetime'],
                ['__btn__']
            ])
            ->addTopButton('add', '添加', url('addDatabase'), 'pop')
            ->addRightButton('edit', '', url('editDatabase'), 'pop', ['hide' => ['id' => '0']])
            ->addRightButton('delete', '', url('deleteDatabase'), 'ajax', ['hide' => ['id' => '0']])
            ->setRowList($databases)
            ->fetch();
    }

    public function index()
    {
        $currentTab = input('current', 'default');
        return SBuilder::makeTable()
            ->setPageTitle('数据库管理')
            ->setNavTab($this->getTabs())
            ->setNavTabCurrent($currentTab)
            ->addColumns([
                ['__index__'],
                ['name', '表名'],
                ['engine', '引擎'],
                ['table_rows', '行数'],
                ['data_length', '数据大小', 'byte'],
                ['index_length', '索引大小', 'byte'],
                ['table_collation', '字符集'],
                ['table_comment', '表注释'],
                ['__btn__'],
            ])
            ->setRowListUrl(url('getTableList', ['current' => $currentTab]))
            ->addTopButton('connections', '数据源管理', url('connections'), 'pop', ['batch' => false])
            ->addRightButton('data', '表数据', url('tableData', ['database' => $currentTab, 'table' => '__name__']), '', ['text' => true])
            ->addRightButton('fields', '表字段', url('tableFields', ['database' => $currentTab, 'table' => '__name__']), '', ['text' => true])
            ->addRightButton('field_type', '类型映射', url('fieldsTypeMapping', ['database' => $currentTab, 'table' => '__name__']), '', ['text' => true, 'span' => 20])
            ->fetch();
    }

    /**
     * 根据数据库获取表列表
     * @throws
     */
    public function getTableList()
    {
        $current = input('current', 'default');

        $list = DatabaseModel::getTables($current);

        return admin_data($list);
    }

    public function addDatabase()
    {
        if(is_page()) {
            return SBuilder::makeForm(['width' => '50%'])
                ->setPageTitle('添加数据源')
                ->addFormItems([
                    ['name', '名称', '', '连接名'],
                    ['remark', '备注', '', '用于显示区分'],
                    ['hostname', '主机名'],
                    ['hostport', '端口', 'number', '', 3306],
                    ['database', '数据库名'],
                    ['username', '用户名'],
                    ['password', '密码', 'password'],
                ])
                ->fetch();
        }else {
            $config = [
                'name' => input('name'),
                'remark' => input('remark'),
                'type' => 'mysql',
                'hostname' => input('hostname'),
                'hostport' => input('hostport'),
                'database' => input('database'),
                'username' => input('username'),
                'password' => input('password'),
            ];
            // 测试连接
            Db::connect($config)->query('show tables');

            DatabaseModel::create($config);
            return admin_success('添加成功', '__refresh__');
        }
    }

    public function editDatabase()
    {
        $id = input('id');
        $obj = DatabaseModel::get($id)->hidden(['password']);
        if(empty($obj)) {
            return admin_error('数据不存在');
        }
        if(is_page()) {
            return SBuilder::makeForm(['width' => '50%'])
                ->setPageTitle('修改数据源')
                ->addFormItems([
                    ['name', '名称', '', '连接名'],
                    ['remark', '备注', '', '用于显示区分'],
                    ['hostname', '主机名'],
                    ['hostport', '端口', 'number', '', 3306],
                    ['database', '数据库名'],
                    ['username', '用户名'],
                    ['password', '密码', 'password'],
                ])
                ->setFormData($obj)
                ->fetch();
        }else {
            $config = [
                'name' => input('name'),
                'remark' => input('remark'),
                'type' => 'mysql',
                'hostname' => input('hostname'),
                'hostport' => input('hostport'),
                'database' => input('database'),
                'username' => input('username'),
            ];
            $password = input('password');
            if (!empty($password)) {
                $config['password'] = $password;
            }
            // 测试连接
            Db::connect($config)->query('show tables');

            $obj->save($config);
            return admin_success('编辑成功', '__refresh__');
        }
    }

    public function deleteDatabase()
    {
        $id = input('id');
        $obj = DatabaseModel::get($id);
        if(empty($obj)) {
            return admin_error('数据不存在');
        }
        $obj->delete();
        return admin_success('删除成功', '__refresh__');
    }

    /**
     * 表字段列表
     */
    public function tableFields()
    {
        $database = input('database', 'default');
        $table = input('table');

        if(is_page()) {
            // 获取表结构
            $fieldsType = DatabaseModel::getTableQuery($database, $table)->getFieldsType();
            // 预处理已设置的类型
            $fieldsSetting = TableFieldType::where('database', $database)->where('table', $table)->select();
            foreach ($fieldsSetting as $item) {
                $fieldsType[$item->field] = $item->toArray();
            }
            // 处理表注释
            $dbname = DatabaseModel::getDatabaseConfig($database)['database'];
            $fieldsComment = DatabaseModel::getDatabaseQuery($database)->query("select column_name as field, column_comment as comment from information_schema.columns where table_name = '$table' and table_schema = '$dbname' ");
            $fields = [];
            foreach ($fieldsComment as $item) {
                $fieldType = $fieldsType[$item['field']];
                if(is_string($fieldType)) {
                    $fields[] = [
                        'field' => $item['field'],
                        'type' => $fieldType,
                        'table_type' => 'text',
                        'form_type' => 'text',
                        'comment' => $item['comment'],
                    ];
                }else {
                    $fieldType['comment'] = $item['comment'];
                    $fields[] = $fieldType;
                }
            }
            return SBuilder::makeTable()
                ->setPageTitle($table . ' 表字段')
                ->addColumns([
                    ['field', '字段名'],
                    ['type', '字段类型'],
                    ['comment', '字段注释'],
                    ['table_type', '表格类型', 'select', DatabaseModel::getTableTypes()],
                    ['form_type', '表单类型', 'select', DatabaseModel::getFormTypes(), '', ['multiple' => true]],
                    ['__btn__'],
                ])
                ->setPk('field')
                ->setRowList($fields)
                ->addRightButton('setType', '类型映射', url('fieldTypeMapping', ['database' => $database, 'table' => $table]), 'pop', ['extra_data' => [
                    'field' => '__field__',
                ]])
                ->setQuickEditUrl(url('tableFieldTypeQuickEdit', ['database' => $database, 'table' => $table]))
                ->fetch();
        }
    }

    public function tableFieldTypeQuickEdit()
    {
        $database = input('database');
        $table = input('table');
        $_id = input('_id');
        $field = input('field');
        $value = input('value');
        TableFieldType::where('database', $database)->where('table', $table)->where('field', $_id)->update([
            $field => $value
        ]);
        return admin_success('操作成功');
    }

    public function fieldTypeMapping()
    {
        $database = input('database', 'default');
        $table = input('table');
        $field = input('field');
        if(is_page()) {
            $fieldSetting = TableFieldType::where('database', $database)->where('table', $table)->where('field', $field)->find();
            if(empty($fieldSetting)) {
                $fieldsType = DatabaseModel::getTableQuery($database, $table)->getFieldsType();
                $fieldSetting = TableFieldType::create([
                    'database' => $database,
                    'table' => $table,
                    'field' => $field,
                    'type' => $fieldsType[$field],
                    'table_type' => 'text',
                    'form_type' => 'text',
                ]);
            }
            $formTypes = DatabaseModel::getFormTypes();

            $tableTypes = DatabaseModel::getTableTypes();
            return SBuilder::makeForm()
                ->setPageTitle("字段映射: $database.$table.$field")
                ->addFormItems([
                    ['field', '字段名', 'static',],
                    ['type', '数据库类型', 'static',],
                    ['table_type', '列表类型', 'select', $tableTypes,],
                    ['form_type', '表单类型', 'select', $formTypes,],
                ])
                ->setFormData($fieldSetting)
                ->fetch();
        }else {
            TableFieldType::where('database', $database)
                ->where('table', $table)
                ->where('field', $field)
                ->update([
                    'table_type' => input('table_type', 'text'),
                    'form_type' => input('form_type', 'text'),
                ]);
            return admin_success('', '__refresh__');
        }
    }

    /**
     * 表数据查看
     */
    public function tableData()
    {
        $database = input('database', 'default');
        $table = input('table');
        if(is_page()) {
            // 获取表结构
            $fields = DatabaseModel::getTableQuery($database, $table)->getTableFields();
            $columns = [];
            foreach ($fields as $field) {
                $columns[] = [$field, $field];
            }
            $columns[] = ['__btn__', '', '', '', '', ['fixed' => 'right']];
            // 处理列宽
            $columnWidth = [];
            foreach ($fields as $field) {
                $columnWidth[$field] = (strlen($field) * 10 + 50) . 'px';
            }
//            dd($columnWidth);
            return SBuilder::makeTable()
                ->setPageTitle($table . ' 表数据')
                ->addColumns($columns)
                ->setOrder($fields)
//                ->setColumnWidth($fields, 300)
                ->setRowListUrl(url('', ['database' => $database, 'table' => $table]))
                ->setColumnWidth($columnWidth)
                ->fetch();
        }else {
            $data = DatabaseModel::getTableQuery($database, $table)->order($this->getOrder())->paginate(input('page_size'));
            return admin_data($data);
        }
    }

    public function fieldsTypeMapping()
    {
        $database = input('database', 'default');
        $table = input('table');
        if(is_page()) {
            // 获取表结构
            $fields = DatabaseModel::getTableQuery($database, $table)->getFieldsType();
            $groupItems = [];
            foreach ($fields as $field => $type) {
                $groupItems[$field] = [
                    [$field . '_type', '数据库类型', 'static', '', $type],
                    [$field . '_table_type', '列表类型', 'select', [], '', 'text'],
                    [$field . '_form_type', '表单类型', 'select', [], '', 'text'],
                ];
            }
            $formItems[] = ['fileds', $groupItems, 'group'];

            return SBuilder::makeForm()
                ->setPageTitle($table . '表字段类型映射')
                ->addFormItems($formItems)
                ->fetch();
        }
    }
}