<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 * Date: 2019/5/3
 * Time: 21:38
 */

namespace app\system\admin;


use app\system\model\Attachment as AttachmentModel;
use app\system\service\AttachmentService;
use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;
use Exception;
use think\helper\Str;

class Attachment extends Admin
{

    /**
     * 附件列表
     */
    public function index()
    {
        if(is_page()){
            return SBuilder::make('table')
                ->setPageTitle('附件管理')
                ->addColumns([
                    ['__checkbox__'],
                    ['id', 'ID'],
                    ['uid', '上传用户ID', '', [0 => '']],
                    ['name', '文件名', 'link', [
                        'type' => 'link',
                        'target' => '_blank',
                        'url' => url('attachment/download', ['id' => '__id__']),
                    ]],
                    ['id', '预览', 'image', '', '', ['trigger' => ['mime' => ['image/png', 'image/jpeg', 'image/gif']]]],
                    ['mime', 'MIME', ''],
                    ['size', '文件大小', 'byte'],
                    ['driver', '驱动', ''],
                    ['status', '状态', 'status', ['停用', '正常']],
                    ['create_time', '添加时间', 'datetime',],
                    ['__btn__'],
                ])
                ->setOrder('id,create_time')
                ->setColumnWidth('id', 100)
                ->setFilters('uid', AttachmentModel::distinct(true)->column('uid', 'uid'))
                ->setFilters('status', ['停用', '正常'])
                ->addRightButton('enable')
                ->addRightButton('disable')
                ->addRightButton('delete')
                ->addTopButton('enable')
                ->addTopButton('disable')
                ->addTopButton('delete')
                ->addTopButton('uploadImage', '上传图片', url('uploadImage'), 'pop', ['type' => 'success', 'batch' => false])
                ->addTopButton('uploadVideo', '上传视频', url('uploadVideo'), 'pop', ['type' => 'success', 'batch' => false])
                ->addTopButton('uploadFile', '上传文件', url('uploadFile'), 'pop', ['type' => 'success', 'batch' => false])
                ->addTopButton('config', '配置管理', url('OssConfig/index'), 'link', ['type' => 'info', 'batch' => false, 'icon' => 'el-icon-Setting'])
                ->fetch();
        }
    }

    /**
     * 图片上传
     */
    public function uploadImage()
    {
        if (is_page()) {
            return SBuilder::makeForm()
                ->setPageTitle('图片上传')
                ->addImages('image', '请选择图片', '可多选, 最多9张', '', [
                    'limit' => 9,
                    'drag' => true
                ])
                ->hideBtn('submit')
                ->setBtnTitle('cancel', '确认')
                ->fetch();
        }
    }

    /**
     * 视频上传
     */
    public function uploadVideo()
    {
        if (is_page()) {
            return SBuilder::makeForm()
                ->setPageTitle('视频上传')
                ->addVideo('image', '请选择视频', '支持多选, 最多9个', '', [
                    'limit' => 9,
                    'drag' => true
                ])
                ->hideBtn('submit')
                ->setBtnTitle('cancel', '确认')
                ->fetch();
        }
    }

    /**
     * 文件上传
     */
    public function uploadFile()
    {
        return SBuilder::makeForm()
            ->setPageTitle('文件上传')
            ->addFiles('files', '请选择文件', '可多选, 最多9个', '', [
                'limit' => 9,
                'drag' => true
            ])
            ->hideBtn('submit')
            ->setBtnTitle('cancel', '确认')
            ->fetch();
    }

    /**
     * 获取文件地址
     * @param string $ids
     */
    public function getFilesPath($ids = '')
    {
        if(empty($ids)) {
            return res_error('id不能为空');
        }
        if(is_string($ids)){
            $ids = explode(',', $ids);
        }
        $filePaths = get_files_path($ids);
        return admin_data($filePaths);
    }

    /**
     * 文件上传
     * 图片存储地址 public/uploads/images
     * 图片存储地址 public/uploads/videos
     * 其他文件存储地址 public/uploads/files
     * 文件类型, 根据传入的dir区分
     * @param string $dir 存储目录 images-图片 files-其他文件
     * @return \think\Response|\think\response\Json
     * @throws Exception
     */
    public function upload($dir = '')
    {
        if(empty($dir)) return admin_error('没有指定上传目录');

        $file = $this->request->file('file');
        // 判断附件格式是否符合
        $fileName = $file->getInfo('name');
        $fileExt  = strtolower(substr($fileName, strrpos($fileName, '.')+1));
        $extLimit = $dir == 'images' ? config('filesystem.upload_image_ext') : ($dir == 'videos' ? config('filesystem.upload_video_ext') : config('filesystem.upload_file_ext'));
        $extLimit = explode(',', $extLimit);

        // 判断是否非法文件
        if ($file->getMime() == 'text/x-php' || $file->getMime() == 'text/html') {
            return admin_error('禁止上传非法文件！');
        }
        if (preg_grep("/php/i", $extLimit)) {
            return admin_error('禁止上传非法文件！');
        }
        // 判断文件后缀类型是否合法
        if (!preg_grep("/$fileExt/i", $extLimit)) {
            return admin_error('文件类型不正确！');
        }

        $attachment = AttachmentService::upload($file, $dir);

        return admin_data([
            'id' => $attachment->id,
            'url' => $attachment->url,
            'thumb' => $attachment->thumb,
            'width' => $attachment->width,
            'height' => $attachment->height,
        ]);
    }

    /**
     * 大文件上传-目前仅支持本地存储
     * 图片存储地址 public/uploads/images
     * 图片存储地址 public/uploads/videos
     * 其他文件存储地址 public/uploads/files
     * 文件类型, 根据传入的dir区分
     * @param string $dir 存储目录 images-图片 files-其他文件
     * @return \think\Response|\think\response\Json
     * @throws Exception
     */
    public function bigupload($dir = '')
    {
        if(empty($dir)) return admin_error('没有指定上传目录');

        $file = $this->request->file('file');
        // 判断附件格式是否符合
        $fileName = input('fileName', '');
        $totalChunks = intval(input('totalChunks', 0));
        $chunkIndex = intval(input('chunkIndex', 0));
        $fileExt  = strtolower(substr($fileName, strrpos($fileName, '.')+1));
        $extLimit = $dir == 'images' ? config('filesystem.upload_image_ext') : ($dir == 'videos' ? config('filesystem.upload_video_ext') : config('filesystem.upload_file_ext'));
        $extLimit = explode(',', $extLimit);

        // 判断是否非法文件
        if ($file->getMime() == 'text/x-php' || $file->getMime() == 'text/html') {
            return admin_error('禁止上传非法文件！');
        }
        if (preg_grep("/php/i", $extLimit)) {
            return admin_error('禁止上传非法文件！');
        }
        // 判断文件后缀类型是否合法
        if (!preg_grep("/$fileExt/i", $extLimit)) {
            return admin_error('文件类型不正确！');
        }

        $res = AttachmentService::bigFileUpload($file, $fileName, $chunkIndex, $totalChunks, $dir);

        return admin_data($res);
    }

    /**
     * 附件下载
     */
    public function download()
    {
        $external = $this->request->param('external', false);
        if(!$external){
            // 如果是内部附件
            $res = $this->validate($this->request->param(), [
                'id' => 'require'
            ]);
            if($res !== true){
                return admin_error($res);
            }
            $id = $this->request->param('id');
            $url = get_file_path($id);
            if(Str::startsWith($url, '/uploads')){
                $path = public_path($url);
            }else {
                $path = $url;
            }
        }else {
            $res = $this->validate($this->request->param(), [
                'url' => 'require'
            ]);
            if($res !== true){
                return admin_error($res);
            }
            $path = $this->request->param('url');
        }
        return download($path);
    }

}