<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 * Date: 2019/5/16
 * Time: 16:12
 */
namespace app\system\admin;


use app\system\model\AuthGroup as AuthGroupModel;
use app\system\model\AuthRule as AuthRuleModel;
use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\Exception;
use think\exception\DbException;

class AuthGroup extends Admin
{

    /**
     * 用户组列表页面
     * @return mixed
     * @throws Exception
     */
    public function index()
    {
        return SBuilder::make('table')
            ->setPageTitle('角色管理')
            ->addColumns([
                ['__checkbox__'],
                ['id', 'ID'],
                ['name', '名称'],
                ['status', '状态', 'status'],
                ['create_time', '创建时间', 'datetime'],
                ['__btn__'],
            ])
            ->autoAdd([
                ['name', '名称'],
                ['status', '状态', 'status'],
            ], [
                'name' => 'require'
            ])
            ->addTopSelect('status', '状态', ['禁用', '启用'])
            ->addRightButton('edit')
            ->addRightButton('enable')
            ->addRightButton('disable')
            ->addRightButton('delete')
            ->addTopButton('enable')
            ->addTopButton('disable')
            ->addTopButton('delete')
            ->fetch();
    }

    /**
     * 添加分组
     * @return mixed
     * @throws Exception
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws DbException
     */
    public function add()
    {
        if(is_page()){
            $tree = AuthRuleModel::getAllEnableTree($this->currentUser->is_dev, 'id,name,pid');
            return SBuilder::make('form')
                ->setPageTitle('新增角色')
                ->addFormItems([
                    ['name', '名称'],
                    ['rule_ids', '权限列表', 'tree', $tree],
                    ['status', '状态', 'status'],
                ])
                ->fetch();
        }else {
            $this->validate($this->request->param(), [
                'name|角色名' => 'require',
            ]);
            $name = $this->request->param('name');
            $rule_ids = $this->request->param('rule_ids', []);
            $status = $this->request->param('status', 1);

            // 获取所有权限的父级权限ID
            $pids = AuthRuleModel::getAllTierPids($rule_ids);
            $rule_ids = array_unique(array_merge($pids, $rule_ids));

            AuthGroupModel::create([
                'name' => $name,
                'rule_ids' => implode(',', $rule_ids),
                'status' => $status,
            ]);
            return admin_success('添加角色成功');
        }
    }

    public function edit()
    {
        if(is_page()){
            $tree = AuthRuleModel::getAllEnableTree($this->currentUser->is_dev, 'id,name,pid');
            $id = $this->request->param('id');
            if (empty($id)) return admin_error('缺少主键');

            $group = AuthGroupModel::find($id);
            $ruleIds = explode(',', $group->rule_ids);
            // 过滤权限ID中的非子节点列表
            $rulePids = AuthRuleModel::where('id', 'in', $ruleIds)->column('pid');
            $ruleIds = array_diff($ruleIds, array_unique($rulePids));

            $group->rule_ids = array_values($ruleIds);

            return SBuilder::make('form')
                ->setPageTitle('新增角色')
                ->addFormItems([
                    ['name', '名称'],
                    ['rule_ids', '权限列表', 'tree', $tree],
                    ['status', '状态', 'status'],
                ])
                ->setFormData($group)
                ->fetch();
        }else {
            $this->validate($this->request->param(), [
                'id|ID' => 'require|integer|min:1',
                'name|角色名' => 'require',
            ]);
            $name = $this->request->param('name');
            $rule_ids = $this->request->param('rule_ids', []);
            $status = $this->request->param('status', 1);

            // 获取所有权限的父级权限ID
            $pids = AuthRuleModel::getAllTierPids($rule_ids);
            $rule_ids = array_unique(array_merge($pids, $rule_ids));

            $group = AuthGroupModel::find($this->request->param('id'));
            $group->save([
                'name' => $name,
                'rule_ids' => implode(',', $rule_ids),
                'status' => $status,
            ]);
            return admin_success('修改角色成功');
        }
    }

}