<?php


namespace app\system\admin;


use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;
use evan\sbuilder\module\Store;

class Setting extends Admin
{
    public function index()
    {
        $moduleInfo = Store::getLocalModules();
        $modules = [];
        $moduleTitles = \app\system\model\Setting::where('key', 'title')->column('value', 'module');

        foreach ($moduleInfo as $module) {
            $modules[$module['name']] = $moduleTitles[$module['name']] ?? $module['name'];
        }
        $fields = [
            ['module:require', '模块', 'select', $modules],
            ['key:require', '键名'],
            ['value:require', '配置值', 'textarea'],
            ['desc', '配置项说明'],
        ];
        $validate = [
            'module' => 'require',
            'key' => 'require',
            'value' => 'require',
        ];
        return SBuilder::make('table')
            ->setPageTitle('配置管理')
            ->addTopSelect('module', '模块', $modules)
            ->setSearch('key|键名')
            ->autoAdd($fields, $validate, true)
            ->addColumns([
                ['module', '模块', 'text', $modules],
                ['key', '键名'],
                ['value', '配置值'],
                ['desc', '配置项说明'],
                ['__btn__'],
            ])
            ->autoEdit($fields, $validate, true)
            ->setOrder('module')
            ->fetch();
    }

}