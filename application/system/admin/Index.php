<?php


namespace app\system\admin;


use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;
use app\system\model\User as UserModel;
use think\facade\Cache;
use think\facade\Session;
use think\helper\Str;
use think\response\Json;

class Index extends Admin
{

    public function index()
    {
        return $this->homePage();
    }

    /**
     * 系统配置
     */
    public function config()
    {
        return $this->systemConfig();
    }

    /**
     * 清空缓存
     */
    public function cache()
    {
        $opt = $this->request->param('opt');
        if($opt == 'clear'){
            Cache::clear();
        }else if($opt == 'rm') {
            $key = $this->request->param('key');
            Cache::rm($key);
        }
        return admin_success('操作成功', '');
    }

    /**
     * 清空session
     */
    public function session()
    {
        $opt = $this->request->param('opt');
        if($opt == 'clear'){
            Session::clear();
        }else if($opt == 'rm') {
            $key = $this->request->param('key');
            Session::delete($key);
        }
        return admin_success('操作成功', '');
    }

    /**
     * 获取前端初始数据, 包含用户信息与菜单信息
     */
    public function getInitData()
    {
        $user = $this->currentUser;
        $menus = empty($user) ? [] : UserModel::getMenus($user, true);
        return admin_data([
            'menus' => $menus,
            'user' => $user,
        ]);
    }

    /**
     * 获取当前登陆的用户信息
     */
    public function currentUser()
    {
        return admin_data($this->currentUser);
    }

    /**
     * 获取系统菜单
     * @return Json
     */
    public function menus()
    {
        $tree = UserModel::getMenus($this->currentUser, true);
        return admin_data(['list' => $tree]);
    }

    /**
     * 登陆操作
     */
    public function login()
    {
        $result = $this->validate($this->request->param(), [
            'username|用户名' => 'require',
            'password|密码' => 'require|length:6,32',
            'captcha|验证码' => 'require'
        ]);
        if($result !== true){
            return admin_error($result);
        }
        $username = $this->request->param('username');
        $password = $this->request->param('password');
        $captcha = $this->request->param('captcha');
        if(!captcha_check($captcha)){
            return admin_error('验证码错误');
        }

        $user = UserModel::where('username', $username)->find();
        if(empty($user)){
            return admin_error('账号或密码错误');
        }
        if($user->status != 1){
            return admin_error('您已被禁止登录');
        }
        if(UserModel::hashPassword($password, $user->salt) != $user->password){
            return admin_error('账号或密码错误');
        }
        // 登陆成功,写入用户信息到session
        session('current_user', $user);

        return admin_success('登陆成功', '');
    }

    /**
     * 退出登陆
     */
    public function logout()
    {
        session('current_user', null);
        session('user_menus', null);
        return admin_success();
    }

    /**
     * 修改密码
     */
    public function modifyPassword()
    {
        $user = $this->currentUser;
        if(empty($user)) {
            return admin_error('未登录');
        }
        if(is_page()) {
            return SBuilder::makeForm()
                ->setPageTitle('修改密码')
                ->addFormItems([
                    ['password:require', '原密码', 'password'],
                    ['newPassword:require', '新密码', 'password'],
                    ['reNewPassword:require', '确认新密码', 'password'],
                ])
                ->fetch();
        }else {
            $this->checkParam([
                'password' => 'require|min:6',
                'newPassword' => 'require|min:6',
                'reNewPassword' => 'require|confirm:newPassword',
            ]);
            // 验证原密码
            $password = input('password');
            $newPassword = input('newPassword');
            if(UserModel::hashPassword($password, $user->salt) != $user->password) {
                return admin_error('密码错误');
            }
            $salt = Str::random();
            $user->password = UserModel::hashPassword($newPassword, $salt);
            $user->salt = $salt;
            $user->save();
            return admin_success('密码修改成功');
        }
    }


}