<?php


namespace app\system\admin;


use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;
use app\system\model\Icon as IconModel;
use think\Exception;
use think\helper\Str;


class Icon extends Admin
{
    protected $fields = [
        ['type', '字体类型', 'select', ['1' => '阿里iconfont图标库'], '', '1'],
        ['name', '名称', '', ''],
        ['css_url', 'css地址', '', '用于图标的显示'],
        ['status', '状态', 'status'],
    ];
    protected $validate = [
        'type' => 'require',
        'name' => 'require',
        'css_url' => 'require',
    ];

    /**
     * @return mixed
     * @throws Exception
     */
    public function index()
    {
        return SBuilder::make('table')
            ->setPageTitle('图标扩展')
            ->setPageTips('图标更新后需要刷新页面才生效')
            ->addColumns([
                ['id', 'ID'],
                ['type', '字体类型', '', ['1' => '阿里iconfont图标库']],
                ['name', '字体名'],
                ['font_family', 'font-family'],
                ['status', '状态', 'status'],
                ['create_time', '添加时间', 'datetime'],
                ['__btn__']
            ])
            ->addTopButton('add', '', '', 'pop')
            ->addTopButton('guide', '图标扩展指南', 'https://www.kancloud.cn/evanlee/sphp/1092573', '', [
                'target' => '_blank',
                'type' => 'text',
                'batch' => false,
            ])
            ->addRightButton('edit', '', '', 'pop')
            ->addRightButton('enable')
            ->addRightButton('disable')
            ->addRightButton('delete')
            ->fetch();
    }

    public function add()
    {
        if(is_page()){
            return SBuilder::make('form')
                ->setPageTitle('新增')
                ->addFormItems($this->fields)
                ->setLabelWith(150)
                ->fetch();
        }else {
            $res = $this->validate($this->request->param(), $this->validate);
            if($res !== true){
                return admin_error($res);
            }
            $name = $this->request->param('name');
            $type = $this->request->param('type', 1);
            $css_url = $this->request->param('css_url');
            $status = $this->request->param('status', 1);

            list($font_family, $icons) = $this->getIconInfoByCssUrl($css_url);
            $icon = new IconModel();
            $icon->name = $name;
            $icon->type = $type;
            $icon->font_family = $font_family;
            $icon->css_url = $css_url;
            $icon->icons = implode(',', $icons);
            $icon->status = $status;
            $icon->save();
            return admin_success('添加成功');
        }

    }

    public function edit()
    {
        $id = $this->request->param('id');
        if (empty($id)) return admin_error('缺少主键');
        $icon = IconModel::get($id);

        if(is_page()){
            return SBuilder::make('form')
                ->setPageTitle('编辑')
                ->addFormItems($this->fields)
                ->setFormData($icon)
                ->setLabelWith(150)
                ->fetch();
        }else {
            $res = $this->validate($this->request->param(), $this->validate);
            if($res !== true){
                return admin_error($res);
            }
            $name = $this->request->param('name');
            $type = $this->request->param('type', 1);
            $css_url = $this->request->param('css_url');
            $status = $this->request->param('status', 1);

            list($font_family, $icons) = $this->getIconInfoByCssUrl($css_url);

            $icon->name = $name;
            $icon->type = $type;
            $icon->font_family = $font_family;
            $icon->css_url = $css_url;
            $icon->icons = implode(',', $icons);
            $icon->status = $status;
            $icon->save();
            return admin_success('添加成功');
        }
    }

    protected function getIconInfoByCssUrl($cssUrl)
    {
        try{
            // 根据jsUrl获取文件内容
            if (Str::startsWith($cssUrl, '//')){
                $cssUrl = 'http:' . $cssUrl;
            }
            $content = file_get_contents($cssUrl);
        }catch (\Exception $e){
            return admin_error('css地址无效');
        }

        // 解析字体名称
        $font_family = '';
        $pattern = '/font-family: "(.*)";/';
        if (preg_match($pattern, $content, $match)) {
            $font_family = $match[1];
        } else {
            return admin_error('无法获取字体名');
        }
        if(!Str::startsWith($font_family, 'el-icon-')){
            $font_family = 'el-icon- ' . $font_family;
        }
        // 解析图标列表
        $pattern = '/\.(.*-.*):before/';
        preg_match_all($pattern, $content, $matches);
        $icons = $matches[1];

        return [$font_family, $icons];
    }

}