<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 * Date: 2019/5/4
 * Time: 11:55
 */

namespace app\system\utils;


use app\common\exception\CommonResponseException;
use app\common\ResCode;
use app\system\model\OssConfig;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use think\File;
use think\helper\Str;

class Aws
{
    /**
     * 文件上传
     * @param OssConfig $config
     * @param File|string $file
     * @param string $dir
     * @param string $ext
     * @return mixed
     */
    public static function upload(OssConfig $config, $file, $dir = '', $ext = '')
    {
        $client = new S3Client([
            'version' => 'latest',
            'region' => $config->region,
            'credentials' => [
                'key'    => $config->access_key,
                'secret' => $config->secret_key,
            ]
        ]);
        if (is_string($file)) {
            $filename = $file;
            $filepath = $filename;
        }else {
            $filename = $file->getInfo('name');
            $filepath = $file->getRealPath();
        }

        $ext = $ext ?: pathinfo($filename, PATHINFO_EXTENSION);
        if(!Str::endsWith($dir, '/')) {
            $dir .= '/';
        }
        if(Str::startsWith($dir, '/')) {
            $dir = substr($dir, 1);
        }
        $content = file_get_contents($filepath);
        $path = $dir . md5($content) . '.' . $ext;
        try{
            $result = $client -> putObject([
                'Bucket' => $config->bucket,
                'Key'    => $path,
//                'Body'   => fopen($file->getRealPath(), 'r'),
                'Body'   => $content,
                'ACL'    => 'public-read'
            ]);
            return $result -> get('ObjectURL');
//            return $url = $config['domain'] . '/' . $path;
        }catch (S3Exception $e){
            throw new CommonResponseException(ResCode::unknow_error, $e->getMessage());
        }
    }
}