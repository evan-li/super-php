<?php

namespace app\system\utils;

use app\common\exception\CommonResponseException;
use app\common\ResCode;
use app\system\model\OssConfig;
use Qcloud\Cos\Client;
use think\File;
use think\helper\Str;

/**
 * 腾讯云存储
 */
class Tencent
{

    /**
     * @param OssConfig $config
     * @param File $file
     * @param string $dir
     * @param string $ext 文件扩展名
     * @return string
     * @throws
     */
    public static function upload(OssConfig $config, $file, $dir = '', $ext = '') {

        $cosClient = new Client([
            'region' => $config->region,
            'schema' => $config->schema,
            'credentials' => [
                'secretId' => $config->access_key,
                'secretKey' => $config->secret_key,
            ],
        ]);

        $ext = $ext ?: pathinfo($file->getInfo('name'), PATHINFO_EXTENSION);
        $key = (Str::endsWith($dir, '/') ? $dir : $dir . '/') . $file->hash('md5') . '.' .  $ext;


        $result = $cosClient->upload($config->bucket, $key, fopen($file->getRealPath(), 'rb'));
//        dd($result);
//        if($err !== null){
//            throw new CommonResponseException(ResCode::unknow_error, '文件上传失败 : ' . var_export($err, 1));
//        }
        return $config->domain . '/' . $result['Key'];
    }

}