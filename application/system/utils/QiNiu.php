<?php
/**
 * Create By Evan.
 * Date: 2019/7/23
 * Time: 22:07
 */

namespace app\system\utils;


use app\common\exception\CommonResponseException;
use app\common\ResCode;
use app\system\model\OssConfig;
use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use think\File;
use think\helper\Str;

class QiNiu {

    /**
     * 上传文件
     * @param OssConfig $config
     * @param File $file
     * @param string $dir
     * @param string $ext 文件扩展名
     * @return string
     * @throws
     */
    public static function upload(OssConfig $config, $file, $dir = '', $ext = '') {
        // 构建鉴权对象
        $auth = new Auth($config->access_key, $config->secret_key);
        // 生成上传 Token
        $token = $auth->uploadToken($config->bucket);
        $uploadMgr = new UploadManager();

        $ext = $ext ?: pathinfo($file->getInfo('name'), PATHINFO_EXTENSION);
        $key = (Str::endsWith($dir, '/') ? $dir : $dir . '/') . $file->hash('md5') . '.' .  $ext;

        list($ret, $err) = $uploadMgr->putFile($token, $key, $file->getRealPath());
        if($err !== null){
            throw new CommonResponseException(ResCode::unknow_error, '文件上传失败 : ' . var_export($err, 1));
        }
        return $config->domain . '/' . $ret['key'];
    }

    /**
     * 根据文件路径上传文件
     * @param OssConfig $config
     * @param $path
     * @param string $dir
     * @return string
     * @throws \Exception
     */
    public static function uploadByPath(OssConfig $config, $path, $dir = '') {

        // 构建鉴权对象
        $auth = new Auth($config->access_key, $config->secret_key);
        // 生成上传 Token
        $token = $auth->uploadToken($config->bucket);
        $uploadMgr = new UploadManager();

        $key = (Str::endsWith($dir, '/') ? $dir : $dir . '/') . md5(file_get_contents($path)) . '.' .  pathinfo($path, PATHINFO_EXTENSION);
        list($ret, $err) = $uploadMgr->putFile($token, $key, $path);
        if($err !== null){
            throw new CommonResponseException(ResCode::unknow_error, '文件上传失败 : ' . var_export($err, 1));
        }
        return $config->domain . '/' . $ret['key'];
    }

    /**
     * 根据文件内容上传文件
     * @param OssConfig $config
     * @param $content
     * @param string $filename
     * @param string $dir
     * @return string
     */
    public static function uploadByContent(OssConfig $config, $content, $filename, $dir = '')
    {
        // 构建鉴权对象
        $auth = new Auth($config->access_key, $config->secret_key);
        // 生成上传 Token
        $token = $auth->uploadToken($config->bucket);
        $uploadMgr = new UploadManager();

        $key = (Str::endsWith($dir, '/') ? $dir : $dir . '/') . $filename;
        list($ret, $err) = $uploadMgr->put($token, $key, $content);
        if($err !== null){
            throw new CommonResponseException(ResCode::unknow_error, '文件上传失败 : ' . var_export($err, 1));
        }
        return $config->domain . '/' . $ret['key'];

    }
}