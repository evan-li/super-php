<?php


namespace app\system\utils;


use app\system\model\OssConfig;
use OSS\Core\OssException;
use OSS\OssClient;
use think\File;
use think\helper\Str;

class AliOss
{

    /**
     * 文件上传
     * @param OssConfig $config
     * @param File $file
     * @param string $dir 上传目录
     * @param string $ext
     * @return string
     * @throws
     */
    public static function upload(OssConfig $config, $file, $dir = '', $ext = '')
    {
        $client = new OssClient($config->access_key, $config->secret_key, $config->endpoint);

        $ext = $ext ?: pathinfo($file->getInfo('name'), PATHINFO_EXTENSION);
        if(!Str::endsWith($dir, '/')) {
            $dir .= '/';
        }
        if(Str::startsWith($dir, '/')) {
            $dir = substr($dir, 1);
        }
        $path = $dir . $file->hash('md5') . '.' . $ext;
        $client->uploadFile($config->bucket, $path, $file->getRealPath());

        return $config->domain . '/' . $path;
    }

    /**
     * @param OssConfig $config
     * @param $filename
     * @param string $dir
     * @param string $ext
     * @return string
     * @throws OssException
     */
    public static function uploadImageByPath(OssConfig $config, $filename, $dir = '', $ext = '')
    {
        $client = new OssClient($config->access_key, $config->secret_key, $config->endpoint);

        $ext = $ext ?: pathinfo($filename, PATHINFO_EXTENSION);
        if(!Str::endsWith($dir, '/')) {
            $dir .= '/';
        }
        if(Str::startsWith($dir, '/')) {
            $dir = substr($dir, 1);
        }
        $path = $dir . md5(file_get_contents($filename)) . '.' . $ext;
        $client->uploadFile($config->bucket, $path, $filename);

        return $config->domain . '/' . $path;

    }

}