<?php

namespace app\system\service;

use app\system\model\AuthRule;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\helper\Str;

class MenuService
{

    /**
     * 创建菜单
     * @param array $menus 要添加的菜单列表
     * @param int|string $parent 父级规则url或id
     * @param string $mode 添加模式(规则url重复时):cover=覆盖旧菜单,rename=重命名新菜单,ignore=忽略
     */
    public static function create(array $menus, $parent = 0, string $mode = 'cover')
    {

        $pid = 0;
        if (!$parent instanceof AuthRule) {
            $parent = AuthRule::where(is_numeric($parent) ? 'id' : 'url', $parent)->find();
        }
        if ($parent) {
            $pid = $parent['id'];
        }
        $menuObjs = [];
        foreach ($menus as $item) {
            if (!self::checkAttr($item)) {
                continue;
            }
            $item['status'] = 1;
            if (!isset($item['pid']) && $pid) {
                $item['pid'] = $pid;
            }
            $menu = AuthRule::where('url', $item['url'])->find();
            if ($menu) {
                // 存在相关名称的菜单规则
                if ($mode == 'cover') {
                    $menu->save($item);
                } elseif ($mode == 'rename') {
                    $count = AuthRule::where('url', $item['url'])->count();
                    $item['name'] = $item['name'] . '-conflicting-' . $count;
                    $menu = AuthRule::create($item);
                } elseif ($mode == 'ignore') {
                    // 忽略模式, 不再添加
                }
            }else {
                $menu = AuthRule::create($item);
            }
            if (isset($item['children']) && $item['children']) {
                $menu->children = self::create($item['children'], $menu, $mode);
            }
            $menuObjs[] = $menu;
        }
        return $menuObjs;
    }

    /**
     * 快捷创建一个模块的增删改查菜单
     * @param array|string $module
     * @param $controller
     * @param $name
     * @param $parent
     * @param $icon
     * @param string $mode 添加模式(规则url重复时):cover=覆盖旧菜单,rename=重命名新菜单,ignore=忽略
     * @return array
     */
    public static function createCurdMenu($module, $controller = '', $name = '', $parent = 0, $icon = '', string $mode = 'cover')
    {
        if (is_array($module)) {
            $menusObj = [];
            foreach ($module as $item) {
                $menus = self::createCurdMenu(...$item);
                if (isset($menus[0])) {
                    $menusObj[] = $menus[0];
                }
            }
            return $menusObj;
        }
        $controller = Str::snake($controller);
        $menus = [
            ['module' => $module, 'name' => $name, 'url' => "/$module/$controller/index", 'icon' => $icon, 'children' => [
                ['module' => $module, 'name' => '数据列表', 'url' => "/$module/$controller/getlist", 'is_menu' => 0],
                ['module' => $module, 'name' => '新增', 'url' => "/$module/$controller/add", 'is_menu' => 0],
                ['module' => $module, 'name' => '编辑', 'url' => "/$module/$controller/edit", 'is_menu' => 0],
                ['module' => $module, 'name' => '删除', 'url' => "/$module/$controller/delete", 'is_menu' => 0],
                ['module' => $module, 'name' => '启用', 'url' => "/$module/$controller/enable", 'is_menu' => 0],
                ['module' => $module, 'name' => '禁用', 'url' => "/$module/$controller/disable", 'is_menu' => 0],
                ['module' => $module, 'name' => '快速编辑', 'url' => "/$module/$controller/quickedit", 'is_menu' => 0],
                ['module' => $module, 'name' => '批量操作', 'url' => "/$module/$controller/batch", 'is_menu' => 0],
            ]]
        ];
        return self::create($menus, $parent, $mode);
    }

    /**
     * 删除菜单
     * @param string|int $id 规则name或id
     * @param bool $recursion 是否递归删除子级菜单
     * @return bool
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     * @throws \Exception
     */
    public static function delete($id, $recursion = true)
    {
        if (!$id) {
            return true;
        }
        $menu = AuthRule::where((is_numeric($id) ? 'id' : 'url'), $id)->find();
        if (!$menu) {
            return true;
        }

        $children = AuthRule::where('pid', $menu['id'])->select()->toArray();
        if ($recursion && $children) {
            foreach ($children as $child) {
                self::delete($child['id'], $recursion);
            }
        }

        $menu->delete();
        return true;
    }

    /**
     * 根据模块名删除菜单
     * @param $module
     * @return true
     * @throws \Exception
     */
    public static function deleteByModule($module)
    {
        AuthRule::where('module', $module)->delete();
        return true;
    }

    /**
     * 启用菜单
     * @param string|int $id       规则url或id
     * @return bool
     */
    public static function enable($id): bool
    {
        $menuRule = AuthRule::where((is_numeric($id) ? 'id' : 'url'), $id)->find();
        if (!$menuRule) {
            return false;
        }
        $menuRule->status = 1;
        $menuRule->save();
        return true;
    }

    /**
     * 禁用菜单
     * @param string|int $id       规则url或id
     * @return bool
     */
    public static function disable($id): bool
    {
        $menuRule = AuthRule::where((is_numeric($id) ? 'id' : 'url'), $id)->find();
        if (!$menuRule) {
            return false;
        }
        $menuRule->status = 2;
        $menuRule->save();
        return true;
    }

    /**
     * 获取模块菜单列表
     * @param $module
     * @return AuthRule[]|\think\Collection
     */
    public static function getListByModule($module)
    {
        return AuthRule::where('module', $module)->select();
    }

    public static function checkAttr($menu): bool
    {
        $attrs = ['module', 'name', 'url'];
        foreach ($attrs as $attr) {
            if (!array_key_exists($attr, $menu)) {
                return false;
            }
            if (!$menu[$attr]) {
                return false;
            }
        }
        return true;
    }

}