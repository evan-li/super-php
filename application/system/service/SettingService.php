<?php
/**
 * Create By Evan.
 * Date: 2019/7/27
 * Time: 0:16
 */

namespace app\system\service;


use app\system\model\Setting;
use think\facade\Cache;

class SettingService {

    /**
     * 设置配置信息
     * @param $module
     * @param $key
     * @param $value
     * @return mixed
     */
    public static function set($module, $key, $value) {
        $setting = Setting::where('key', $key)->where('module', $module)->find();
        if(empty($setting)){
            $setting = new SettingService();
            $setting->key = $key;
            $setting->module = $module;
        }
        $setting->value = $value;
        $setting->save();
        Cache::rm('module:setting:' . $module);
        return $value;
    }

    /**
     * 获取配置, 不指定key时获取整个模块的配置
     * @param $module
     * @param $key
     * @param null $default
     * @return mixed
     * @throws
     */
    public static function get($module, $key = null, $default = null) {
        $moduleConfig = Cache::get('system:setting:' . $module);
        if(empty($moduleConfig)){
            $settings = Setting::where('module', $module)->select();
            $moduleConfig = [];
            foreach ($settings as $setting) {
                $value = $setting->value;
                if(!empty($arr = json_decode($value, 1))){
                    $value = $arr;
                }
                $moduleConfig[$setting->key] = $value;
            }
            Cache::set('module:setting:' . $module, $moduleConfig);
        }
        if(is_null($key)){ // 获取模块配置
            return $moduleConfig;
        }else {
            return $moduleConfig[$key] ?? $default;
        }
    }

}