<?php


namespace app\system\service;


use app\common\exception\CommonResponseException;
use app\common\ResCode;
use app\system\model\OssConfig;
use app\system\utils\AliOss;
use app\system\utils\Aws;
use app\system\utils\QiNiu;
use app\system\model\Attachment;
use app\system\utils\Tencent;
use GuzzleHttp\Client;
use think\File;
use think\Image;

class AttachmentService
{
    /**
     * 文件上传, 支持表单提交的文件对象及根据路径上传的文件
     * @param string|File $file
     * @param string $ext
     * @param string $dir
     * @return Attachment
     * @throws \Exception
     */
    public static function upload( $file, $dir = 'images', $ext = '')
    {
        $isNetFile = false;
        if(! $file instanceof File) {
            // 本地文件或远程文件, 首先处理成File类型
            // 若没有后缀, 根据文件名获取文件后缀
            $ext = $ext ?: pathinfo($file, PATHINFO_EXTENSION);
            if(is_file($file)) {
                $file = new File($file);
            }else {
                $isNetFile = true;
                // 网络文件, 暂存到本地
                $tmpFilepath = public_path('uploads/' . DIRECTORY_SEPARATOR . 'tmp');
                if (!is_dir($tmpFilepath)) {
                    mkdir($tmpFilepath, 0777, true);
                }
                // 将文件暂存到本地, 微信图片直接使用file_get_contents会出现很久连接才关闭的问题, 所以直接使用curl
                $client = new Client();
                $response = $client->get($file);
                if($response->getStatusCode() != 200) {
                    throw new CommonResponseException(ResCode::param_invalid, '图片读取失败');
                }
                $content = $response->getBody()->getContents();
                $tmpFilename = $tmpFilepath . DIRECTORY_SEPARATOR . md5($content) . '.' .  $ext;
                file_put_contents($tmpFilename, $content);
                $file = new File($tmpFilename);
            }
        }

        // 是否开启秒传
        if (config('filesystem.flash')) {
            $attachment = Attachment::where('md5', $file->hash('md5'))->find();
            if(!empty($attachment)){
                // 如果文件存在, 则为秒传
                return $attachment;
            }
        }

        // 存储驱动
        $config = OssConfig::where('status', OssConfig::status_1)->find();
        if (empty($config)) {
            throw new CommonResponseException('文件存储配置信息不存在');
        }
        if($config->driver == 'qiniu'){ // 七牛存储
            $url = QiNiu::upload($config, $file, $dir, $ext);
        }else if($config->driver == 'aliyun') { // 阿里云oss存储
            $url = AliOss::upload($config, $file, $dir, $ext);
        }else if($config->driver == 'tencent') { // 腾讯云cos存储
            $url = Tencent::upload($config, $file, $dir, $ext);
        }else if ($config->driver == 'aws') {
            $url = Aws::upload($config, $file, $dir, $ext);
        }else { // 本地存储
            $path = public_path($config->prefix . DIRECTORY_SEPARATOR . $dir);
            $file = $file->move($path);
            if(!$file){ // 文件保存失败
                throw new CommonResponseException($file->getError());
            }
            $url = "/{$config->prefix}/$dir/" . str_replace('\\', '/', $file->getSaveName());
        }
        if($dir == 'images'){ // 获取图片宽高
            $img = Image::open($file);
            $img_width = $img->width();
            $img_height = $img->height();
        }
        if ($dir == 'videos') { // 视频文件, 获取视频缩略图
            if ($config->driver == 'qiniu') { // 七牛云
                $thumb = $url . '?vframe/jpg/offset/0'; // 截取第一帧的视频帧
            }else if($config->driver == 'oss') {
                $thumb = $url . '?x-oss-process=video/snapshot.t_1000,f_jpg,w_0,h_0,m_fast'; // 截取第一秒的视频帧
            }
        }

        // 创建附件数据
        $attachment = Attachment::create([
            'name'   => $file->getInfo('name'),
            'mime'   => $file->getInfo('type'),
            'url'   => $url,
            'ext'    => $file->getExtension(),
            'size'   => $file->getSize(),
            'md5'    => $file->hash('md5'),
            'sha1'   => $file->hash('sha1'),
            'driver' => $config->driver,
            'thumb'  => $thumb ?? '',
            'width'  => $img_width ?? 0,
            'height' => $img_height ?? 0,
        ]);
        // 如果是网络图片, 删除本地的临时图片
        if($isNetFile && isset($tmpFilename)) {
            unset($file);
            unlink($tmpFilename);
        }
        return $attachment;
    }

    /**
     * 大文件上传, 支持表单提交的文件对象及根据路径上传的文件
     * @param File $file
     * @param string $dir
     * @param string $fileName
     * @return array
     * @throws \Exception
     */
    public static function bigFileUpload($file, $fileName, $chunkIndex, $totalChunks, $dir = 'files', $md5 = '')
    {

        // 是否开启秒传
        if (config('filesystem.flash') && $md5) {
            $attachment = Attachment::where('md5', $md5)->find();
            if(!empty($attachment)){
                // 如果文件存在, 则为秒传
                return ['status' => 'completed', 'message' => '文件上传成功', 'attachment' => $attachment];
            }
        }

        // 存储驱动
        $config = OssConfig::where('status', OssConfig::status_1)->find();
        if (empty($config)) {
            throw new CommonResponseException('文件存储配置信息不存在');
        }
        if($config->driver != 'local'){
            throw new CommonResponseException('大文件存储暂不支持oss');
        }

        // 存储分片文件的临时目录
        $savePath = public_path($config->prefix . DIRECTORY_SEPARATOR . $dir);
        $tempDir = "$savePath/" . md5($fileName);
        if (!is_dir($tempDir)) {
            mkdir($tempDir, 0777, true);
        }

        // 将分片文件保存到临时目录
        $file->move($tempDir, $chunkIndex, true, false);

        // 检查是否所有分片都上传完成
        if ($chunkIndex === $totalChunks - 1) {
            // 合并文件
            $sumFile = "$savePath/$fileName";
            $out = fopen($sumFile, 'wb');

            // 计算md5值
            $hash = hash_init('md5');
            for ($i = 0; $i < $totalChunks; $i++) {
                $chunkFile = $tempDir . '/' . $i;
                $in = fopen($chunkFile, 'rb');
                while ($buffer = fread($in, 4096)) {
                    fwrite($out, $buffer);
                    // 将数据块更新到哈希上下文中
                    hash_update($hash, $buffer);
                }
                fclose($in);
                // 删除分片文件
                unlink($chunkFile);
            }
            // 完成计算并返回哈希值
            $md5 = hash_final($hash);

            fclose($out);
            // 删除临时目录
            rmdir($tempDir);

            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            $finalFileName = $md5 . '.' . $ext;
            $finalFilePath = $savePath . DIRECTORY_SEPARATOR . $finalFileName;
            // 将文件重命名为md5值的文件名
            rename($sumFile, $finalFilePath);

            // 使用stat函数而不是filesize函数获取文件大小, 防止文件过大时使用filesize会溢出
            $fileStat = stat($finalFilePath);
            // 创建附件数据
            $attachment = Attachment::create([
                'name'   => $fileName,
                'mime'   => $file->getInfo('type'),
                'url'   => "/{$config->prefix}/$dir/$finalFileName",
                'ext'    => $ext,
                'size'   => $fileStat['size'],
                'md5'    => $md5,
                'sha1'   => '',
                'driver' => $config->driver,
                'thumb'  => $thumb ?? '',
            ]);

            return ['status' => 'completed', 'message' => '文件上传成功', 'attachment' => $attachment];
        } else {
            return ['status' => 'ongoing', 'message' => '分片上传成功'];
        }


    }
}