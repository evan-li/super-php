<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">

    <title>{{$article['title']}}</title>
    <!--加载图标库-->
    <link rel="stylesheet" href="//at.alicdn.com/t/font_1132131_j4ifv6ljq6l.css">
    <style>

    </style>
</head>
<body style="padding: 10px;box-sizing: border-box;margin: auto;max-width: 960px;">
{!! $article['content'] !!}

</body>
</html>
