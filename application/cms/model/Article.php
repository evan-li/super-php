<?php


namespace app\cms\model;


use app\common\model\BaseModel;

/**
 * 文章模型
 * Class Article
 * @package app\cms\model
 *
 * @property int id
 * @property string title
 * @property string summary
 * @property string thumb
 * @property string content
 * @property int creator_id
 * @property int create_time
 * @property int update_time
 *
 * 关联属性
 * @property Tag[] tags
 */
class Article extends BaseModel
{
    protected $name = 'cms_article';

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'cms_article_tag');
    }

}