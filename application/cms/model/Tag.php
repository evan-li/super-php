<?php


namespace app\cms\model;


use app\common\model\BaseModel;

/**
 * 文章标签表
 * Class Tag
 * @package app\cms\model
 *
 * @property int id
 * @property int pid
 * @property int front_show
 * @property string name
 * @property int create_time
 * @property int update_time
 *
 * 关联属性
 * @property Article[] articles
 * @property Tag parent
 * @property Tag[] children
 */
class Tag extends BaseModel
{
    protected $name = 'cms_tag';

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'cms_article_tag');
    }

    public function parent() {
        return $this->belongsTo(Tag::class, 'pid')->selfRelation();
    }

    /**
     * 子标签
     */
    public function children()
    {
        return $this->hasMany(Tag::class, 'pid')->selfRelation();
    }


}