
CREATE TABLE IF NOT EXISTS `__PREFIX__cms_article` (
    `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '文章ID',
    `title` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '标题',
    `summary` VARCHAR(500) NOT NULL DEFAULT '' COMMENT '文章概要',
    `thumb` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '封面图',
    `content` TEXT NOT NULL COMMENT '文章内容',
    `status` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '状态 0-禁用 1-正常',
    `creator_id` INT(11) NOT NULL DEFAULT '0' COMMENT '创建人ID(后台用户ID)',
    `create_time` INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
    `update_time` INT(11) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
    PRIMARY KEY (`id`),
    INDEX `status` (`status`)
) COMMENT='文章表' COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `__PREFIX__cms_article_tag` (
   `article_id` INT(11) NOT NULL COMMENT '文章ID',
   `tag_id` INT(11) NOT NULL COMMENT '标签ID',
   PRIMARY KEY (`article_id`, `tag_id`)
) COMMENT='文章标签表' COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `__PREFIX__cms_tag` (
   `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '标签ID',
   `pid` INT(11) NOT NULL DEFAULT '0' COMMENT '上级标签ID',
   `front_show` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '前端是否展示, 用于标识一些专门用于后台区分文章类型的标签',
   `name` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '标签名',
   `status` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '状态 0-禁用 1-正常',
   `create_time` INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
   `update_time` INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
   PRIMARY KEY (`id`),
   INDEX `pid` (`pid`),
   INDEX `front_show` (`front_show`),
   INDEX `status` (`status`)
) COMMENT='文章标签表' COLLATE='utf8mb4_unicode_ci' ENGINE=InnoDB;

