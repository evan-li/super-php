<?php
/**
 * Create By Evan.
 * Date: 2019/8/7
 * Time: 15:24
 */

namespace app\cms\controller;


use evan\sbuilder\controller\Base;

class Article extends Base {

    public function detail() {
        $id = $this->request->param('id');
        if(empty($id)){
            $this->error('主键不能为空');
        }
        $article = \app\cms\model\Article::find($id);
        if(empty($article)){
            $this->error('文章信息不存在');
        }
        $this->assign('article', $article);
        return view('article/detail-h5');
    }
}