<?php


namespace app\cms\admin;


use app\cms\model\Tag as TagModel;
use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;
use think\exception\DbException;

class Tag extends Admin
{
    /**
     * 标签管理
     */
    public function index()
    {
        $fields = [
            ['pid', '', 'hidden'],
            ['name:require', '标签名'],
            ['front_show', '前端是否展示', 'switch', '用于标识一些专门用于后台区分文章类型的标签', 1],
        ];
        $validate = ['name' => 'require'];
        return SBuilder::make('table')
            ->setPageTitle('标签管理')
            ->autoAdd($fields, $validate, true)
            ->addTopButton('enable')
            ->addTopButton('disable')
            ->addTopButton('delete')
            ->addTopSelect('front_show', '前端是否展示', ['不展示', '展示'])
            ->setSearch(['name' => '标签名', 'id' => 'ID'])
            ->addColumns([
                ['__checkbox__'],
                ['id', 'ID'],
                ['name', '标签名'],
                ['parent.name', '上级标签', ''],
                ['front_show', '前端是否展示', 'yesno'],
                ['status', '状态', 'status'],
                ['create_time', '创建时间', 'datetime'],
                ['__btn__']
            ])
            ->autoEdit($fields, $validate, true)
            ->addRightButton('addSub', '添加子标签', url('add', ['pid' => '__id__']), 'pop', [
                'icon' => 'el-icon-plus'
            ])
            ->addRightButton('enable')
            ->addRightButton('disable')
            ->addRightButton('delete')
            ->fetch();
    }

    /**
     * @throws DbException
     */
    public function getList()
    {
        $data = TagModel::where($this->getWhere())
            ->order($this->getOrder())
            ->with('parent')
            ->paginate();
        return admin_data($data);
    }

    public function add() {
        $pid = $this->request->param('pid', 0);
        if($pid) {
            $parent = TagModel::get($pid);
            if(empty($parent)) {
                return admin_error('父级标签不存在');
            }
        }
        if(is_page()){
            return SBuilder::make('form')
                ->setPageTitle('添加标签')
                ->addFormItems([
                    ['parent_name', '上级标签', 'static', '', isset($parent) ? $parent->name : '顶级标签'],
                    ['name:require', '标签名'],
                    ['front_show', '前端是否展示', 'switch', '用于标识一些专门用于后台区分文章类型的标签', 1],
                ])
                ->fetch();
        }else {
            $res = $this->validate($this->request->param(), ['name' => 'require']);
            if($res !== true){
                $this->error($res);
            }
            $name = $this->request->param('name');
            $front_show = $this->request->param('front_show', 1);
            TagModel::create([
                'pid' => $pid,
                'name' => $name,
                'front_show' => $front_show,
            ]);
            return admin_success('添加标签成功', '__relist__');
        }
    }

    public function edit() {

        $id = $this->request->param('id');
        empty($id) && $this->error('缺少主键');
        // 获取模型
        $obj = TagModel::get($id);
        if(empty($obj)){
            return admin_error('数据不存在，主键ID：' . $id);
        }
        if(is_page()) {
            $parent = $obj->parent;
            return SBuilder::make('form')
                ->setPageTitle('编辑')
                ->addFormItems([
                    ['parent_name', '上级标签', 'static', '', !empty($parent) ? $parent->name : '顶级标签'],
                    ['name:require', '标签名'],
                    ['front_show', '前端是否展示', 'switch', '用于标识一些专门用于后台区分文章类型的标签', 1],
                ])
                ->setFormData($obj)
                ->setLabelWith(150)
                ->fetch();
        }else {
            $res = $this->validate($this->request->param(), ['name' => 'require']);
            if($res !== true){
                $this->error($res);
            }
            $name = $this->request->param('name');
            $front_show = $this->request->param('front_show', 1);
            $obj->save([
                'name' => $name,
                'front_show' => $front_show,
            ]);
            return admin_success('添加标签成功', '__relist__');
        }
    }

    /**
     * 自定义删除操作
     * @throws
     */
    public function delete()
    {
        $id = $this->request->param('id');
        empty($id) && $this->error('缺少主键');
        $tag = TagModel::get($id);
        if(empty($tag)){
            return admin_error('标签不存在');
        }

        if(!empty($tag->children()->find())){
            return admin_error('该标签先存在子标签, 不能删除');
        }

        $article = $tag->articles()->find();
        if(!empty($article)){
            return admin_error('该标签下存在文章, 请勿删除');
        }
        $tag->delete();
        return admin_success('删除成功', '__relist__');
    }


}