<?php


namespace app\cms\admin;


use app\cms\model\Article as ArticleModel;
use app\cms\model\Tag as TagModel;
use app\system\model\User;
use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;

class Article extends Admin
{

    public function index()
    {
        return SBuilder::make('table')
            ->setPageTitle('文章管理')
            ->addTopButton('add')
            ->addTopButton('disable')
            ->addTopButton('enable')
            ->addColumns([
                ['__checkbox__'],
                ['id', 'ID'],
                ['title', '标题'],
                ['summary', '摘要'],
                ['thumb', '封面图', 'image'],
                ['tags', '标签', 'tags'],
                ['creator', '创建人'],
                ['status', '状态', 'status'],
                ['create_time', '添加时间', 'datetime'],
                ['update_time', '最后更新时间', 'datetime'],
                ['__btn__'],
            ])
            ->addRightButton('edit')
            ->addRightButton('disable')
            ->addRightButton('enable')
            ->addRightButton('delete')
            ->fetch();
    }

    public function getList()
    {
        $data = ArticleModel::where($this->getWhere())
            ->order($this->getOrder())
            ->with('tags')
            ->paginate();
        foreach ($data as $item) {
            $username = User::where('id', $item->creator_id)->field('username')->value('username', '');
            $item->creator = "[$item->creator_id] $username";
        }
        return admin_data($data);
    }

    public function add()
    {
        if(is_page()){
            $tagNames = TagModel::where('status', 1)->column('name');
            $tags = [];
            foreach ($tagNames as $tag) {
                $tags[$tag] = $tag;
            }
            $fields = [
                ['title', '文章标题'],
                ['summary', '文章摘要'],
                ['thumb', '封面图', 'image'],
                ['tags', '标签', 'select', $tags, '', null, ['allow_create' => true, 'multiple' => true]],
                ['content', '内容', 'editor'],
            ];
            return SBuilder::make('form')
                ->setPageTitle('新增文章')
                ->addFormItems($fields)
                ->fetch();
        }else {
            $res = $this->validate($this->request->param(), [
                'title' => 'require',
            ]);
            if($res !== true){
                $this->error($res);
            }
            $title = $this->request->param('title');
            $summary = $this->request->param('summary');
            $thumb = $this->request->param('thumb');
            $content = $this->request->param('content');
            $tagNames = $this->request->param('tags', []);
            if(is_string($tagNames)){
                $tagNames = explode(',', $tagNames);
            }
            $article = new ArticleModel();
            $article->title = $title;
            $article->summary = $summary;
            $article->thumb = $thumb;
            $article->content = $content;
            $article->creator_id = $this->currentUser->id;
            $article->save();
            // 保存文章标签关联
            foreach ($tagNames as $tagName) {
                $tag = TagModel::where('name', $tagName)->find();
                if(empty($tag)){
                    // 如果标签不存在, 则新增标签并保存关联关系
                    $article->tags()->save(['name' => $tagName]);
                }else {
                    // 如果标签存在, 保存关联关系
                    $article->tags()->save($tag);
                }
            }
            return admin_success('添加文章成功');
        }
    }

    public function edit()
    {
        $id = $this->request->param('id');
        empty($id) && $this->error('缺少主键');
        /** @var ArticleModel $article */
        $article = ArticleModel::get($id);
        if(empty($article)) {
            return admin_error('文章信息不存在');
        }

        if(is_page()){
            $allTagNames = TagModel::where('status', 1)->column('name');
            $allTags = [];
            foreach ($allTagNames as $tag) {
                $allTags[$tag] = $tag;
            }
            $tags = [];
            foreach ($article->tags as $tag) {
                $tags[] = $tag['name'];
            }
            $article->tag_names = $tags;
            $fields = [
                ['title', '文章标题'],
                ['summary', '文章摘要'],
                ['thumb', '封面图', 'image'],
                ['tag_names', '标签', 'select', $allTags, '', null, ['allow_create' => true, 'multiple' => true]],
                ['content', '内容', 'editor'],
            ];
            return SBuilder::make('form')
                ->setPageTitle('修改文章')
                ->addFormItems($fields)
                ->setFormData($article)
                ->fetch();
        }else {
            $this->validate($this->request->param(), [
                'title' => 'require',
            ]);
            $title = $this->request->param('title');
            $summary = $this->request->param('summary');
            $thumb = $this->request->param('thumb');
            $content = $this->request->param('content');
            $tagNames = $this->request->param('tag_names', []);
            if(is_string($tagNames)){
                $tagNames = explode(',', $tagNames);
            }
            $article->title = $title;
            $article->summary = $summary;
            $article->thumb = $thumb;
            $article->content = $content;
            $article->creator_id = $this->currentUser->id;
            $article->save();
            // 删除所有文章标签的关联关系
            foreach ($article->tags as $tag) {
                $article->tags()->detach($tag);
            }
            // 保存文章标签关联
            foreach ($tagNames as $tagName) {
                $tag = TagModel::where('name', $tagName)->find();
                if(empty($tag)){
                    // 如果标签不存在, 则新增标签并保存关联关系
                    $article->tags()->save(['name' => $tagName]);
                }else {
                    // 如果标签存在, 保存关联关系
                    $article->tags()->save($tag);
                }
            }
            return admin_success('修改文章成功');
        }
    }

}