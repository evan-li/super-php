<?php

namespace app\cms;

use app\system\service\MenuService;

class Cms
{
    public function install()
    {
        MenuService::create([
            ['module' => 'cms', 'name' => 'CMS系统', 'url' => "cms", 'icon' => 'el-icon-Document']
        ]);
        // 添加菜单
        MenuService::createCurdMenu('cms', 'article', '文章管理', 'cms', 'el-icon-Document');
        MenuService::createCurdMenu('cms', 'tag', '标签管理', 'cms', 'el-icon-CollectionTag');
    }

    /**
     * @throws \Exception
     */
    public function uninstall()
    {
        // 删除菜单
        MenuService::deleteByModule('cms');
    }

    public function enable()
    {
        // 启用菜单
        $list = MenuService::getListByModule('store');
        foreach ($list as $item) {
            MenuService::enable($item['id']);
        }
    }

    public function disable()
    {
        // 禁用菜单
        $list = MenuService::getListByModule('store');
        foreach ($list as $item) {
            MenuService::disable($item['id']);
        }
    }

}