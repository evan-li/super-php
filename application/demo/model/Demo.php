<?php

namespace app\demo\model;

use app\common\model\BaseModel;

class Demo extends BaseModel
{

    const types = ['1' => '订单', '2' => '商品', '3' => '百鸟朝凤'];

    public function getMpContentAttr($value)
    {
        return json_str_to_arr($value);
    }

    public function setMpContentAttr($value)
    {
        return arr_to_json_str($value);
    }

    public function getDaterangeAttr($value)
    {
        return json_str_to_arr($value);
    }
    public function setDaterangeAttr($value)
    {
        return arr_to_json_str($value);
    }
    public function getDatetimerangeAttr($value)
    {
        return json_str_to_arr($value);
    }
    public function setDatetimerangeAttr($value)
    {
        return arr_to_json_str($value);
    }
    public function getArrayAttr($value)
    {
        return json_str_to_arr($value);
    }
    public function setArrayAttr($value)
    {
        return arr_to_json_str($value);
    }
}