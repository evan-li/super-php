<?php

namespace app\demo;

use app\system\service\MenuService;

class Demo
{

    public function install()
    {
        MenuService::create([
            ['module' => 'demo', 'name' => '示例模块', 'url' => "demo", 'children' => [
                ['module' => 'demo', 'name' => '列表示例', 'url' => "/demo/demo/index"],
                ['module' => 'demo', 'name' => '详情示例', 'url' => "/demo/demo/detail"],
                ['module' => 'demo', 'name' => '多区块示例', 'url' => "/demo/demo/multiBlock"],
                ['module' => 'demo', 'name' => 'Html', 'url' => "/demo/demo/html"],
            ]]
        ]);
    }

    /**
     * @throws \Exception
     */
    public function uninstall()
    {
        MenuService::deleteByModule('demo');
    }

}