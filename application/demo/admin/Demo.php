<?php

namespace app\demo\admin;

use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;
use think\Exception;

class Demo extends Admin
{

    public function index()
    {
        return SBuilder::makeTable()
            ->addColumns([
                ['id', 'ID'],
                ['byte', '字节'],
                ['name', '名称'],
                ['name', '名称', 'text.edit'],
                ['avatar', '头像', 'image'],
                ['pics', '多图', 'images'],
                ['html', 'Html内容', 'html'],
                ['link', '链接', 'link', 'blank:https://baidu.com', '', ['type' => 'warning']],
                ['type', '类型', 'tags', \app\demo\model\Demo::types],
                ['type', '类型', 'select', \app\demo\model\Demo::types],
                ['status', '状态', 'status', [1 => '启用', 2 => '禁用']],
                ['__btn__']
            ])
            ->addTopButton('add')
            ->addRightButton('edit')
            ->addRightButton('enable')
            ->addRightButton('disable')
            ->fetch();
    }

    public function getList()
    {
        $data = \app\demo\model\Demo::where($this->getWhere())->order($this->getOrder())->paginate(input('page_size'));
        foreach ($data as $item) {
            $item->html = '<div class="c-success-dark-2">这里是 <span class="c-success">html</span> <span class="c-success-light-5">内容</span><br><span class="f-s-12 c-danger" style="zoom: 80%">可以随意自定义格式</span></div>';
        }
        return admin_data($data);
    }

    public function add()
    {
        if (is_page()) {
            return SBuilder::makeForm()
                ->setPageTitle('示例表单')
                ->addFormItems([
                    ['type:require', '类型', 'select', \app\demo\model\Demo::types, 'Select组件', '', ['prepend']],
                    ['name:require', '名称', '', 'Text组件'],
                    ['name', '名称', '', 'Text组件, 非必填, 可添加 <span class="c-warning">prepend append prefix suffix</span><br> <em>tips支持html</em>', '', ['prepend' => 'Prepend', 'append' => 'Append', 'prefix' => 'i.el-icon-Calendar', 'suffix' => 'i.el-icon-Search']],
                    ['byte', '字节大小', 'number', '请输入字节数,列表中会自动格式化'],
                    ['status', '状态', 'status', [], 'status组件, 前端渲染为radio组件'],
                    ['status', '状态', 'radio', [0 => '未注册', 1 => '启用', 2 => '禁用', 3 => '封号100年'], 'radio组件, 自定义option列表', 1],
                    ['type', '类型', 'select', \app\demo\model\Demo::types],
                    ['avatar', '头像', 'image', 'image组件'],
                    ['avatar', '头像', 'image', 'image组件, 不预览 list_type=text', '', ['list_type' => 'text']],
                    ['avatar', '头像', 'image', 'image组件, 拖拽上传 drag=true', '', ['drag' => true]],
                    ['pics', '多图', 'images', 'images组件, 前端渲染为image组件, 默认最多99张', ''],
                    ['pics', '多图', 'images', 'images组件, 前端渲染为image组件, 默认最多99张, 可拖拽排序', '', ['draggable' => true]],
                    ['pics', '多图', 'images', '大图上传', '', ['big' => true]],
                    ['video', '视频', 'video', '视频上传组件', ''],
                    ['link', '链接内容', ''],
                    ['color', '颜色选择器', 'color'],
                    ['icon', '颜色选择器', 'icon'],
                    ['date', '日期', 'date'],
                    ['daterange', '日期范围', 'daterange'],
                    ['datetime', '日期时间', 'datetime'],
                    ['datetimerange', '日期时间', 'datetimerange'],
                    ['time', '时间选择', 'time'],
                    ['time_select', '时间下拉框', 'time', '', '', ['start' => '12:00', 'end' => '23:30']],
                    ['array', '数组', 'array'],
                    ['table', '表格数据', 'table', [
                        ['name', '姓名', ''],
                        ['age', '年龄', 'number'],
                        ['gender', '性别', 'select', [1 => '男', 2 => '女']],
                        ['avatar', '头像', 'image'],
                    ]],
                    ['content', '富文本(QEditor)', 'editor'],
                    ['content', '富文本(UEditor)', 'ueditor'],
                    ['mp_content', '小程序图文', 'mp_editor'],
                ])
                ->fetch();
        }else {
            $this->checkParam([
                'name' => 'require',
                'type' => 'require'
            ]);
            $param = input();
            $demo = \app\demo\model\Demo::create($param);
            return admin_success('创建成功', '__relist__');
        }
    }
    public function edit()
    {
        $id = $this->request->param('id');
        if (empty($id)) return admin_error('缺少主键');
        $obj = \app\demo\model\Demo::get($id);
        if(empty($obj)){
            return admin_error('数据不存在，主键ID：' . $id);
        }
        if (is_page()) {
            return SBuilder::makeForm()
                ->setPageTitle('示例表单')
                ->addFormItems([
                    ['type:require', '类型', 'select', \app\demo\model\Demo::types, 'Select组件', '', ['prepend']],
                    ['name:require', '名称', '', 'Text组件'],
                    ['name', '名称', '', 'Text组件, 非必填, 可添加 <span class="c-warning">prepend append prefix suffix</span><br> <em>tips支持html</em>', '', ['prepend' => 'Prepend', 'append' => 'Append', 'prefix' => 'i.el-icon-Calendar', 'suffix' => 'i.el-icon-Search']],
                    ['byte', '字节大小', 'number', '请输入字节数,列表中会自动格式化'],
                    ['status', '状态', 'status', [], 'status组件, 前端渲染为radio组件'],
                    ['status', '状态', 'radio', [0 => '未注册', 1 => '启用', 2 => '禁用', 3 => '封号100年'], 'radio组件, 自定义option列表', 1],
                    ['type', '类型', 'select', \app\demo\model\Demo::types],
                    ['avatar', '头像', 'image', 'image组件'],
                    ['avatar', '头像', 'image', 'image组件, 不预览 list_type=text', '', ['list_type' => 'text']],
                    ['avatar', '头像', 'image', 'image组件, 拖拽上传 drag=true', '', ['drag' => true]],
                    ['pics', '多图', 'images', 'images组件, 前端渲染为image组件, 默认最多99张', ''],
                    ['pics', '多图', 'images', 'images组件, 前端渲染为image组件, 默认最多99张, 可拖拽排序', '', ['draggable' => true]],
                    ['pics', '多图', 'images', '大图上传', '', ['big' => true]],
                    ['video', '视频', 'video', '视频上传组件', ''],
                    ['link', '链接内容', ''],
                    ['color', '颜色选择器', 'color'],
                    ['icon', '颜色选择器', 'icon'],
                    ['date', '日期', 'date'],
                    ['daterange', '日期范围', 'daterange'],
                    ['datetime', '日期时间', 'datetime'],
                    ['datetimerange', '日期时间', 'datetimerange'],
                    ['time', '时间选择', 'time'],
                    ['time_select', '时间下拉框', 'time', '', '', ['start' => '12:00', 'end' => '23:30']],
                    ['array', '数组', 'array'],
                    ['table', '表格数据', 'table'],
                    ['tree', '树形选择器', 'tree'],
                    ['content', '富文本(QEditor)', 'editor'],
                    ['content', '富文本(UEditor)', 'ueditor'],
                    ['mp_content', '小程序图文', 'mp_editor'],
                ])
                ->setFormData($obj)
                ->fetch();
        }else {
            $this->checkParam([
                'name' => 'require',
                'type' => 'require'
            ]);
            $param = input();
            $obj->save($param);
            return admin_success('修改成功', '__relist__');
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function detail()
    {
        if(is_page()){
            return SBuilder::make('form')
                ->setPageTitle('添加页面')
                ->setPageTips('批量添加1')
                ->setPageTips('批量添加2', 'success')
                ->setNavTab([
                    ['test', '测试tab', '/demo/demo/detail'],
                    ['test1', '测试追加tab', '/system/demo/detail1'],
                    ['test2', '测试批量添加tab', '/system/demo/detail2'],
                ])
                ->setNavTabCurrent('test')
                ->setBtnTitle('submit', '修改提交按钮的文字')
                ->setBtnTitle('cancel', '修改取消按钮的文字')
//                ->hideBtn('cancel')
                ->setBtnExtra('额外按钮，跳转链接', 'http://baidu.com')
                ->setBtnExtra([
                    ['额外按钮，跳转链接', 'http://baidu.com'],
                    ['额外按钮, 打开弹框', '/system/demo/detail', 'pop', ['confirm' => true]],
                ])
                ->setBtnExtra('删除操作', url('delete'), 'ajax', [
                    'confirm' => [
                        'title' => '确认要删除吗？',
                        'tips' => '删除后将不可恢复！',
                        'type' => 'error',
                    ]
                ])
                ->addText('pay_config', '支付配置')
                // 分组
                ->addGroup('pay_config_group', [
                    '支付宝支付' => [
                        ['alipay_id', '支付宝商户ID', 'text', '', '', ['prefix' => 'i.el-icon-setting', 'suffix' => '后缀'] ],
                        ['alipay_key', '支付宝密钥', 'text', '', '', ['prepend' => '前追加', 'append' => '后追加'] ],
                        ['test_group_status', '分组中的下拉框', 'select', ['禁用', '启用'], '', 1],
                        ['test_group_status2', '数字索引', 'select', [1 => '禁用', 2=> '启用'], '', 1],
                        ['test_group_status3', '字符串格式的数字索引', 'select', ['1' => '禁用', '2'=> '启用'], '', 1],
                        ['test_group_status4', '字符串索引', 'select', ['b' => '禁用', 'a'=> '启用'], '', 'a'],
                    ],
                    '微信支付' => [
                        ['wx_mch_id', '微信商户ID', 'text', '', '', ['prefix' => 'i.el-icon-setting', 'suffix' => '后缀'] ],
                        ['wx_mch_key', '微信支付密钥', 'text', '', '', ['prepend' => '前追加', 'append' => '后追加'] ],
                        ['status', '单选', 'radio', ['启用', '停用']],
                        ['check', '多选框', 'checkbox', ['gz' => '广州', 'wh' => '武汉'], '多选框提示文字']
                    ]
                ])
                ->setTrigger('pay_config_group', 'pay_config', '1', false)
                ->addFile('file', '单文件上传', '点击上传文件')
                ->addFiles('files', '多文件上传', '点击上传文件')
                ->addImages('pics', '图片列表')
                ->addImage('avatar', '头像', '支持jpg、jpeg、png类型，尺寸为120*120px，大小不超过3M', '', [
                    'width' => 120,
                    'height' => 120,
                    'size' => 3*1024*1024,
                    'format' => ['image/png', 'image/jpeg']
                ])
                ->addBigfile('bigfile', '大文件单文件上传', '')
                ->addBigfiles('bigfiles', '大文件多文件上传', '')
                ->addSelect('city', '城市', ['wh' => '武汉', 'sz' => '深圳'], '', 'sz', ['disabled' => 'wh'])
                ->addSelect('city', '城市', ['wh' => '武汉', 'sz' => '深圳'], '', 'sz')
                ->addSelect('multi_city', '城市', ['wh' => '武汉', 'sz' => '深圳', 'gz' => '广州'], '', '', ['multiple' => true])

                ->addText('name', '名称', '', '', ['prefix' => 'i.el-icon-date'])
                ->addText('name', '名称|名称占位符', '请输入名称', '', ['prefix' => 'i.el-icon-setting', 'suffix' => '后缀'] )
                ->addText('intro', '描述|占位符', '请输入描述', '', ['prepend' => '前追加', 'append' => '后追加'])

                ->addNumber('sort', '排序', '排序由大到小，1-1000之间，步长为5', null, [
                    'min' => 1,
                    'max' => 1000,
                    'step' => 5,
                    'precision' => 2,
                ])
                ->addPassword('password', '密码')
                ->addTextarea('textarea', '文本域', '文本域提示文字', '', ['min_rows' => 6, 'max_rows' => 12])

                ->addSwitch('is_menu', '是否菜单', '', 1, [
                    'active_text' => '是',
                    'inactive_text' => '否',
                    'active_color' => '#409EFF',
                    'inactive_color' => 'red',
                ])
                ->addRadio('status', '状态', ['禁用', '启用', '此选项不可选', '又一个不可选的选项'], '', 1, ['disabled' => '2,3'])
                ->addCheckbox('check', '多选框', ['gz' => '广州', 'wh' => '武汉'], '多选框提示文字')
                ->addCheckbox('check', '多选框', ['gz' => '广州', 'wh' => '武汉'], '多选框提示文字', [], ['button' => true])
                ->addCheckbox('check', '多选框', ['gz' => '广州', 'wh' => '武汉'], '多选框提示文字', [], ['border' => true])
                ->addCheckbox('check', '多选框', ['gz' => '广州', 'wh' => '武汉'], '多选框提示文字', [], ['disabled' => 'wh'])
//                ->addDate('date', '日期')
//                ->addDate('date', '日期', '', '', ['min' => '2019-05-01', 'max' => '2019-06-03'])
//                ->addDatetime('datetime', '日期时间', '', '', ['min' => '2019-05-01', 'max' => '2019-06-03'])
                ->addDaterange('daterange', '日期范围', '', [], ['min' => '2019-05-01', 'max' => '2019-06-03'])
                ->addDatetimerange('datetimerange', '日期时间范围', '', [], ['min' => '2019-05-01 00:00:00', 'max' => '2019-06-03 23:59:59'])
                ->addTime('time', '时间', '', '', [
                    'selectable_range' => '18:30:00 - 20:30:00',
                ])
                ->addTime('time_select', '时间select', '', '', [
                    'start' => '06:00',
                    'end' => '18:00',
                    'step' => '00:10',
                    'min' => '08:00',
                    'max' => '18:00',
                ])
                ->addTimeRange('timerange', '时间范围', '', null, ['selectable_range' => ['08:30:00 - 18:30:00']])

                ->addIcon('icon', '菜单图标')
                ->addColor('color', '颜色')

                ->addEditor('content', '富文本(QEditor)')
                ->addUEditor('content', '富文本(UEditor)')
                ->addMpEditor('mp_content', '小程序富文本', '', '', ['image_drag' => true])
                ->addStatic('status', '静态文本', [1 => '成功', 2 => '失败'], '', '1', 'c-primary')

//                ->setLabelWith('150')
                ->setFormData([
//                    'name' => '123456',
                    'desc' => 'testdesc',
                    'intro' => 'testintro',
//                    'avatar' => '1,3,5',
                ])
                ->setBtnTitle([
                    'submit' => '修改确定按钮的文字',
                    'cancel' => '修改取消按钮的文字',
                ])
                ->submitConfirm('提交', '您的提交将保存')
                ->fetch();
        }else {
            $name = $this->request->param('name');
            if (empty($name)){
                return res_error('名称不能为空');
            }
            return admin_data();
        }
    }

    public function multiBlock()
    {
        return SBuilder::makeTable()
            ->setPageTitle('多区块测试')
            ->setNavTab([
                ['test', '测试tab', '/system/demo/index'],
                ['test1', '测试追加tab', '/system/demo/index1'],
                ['test2', '测试批量添加tab', '/system/demo/index2'],
                ['multi', '多区块', '/system/demo/multiBlock'],
            ])
            ->setNavTabCurrent('multi')
            ->addColumn('id', 'ID')
            ->addColumn('name', '姓名')
            ->addBlock('form', SBuilder::makeForm()
                ->setPageTitle('dsafds')
                ->addText('name', '姓名')
                , [
                    'span' => 6
                ])
            ->addBlock('form2', SBuilder::makeForm()
                ->setPageTitle('dsafds')
                ->addText('name', '姓名')
                , [
                    'span' => 6
                ])
            ->addBlock('charts', SBuilder::makeCharts([
                'width' => '500px',
                'height' => '300px'
            ])
                ->setTitle('图表示例')
                ->setData([
                    ['id' => 1, 'name' => '衬衫', 'count' => '35', 'value' => 5],
                    ['id' => 2, 'name' => '羊毛衫', 'count' => '567', 'value' => 20],
                    ['id' => 3, 'name' => '雪纺衫', 'count' => '34', 'value' => 36],
                    ['id' => 4, 'name' => '裤子', 'count' => '658', 'value' => 10],
                ], 'name')
                ->addBar('value')
                ->addBar('count')
                ->addLine('count',  '',true)
                ->addLine('value', '', true)
            )
            /**
             * json_decode('[
            {id: 1, name: \'衬衫\', type: \'销量\', value: 5},
            {id: 2, name: \'羊毛衫\', type: \'销量\', value: 20},
            {id: 3, name: \'雪纺衫\', type: \'销量\', value: 36},
            {id: 4, name: \'裤子\', type: \'销量\', value: 10},
            ]')
             */
            ->fetch();
    }

    public function html()
    {
        return SBuilder::makeHtml()
            ->setPageTitle('html区块示例')
            ->setContent(<<<html
<div>
<div class="c-success">这里是html区块</div>
<div class="c-tips">注意: html区块中的html内容不能使用vue组件</div>
</div>
html
            )->fetch();
    }

    public function iframe()
    {
        return SBuilder::makeIframe(['width' => '100%', 'height' => '600px'])
            ->setPageTitle('iframe区块示例')
            ->setUrl('/iframe-demo.html')
            ->fetch();
    }

    public function dataPanel()
    {
        return SBuilder::makeDataPanel()
            ->setPageTitle('数据面板')
            ->setData([
                ['title' => '会员注册量', 'icon' => 'fa fa-line-chart', 'icon_color' => '#8595F4', 'value' => '5468', 'second_value' => '+58%'],
                ['title' => '附件上传量', 'icon' => 'fa fa-file-text', 'icon_color' => '#AD85F4', 'value' => '1234', 'second_value' => '+14%'],
                ['title' => '会员总数', 'icon' => 'fa fa-users', 'icon_color' => '#74A8B5', 'value' => '9486', 'second_value' => '+28%'],
                ['title' => '已装插件数', 'icon' => 'fa fa-object-group', 'icon_color' => '#F48595', 'value' => '875', 'second_value' => '+88%'],
                ['title' => '只定义value', 'value' => '875'],
                ['title' => '不定义icon', 'value' => '875', 'second_value' => '+54%'],
                ['title' => '不定义icon', 'value' => '875', 'second_value' => '+54%'],
                ['title' => '不定义icon', 'value' => '875', 'second_value' => '+54%'],
            ])
            ->fetch();
    }

    public function cardList()
    {
        return SBuilder::makeCardList()
            ->setPageTitle('数据面板')
            ->setPageTips('请选择您想要的商品', 'success')
            ->setData([
                ['name' => '会员注册', 'count' => '5468', 'price' => '500', 'crossed_price' => '1200',
                    'show_price' => true, 'price_prefix' => 'S$', 'price_unit' => '/人', 'tags' => [
                    ['title' => '海量注册用户', 'type' => 'success'],
                    ['title' => '价格贼贵', 'type' => 'danger'],
                ]],
                ['name' => '会员注册', 'count' => '5468', 'price' => '500', 'crossed_price' => '1200',
                    'show_price' => true, 'price_prefix' => 'S$', 'price_unit' => '/人', 'tags' => [
                    ['title' => '海量注册用户', 'type' => 'success'],
                    ['title' => '价格贼贵', 'type' => 'danger'],
                ]],
                ['name' => '会员注册', 'count' => '5468', 'price' => '500', 'crossed_price' => '1200',
                    'show_price' => true, 'price_prefix' => 'S$', 'price_unit' => '/人', 'tags' => [
                    ['title' => '海量注册用户', 'type' => 'success'],
                    ['title' => '价格贼贵', 'type' => 'danger'],
                ]],
                ['name' => '会员注册', 'count' => '5468', 'price' => '500', 'crossed_price' => '1200',
                    'show_price' => true, 'price_prefix' => 'S$', 'price_unit' => '/人', 'tags' => [
                    ['title' => '海量注册用户', 'type' => 'success'],
                    ['title' => '价格贼贵', 'type' => 'danger'],
                ]],
            ])
            ->fetch();
    }
}