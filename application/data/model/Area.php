<?php


namespace app\data\model;


use app\common\model\BaseModel;
use think\facade\Cache;

/**
 * 地区库
 * Class Area
 * @package app\system\model
 *
 * @property int id
 * @property string name
 * @property int type
 * @property int tier
 * @property int area_code
 * @property string spell
 * @property string letter
 * @property string first_letter
 * @property int status
 * @property int pid
 */
class Area extends BaseModel
{
    protected $name = 'data_area';

    public static function init()
    {
        self::event('after_write', function () {
            // 清空缓存
            Cache::rm('system_area_tree:1');
            Cache::rm('system_area_tree:2');
            Cache::rm('system_area_tree:3');
        });
    }

    /**
     * 获取地区树
     */
    public static function getTree($tier = 3)
    {
        $tree = Cache::get('system_area_tree:' . $tier);
        if(empty($tree)){
            $list = self::where('tier', '<=', $tier)->select();
            $tree = convert_list_to_tree($list);
            // 缓存7天
            Cache::set('system_area_tree:' . $tier, $tree, 3600 * 24 * 7);
        }
        return $tree;
    }

    /**
     * 根据地区ID获取省市区
     * @param $areaId
     * @return Area[]
     */
    public static function getTiersByAreaId($areaId)
    {
        $area = Area::get($areaId);
        $tiers = [];
        if ($area->tier > 1) {
            $tiers = self::getTiersByAreaId($area->pid);
        }
        array_push($tiers, $area);
        return $tiers;
    }

}