<?php

namespace app\data;

use app\system\service\MenuService;

class Data
{
    public function install()
    {
        // 添加菜单
        MenuService::createCurdMenu([
            ['data', 'area', '地区管理', 'system'],
        ]);
    }

    /**
     * @throws \Exception
     */
    public function uninstall()
    {
        // 删除菜单
        MenuService::deleteByModule('data');
    }

    /**
     *
     */
    public function enable()
    {
        // 启用菜单
        $list = MenuService::getListByModule('data');
        foreach ($list as $item) {
            MenuService::enable($item['id']);
        }
    }

    public function disable()
    {
        // 禁用菜单
        $list = MenuService::getListByModule('data');
        foreach ($list as $item) {
            MenuService::disable($item['id']);
        }
    }

}