<?php


namespace app\data\api;


class Area
{

    public function tree()
    {
        $tree = \app\data\model\Area::getTree();
        return res_ok(['list' => $tree]);
    }
}