-- 导出  表 system_area 结构
CREATE TABLE IF NOT EXISTS `__PREFIX__data_area`(
    `id` int(11) NOT NULL  COMMENT '地区id' ,
    `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL  COMMENT '地区名称' ,
    `type` tinyint(4) NOT NULL  DEFAULT 0 COMMENT '类型，保留字段' ,
    `tier` tinyint(4) NOT NULL  DEFAULT 1 COMMENT '路径，从1开始' ,
    `area_code` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL  COMMENT '区号' ,
    `spell` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL  COMMENT '拼音' ,
    `letter` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL  COMMENT '简拼' ,
    `first_letter` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL  COMMENT '首字母' ,
    `status` tinyint(4) NOT NULL  DEFAULT 1 COMMENT '状态 1-正常 2-禁用' ,
    `pid` int(11) NOT NULL  DEFAULT 0 COMMENT '父ID，如果是省份，则父ID为0' ,
    PRIMARY KEY (`id`) ,
    KEY `areas_area_code_index`(`area_code`) ,
    KEY `areas_spell_index`(`spell`) ,
    KEY `areas_letter_index`(`letter`) ,
    KEY `areas_first_letter_index`(`first_letter`) ,
    KEY `areas_status_index`(`status`) ,
    KEY `areas_parent_id_index`(`pid`)
    ) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_unicode_ci' COMMENT='地区表';

CREATE TABLE IF NOT EXISTS `__PREFIX__data_country` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `group_id` INT(11) NOT NULL DEFAULT '0' COMMENT '分组ID',
  `code` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '二位标识码',
  `code_three` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '三维标识码',
  `code_number` INT(11) NOT NULL DEFAULT '0' COMMENT '数字标识码',
  `code_app` VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'app特殊标识码',
  `iso` VARCHAR(50) NOT NULL DEFAULT '' COMMENT 'ISO-3166-2标识码',
  `name` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '国家地区名',
  `en_name` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '国家地区英文名',
  `status` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '状态0-无效1-有效',
  `create_time` INT(11) NOT NULL DEFAULT '0',
  `update_time` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `code` (`code`) USING BTREE,
  INDEX `code_app` (`code_app`) USING BTREE,
  INDEX `status` (`status`) USING BTREE,
  INDEX `group_id` (`group_id`) USING BTREE
)
COMMENT='国家列表' COLLATE='utf8mb4_general_ci' ENGINE=InnoDB;
