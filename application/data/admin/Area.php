<?php


namespace app\data\admin;


use app\data\model\Area as AreaModel;
use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;
use think\Exception;
use think\exception\DbException;
use think\exception\PDOException;

class Area extends Admin
{

    /**
     * 地区管理页面
     * @return mixed
     * @throws
     */
    public function index()
    {
        $pid = $this->request->param('pid', 0);
        /** @var AreaModel $parent */
        $parent = AreaModel::where('id', $pid)->find();
        $builder = SBuilder::make('table')
            ->setPageTitle(empty($parent) ? '地区管理' : $parent->name);
        if(empty($parent) || $parent->tier < 3){
            $builder->addTopButton('add', '新增地区', url('add', ['pid' => $pid]), 'pop');
        }
        return $builder
            ->addColumns([
                ['id', 'ID'],
                ['name', '名称'],
                ['area_code', '区号'],
                ['spell', '拼音'],
                ['letter', '简拼'],
                ['first_letter', '首字母'],
                ['status', '状态', 'status'],
                ['__btn__'],
            ])
            ->addRightButton('edit', '', '', 'pop')
            ->addRightButton('enable')
            ->addRightButton('disable')
            ->addRightButton('show_sub', '查看下级', url('index', ['pid' => '__id__']), '', [
                'icon' => 'el-icon-s-order',
                'hide' => ['tier' => '3']
            ])
            ->setRowListUrl(url('getList', ['pid' => $pid]))
            ->addRightButton('delete')
            ->setColumnWidth(['id' => 100])
            ->fetch();
    }

    public function getList()
    {
        $pid = $this->request->param('pid', 0);
        $list = AreaModel::where('pid', $pid)->select();
        return admin_data($list);
    }

    /**
     * 添加
     * @throws Exception
     */
    public function add()
    {
        $pid = $this->request->param('pid', 0);
        $parent = AreaModel::where('id', $pid)->find();
        if(is_page()){
            return SBuilder::make('form')
                ->setPageTitle('新增地区')
                ->addFormItems([
                    ['parent', '所属地区', 'static', empty($parent) ? '中国' : $parent->name],
                    ['id', '地区ID',],
                    ['name', '名称'],
                    ['area_code', '区号'],
                    ['spell', '拼音'],
                    ['letter', '简拼'],
                    ['first_letter', '首字母'],
                    ['status', '状态', 'status'],
                ])
                ->fetch();
        }
        $result = $this->validate($this->request->param(), [
            'id' => 'require|unique:area',
            'name' => 'require',
            'spell' => 'require',
            'letter' => 'require',
            'first_letter' => 'require',
        ]);
        if($result !== true){
            return admin_error($result);
        }
        AreaModel::create([
            'id' => $this->request->param('id'),
            'pid' => $pid,
            'tier' => empty($parent) ? 1 : $parent->tier +1,
            'name' => $this->request->param('name'),
            'spell' => $this->request->param('spell'),
            'letter' => $this->request->param('letter'),
            'first_letter' => $this->request->param('first_letter'),
            'status' => $this->request->param('status', 1),
        ]);
        return admin_success('新增地区成功', '__relist__');
    }

    /**
     * 编辑
     * @throws Exception
     */
    public function edit()
    {
        $id = $this->request->param('id');
        if (empty($id)) return admin_error('缺少主键');
        $area = AreaModel::where('id', $id)->find();
        if(empty($area)){
            return admin_error('数据不存在，主键ID：' . $id);
        }

        if(is_page()){
            $parentName = AreaModel::where('id', $area->pid)->value('name', '中国');
            return SBuilder::make('form')
                ->setPageTitle('新增地区')
                ->addFormItems([
                    ['parent', '所属地区', 'static', $parentName],
                    ['id', '地区ID',],
                    ['name', '名称'],
                    ['area_code', '区号'],
                    ['spell', '拼音'],
                    ['letter', '简拼'],
                    ['first_letter', '首字母'],
                    ['status', '状态', 'status'],
                ])
                ->setFormData($area)
                ->fetch();
        }
        $result = $this->validate($this->request->param(), [
            'id' => 'require|unique:area',
            'name' => 'require',
            'spell' => 'require',
            'letter' => 'require',
            'first_letter' => 'require',
        ]);
        if($result !== true){
            return admin_error($result);
        }

        $area->data([
            'id' => $this->request->param('id'),
            'name' => $this->request->param('name'),
            'spell' => $this->request->param('spell'),
            'letter' => $this->request->param('letter'),
            'first_letter' => $this->request->param('first_letter'),
            'status' => $this->request->param('status', 1),
        ])->save();
        return admin_success('编辑地区成功', '__relist__');
    }

    /**
     * 删除地区
     * @throws Exception
     * @throws DbException
     * @throws PDOException
     */
    public function delete()
    {
        $id = $this->request->param('id');
        if (empty($id)) return admin_error('缺少主键');
        // 检查是否有下级
        if(!empty( AreaModel::where('pid', $id)->find() )) {
            return admin_error('该地区存在下级, 不可删除');
        }
        return parent::delete();
    }

    public function tree()
    {
        $tier = input('tier', 3);
        $tree = AreaModel::getTree($tier);
        return res_ok(['list' => $tree]);
    }
}