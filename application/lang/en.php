<?php

return [
    'request success' => 'request success',
    'api not found' => 'api not found',
    'api closed' => 'api has been closed',
    'un know error' => 'Unknown error',
    'file size exceed' => 'File size exceeds the limit allowed',
    'file size too large, only {:size}' => 'Maximum size should be no more than {:size}',
    'permission denied' => 'Permission denied',
    'http request fail' => 'HTTP request failed',
    'third api unload' => 'Third API not allowed request.',

    'param invalid' => 'Illegal parameter',
    'un register' => 'Not registered',
    'un login' => 'Not logged in',
    'token expire' => 'Invalid or incorrect token',
    'user not exists' => 'User does not exist',
    'user password error' => 'The password you entered is incorrect',
    'user already exists' => 'This user already exists. Please try another.',
    'device no invalid' => 'Invalid device number',

    'data not found' => 'Data does not exist',
    'data already exists' => 'Data already exists',
    'data not allow delete' => 'Unable to delete data',
    'data not allow update' => 'Unable to update data',
    'data operation fail' => 'Database data manipulation failed',

    'email send error' => 'Email delivery failed.',
    'email verify code error' => 'Incorrect email verification code',
    'email send too fast' => 'Too frequent email delivery',

    'user number error' => 'User number to push has error',
    'push error' => 'Push error',

    'app version need update' => 'Please update to the latest version',
    'app version force update' => 'Update is required before continuing',
    'app version error' => 'Wrong version number',

    'payment channel not exists' => 'No available payment channels',
    'payment api verify fail' => 'Purchase Credential API validation failed',
    'bundle ID error' => 'Inconsistent Bundle ID validation results',

];