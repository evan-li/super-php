<?php

return [
    'request success' => '请求成功',
    'api not found' => '接口不存在',
    'api closed' => '接口关闭',
    'un know error' => '未知错误',
    'file size exceed' => '文件大小超出限制',
    'file size too large, only {:size}' => '文件太大, 不能超过{:size}',
    'permission denied' => '没有权限',
    'http request fail' => 'http请求失败',
    'third api unload' => '第三方接口不允许请求',

    'param invalid' => '参数不合法',
    'un register' => '未注册',
    'un login' => '未登录',
    'token expire' => 'token失效或错误',
    'user not exists' => '用户不存在',
    'user password error' => '用户密码错误',
    'user already exists' => '用户已存在',
    'device no invalid' => '设备号无效',

    'data not found' => '数据不存在',
    'data already exists' => '数据已存在',
    'data not allow delete' => '数据不允许删除',
    'data not allow update' => '数据不允许更新',
    'data operation fail' => '数据库数据操作失败',

    'email send error' => '邮件发送失败',
    'email verify code error' => '邮箱验证码错误',
    'email send too fast' => '邮件发送过于频繁',

    'user number error' => '推送用户数量错误',
    'push error' => '推送失败',

    'app version need update' => '需要更新版本(非强制)',
    'app version force update' => '需要强制更新版本',
    'app version error' => '版本号错误',

    'payment channel not exists' => '无可用支付渠道',
    'payment api verify fail' => '购买凭证验证API失败',
    'bundle ID error' => 'bundleID验证不一致',

];