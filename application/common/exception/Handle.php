<?php


namespace app\common\exception;


use app\common\ResCode;
use Exception;
use think\exception\HttpException;
use think\facade\Log;
use think\response\Redirect;

class Handle extends \think\exception\Handle
{

    /**
     * 异常处理
     * @param Exception $e
     * @return \think\Response
     */
    public function render(Exception $e)
    {
        if(SITE_NAME == 'admin') { // admin模块异常处理
            return $this->adminRender($e);
        }else if(SITE_NAME == 'api') { // api接口渲染
            return $this->renderApi($e);
        }else {
            return parent::render($e);
        }
    }

    protected function adminRender(Exception $e)
    {
        if($e instanceof CommonResponseException){ // 如果是通用的响应异常, 直接走框架的异常渲染
            Log::warning('返回客户端错误信息: ' . $e->getMessage() . "\r\n" . $e->getTraceAsString());
            return $e->getResponse();
        }
        // 如果是ajax请求
        if(request()->isAjax()){
            if($e instanceof HttpException && $e->getStatusCode() == 404){
                // 返回接口不存在
                return res_error(404, '接口不存在');
            }else {
                // 如果不是http异常, 返回错误信息并记录日志
                Log::error('系统发生错误, 异常信息:' . $e->getMessage() . "\r\n" . $e->getTraceAsString());
                return res_error(500, $e->getMessage());
            }
        }else {
            // 404时, 重定向到根页面, 由前端vue去处理
            if($e instanceof HttpException && $e->getStatusCode() == 404){
                $url = request()->url();
                $response = new Redirect('/index.html?__to=' . urlencode($url));
                return $response;
            }else {
                // 如果不是http异常, 返回错误信息并记录日志
                return parent::render($e);
//                Log::error('系统发生错误, 异常信息:' . $e->getMessage() . "\r\n" . $e->getTraceAsString());
//                return res_error(500, $e->getMessage());
            }
        }
    }

    protected function renderApi(Exception $e)
    {
        if($e instanceof CommonResponseException){ // 如果是通用的响应异常, 直接走框架的异常渲染
            Log::warning('返回客户端错误信息: ' . json_encode($e->getData(), JSON_UNESCAPED_UNICODE) . "\r\n" . $e->getMessage() . "\r\n" . $e->getTraceAsString());
            return $e->getResponse();
        }
        if($e instanceof HttpException && $e->getStatusCode() == 404) {
            // 返回接口不存在
            return res_error(ResCode::api_not_found, '接口不存在');
        }else {
            // 如果不是http异常, 返回错误信息并记录日志
            Log::error('系统发生错误, 异常信息:' . $e->getMessage() . "\r\n" . $e->getTraceAsString());
            return res_error(ResCode::unknow_error, $e->getMessage());
        }
    }

}