<?php


namespace app\common\exception;


use app\common\ResCode;
use think\exception\HttpResponseException;

class CommonResponseException extends HttpResponseException
{

    public function __construct($code = 0, $msg = '', $data = [])
    {
        if (is_string($code)) {
            $msg = $code;
            $code = ResCode::unknow_error;
        }
        $this->message = $msg;
        $response = res_error($code, $msg, $data);
        parent::__construct($response);
    }

    public function getData()
    {
        return $this->response->getData();
    }

}