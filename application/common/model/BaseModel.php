<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 * Date: 2019/5/4
 * Time: 11:55
 */

namespace app\common\model;


use think\Collection;
use think\db\Query;
use think\Model;
use think\Paginator;

/**
 * 基础模型,主要用于注解方法,辅助编辑器识别相关返回值等
 * Class BaseModel
 * @package app\common\model
 *
 * @method static static find(mixed $data = null) 查询单个记录
 * @method static Collection|array select(mixed $data = null) 查询多个记录
 * @method static static get(mixed $data = null,mixed $with =[],bool $cache= false) 查询单个记录 支持关联预载入
 * @method static static getOrFail(mixed $data = null,mixed $with =[],bool $cache= false) static 查询单个记录 不存在则抛出异常
 * @method static static findOrEmpty(mixed $data = null,mixed $with =[],bool $cache= false) static 查询单个记录  不存在则返回空模型
 * @method static boolean chunk($count, $callback, $column = null, $order = 'asc') 分批数据返回处理
 * @method static Collection|array all(mixed $data = null,mixed $with =[],bool $cache= false) static 查询多个记录 支持关联预载入
 * @method static Query|static whereBetween($field, $condition, $logic = 'AND') 指定Between查询条件
 * @method Paginator|$this paginate($listRows = null, $simple = false, $config = []) static 分页
 *
 * @property int id
 * @property int create_time
 * @property int update_time
 */
class BaseModel extends Model
{

}