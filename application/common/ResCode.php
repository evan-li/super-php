<?php


namespace app\common;

class ResCode
{
    const success = 200;

    // 服务器相关错误
    const api_not_found = 90001; // 接口不存在
    const unknow_error = 90002; // 服务器错误
    const file_size_exceed = 90003; // 文件大小超出限制
    const api_closed = 90004; // 接口关闭
    const permission_denied = 90005; // 没有权限
    const http_request_error = 90010; // http请求失败
    const third_api_unload = 90011; // 第三方接口不允许请求

    // -- 基础错误
    const param_invalid = 10001; // 参数不合法
    const un_register = 10002; // 未注册
    const un_login = 10003; // 未登录
    const login_expire = 10004; // token失效或错误
    const un_user = 10005; // 用户不存在
    const password_error = 10006; //用户密码错误
    const user_exists = 10007; // 用户已存在
    const device_no_invalid = 10010; // 设备号无效

    // -- 数据库相关
    const data_not_exist = 11001; // 数据不存在
    const data_exist = 11002; // 数据已存在
    const data_not_allow_delete = 11008; // 数据不允许删除
    const data_not_allow_update = 11009; // 数据不允许更新
    const data_operation_fail = 11010; //  数据操作失败

    // 邮件相关
    const email_send_error = 12001; // 邮件发送失败
    const email_verify_code_error = 12002; // 邮箱验证码错误
    const email_send_too_fast = 12003; // 邮件发送过于频繁

    //--推送相关
    const user_number_error = 12011; //用户人数错误
    const push_error = 12012; // 推送失败

    // -- app相关
    const app_version_update = 20001; // 需要更新版本, 非强制
    const app_version_force_update = 20002; // 需要更新版本, 强制
    const app_version_error = 20003; //  版本号错误


    // --app支付相关
    const payment_not_exist = 21001; // 无可用支付渠道
    const payment_api_failed = 21002; // 购买凭证验证API失败
    const payment_bundleid_error = 21003; // bundleID验证不一致


    public static function getMessageByCode($code)
    {
        $ar = [
            self::success => lang('request success'), // 请求成功
            self::api_not_found => lang('api not found'), // 接口不存在
            self::unknow_error => lang('un know error'), // 服务器错误
            self::file_size_exceed => lang('file size exceed'), // 文件大小超出限制
            self::api_closed => lang('api closed'), // 服务器错误
            self::permission_denied => lang('permission denied'), // 没有权限
            self::http_request_error => lang('http request fail'), // http请求失败
            self::third_api_unload => lang('third api unload'), // 第三方接口不允许请求

            self::param_invalid => lang('param invalid'), // 参数不合法
            self::un_register => lang('un register'), // 未注册
            self::un_login => lang('un login'), // 未登录
            self::login_expire => lang('token expire'), // token失效或错误
            self::un_user => lang('user not exists'), // 用户不存在
            self::password_error => lang('user password error'), // 用户密码错误
            self::user_exists => lang('user already exists'), // 用户已存在
            self::device_no_invalid => lang('device no invalid'), // 设备号无效

            self::data_not_exist => lang('data not found'), // 数据不存在
            self::data_exist => lang('data already exists'), // 数据已存在
            self::data_not_allow_delete => lang('data not allow delete'), // 数据不允许删除
            self::data_not_allow_update => lang('data not allow update'), // 数据不允许更新
            self::data_operation_fail => lang('data operation fail'), // 数据库数据操作失败

            self::email_send_error => lang('email send error'), // 邮件发送失败
            self::email_verify_code_error => lang('email verify code error'), // 邮箱验证码错误
            self::email_send_too_fast => lang('email send too fast'), // 邮件发送过于频繁

            self::user_number_error => lang('user number error'), // 用户人数错误
            self::push_error => lang('push error'), // 推送失败

            self::app_version_update => lang('app version need update'), // 需要更新版本, 非强制
            self::app_version_force_update => lang('app version force update'), // 需要更新版本, 强制
            self::app_version_error => lang('app version error'), // 版本号错误

            self::payment_not_exist => lang('payment channel not exists'), // 无可用支付渠道
            self::payment_api_failed => lang('payment api verify fail'), // 购买凭证验证API失败
            self::payment_bundleid_error => lang('bundle ID error'), // bundleID验证不一致

        ];
        return $ar[$code] ?? lang('un know error'); //用户人数错误

    }

}