<?php


namespace app\common\task;


use think\Console;
use yunwuxin\cron\Task;

class DemoTask extends Task
{

    /**
     * 示例任务, 每日凌晨执行
     */
    public function configure()
    {
        $this->dailyAt('00:00'); // 每天凌晨0点执行
    }

    /**
     * 执行任务
     * @return mixed
     */
    protected function execute()
    {
        Console::call('test');
    }
}