<?php


namespace app\common\util;


use app\common\exception\CommonResponseException;
use app\common\ResCode;
use GuzzleHttp\Client;
use think\facade\Log;

class Http
{
    static $client;

    /**
     * get请求
     * @param $url
     * @param array $params
     * @return array
     */
    public static function get($url, $params = []){

        $response = self::getClient()->get($url, [
            'query' => $params,
        ]);
        if($response->getStatusCode() != 200){
            Log::error('http请求发送失败, 返回: [ ' . $response->getStatusCode() . ' ] ' . $response->getBody()->getContents());
            throw new CommonResponseException(ResCode::http_request_error);
        }else {
            return json_decode($response->getBody()->getContents(), 1);
        }
    }

    /**
     * post 请求
     * @param $url
     * @param $params
     * @return mixed
     */
    public static function post($url, $params){

        $response = self::getClient()->post($url, [
            'form_params' => $params,
        ]);
        if($response->getStatusCode() != 200){
            Log::error('http请求发送失败, 返回: [ ' . $response->getStatusCode() . ' ] ' . $response->getBody()->getContents());
            throw new CommonResponseException(ResCode::http_request_error);
        }else {
            return json_decode($response->getBody()->getContents(), 1);
        }
    }

    /**
     * 获取http客户端
     * @param string $baseUri
     * @return Client
     */
    protected static function getClient($baseUri = ''){
        if(self::$client){
            return self::$client;
        }
        self::$client = new Client([
            'base_uri' => $baseUri
        ]);
        return self::$client;
    }


}