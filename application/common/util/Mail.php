<?php


namespace app\common\util;


use PHPMailer\PHPMailer\PHPMailer;

class Mail
{

    /**
     * @param string|array $to
     * @param $subject
     * @param $content
     * @return bool
     * @throws
     */
    public static function send($to, $subject, $content)
    {
        $config = config('mail.');

        //实例化PHPMailer核心类
        $mail = new PHPMailer();

        //$mail->SMTPDebug = 1;#是否启用smtp的debug进行调试 开发环境建议开启 生产环境注释掉即可 默认关闭debug调试模式
        $mail->isSMTP(); #使用smtp鉴权方式发送邮件
        $mail->SMTPAuth = true; #smtp需要鉴权 这个必须是true
        $mail->Host = $config['host']; #链接qq域名邮箱的服务器地址

        $mail->SMTPSecure = $config['smtp_secure']; #设置使用ssl加密方式登录鉴权
        $mail->Port = $config['port']; #设置ssl连接smtp服务器的远程服务器端口号

        $mail->CharSet = 'UTF-8'; #设置发送的邮件的编码
        $mail->FromName = $config['from']; #设置发件人昵称 显示在收件人邮件的发件人邮箱地址前的发件人姓名
        $mail->Username = $config['username']; #smtp登录的账号
        $mail->Password = $config['password']; #smtp登录的密码 使用生成的授权码
        $mail->From = $config['username']; #设置发件人邮箱地址 同登录账号

        //邮件正文是否为html编码
        $mail->isHTML(true);
        //设置收件人邮箱地址
        if(is_array($to)) {
            foreach ($to as $item) {
                $mail->addAddress($item);
            }
        }else {
            $mail->addAddress($to);
        }
        //添加该邮件的主题
        $mail->Subject = $subject;
        //添加邮件正文
        $mail->Body = $content;
        //为该邮件添加附件
        //$mail->addAttachment('./example.pdf');
        //发送邮件 返回状态
        if(!$mail->send()) {
            log_error('邮件发送失败, 错误信息: ' . $mail->ErrorInfo);
            return false;
        }
        return true;
    }
}