<?php
namespace app\common\util;


use think\facade\Cache;

class RedisLock {

    private static $_redis;

    public static function getClient()
    {
        if(!self::$_redis) {
//            $handler = Cache::handler();
            return self::$_redis = Cache::handler();
        }
        return self::$_redis;
    }

    /**
     * 获取锁
     * @param  String  $key    锁标识
     * @param  Int     $expire 锁过期时间
     * @param  Int     $num    重试次数
     * @return Boolean
     */
    public static function lock($key, $expire = 3, $num = 0){
        $is_lock = self::getClient()->setnx($key, time()+$expire);

        if(!$is_lock) {
            //获取锁失败则重试{$num}次
            for($i = 0; $i < $num; $i++){

                $is_lock = self::getClient()->setnx($key, time()+$expire);

                if($is_lock){
                    break;
                }
                sleep(1);
            }
        }

        // 不能获取锁
        if(!$is_lock){

            // 判断锁是否过期
            $lock_time = self::getClient()->get($key);

            // 锁已过期，删除锁，重新获取
            if(time()>$lock_time){
                self::unlock($key);
                $is_lock = self::getClient()->setnx($key, time()+$expire);
            }
        }

        return $is_lock? true : false;
    }

    /**
     * 释放锁
     * @param  String  $key 锁标识
     * @return Boolean
     */
    public static function unlock($key){
        return self::getClient()->del($key);
    }

}