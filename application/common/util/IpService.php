<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 */
namespace app\common\util;

use GeoIp2\Database\Reader;

class IpService
{
    private static $reader;

    public static function getName($ip, $locales = ['zh-CN', 'en'])
    {
        try {
            if (is_null(self::$reader)) {
                self::$reader = new Reader(__DIR__ . '/GeoLite2-City.mmdb', $locales);
            }
            $record = self::$reader->city($ip);
            return $record->country->name . ' ' . $record->city->name;
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * 根据ip获取国家信息
     * @param string $ip
     * @return array
     */
    public static function getCountryCode(string $ip, $locales = ['zh-CN', 'en'])
    {
        $res = [
            '',
            'Asia/Hong_Kong',
            ''
        ];

        if (empty($ip)) {
            return $res;
        }

        try {
            if (is_null(self::$reader)) {
                self::$reader = new Reader(__DIR__ . '/GeoLite2-City.mmdb', $locales);
            }
            $record = self::$reader->city($ip);
            $res[0] = $record->country->isoCode ?? "";
            $res[1] = $record->location->timeZone;
            $res[2] = (string)(intval(str_replace('-', '', $record->location->longitude) ?? '1'));
            $res[3] = $record->country->name;
        } catch (\Exception $e) {
            //$record
            $res = self::getCountryCodeOnline($ip);
            //失败的情况下记录日志，$res[0]为国家代码
            if (empty($res[0])) {
                log_error("get ip online fail: ".$ip . " reason: " . $e->getMessage());
            }
        }
        return $res;
    }

    /**
     * 用过ip远程获取国家代码
     * @param string $ip
     * @return array
     */
    public static function getCountryCodeOnline(string $ip): array
    {
        $api = 'https://api.ip.sb/geoip/' . $ip;
        try {
            $data = Http::get($api);
        } catch (\Exception $e) {
            log_error("http get country by ip fail: " . $e->getMessage());
            $data = [];
        }

        $res = [
            '',
            'Asia/Hong_Kong',
            ''
        ];
        if (empty($data)) {
            return $res;
        }
        $res[0] = $data['country_code'] ?? "";
        $res[1] = $data['timezone'] ?? "";
        $res[2] = (string)(intval($data['longitude'] ?? '1'));
        $res[3] = $data['country'] ?? '';
        return $res;
    }
}
