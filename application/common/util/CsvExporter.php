<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 * Date: 2019/5/4
 * Time: 11:55
 */

namespace app\common\util;

use think\Collection;
use think\db\Query;
use think\Model;

define('UTF8_BOM',  chr(0xEF) . chr(0xBB) . chr(0xBF));
class CsvExporter
{
    public $file;

    /**
     * 下载数据为csv文件
     * @param Collection|array|Model|Query $data
     * @param string $filename
     * @param array $headers {id: ID, name: 名称}
     * @param callable $map
     * @throws \think\exception\DbException
     */
    public static function download($data, $headers, $filename, $map = null, $chunkId = 'id')
    {

        header('Content-Encoding: UTF-8');
        header('Content-Type: application/vnd.ms-execl');
        header('Content-Disposition: attachment;filename="' . $filename . '.csv"');

        echo "\xEF\xBB\xBF";
        $exporter = new CsvExporter();
        $exporter->setFile('php://output')->export($data, $headers, $map, $chunkId);

        ob_flush();
        flush();
    }


    /**
     * 导出数据
     * @param Collection|array|Model|Query $data
     * @param array $headers
     * @param callable $map
     * @param string $chunkId
     * @return CsvExporter
     * @throws \think\exception\DbException
     */
    public function export($data, $headers = null, $map = null, $chunkId = 'id')
    {
        set_time_limit(0); // 导出时, 统一设置请求超时时间为0
        $this->writeBOM();
        $this->writeHeader($headers);
        // 如果
        if($data instanceof Model || $data instanceof Query){
            $data->chunk(500, function($list) use ($headers, $map){
                $this->writeBody($list, $headers, $map);
            }, $chunkId);
        }else {
            $this->writeBody($data, $headers, $map);
        }
        return $this;
    }

    /**
     * 向csv数据中写数据
     * @param array $data
     * @param array $headers
     * @param callable $map
     * @return CsvExporter
     */
    public function writeBody($data, $headers = null, $map = null)
    {
        foreach ($data as $item) {
            if($map){
                $item = $map($item);
            }
            if($item instanceof Model){
                $item = $item->toArray();
            }
            $line = [];
            foreach ($headers as $key => $value) {
//                $line[$key] = mb_convert_encoding($item[$key], 'GB2312', 'UTF-8'); // 这里将UTF-8转为GBK编码
                $line[$key] = $item[$key];
            }
            $this->writeLine($line);
        }
        return $this;
    }

    /**
     * 根据编码给导出的文件写BOM头
     * @param string $charset
     */
    public function writeBOM($charset = 'utf-8')
    {
        if($charset == 'utf-8'){
            fwrite($this->file, UTF8_BOM);
        }
    }

    public function writeHeader($headers = null)
    {
//        foreach ($headers as $key => $value) {
//            $headers[$key] =  mb_convert_encoding($value, 'GB2312', 'UTF-8');
//        }
        $this->writeLine($headers);
        return $this;
    }

    public function writeLine($line)
    {
//        if(is_array($line) || $line instanceof Arrayable){
//            $line = implode(',', $line);
//        }
//        \Log::info('aaaaaaaaaa', ($line));
        fputcsv($this->file, $line);
        return $this;
    }

    public function setFile($filename)
    {
        $this->file = fopen($filename, 'a');
        return $this;
    }

}