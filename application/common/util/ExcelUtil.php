<?php

namespace app\common\util;

use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use think\Collection;
use think\db\Query;
use think\Model;

class ExcelUtil
{

    /**
     * 从文件获取数据
     * @param $filename string
     * @param $dataHandler callable
     * @return void
     * @throws Exception
     */
    public static function loadShippingDataFromFile($filename, $dataHandler)
    {
        self::loadDataFromFile($filename, function (&$row, $index) use ($dataHandler) {
            if ($index == 1) { // header不处理
                return;
            }
//            $data = [
//                'logistics_company' => $row[0] ?? '',
//                'logistics_no' => $row[1] ?? '',
//                'sub_order_no' => $row[2] ?? '',
//                'order_no' => $row[3] ?? '',
//            ];
            $dataHandler($row);
        });
    }

    /**
     * 从文件获取数据
     * @param $filename string
     * @param $dataHandler callable
     * @return void
     * @throws Exception
     */
    public static function loadDataFromFile($filename, $dataHandler, $skipHeader = true)
    {
        $sheet = self::loadFile($filename)->getActiveSheet();

        $maxRow = $sheet->getHighestRow();
        $maxColumn = $sheet->getHighestColumn();
        $maxColumnIndex = Coordinate::columnIndexFromString($maxColumn);

        self::loadDataFromSheet($sheet, $maxRow, $maxColumnIndex, function (&$row, $index) use ($dataHandler, $skipHeader) {
            if ($skipHeader && $index == 1) { // header不处理
                return;
            }
            $dataHandler($row, $index);
        });
    }

    /**
     * @param array|Collection|Model|Query $data
     * @param $header
     * @param $filename
     * @param null $dataHandler
     * @param string $chunkId
     * @param string $order
     * @return void
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \think\exception\DbException
     */
    public static function download($data, $header, $filename, $dataHandler = null, $chunkId = 'id', $order = 'desc')
    {
        $spreadsheet = new Spreadsheet();

        // 1获取活动工作薄
        $sheet = $spreadsheet->getActiveSheet();

        $maxColumn = count($header);
        $columnNames = array_keys($header);
        // 写header
        self::writeLine($sheet, 1, $header, $maxColumn, $columnNames);

        // 写内容, 从第二行开始写
        $currentRowIndex = 2;
        if ($data instanceof Model || $data instanceof Query) {
            $data->chunk(500, function($list) use ($sheet, $maxColumn, &$columnNames, &$currentRowIndex, $dataHandler) {
                log_info('in chunk list', $list);
                foreach ($list as $item) {
                    if (!empty($dataHandler)) {
                        $item = $dataHandler($item);
                    }
                    self::writeLine($sheet, $currentRowIndex, $item, $maxColumn, $columnNames);
                    $currentRowIndex ++;
                }
            }, $chunkId, $order);
        }else {
            foreach ($data as $item) {
                if (!empty($dataHandler)) {
                    $item = $dataHandler($item);
                }
                self::writeLine($sheet, $currentRowIndex, $item, $maxColumn, $columnNames);
                $currentRowIndex ++;
            }
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Encoding: UTF-8');
//        header('Content-Type: text/xlsx; application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Type: text/xlsx');
//        header('Content-Type: text/csv'); // 微信小程序中不能是被ms-excel的类型
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('x-filename: ' . $filename);
        $writer->save('php://output');
    }

    /**
     * 写一行数据
     * @param Worksheet $sheet
     * @param $rowIndex
     * @param $rowData
     * @param $maxColumn
     * @param $columnNames
     * @return void
     * @throws Exception
     */
    public static function writeLine($sheet, $rowIndex, $rowData, $maxColumn, $columnNames)
    {
        for ($col = 1; $col <= $maxColumn; $col ++) {
            $sheet->getCell([$col, $rowIndex])->setValue($rowData[$columnNames[$col-1]]);
        }
    }

    /**
     * @param $sheet
     * @param $maxRowIndex
     * @param $maxColumnIndex
     * @param $rowHandler callable
     * @return void
     */
    public static function loadDataFromSheet($sheet, $maxRowIndex, $maxColumnIndex, $rowHandler)
    {
        for ($rowIndex = 1; $rowIndex <= $maxRowIndex; $rowIndex ++) {
            $row = [];
            for ($columnIndex = 1; $columnIndex <= $maxColumnIndex; $columnIndex ++) {
                $row[] = $sheet->getCell([$columnIndex, $rowIndex])->getValue();
            }
            $rowHandler($row, $rowIndex);
        }
    }

    public static function loadFile($filename)
    {
        return IOFactory::load($filename);
    }

}