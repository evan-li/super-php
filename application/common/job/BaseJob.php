<?php


namespace app\common\job;


use app\common\exception\CommonResponseException;
use app\common\model\FailedJob;
use think\queue\Job;
use think\queue\job\Sync;

abstract class BaseJob
{
    /** @var string 队列名, 为空即在默认队列执行  */
    static $QUEUE_NAME = null;
    /**
     * @var int 任务重试次数
     */
    protected $attempts = 3;
    /**
     * @var int 重试任务的延迟时间
     */
    protected $delay = 0;

    public function fire(Job $job, $data)
    {

        //....这里执行具体的任务
        try {
            $res = $this->handle($job, $data);
        }catch (\Throwable $e) {
            $res = false;
        }

        if($res) {
            //如果任务执行成功后 记得删除任务，不然这个任务会重复执行，直到达到最大重试次数后失败后，执行failed方法
            $job->delete();
        }else {

            if ($job->attempts() < $this->attempts) {
                //通过这个方法可以检查这个任务已经重试了几次了
                $job->release($this->delay); //$delay为延迟时间
            }else {
//                $job->failed();
                $this->fail($job, $e ?? null);
                $job->delete();
            }
        }
    }

    /**
     * 执行具体的业务逻辑, 返回true表示执行成功, 返回false表示执行失败
     * @param Job $job
     * @param $data
     * @return bool
     */
    abstract public function handle(Job $job, $data): bool ;

    public function fail(Job $job, \Throwable $e = null)
    {
        // job : static::class, $data, $queueName
        $data = [
            'queue' => $job->getQueue(),
            'payload' => $job->getRawBody(),
        ];
        if(!is_null($e)) {
            $msg = '';
            if($e instanceof CommonResponseException) {
                $msg = json_encode($e->getData(), JSON_UNESCAPED_UNICODE) . "\n";
            }
            $msg .= $e->getMessage() . "\n";
            $data['exception'] = $msg . $e->getTraceAsString();
        }
        FailedJob::create($data);
    }

    /**
     * 不走队列, 直接执行
     * @param $data
     * @return bool
     */
    public function exec($data)
    {
        $job = new Sync([]);
        return $this->handle($job, $data);
    }

}