<?php


namespace app\common\job;


use think\queue\Job;

class TestJob extends BaseJob
{

    const QUEUE_NAME = "testJob:test";

    /**
     * 执行具体的业务逻辑, 返回true表示执行成功, 返回false表示执行失败
     * @param Job $job
     * @param $data
     * @return bool
     */
    public function handle(Job $job, $data): bool
    {
        $data = [
            'key' => 'test',
            'value' => '999999'
        ];
        log_info('test job handle', $data);
        return true;
    }
}