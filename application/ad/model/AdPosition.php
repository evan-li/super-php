<?php


namespace app\ad\model;


use app\common\model\BaseModel;

/**
 * Class AdPosition
 * @package app\ad\model
 *
 * @property int id
 * @property string name
 * @property string desc
 * @property string code
 * @property string image_sizes
 * @property int show_count_of_day
 * @property string show_days
 * @property int status
 * @property string ext_fields
 * @property int create_time
 * @property int update_time
 *
 * 关联属性
 * @property Ad[] ads
 */
class AdPosition extends BaseModel
{
    protected $name = 'ad_position';

    public function setImageSizesAttr($value)
    {
        return $value ? json_encode($value) : null;
    }

    public function getImageSizesAttr($value)
    {
        return $value ? json_decode($value, 1) : null;
    }

    public function getShowDaysAttr($value)
    {
        return $value ? explode(',', $value) : [];
    }

    public function setShowDaysAttr($value)
    {
        return $value ? implode(',', $value) : [];
    }

    public function ads()
    {
        return $this->hasMany(Ad::class, 'position_id');
    }


}