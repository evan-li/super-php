<?php


namespace app\ad\model;


use app\common\model\BaseModel;

/**
 * Class Ad
 * @package app\ad\model
 *
 * @property int id
 * @property int project_id
 * @property int sort
 * @property int position_id
 * @property string position_code
 * @property string name
 * @property string desc
 * @property int status
 * @property string image
 * @property int link_type
 * @property array payload
 * @property array items
 * @property string version_limit
 * @property int start_time
 * @property int end_time
 * @property object ext_data
 * @property int create_time
 * @property int update_time
 *
 * 关联属性
 * @property AdPosition position
 */
class Ad extends BaseModel
{
    protected $name = 'ad';

    public function getContentLevelsAttr($value)
    {
        return comma_str_to_arr($value);
    }

    public function setContentLevelsAttr($value)
    {
        return arr_to_comma_str($value);
    }

    public function setPayloadAttr($value)
    {
        return $value ? json_encode($value) : null;
    }

    public function getPayloadAttr($value)
    {
        return $value ? json_decode($value, 1) : null;
    }

    public function setItemsAttr($value)
    {
        return $value ? json_encode($value) : null;
    }

    public function getItemsAttr($value)
    {
        return $value ? json_decode($value, 1) : null;
    }

    public function setExtDataAttr($value)
    {
        return $value ? json_encode($value) : null;
    }

    public function getExtDataAttr($value)
    {
        return $value ? json_decode($value, 1) : null;
    }

    public function position()
    {
        return $this->belongsTo(AdPosition::class, 'position_id');
    }

}