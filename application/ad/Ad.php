<?php

namespace app\ad;

use app\system\service\MenuService;

class Ad
{
    public function install()
    {
        // 添加菜单
        MenuService::create([
            ['module' => 'ad', 'name' => '广告模块', 'url' => "ad", 'icon' => 'fa fa-buysellads'],
        ]);
        MenuService::createCurdMenu('ad', 'ad_position', '广告位管理', 'ad');
        MenuService::createCurdMenu('ad', 'ad', '广告管理', 'ad');
    }

    /**
     * @throws \Exception
     */
    public function uninstall()
    {
        // 删除菜单
        MenuService::deleteByModule('ad');
    }

    /**
     *
     */
    public function enable()
    {
        // 启用菜单
        $list = MenuService::getListByModule('ad');
        foreach ($list as $item) {
            MenuService::enable($item['id']);
        }
    }

    public function disable()
    {
        // 禁用菜单
        $list = MenuService::getListByModule('ad');
        foreach ($list as $item) {
            MenuService::disable($item['id']);
        }
    }
}