<?php

namespace app\ad\api;

use app\ad\model\Ad;
use app\ad\model\AdPosition;
use think\Collection;
use think\db\Query;
use think\helper\Str;

class Vert
{

    /**
     */
    public function index()
    {
        $code = input('code');
        list($position, $list) = $this->getPositionAndListByCode($code);

        return res_ok(['list' => $list, 'position' => $position]);
    }

    /**
     * 批量获取广告位的广告列表
     */
    public function batchGetList() {
        $codeList = input('codeList');
        if(is_string($codeList)) {
            $codeList = explode(',', $codeList);
        }

        $list = [];
        $positions = [];
        foreach ($codeList as $code) {
            list($position, $ads) = $this->getPositionAndListByCode($code);
            $list[$code] = $ads;
            $positions[$code] = $position;
        }
        return res_ok(['list' => $list, 'positions' => $positions]);
    }

    private function getPositionAndListByCode($code)
    {
        $position = AdPosition::where('code', $code)->field('id,name,code,status')->find();
        if(empty($position) || $position->status != 1){
            $list = [];
        }else {
            $query = Ad::where('position_code', $code)
                ->where('status', 1)
                ->where(function(Query $query) {
                    $query->where('start_time', 0)
                        ->whereOr(function(Query $query){
                            $time = time();
                            $query->where('start_time', '<=', $time)
                                ->where('end_time', '>=', $time);
                        });
                })
                ->order(['sort','id']);
            if (check_is18()) {
                $query->where('find_in_set(4, content_levels)');
            }else {
                $query->where('find_in_set(1, content_levels)');
            }
            $list = $query->select();
            $list = $this->filterByVersion($list);
        }
        foreach ($list as $ad) {
            $items = [];
            foreach ($ad->items as $item) {
                $item['image'] = get_file_path($item['image']);
                $items[] = $item;
            }
            $ad['items'] = $items;
        }
        // 谷歌审核
        if (($code == 'index-comic-banner' || $code == 'index-novel-banner') && google_play_is_in_audit()) {
            // 漫画banner
            $pic = request()->domain() . '/static/image/default/banner-1.png';
            // 小说banner
            if ($code == 'index-novel-banner') {
                $pic = request()->domain() . '/static/image/default/banner-2.png';
            }
            // 替换banner图片
            foreach ($list as $index => $ad) {
                $ad = $ad->toArray();
                if (count($ad['items']) <= 0) continue;
                $item = $ad['items'][0];
                $item['image'] = $pic;
                $ad['items'] = [$item];
                $list[$index] = $ad;
            }
        }

        return [$position, $list];
    }

    /**
     * 根据版本过滤广告列表
     * @param array $list
     * @return array
     */
    private function filterByVersion($list)
    {
        if($list instanceof Collection) {
            $list = $list->all();
        }
        $version = request()->header('version') ?: input('version');
        /**@var Ad $v*/
        foreach ($list as $key => $v) {
            if(!$v->version_limit) continue;
            if(Str::startsWith($v->version_limit, '>=')){
                $limitVersion = Str::substr($v->version_limit, 2);
                if($version < $limitVersion) {
                    unset($list[$key]);
                }
            }else if(Str::startsWith($v->version_limit, '<=')) {
                $limitVersion = Str::substr($v->version_limit, 2);
                if($version > $limitVersion) {
                    unset($list[$key]);
                }
            }else if(Str::startsWith($v->version_limit, '=')) {
                $limitVersion = Str::substr($v->version_limit, 1);
                if($version != $limitVersion) {
                    unset($list[$key]);
                }
            }else if(Str::startsWith($v->version_limit, '>')) {
                $limitVersion = Str::substr($v->version_limit, 1);
                if($version <= $limitVersion) {
                    unset($list[$key]);
                }
            }else if(Str::startsWith($v->version_limit, '<')) {
                $limitVersion = Str::substr($v->version_limit, 1);
                if($version >= $limitVersion) {
                    unset($list[$key]);
                }
            }else {
                if($version != $v->version_limit) {
                    unset($list[$key]);
                }
            }
        }
        return array_values($list);
    }

}