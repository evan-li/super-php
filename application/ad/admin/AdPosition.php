<?php


namespace app\ad\admin;


use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;

class AdPosition extends Admin
{
    public function index()
    {
        $fields = [
            ['name', '名称'],
            ['code', 'Code'],
            ['desc', '描述'],
            ['status', '状态', 'status'],
            ['image_sizes', '图片大小配置', 'array', '可添加多个尺寸限制, 添加广告图片时将按顺序轮流限制图片大小, 每行为一张图片尺寸, 宽高之间用逗号分隔, 单位为像素'],
            ['show_count_of_day', '每天展示次数', 'number', '0表示不限制展示次数'],
            ['show_days', '展示日期', 'checkbox', ['周日', '周一', '周二', '周三', '周四', '周五', '周六']],
            ['ext_fields', '额外字段配置', 'textarea', '配置格式: 每行一个字段, 每行包括元素: 字段名,字段说明,字段类型'],
        ];
        $validate =  [
            'name' => 'require',
            'code' => 'require|unique:ad_position',
            'image_sizes' => 'require',
        ];
        return SBuilder::make('table')
            ->setPageTitle('广告位管理')
            ->setSearch([
                'code|Code',
                'name|名称',
                'id|ID',
            ])
            ->autoAdd($fields, $validate)
            ->addTopButton('enable')
            ->addTopButton('disable')
            ->addColumns([
                ['__checkbox__'],
                ['id', 'ID'],
                ['name', '名称'],
                ['desc', '描述'],
                ['code', 'Code'],
                ['status', '状态', 'status'],
                ['create_time', '添加时间', 'datetime'],
                ['__btn__']
            ])
            ->addRightButton('ad_list', '广告列表', url('ad/index'), '', ['icon' => 'el-icon-reading', 'extra_data' => ['position_id' => '__id__']])
            ->autoEdit($fields, $validate)
            ->addRightButton('enable')
            ->addRightButton('disable')
            ->addRightButton('delete')
            ->fetch();
    }

    /**
     * 删除广告位
     */
    public function delete()
    {
        $id = $this->request->param('id');
        if(empty($id)){
            return admin_error('主键不能为空');
        }
        $ad = \app\ad\model\Ad::where('position_id', $id)->find();
        if(!empty($ad)){
            return admin_error('该广告为下存在广告, 不能删除');
        }
        $position = \app\ad\model\AdPosition::where('id', $id)->find();
        if(empty($position)){
            return admin_error('广告位信息不存在');
        }
        $position->delete();
        return admin_success('删除成功', '__relist__');
    }


}