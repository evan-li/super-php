<?php

namespace app\ad\project;

use app\ad\model\Ad as AdModel;
use app\ad\model\AdPosition as AdPositionModel;
use app\project\project\BaseProject;
use evan\sbuilder\builder\SBuilder;
use think\Exception;

class Ad extends BaseProject
{

    public function index()
    {
        $adPositions = AdPositionModel::column('name', 'id');
        return SBuilder::makeTable()
            ->setPageTitle("广告管理")
            ->setPageTips('目前因为使用uniapp开发, 广告图跳转地址非外链的话, 直接配置小程序跳转参数即可')
            ->addTopButton('add', '添加广告')
            ->addTopButton('enable')
            ->addTopButton('disable')
            ->addTopButton('delete')
            ->setSearch('name|广告名,position_code|广告位Code|=,id|ID|=')
            ->addTopSelect('position_id', '广告位', $adPositions, '', array_keys($adPositions)[0])
            ->addTopSelect('status', '状态', ['禁用', '正常'])
            ->addColumns([
                ['__checkbox__'],
                ['id', 'ID'],
                ['sort', '排序值', 'text.edit'],
//                ['position_code', '广告位Code'],
                ['name', '广告名'],
                ['desc', '广告描述'],
//                ['version_limit', '版本限制', '', ['' => '无']],
                ['images', '广告图片', 'images'],
                ['start_time', '生效时间', 'datetime'],
                ['end_time', '失效时间', 'datetime'],
                ['status', '状态', 'status'],
                ['__btn__']
            ])
            ->addRightButton('edit')
            ->addRightButton('enable')
            ->addRightButton('disable')
            ->addRightButton('delete')
            ->fetch();
    }

    /**
     * 获取广告列表
     */
    public function getList()
    {
        $positionId = $this->request->param('position_id');
        $where = $this->getWhere();
        if(!empty($positionId)){
            $where[] = ['position_id', '=', $positionId];
        }
        $query = AdModel::where('project_id', $this->currentProjectId)->where($where)
            ->order('sort')
            ->order($this->getOrder('id asc'));
        if(isset($whereKv['content_levels']) && !empty($whereKv['content_levels'])) {
            $contentLevels = comma_str_to_arr($whereKv['content_levels']);
            $query->where(function($query) use ($contentLevels) {
                foreach ($contentLevels as $contentLevel) {
                    $query->whereOr("find_in_set($contentLevel, content_levels)");
                }
            });
        }
        $data = $query->paginate();
        foreach ($data as $ad) {
            $images = [];
            foreach ($ad->items as $item) {
                $images[] = $item['image'];
            }
            $ad['images'] = $images;
        }
        return admin_data($data);
    }

    /**
     * 新增广告
     * @return mixed
     */
    public function add()
    {
        if(is_page()){
            $positionId = $this->request->param('position_id', '');
            $positions = AdPositionModel::where('status', 1)->field('id,name,image_sizes')->select();
            $positionNames = [];
            foreach ($positions as $item) {
                $positionNames[$item->id] = $item->name;
            }
            $fields = [
                ['position_id:require', '广告位', 'select', $positionNames, '', $positionId],
                ['name:require', '广告名称'],
                ['desc', '广告描述'],
                ['status', '状态', 'status'],
                ['daterange', '有效期', 'daterange', '广告有效期, 为空则不限制'],
                ['sort', '排序', 'number'],
            ];
            // 额外字段配置
            $position = AdPositionModel::get($positionId);
            if(!empty($position->ext_fields)) {
                $extFields = explode("\n", $position->ext_fields);
                foreach ($extFields as $value) {
                    $field = explode(',', $value);
                    if(isset($field[0])) {
                        $name = $field[0];
                        $desc = $field[1] ?? $field[0];
                        $type = $field[2] ?? 'text';
                        $fields[] = [$name, $desc, $type];
                    }
                }
            }
            $fields[] = ['items', '广告图配置', 'ad_items', '', '', ['positions' => $positions]];
            return SBuilder::makeForm(['span' => 24])
                ->setPageTitle('添加广告')
                ->setPageTips('广告链接配置说明: 链接目前统一配置到普通跳转的小程序字段, 支持应用内部页面和外部链接, 以及特殊链接', 'success')
                ->setPageTips('特殊链接说明:', 'success', '__365quan__: 365券跳转, 跳转前需要登录<br>__eleme__: 饿了么每日领红包, 跳转前需要登录及淘宝授权 <br>tbk_activity:{淘客官方活动id} : 淘客官方活动链接, 如双12主会场: tbk_activity:1574664919372, 跳转前需要登录及淘宝授权, 活动id在淘宝联盟后台获取')
                ->addFormItems($fields)
                ->fetch();
        }else {
            $res = $this->validate($this->request->param(), [
                'position_id' => 'require',
                'name' => 'require',
                'items' => 'require',
            ]);
            if($res !== true){
                return admin_error($res);
            }
            $positionId = $this->request->param('position_id');
            $position = AdPositionModel::get($positionId);
            if(empty($position)){
                return admin_error('广告位信息不存在');
            }

            $daterange = $this->request->param('daterange');
            if($daterange) {
                $startTime = strtotime($daterange[0]);
                $endTime = strtotime($daterange[1]);
            }else {
                $startTime = 0;
                $endTime = 0;
            }

            $ad = new AdModel();
            $ad->project_id = $this->currentProjectId;
            $ad->position_id = $positionId;
            $ad->position_code = $position->code;
            $ad->name = $this->request->param('name');
            $ad->desc = $this->request->param('desc');
            $ad->status = $this->request->param('status', 1);
            $ad->sort = $this->request->param('sort', 0);
            $ad->items = $this->request->param('items');
            $ad->start_time = $startTime;
            $ad->end_time = $endTime;

            // 获取扩展字段
            if(!empty($ad->position->ext_fields)) {
                $extFields = explode("\n", $ad->position->ext_fields);
                $extData = [];
                foreach ($extFields as $value) {
                    $field = explode(',', $value);
                    if(isset($field[0])) {
                        $value = $this->request->param($field[0], '');
                        $extData[$field[0]] = $value;
                    }
                }
                $ad->ext_data = $extData;
            }

            $ad->save();
            return admin_success('广告添加成功', '__relist__');
        }
    }

    /**
     * 编辑广告
     * @return mixed
     */
    public function edit()
    {
        $id = $this->request->param('id');
        if (empty($id)) return admin_error('缺少主键');

        if(is_page()){

            $ad = AdModel::with(['position' => function($query){
                $query->field('id,name,code,image_sizes,ext_fields');
            }])->where('project_id', $this->currentProjectId)
                ->get($id);
            if(empty($ad)){
                return admin_error('广告信息不存在');
            }

            $daterange = $ad->start_time > 0 ? [date('Y-m-d', $ad->start_time), date('Y-m-d', $ad->end_time)] : [];
            $fields = [
                ['position_id', '', 'hidden'],
                ['position_text', '广告位', 'static', '', "[{$ad->position->code}]{$ad->position->name}"],
                ['name:require', '广告名称'],
                ['desc', '广告描述'],
                ['status', '状态', 'status'],
                ['daterange', '有效期', 'daterange', '广告有效期, 为空则不限制', $daterange],
                ['sort', '排序', 'number'],
            ];
            // 额外字段配置
            if(!empty($ad->position->ext_fields)) {
                $extFields = explode("\n", $ad->position->ext_fields);
                foreach ($extFields as $value) {
                    $field = explode(',', $value);
                    if(isset($field[0])) {
                        $name = $field[0];
                        $desc = $field[1] ?? $field[0];
                        $type = $field[2] ?? 'text';
                        $fields[] = [$name, $desc, $type, '', $ad->ext_data[$field[0]] ?? ''];
                    }
                }
            }
            $fields[] = ['items', '广告图配置', 'ad_items', '', '', ['positions' => [$ad->position]]];
            return SBuilder::makeForm(['span' => 24])
                ->setPageTitle('编辑广告')
                ->setPageTips('广告链接配置说明: 链接目前统一配置到普通跳转的小程序字段, 支持应用内部页面和外部链接, 以及特殊链接', 'success')
                ->setPageTips('特殊链接说明:', 'success',
                    '__365quan__: 365券跳转, 跳转前需要登录<br>__eleme__: 饿了么每日领红包, 跳转前需要登录及淘宝授权 <br>tbk_activity:{淘客官方活动id} : 淘客官方活动链接, 如双12主会场: tbk_activity:1574664919372, 跳转前需要登录及淘宝授权, 活动id在淘宝联盟后台获取')
                ->addFormItems($fields)
                ->setFormData($ad)
                ->fetch();
        }else {
            $this->checkParam([
                'position_id' => 'require',
                'name' => 'require',
                'items' => 'require',
            ]);

            $ad = AdModel::where('project_id', $this->currentProjectId)->get($id);
            if(empty($ad)){
                return admin_error('广告信息不存在');
            }
            $daterange = $this->request->param('daterange');
            if($daterange) {
                $startTime = strtotime($daterange[0]);
                $endTime = strtotime($daterange[1]);
            }else {
                $startTime = 0;
                $endTime = 0;
            }

            $ad->name = $this->request->param('name');
            $ad->desc = $this->request->param('desc');
            $ad->status = $this->request->param('status', 1);
            $ad->sort = $this->request->param('sort', 0);
            $ad->items = $this->request->param('items');
            $ad->start_time = $startTime;
            $ad->end_time = $endTime;

            // 获取扩展字段
            if(!empty($ad->position->ext_fields)) {
                $extFields = explode("\n", $ad->position->ext_fields);
                $extData = [];
                foreach ($extFields as $value) {
                    $field = explode(',', $value);
                    if(isset($field[0])) {
                        $value = $this->request->param($field[0], '');
                        $extData[$field[0]] = $value;
                    }
                }
                $ad->ext_data = $extData;
            }

            $ad->save();
            return admin_success('广告修改成功', '__relist__');
        }
    }
}