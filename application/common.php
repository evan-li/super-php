<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

use app\common\exception\CommonResponseException;
use app\common\ResCode;
use think\api\Client;
use think\facade\App;
use think\Paginator;


if (!function_exists('tpapi')) {
    function tpapi(){
        return new Client("993342993551f34f0f1e70987fa0650e");
    }
}

if (!function_exists('google_play_is_in_audit')) {
    function google_play_is_in_audit(){
        $config = module_config('system', 'google_play_audit_version');
        return is_android() &&  in_array(input('version'), explode(',', $config));
    }
}

if (!function_exists('check_is18')) {
    function check_is18() {
        $is18 = input('is18');
        return ($is18 == 'true' || $is18 === true || $is18 === 1 || $is18 === '1') && !google_play_is_in_audit();
    }
}

if (!function_exists('ios_is_in_audit')) {
    function ios_is_in_audit(){
        return module_config('system', 'google_play_audit_version') == input('version');
    }
}

function res_ok($data = [], $msg = '') {
    if(is_string($data)) {
        $msg = $data;
    }
    if($data instanceof Paginator) {
        $data = [
            'list' => $data->items(),
            'total' => $data->total(),
            'page_size' => $data->listRows(),
            'page' => $data->currentPage(),
            'last_page' => $data->lastPage(),
        ];
    }
    return json([
        'code' => ResCode::success,
        'msg' => $msg ?: ResCode::getMessageByCode(ResCode::success),
        'data' => $data,
        'timestamp' => time(),
    ]);
}

function res_error($code, $msg = '', $data = null){
    if(!is_string($msg)) {
        $data = $msg;
        $msg = '';
    }
    if (is_string($code)) {
        $msg = $code;
        $code = ResCode::unknow_error;
    }
    return json([
        'code' => $code,
        'msg' => $msg ?: ResCode::getMessageByCode($code),
        'data' => $data,
        'timestamp' => time(),
    ]);
}

/**
 * 根据时间戳获取utc0时区当天的开始与结束时间戳
 * @param $time
 * @return array
 */
function get_utc0_day($time){
    $start = strtotime( '+8 hour', strtotime(date('Y-m-d 00:00:00', $time)));
    $end = $start + 86399;
    return [$start,$end];
}

/**
 * 根据时间戳获取utc0时区前一天的开始与结束时间戳
 * @param $time
 * @return array
 */
function get_utc0_yesterday($time = null){
    return get_utc0_day((is_null($time)?time():$time) - 86400);
}

/**
 * 根据时区获取时间戳对应时区的时间
 * @param $unix_time
 * @param string $timeZone
 * @return false|string
 */
function get_timeZone_time($unix_time,$timeZone = 'Asia/Seoul')
{
    date_default_timezone_set($timeZone);
    $tz_time = date('Y-m-d H:i:s',$unix_time);
    date_default_timezone_set('Asia/Shanghai');//还原时区
    return $tz_time;
}

/**
 * 根据时区获取对应时区0:00:00和23:59:59的时间戳
 * @param string $timeZone
 * @return array
 */
function get_timeZone_0_unix($timeZone = 'Asia/Seoul')
{
    date_default_timezone_set($timeZone);
    $start = strtotime(date('Y-m-d 0:00:00',time()));
    $end = $start + 86399;
    date_default_timezone_set('Asia/Shanghai');//还原时区
    return [$start,$end];
}
/**
 * +8时区时间转换为指定时区的时间
 * @param $date '2020-09-07 8:00:00'
 * @param string $from_tz
 * @param string $to_tz
 * @param string $fm
 * @return string
 * @throws Exception
 */
function toTimeZone($date, $from_tz = 'Asia/Shanghai', $to_tz = 'Asia/Seoul', $fm = 'Y-m-d H:i:s') {
    $datetime = new \DateTime($date, new \DateTimeZone($from_tz));
    $datetime->setTimezone(new \DateTimeZone($to_tz));
    return $datetime->format($fm);
}

/**
 * 请求参数加密
 * @param $data array
 * @return string
 */
function aes_encrypt($data, $key) {
    if(is_array($data)){
        $data = json_encode($data);
    }
    return base64_encode(openssl_encrypt($data, 'AES-128-ECB', $key, OPENSSL_RAW_DATA));
}

/**
 * 请求参数解密
 * @param $data string
 * @return array|string
 */
function aes_decrypt($data, $key) {
    $data = openssl_decrypt(base64_decode($data), 'AES-128-ECB', $key, OPENSSL_RAW_DATA);
    $data = json_decode($data,true) ?: $data;
    return $data;
}

// 是否本地环境
if(! function_exists('is_local')) {
    function is_local(){
        return env('app_env') == 'local';
    }
}


/**
 * 检查数据库中的数据是否存在
 * @param $id
 * @param string $model
 * @param string $msg
 * @return \think\Model
 */
function check_db_exist($id, $model = '', $msg = ''){
    $model = model($model);
    if (is_array($id))
    {
        $map = $id;
    }
    else
    {
        $map = [
            array('id','eq',$id)
        ];
    }
    $obj = $model::where($map) ->find();
    if(empty($obj)) {
        throw new CommonResponseException(ResCode::data_not_exist, $msg);
    }
    return $obj;
}

/**
 * 检查数据库中的数据是否不存在
 * @param $id
 * @param string $model
 * @param string $msg
 * @return \think\Model
 */
function check_db_not_exist($id, $model = '', $msg = ''){
    $model = model($model);
    if (is_array($id))
    {
        $map = $id;
    }
    else
    {
        $map = [
            array('id','eq',$id)
        ];
    }
    $obj = $model::where($map) ->find();
    if(!empty($obj)) {
        throw new CommonResponseException(ResCode::data_exist, $msg);
    }
    return $obj;
}

/**
 * 写入支付日志
 * @param $action
 * @param $content
 */
function writePaymentLog($action,$content)
{
    if (!$content || !$action) {
        throw new CommonResponseException(ResCode::param_invalid);
    }
    if(is_array($content)){
        $content=json_encode($content);
    }
    $data=[];
    $data['action']=$action;
    $data['content']=$content;
    $data['ip']=request()->ip();
    \app\common\model\PaymentLog::create($data);
}

function fix_timezone(&$timezoneId)
{
    if ($timezoneId == 'Asia/Yangon') {
        $timezoneId = 'Asia/Rangoon';
    }
}

function str_to_time_by_timezone($time, $timezoneId = 'GMT')
{
    fix_timezone($timezoneId);
    try {
        $timezone = new \DateTimeZone($timezoneId);
        $datetime = new \Datetime($time, $timezone);
    } catch (\Exception $e) {
        return false;
    }

    return $datetime->format('U');
}

function get_client_ip()
{
    $arrIpHeader = array(
        'HTTP_CDN_SRC_IP',
        'HTTP_PROXY_CLIENT_IP',
        'HTTP_WL_PROXY_CLIENT_IP',
        'HTTP_CLIENT_IP',
        'HTTP_X_FORWARDED_FOR',
        'REMOTE_ADDR',
    );
    $client_ip = '';
    foreach ($arrIpHeader as $key) {
        if (!empty($_SERVER[$key]) && strtolower($_SERVER[$key]) != 'unknown') {
            $client_ip = $_SERVER[$key];
            break;
        }
    }

    if (strpos($client_ip, ",") !== false) {
        $clientIpArr = explode(',', $client_ip);
        $client_ip = $clientIpArr[0];
    }
    return $client_ip;
}

/**
 * 根据时区获取当地时区前一天的开始与结束时间戳
 * @param string $timeZone
 * @return array
 */
function get_yesterday_time($timeZone = "Asia/Shanghai"){
    $time = time();
    $startTime = date("Y-m-d 00:00:00", $time - 86400);
    $startTimeStamp = str_to_time_by_timezone($startTime, $timeZone);
    if (empty($startTimeStamp)) {
        $startTimeStamp = strtotime($startTime);
    }

    $endTime = date("Y-m-d 23:59:59", $time - 86400);
    $endTimeStamp = str_to_time_by_timezone($endTime, $timeZone);
    if (empty($endTimeStamp)) {
        $endTimeStamp = strtotime($endTime);
    }

    return [$startTimeStamp, $endTimeStamp];
}

/**
 * 上报记录
 * @param $orderId
 * @param $channel
 * @param $param
 * @param int $status
 * @param string $desc
 * @param int $userId
 * @param int $type
 */
function report_record($orderId, $channel,$param,$status = 1,$desc = '', $userId = 0, $type = 1)
{
    $data = [
        'order_id' => $orderId,
        'channel' => $channel??'',
        'param_json' => json_encode($param),
        'status' => $status,
        'desc' => $desc,
        //'user_id' => $userId,
        //'type' => $type,
    ];
    \app\common\model\Report::create($data);
}


/**
 * 检查任务创建时间是否满足用户所在时区的前一天
 * @param $timeZone
 * @param $mission_create_time
 * @return bool
 */
function check_mission($timeZone,$mission_create_time)
{
    list($start_time,$end_time) = get_timeZone_0_unix($timeZone);
    if ($mission_create_time < $start_time)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * 解析url中的参数
 * @param $url
 * @return array
 */
function analysisUrl($url)
{
    $parse_url = parse_url($url);
    $query = '';
    if (isset($parse_url['query']))
    {
        $query = $parse_url['query'];
    }
    $arr_query = convertUrlQuery($query);
    return $arr_query;
}
function convertUrlQuery($query)
{
    $queryParts = explode('&', $query);
    $params = array();
    foreach ($queryParts as $param) {
        $item = explode('=', $param);
        $params[$item[0]] = $item[1];
    }
    return $params;
}
