<?php

namespace app\mall\model;


use app\common\exception\CommonResponseException;
use app\common\model\BaseModel;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\DbException;
use think\facade\Validate;
use think\helper\Str;

/**
 * 用户验证码
 * Class UserVerifyCode
 * @package app\user\model
 *
 * @property int id
 * @property int type
 * @property string use_type
 * @property string to
 * @property string code
 * @property int expire_time
 * @property int status
 * @property int create_time
 * @property int update_time
 */
class UserVerifyCode extends BaseModel
{
    protected $table = 'mall_user_verify_code';

    const type_mobile = 1;
    const type_email = 2;

    const use_type_register = 'register';
    const use_type_reset_password = 'reset_password';

    /**
     * 发送验证码
     * @param $to
     * @param string $useType
     * @param int $length
     * @param int $expire
     * @return UserVerifyCode
     */
    public static function send($to, $useType = '', $length = 4, $expire = 600)
    {
        if (Validate::is($to, 'mobile')){
            $type = self::type_mobile;
        }else {
            $type = self::type_email;
        }
        $code = '';
        for ($i = 0; $i < $length; $i ++){
            $code .= rand(0, 9);
        }
        $record = new static();
        $record->type = $type;
        $record->use_type = $useType;
        $record->to = $to;
        $record->code = $code;
        $record->expire_time = time() + $expire;
        $record->status = 1;
        $record->save();

        return $record;
    }

    /**
     * 验证验证码
     * @param $to
     * @param $code
     * @param string $useType
     * @return bool
     */
    public static function check($to, $code, $useType = '')
    {
        // todo 暂时使用环境使用万能验证码
        if($code == '6666'){
            return true;
        }
        $record = self::where('to', $to)
            ->where('use_type', $useType)
            ->where('code', $code)
            ->where('status', 1)
            ->find();
        if(empty($record)){
            return false;
        }
        if($record->expire_time < time()){
            // 已超时
            $record->status = 0;
            $record->save();
            return false;
        }

        $record->status = 0;
        $record->save();
        return true;
    }
}