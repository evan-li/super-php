<?php
/**
 * Create By Evan.
 * Date: 2019/8/2
 * Time: 23:18
 */

namespace app\mall\model;


use app\common\model\BaseModel;

/**
 * 收藏表
 * Class Favorite
 * @package app\shop\model
 *
 * @property int id
 * @property int user_id
 * @property int type
 * @property int target_id
 * @property int create_time
 * @property int update_time
 *
 * @property Goods goods
 */
class Favorite extends BaseModel {

    protected $table = 'mall_favorite';

    const type_native = 1;

    /**
     * 所属商品
     */
    public function goods()
    {
        return $this->belongsTo(Goods::class, 'goods_id');
    }


}