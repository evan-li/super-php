<?php

namespace app\mall\model;

use app\common\exception\CommonResponseException;
use app\common\model\BaseModel;
use app\common\ResCode;

/**
 *
 * @property string name
 * @property array shipping_rules
 * @property array unshipping_areas
 */
class ShippingTemplate extends BaseModel
{
    protected $table = 'mall_shipping_template';

    /**
     * 根据商品列表计算运费
     * @param ShippingTemplate|int $shippingTemplate
     * @param $address
     * @param $goodsList
     * @return int 返回运费金额, -1标识不配送 -2收货地址无效
     */
    public static function computeShippingFee($shippingTemplate, $address, $goodsList)
    {
        // 默认免运, 运费为0
        if (empty($shippingTemplate)) {
            return 0;
        }
        if (empty($address)) { // 收货地址无效, 返回-2
            return -2;
        }
        if (is_numeric($shippingTemplate)) {
            $shippingTemplate = ShippingTemplate::where('id', $shippingTemplate)->find();
            if (empty($shippingTemplate)) {
                throw new CommonResponseException(ResCode::unknow_error, '运费模板不存在');
            }
        }
        // 判断是否不配送
        if (self::areaIsContain($shippingTemplate->unshipping_areas, $address)) {
            return -1;
        }
        foreach ($shippingTemplate->shipping_rules as $rule) {
            if (self::areaIsContain($rule['provinces'], $address)) {
                // 判断是否满足包邮条件
                if ($rule['has_free_condition']) { // 包邮条件
                    if ($rule['free_type'] == 1) { // 满足件数包邮
                        if (array_sum(array_column($goodsList, 'number')) >= $rule['free_num']) {
                            return 0;
                        }
                    }else { // 满足重量保量
                        if (array_sum(array_column($goodsList, 'weight')) >= $rule['free_num']) {
                            return 0;
                        }
                    }
                }
                // 计算运费
                if ($rule['fee_type'] == 1) { // 固定运费
                    return $rule['fee'];
                }else if($rule['fee_type'] == 2) { // 按件计费
                    $number = array_sum(array_column($goodsList, 'number'));
                    $shippingFee = $rule['start_fee'];
                    $number -= $rule['start_num'];
                    while ($number > 0) {
                        $number -= $rule['plus_num'];
                        $shippingFee += $rule['plus_fee'];
                    }
                    return $shippingFee;
                }else { // 按重量计费
                    $weight = array_sum(array_column($goodsList, 'weight'));
                    $shippingFee = $rule['start_fee'];
                    $weight -= $rule['start_num'];
                    while ($weight > 0) {
                        $weight -= $rule['plus_num'];
                        $shippingFee += $rule['plus_fee'];
                    }
                    return $shippingFee;
                }
            }
        }
        // 未匹配到的区域默认包邮
        return 0;
    }

    /**
     * 判断运费模板的地区列表中是否包含指定地址
     * @param UserAddress $address
     * @param $provinces
     * @return bool
     */
    public static function areaIsContain($provinces, $address)
    {
        foreach ($provinces as $province) {
            if ($province['id'] == $address->project_id) {
                foreach ($province['cities'] as $city) {
                    if ($city['id'] == $address->city_id) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function getShippingRulesAttr($value)
    {
        return json_str_to_arr($value);
    }
    public function setShippingRulesAttr($value)
    {
        return arr_to_json_str($value);
    }

    public function getUnshippingAreasAttr($value)
    {
        return json_str_to_arr($value);
    }
    public function setUnshippingAreasAttr($value)
    {
        return arr_to_json_str($value);
    }
}