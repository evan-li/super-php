<?php


namespace app\mall\model;

use app\common\model\BaseModel;
use think\model\concern\SoftDelete;
use think\model\relation\BelongsTo;

/**
 * 商品SKU
 * Class GoodsSku
 * @package app\shop\model
 *
 * @property int id
 * @property int goods_id
 * @property string key
 * @property string name
 * @property float sale_price
 * @property float market_price
 * @property float cost_price
 * @property int stock
 * @property int origin_sku_id
 * @property float supply_price
 * @property float commission_rate
 * @property float commission_fee
 * @property int create_time
 * @property int update_time
 *
 * 关联属性
 * @property Goods goods
 */
class GoodsSku extends BaseModel
{
    use SoftDelete;
    protected $table = 'mall_goods_sku';

    /**
     * 关联商品
     * @return BelongsTo
     */
    public function goods()
    {
        return $this->belongsTo(Goods::class);
    }

}