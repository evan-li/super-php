<?php

namespace app\mall\model;

use app\common\model\BaseModel;

/**
 * @property int id
 * @property string country
 * @property string state
 * @property string city
 * @property string region
 * @property string code
 * @property int parentid
 */
class CountryArea extends BaseModel
{

}