<?php

namespace app\mall\model;

use app\common\model\BaseModel;

/**
 *
 * @property int user_id
 * @property int device_type
 * @property string client_id
 * @property string device_token
 *
 * @property User user
 */
class UserDevice extends BaseModel
{
    protected $table = 'mall_user_device';
    public function user()
    {
        return $this->belongsTo(User::class);
    }


}