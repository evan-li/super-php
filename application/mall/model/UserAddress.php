<?php


namespace app\mall\model;

use app\common\model\BaseModel;

/**
 * 用户收货地址
 * Class UserAddress
 * @package app\user\model
 *
 * @property int id
 * @property int user_id
 * @property int is_default
 * @property string country_code
 * @property string name
 * @property string mobile
 * @property int province_id
 * @property string province
 * @property int city_id
 * @property string city
 * @property int district_id
 * @property string district
 * @property string detail
 * @property string detail2
 * @property int create_time
 * @property int update_time
 *
 * @property User user
 */
class UserAddress extends BaseModel
{
    protected $table = 'mall_user_address';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}