<?php


namespace app\mall\model;


use app\common\exception\CommonResponseException;
use app\common\model\BaseModel;
use app\common\ResCode;
use think\Collection;
use think\Db;
use think\model\concern\SoftDelete;

/**
 * 商品模型
 * Class Goods
 * @package app\shop\model
 *
 * @property int id
 * @property int cat_id_1
 * @property int cat_id_2
 * @property int cat_id_3
 * @property int type
 * @property string name
 * @property string desc
 * @property string pic
 * @property string pics
 * @property string video
 * @property string detail
 * @property float weight
 * @property float has_spec
 * @property float sale_price
 * @property float max_sale_price
 * @property float market_price
 * @property float cost_price
 * @property string rebate_rule
 * @property int stock
 * @property int sale_count
 * @property int logistics_type
 * @property int shipping_template_id
 * @property int status
 * @property int group_id
 * @property int origin_goods_id
 * @property float supply_price
 * @property float max_supply_price
 * @property float commission_rate
 * @property float commission_fee
 * @property float max_commission_fee
 * @property int sort
 * @property int create_time
 * @property int update_time
 *
 * 关联属性
 * @property Collection|GoodsSpec[] specs
 * @property Collection|GoodsSku[] skus
 */
class Goods extends BaseModel
{
    use SoftDelete;
    protected $table = 'mall_goods';

    const TYPE_LIBRARY = 1; // 商品库
    const TYPE_GROUP = 2; // 团购商品
    const TYPE_GIFT_YEAR = 3; // 礼包商品[年](送一年VIP)
    const TYPE_GIFT_MONTH = 4; // 礼包商品[月](送一月VIP)

    // 无限库存
    const STOCK_UNLIMIT = null;

    /**
     * 增加商品或sku库存
     * @param $goods
     * @param $sku
     * @param $number
     * @return void
     * @throws \think\Exception
     */
    public static function stockInc($goods, $sku, $number = 1)
    {
        if (!empty($sku)) { // 更新SKU库存
            if (is_numeric($sku)) {
                $sku = GoodsSku::where('id', $sku)->find();
            }
            if (empty($sku)) return;
            if ($sku->stock !== self::STOCK_UNLIMIT) {
                // 返还SKU库存
                GoodsSku::where('id', $sku->id)->setInc('stock', $number);
            }
        }else { // 更新商品库存
            if (is_numeric($goods)) {
                $goods = Goods::where('id', $goods)->find();
            }
            if (empty($goods)) return;
            if ($goods->stock !== self::STOCK_UNLIMIT) {
                // 返还商品库存及已售数量
                Goods::where('id', $goods->id)
                    ->inc('stock', $number)
                    ->update();
            }
        }
    }

    /**
     * 扣除商品或sku库存
     * @param $goods
     * @param $sku
     * @param $number
     * @return void
     * @throws \think\Exception
     */
    public static function stockDec($goods, $sku, $number = 1)
    {
        if (!empty($sku)) { // 更新SKU库存
            if (is_numeric($sku)) {
                $sku = GoodsSku::where('id', $sku)->find();
            }else {
                $sku = GoodsSku::where('id', $sku->id)->find();
            }
            if ($sku->stock !== self::STOCK_UNLIMIT) {
                // 返还SKU库存
                $stock = GoodsSku::where('id', $sku->id)
                    ->where('stock', '>=', $number)
                    ->dec('stock', $number)
                    ->update();
                if ($stock < 1) {
                    throw new CommonResponseException(ResCode::unknow_error, '库存更新失败, 请稍后重试');
                }
            }
        }else { // 更新商品库存
            if (is_numeric($goods)) {
                $goods = Goods::where('id', $goods)->find();
            }else {
                $goods = Goods::where('id', $goods->id)->find();
            }
            if ($goods->stock !== self::STOCK_UNLIMIT) {
                // 返还商品库存及已售数量
                $stock = Goods::where('id', $goods->id)
                    ->where('stock', '>=', $number)
                    ->dec('stock', $number)
                    ->update();
                if ($stock < 1) {
                    throw new CommonResponseException(ResCode::unknow_error, '库存更新失败, 请稍后重试');
                }
            }
        }
    }

    public static function getPicsWithDefault($goods, $default = '')
    {
        return empty($goods->pics) ? [$default] : comma_str_to_arr($goods->pics);
    }

    public static function getTotalStock(Goods $goods)
    {
        $stock = 0;
        if ($goods->has_spec == 1) {
            foreach ($goods->skus as $sku) {
                if ($sku->stock === self::STOCK_UNLIMIT) {
                    $stock = self::STOCK_UNLIMIT;
                }
                if ($stock !== self::STOCK_UNLIMIT) {
                    $stock += $sku->stock;
                }
            }
        }else {
            $stock = $goods->stock;
        }
        return $stock;
    }

    /**
     * 商品规格列表
     */
    public function specs()
    {
        return $this->hasMany(GoodsSpec::class, 'goods_id');
    }

    /**
     * 商品SKU列表
     */
    public function skus()
    {
        return $this->hasMany(GoodsSku::class);
    }

//    public function getDetailAttr($value)
//    {
//        return json_str_to_arr($value, []);
//    }
//    public function setDetailAttr($value)
//    {
//        return arr_to_json_str($value, '');
//    }
    public function getPicsAttr($value)
    {
        return comma_str_to_arr($value);
    }
    public function setPicsAttr($value)
    {
        return arr_to_comma_str($value);
    }

}