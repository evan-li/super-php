<?php


namespace app\mall\model;


use app\common\model\BaseModel;
use think\model\concern\SoftDelete;
use think\model\relation\BelongsTo;
use think\model\relation\HasMany;

/**
 * 商品规格
 * Class GoodsSpec
 * @package app\shop\model
 *
 * @property int id
 * @property int goods_cat_id
 * @property int goods_id
 * @property string name
 *
 * 关联属性
 * @property Goods goods
 * @property GoodsSpecValue[] values
 */
class GoodsSpec extends BaseModel
{
    use SoftDelete;
    protected $table = 'mall_goods_spec';
    public $autoWriteTimestamp = false;

    /**
     * 规格所属商品
     * @return BelongsTo
     */
    public function goods()
    {
        return $this->belongsTo(GoodsCat::class);
    }

    /**
     * 规格值列表
     * @return HasMany
     */
    public function values()
    {
        return $this->hasMany(GoodsSpecValue::class, 'spec_id');
    }
}