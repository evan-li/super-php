<?php

namespace app\mall\model;

use app\common\model\BaseModel;

/**
 * @property int collection_id
 * @property string collection_type
 * @property int sort
 * @property int goods_id
 * @property int goods_type
 * @property string goods_name
 * @property string goods_pic
 * @property string goods_pics
 * @property float goods_price
 *
 * @property Goods goods
 */
class GoodsCollectionItem extends BaseModel
{
    protected $table = 'mall_goods_collection_item';

    public function goods()
    {
        return $this->belongsTo(Goods::class, 'goods_id');
    }
}