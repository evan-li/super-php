<?php


namespace app\mall\model;


use app\common\model\BaseModel;

/**
 * Class UserRequestLog
 * @package app\user\model
 *
 * @property int id
 * @property int user_id
 * @property string date
 * @property string url
 * @property int create_time
 * @property int update_time
 *
 * @property User user
 */
class UserRequestLog extends BaseModel
{
    protected $table = 'mall_user_request_log';

    public function user(){
        return $this->belongsTo(User::class);
    }
}