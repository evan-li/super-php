<?php


namespace app\mall\model;


use app\common\model\BaseModel;
use think\model\relation\BelongsTo;

/**
 * 商品规格值
 * Class GoodsSpecValue
 * @package app\shop\model
 *
 * @property int id
 * @property int spec_id
 * @property int value
 *
 * 关联属性
 * @property GoodsSpec spec
 */
class GoodsSpecValue extends BaseModel
{
    protected $table = 'mall_goods_spec_value';
    public $autoWriteTimestamp = false;

    /**
     * 所属规格
     * @return BelongsTo
     */
    public function spec()
    {
        return $this->belongsTo(GoodsSpec::class, 'spec_id');
    }
}