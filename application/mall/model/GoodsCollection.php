<?php

namespace app\mall\model;

use app\common\model\BaseModel;

/**
 * @property string name
 * @property string code
 * @property string type
 * @property string remark
 * @property string pic
 *
 * @property GoodsCollectionItem[] $items
 */
class GoodsCollection extends BaseModel
{
    protected $table = 'mall_goods_collection';

    public function items()
    {
        return $this->hasMany(GoodsCollectionItem::class, 'collection_id');
    }

}