<?php

namespace app\mall\model;


use app\common\model\BaseModel;
use think\model\concern\SoftDelete;
use think\model\relation\BelongsTo;
use think\model\relation\HasMany;

/**
 * 商品分类
 * Class GoodsCat
 * @package app\shop\model
 *
 * @property int id
 * @property int pid
 * @property int tier
 * @property string name
 * @property int status
 * @property int create_time
 * @property int update_time
 *
 * 关联属性
 * @property GoodsCat parent
 * @property GoodsCat[] children
 * @property Goods[] goodsListCat1
 * @property Goods[] goodsListCat2
 * @property Goods[] goodsListCat3
 */
class GoodsCat extends BaseModel
{
    use SoftDelete;
    protected $table = 'mall_goods_cat';
    const statusNo = 0;
    const statusYes = 1;

    public static function init()
    {
        // 模型写入后事件
        self::event('after_write', function ($cat) {
            // 模型写入后清除缓存
            if ($cat->tier < 3) {
                cache('goods_cat:tree_level_2', null);
            }
            cache('goods_cat:tree', null);
            cache('goods_cat:enable_tree', null);
        });
    }

    /**
     * 获取顶部2层分类树
     * @return array
     */
    public static function getTopLevel2Tree()
    {
        $tree = cache('goods_cat:tree_level_2');
        if(empty($tree)){
            $list = GoodsCat::where('tier', '<=', 2)->select();
            $tree = convert_list_to_tree($list);
            cache('goods_cat:tree_level_2', $tree);
        }
        return $tree;
    }

    /**
     * 获取分类树(全部)
     * @return array
     */
    public static function getTree()
    {
        $tree = cache('goods_cat:tree');
        if(empty($tree)){
            $list = GoodsCat::select();
            $tree = convert_list_to_tree($list);
            cache('goods_cat:tree', $tree);
        }
        return $tree;
    }

    /**
     * 获取启用的分类树
     * @return array
     */
    public static function getEnableTree()
    {
        $tree = cache('goods_cat:enable_tree');
        if(empty($tree)){
            $list = GoodsCat::where('status', 1)->select();
            $tree = convert_list_to_tree($list);
            cache('goods_cat:enable_tree', $tree);
        }
        return $tree;
    }

    /**
     * 上级分类
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(GoodsCat::class, 'pid')->selfRelation();
    }

    /**
     * 子分类
     * @return HasMany
     */
    public function children()
    {
        return $this->hasMany(GoodsCat::class, 'pid')->selfRelation();
    }

    /**
     * 一级分类下的商品列表
     */
    public function goodsListCat1()
    {
        return $this->hasMany(Goods::class, 'cat_id_1');
    }

    /**
     * 二级分类下的商品列表
     */
    public function goodsListCat2()
    {
        return $this->hasMany(Goods::class, 'cat_id_2');
    }

    /**
     * 二级分类下的商品列表
     */
    public function goodsListCat3()
    {
        return $this->hasMany(Goods::class, 'cat_id_3');
    }

}