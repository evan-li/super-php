<?php


namespace app\mall\model;


use app\common\model\BaseModel;
use think\facade\Cache;
use think\helper\Str;
use think\model\relation\BelongsTo;
use think\model\relation\HasMany;
use think\model\relation\HasOne;

/**
 * Class User
 * @package app\user\model
 *
 * @property int id
 * @property string mobile
 * @property string email
 * @property string account
 * @property string password
 * @property string salt
 * @property string nickname
 * @property string avatar
 * @property int gender
 * @property string birthday
 * @property string wx_openid
 * @property string wx_mini_openid
 * @property string wx_unionid
 * @property string qq_openid
 * @property string weibo_openid
 * @property string facebook_openid
 * @property string google_openid
 * @property string apple_openid
 * @property string firebase_uid
 * @property int is_visitor
 * @property string device_no
 * @property int device_type
 * @property int last_login_time
 * @property int last_login_type
 * @property int last_launch_time
 * @property string last_launch_ip
 * @property int status
 *
 * 关联属性
 */
class User extends BaseModel
{
    protected $table = 'mall_user';

    // 登录方式
    const login_type_email = 1; // email 登录
    const login_type_mobile = 2; // 手机号 登录
    const login_type_account = 3; // 账号 登录
    const login_type_wx_mp = 11; // 微信公众号 登录
    const login_type_wx_ma = 12; // 微信小程序 登录
    const login_type_wx_open = 13; // 微信开放平台 登录
    const login_type_qq = 14; // qq 登录
    const login_type_weibo = 15; // 微播 登录
    const login_type_google = 21; // 谷歌 登录
    const login_type_facebook = 22; // facebook 登录
    const login_type_apple = 23; // apple 登录
    const login_type_firebase = 24; // firebase 登录

    /**
     * hash密码
     * @param $password
     * @param $salt
     * @return string
     */
    public static function hashPassword($password, $salt) {
        return md5(md5($password) . $salt);
    }


}