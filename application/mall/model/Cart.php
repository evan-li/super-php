<?php

namespace app\mall\model;

use app\common\model\BaseModel;

/**
 *
 * @property int user_id
 * @property int group_id
 * @property int goods_id
 * @property int sku_id
 * @property int number
 * @property string goods_pic
 * @property string goods_name
 * @property string sku_name
 * @property float market_price
 * @property float sale_price
 */
class Cart extends BaseModel
{
    protected $table = 'mall_cart';

}