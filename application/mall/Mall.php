<?php

namespace app\mall;

use app\system\service\MenuService;

class Mall
{

    public function install()
    {
        $menus = MenuService::create([
            ['module' => 'mall', 'name' => '商城', 'url' => "mall", 'icon' => '',]
        ]);
        $mallMenu = $menus[0];

        $menus = MenuService::create([
            ['module' => 'mall', 'name' => '用户', 'url' => "mall_user", 'icon' => '',]
        ], $mallMenu->id);
        $userMenu = $menus[0];
        MenuService::createCurdMenu([
            ['mall', 'user', '用户管理', $userMenu->id],
            ['mall', 'userAddress', '用户地址', $userMenu->id],
            ['mall', 'userRequestLog', '用户请求记录', $userMenu->id],
            ['mall', 'userVerifyCode', '验证码记录', $userMenu->id],
        ]);

        $menus = MenuService::create([
            ['module' => 'mall', 'name' => '商品', 'url' => "mall_goods", 'icon' => '',]
        ], $mallMenu->id);
        $goodsMenu = $menus[0];
        MenuService::createCurdMenu([
            ['mall', 'goods', '商品管理', $goodsMenu->id],
            ['mall', 'goodsCat', '商品分类', $goodsMenu->id],
            ['mall', 'goodsCollection', '商品集合', $goodsMenu->id],
        ]);

        $menus = MenuService::create([
            ['module' => 'mall', 'name' => '订单', 'url' => "mall_order", 'icon' => '',]
        ], $mallMenu->id);
        $orderMenu = $menus[0];
        MenuService::createCurdMenu([
            ['mall', 'order', '订单管理', $orderMenu->id],
        ]);
    }

    /**
     * @throws \Exception
     */
    public function uninstall()
    {
        MenuService::deleteByModule('mall');
    }

}