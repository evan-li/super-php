<?php

namespace app\mall\api;

use app\common\exception\CommonResponseException;
use app\common\ResCode;
use app\mall\model\User;
use app\mall\model\UserVerifyCode;
use app\mall\service\UserService;
use Exception;
use think\Db;
use think\facade\Cache;
use think\helper\Str;

class Login extends BaseMall
{

    /**
     * 设备号登录
     */
    public function loginByDeviceNo(){

    }

    /**
     * 发送注册邮箱验证码
     * @throws
     */
    public function sendRegisterEmailVerifyCode()
    {
        $this->checkParam([
            'email' => 'require|email',
        ]);
        $email = input('email');
        if(!empty(User::where('email', $email)->find())) {
            throw new CommonResponseException(ResCode::user_exists);
        }

        $verifyCode = UserVerifyCode::send($email, UserVerifyCode::use_type_register);
        return res_ok();
    }

    public function registerByEmail()
    {

        $this->checkParam([
            'email' => 'require|email',
            'password' => 'require',
            're_password' => 'require',
            'verify_Code' => 'require',
        ]);
        $email = input('email');
        $verifyCode = input('verify_code');
        $password = input('password');
        $rePassword = input('re_password');
        $device_no = input('device_no');
        $device_type = input('device_type');
        $nickname = input('nickname', '');
        $avatar = input('avatar', '');
        $birthday = input('birthday', '');
        $gender = input('gender', 0);
        if ($password != $rePassword) {
            throw new CommonResponseException(ResCode::param_invalid);
        }
        if (!UserVerifyCode::check($email, $verifyCode, UserVerifyCode::use_type_register)){
            throw new CommonResponseException(ResCode::email_verify_code_error);
        }

        $user = UserService::registerByEmail($email, input('password'), [
            'device_no' => $device_no,
            'device_type' => $device_type,
            'nickname' => $nickname,
            'avatar' => $avatar,
            'birthday' => $birthday,
            'gender' => $gender,
        ]);
        $token = UserService::saveToken($user);

        return res_ok([
            'user' => $user,
            'token' => $token,
        ]);
    }

    /**
     * 邮箱登录
     * @throws \Exception
     */
    public function loginByEmail()
    {
        $this->checkParam([
            'email' => 'require|email',
            'password' => 'require',
        ]);
        $user = UserService::loginByEmail(input('email'), input('password'), input('device_no'));
        $token = UserService::saveToken($user);
        return res_ok([
            'user' => $user,
            'token' => $token,
        ]);
    }

    /**
     * 发送重置密码邮箱验证码
     * @return \think\response\Json
     * @throws
     */
    public function sendResetPasswordVerifyCode()
    {
        $this->checkParam([
            'email' => 'require|email',
        ]);
        $email = input('email');
        if(empty(User::where('email', $email)->find())) {
            throw new CommonResponseException(ResCode::un_user);
        }
        UserVerifyCode::send($email, UserVerifyCode::use_type_reset_password);
        return res_ok();
    }

    /**
     * 验证验证码, 返回 verify_token
     */
    public function checkVerifyCode()
    {
        $this->checkParam([
            'email' => 'require|email',
            'verify_code' => 'require',
        ]);
        $email = input('email');
        $verify_code = input('verify_code');
        $res = UserVerifyCode::check($email, $verify_code, UserVerifyCode::use_type_reset_password);
        if(!$res) {
            throw new CommonResponseException(ResCode::email_verify_code_error);
        }
        // 生成 verify_token, 缓存24小时
        $verify_token = Str::random(16);
        Cache::set('verify_token:' . $email, $verify_token, 86400);
        return res_ok([
            'verify_token' => $verify_token
        ]);
    }

    public function resetPassword()
    {
        $this->checkParam([
            'email' => 'require|email',
            'password' => 'require',
        ]);
        $email = input('email');
        $password = input('password');
        $verify_token = input('verify_token');
        $verify_code = input('verify_code');
        // verify token验证方式
        if (!empty($verify_token)) {
            if(Cache::get('verify_token:' . $email) != $verify_token) {
                throw new CommonResponseException(ResCode::param_invalid);
            }
            // 清空verify_token
            Cache::rm('verify_token:' . $email);
        }else if (!empty($verify_code)) {
            // verify code验证方式
            if(!UserVerifyCode::check($email, $verify_code, UserVerifyCode::use_type_reset_password)) {
                throw new CommonResponseException(ResCode::email_verify_code_error);
            }
        }else {
            throw new CommonResponseException(ResCode::param_invalid);
        }

        UserService::resetPasswordByEmail($email, $password);
        return res_ok();
    }

    /**
     * firebase登录
     * @throws Exception
     */
    public function loginByFirebase()
    {
        // avatar, nickname, birthday, gender
        $this->checkParam([
            'firebase_uid' => 'require',
        ]);
        $firebase_uid = input('firebase_uid');
        $device_no = input('device_no');
        $device_type = input('device_type');
        $nickname = input('nickname', '');
        $avatar = input('avatar', '');
        $birthday = input('birthday', '') ?: '2000-01-01';
        $gender = input('gender', 0);
        $email = input('email', '');
        $login_type = input('login_type');

        $user = UserService::loginByFirebase($firebase_uid, [
            'device_no' => $device_no,
            'device_type' => $device_type,
            'email' => $email,
            'nickname' => $nickname,
            'avatar' => $avatar,
            'birthday' => $birthday,
            'gender' => $gender,
            'login_type' => $login_type,
        ]);
        $token = UserService::saveToken($user);

        return res_ok([
            'user' => $user,
            'token' => $token,
        ]);
    }

    /**
     * 谷歌登录
     */
    public function loginByGoogle()
    {
        throw new CommonResponseException('暂未支持');
    }

    /**
     * 苹果登录
     * @throws Exception
     */
    public function loginByApple()
    {
        $this->checkParam([
            'apple_openid' => 'require',
        ]);
        $apple_openid = input('apple_openid');
        $device_no = input('device_no');
        $device_type = input('device_type');
        $nickname = input('nickname', '');
        $avatar = input('avatar', '');
        $birthday = input('birthday', '') ?: '2000-01-01';
        $gender = input('gender', 0);
        $email = input('email', '');

        Db::startTrans();
        try {
            $user = UserService::loginByApple($apple_openid, [
                'device_no' => $device_no,
                'device_type' => $device_type,
                'email' => $email,
                'nickname' => $nickname,
                'avatar' => $avatar,
                'birthday' => $birthday,
                'gender' => $gender,
            ]);
            // 生成并保存token
            $token = UserService::saveToken($user);
            Db::commit();
        }catch (Exception $e) {
            Db::rollback();
            throw $e;
        }
        return res_ok([
            'user' => $user,
            'token' => $token,
        ]);
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        UserService::logout($this->token);
        return res_ok();
    }

}