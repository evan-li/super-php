<?php

namespace app\mall\api;

class GoodsCollection
{

    public function getList()
    {
        $data = \app\mall\model\GoodsCollection::paginate(input('page_size'));
        return res_ok($data);
    }

}