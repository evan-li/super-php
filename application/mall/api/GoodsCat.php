<?php

namespace app\mall\api;

class GoodsCat extends BaseMall
{

    public function tree()
    {
        $tree = \app\mall\model\GoodsCat::getEnableTree();
        return res_ok($tree);
    }

}