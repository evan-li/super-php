<?php

namespace app\mall\api;

class CountryArea extends BaseMall
{

    /**
     * 根据子地区列表
     */
    public function getListByParentCode()
    {
        $code = input('code', 0);
        $parent = \app\mall\model\CountryArea::where('code', $code)->find();
        if (empty($parent)) {
            $parentId = 0;
        }else {
            $parentId = $parent->id;
        }
        $list = \app\mall\model\CountryArea::where('parentid', $parentId)->select();
        foreach ($list as $item) {
            $item->name = $item->country ?: $item->state ?: $item->city ?: $item->region;
        }
        return res_ok([
            'list' => $list
        ]);
    }

}