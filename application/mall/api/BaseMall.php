<?php

namespace app\mall\api;

use app\common\exception\CommonResponseException;
use app\common\ResCode;
use app\mall\model\User;
use app\mall\service\UserService;
use evan\sbuilder\controller\Base;
use think\facade\Cache;
use think\helper\Str;

class BaseMall extends Base
{

    /** @var string 当前登录的token */
    protected $token;
    /** 当前登录的用户信息 @var User  */
    protected $currentUser;

    protected function initialize()
    {
        parent::initialize();

        $this->checkToken();
    }

    /**
     * 验证登陆, 并给currentUser属性赋值
     */
    protected function checkToken()
    {
        $this->token = $this->request->param('token');
        if(empty($this->token)) {
            return false;
        }
        $user = Cache::get('user_token:' . $this->token);
        if(empty($user)) {
            // 如果token失效, 判断当前token是否为游客
            if(Str::startsWith($this->token, 'visitor:')) {
                $user = User::where('device_no', $this->request->param('device_no'))
                    ->where('is_visitor', 1)
                    ->find();
                if (!empty($user)) { // 是游客, 重新设置一下token
                    $this->token = UserService::saveToken($user);
                }
            }
        }
        if(!empty($user)) {
            UserService::updateToken($this->token, $this->currentUser);
            $this->currentUser = $user;
            return true;
        }
        return false;
    }

    /**
     * 检查是否是真正用户登录
     * @return bool
     */
    public function checkLogin()
    {
        return !empty($this->currentUser) && $this->currentUser->is_visitor == 0;
    }

    /**
     * 必须登录
     */
    public function needLogin()
    {
        if(!$this->checkLogin()) {
            throw new CommonResponseException(ResCode::un_login);
        }
    }

}