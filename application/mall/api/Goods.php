<?php

namespace app\mall\api;

use app\common\exception\CommonResponseException;

class Goods extends BaseMall
{
    public function getList()
    {
        $catId1 = input('cat_id_1');
        $catId2 = input('cat_id_2');
        $catId3 = input('cat_id_3');
        $keyword = input('keyword');
        $order = input('order', 'id');
        $by = input('by', 'desc');
        $query = new \app\mall\model\Goods();
        if (!empty($catId1)) {
            $query->where('cat_id_1', $catId1);
        }
        if (!empty($catId2)) {
            $query->where('cat_id_2', $catId2);
        }
        if (!empty($catId3)) {
            $query->where('cat_id_3', $catId3);
        }
        if (!empty($keyword)) {
            $query->where('name', 'like', "%$keyword%");
        }
        if (!empty($order)) {
            $query->order($order, $by);
        }
        $data = $query->paginate(input('page_size'));
        return res_ok($data);
    }

    public function detail()
    {
        $id = input('id');
        $goods = \app\mall\model\Goods::find($id);
        if (empty($goods)) {
            throw new CommonResponseException('商品信息不存在');
        }
        return res_ok($goods);
    }

}