<?php

namespace app\mall\service;

use app\common\exception\CommonResponseException;
use app\common\ResCode;
use app\mall\model\Goods;
use app\mall\model\GoodsSku;
use app\mall\model\GoodsSpec;
use app\mall\model\GoodsSpecValue;
use think\Db;

class GoodsService
{

    /**
     * 根据请求参数添加或修改商品, 包含参数验证, 验证不通过抛出 CommonResponseException(ResCode::param_invalid)
     *
     * @param array $goodsData 商品数据, 没有标注必填的都可以为空 格式为: {
     *  cat_id: 分类ID数组或逗号分隔字符串, 分别为1级分类到3级分类, 可以为空, 最多3个元素,
     *  type: 商品类型, 1-商品库 2-团购商品 必填,
     *  desc: 商品描述,
     *  pic: 商品主图,
     *  pics: 商品图片列表,
     *  video: 商品描述,
     *  detail: 商品详情,
     *  has_spec: 是否开启规格,
     *  sale_price: 售价, 必填,
     *  max_sale_price: sku中的最大售价,
     *  market_price,
     *  cost_price,
     *  stock,
     *  sku_info: { sku信息列表, has_spec为1时有
     *      specs: [],
     *      skus: [],
     *  },
     *  shipping_template_id,
     *  status,
     *  sale_count,
     *  group_id: 所属团购ID, 团购商品存在,
     *  origin_goods_id: 源商品ID, 团购商品存在,
     *  supply_price: 供货价, 类型为团购商品需要供货价或佣金比例,
     *  max_supply_price: sku最大供货价,
     *  commission_rate: 佣金比例, 类型为团购商品需要供货价或佣金比例,
     *  commission_fee: 佣金金额, 类型为团购商品需要供货价或佣金比例,
     *  max_commission_fee: sku最大佣金金额,
     *  sort: 排序, 用于团购商品排序,
     * }
     * @param Goods|null $goods 要修改的商品对象,修改商品时需要传
     * @return Goods
     * @throws CommonResponseException
     * @throws \Exception
     */
    public static function saveGoodsWithRequestData($goodsData, $goods = null, $sale_type = null)
    {
        $isAdd = empty($goods);
        // 获取各个参数
        $cat_id = array_get($goodsData, 'cat_id');
        $name = array_get($goodsData, 'name');
        $type = array_get($goodsData, 'type', 1);
        $desc = array_get($goodsData, 'desc', '');
        $pic = array_get($goodsData, 'pic', '');
        $pics = array_get($goodsData, 'pics', '');
        $video = array_get($goodsData, 'video', '');
        $detail = array_get($goodsData, 'detail', '');

        $has_spec = array_get($goodsData, 'has_spec', 0);
        $sale_price = array_get($goodsData, 'sale_price', 0);
        $max_sale_price = array_get($goodsData, 'max_sale_price', 0);
        $market_price = array_get($goodsData, 'market_price', 0);
        $cost_price = array_get($goodsData, 'cost_price', 0);
        $stock = array_get($goodsData, 'stock', Goods::STOCK_UNLIMIT);
        if ($stock === '') $stock = Goods::STOCK_UNLIMIT;
        $sku_info = array_get($goodsData, 'sku_info');

        $shipping_template_id = array_get($goodsData, 'shipping_template_id', 0);
        $status = array_get($goodsData, 'status', 1);
        $sale_count = array_get($goodsData, 'sale_count', 0);

        $group_id = array_get($goodsData, 'group_id', 0);
        $origin_goods_id = array_get($goodsData, 'origin_goods_id', 0);
        $supply_price = array_get($goodsData, 'supply_price', 0);
        $max_supply_price = array_get($goodsData, 'supply_price', 0);
        $commission_rate = array_get($goodsData, 'commission_rate', 0);
        $commission_fee = array_get($goodsData, 'commission_fee', 0);
        $max_commission_fee = array_get($goodsData, 'max_commission_fee', 0);
        $sort = array_get($goodsData, 'sort', 0);

        // 参数验证
        if (empty($name)) {
            throw new CommonResponseException(ResCode::param_invalid, '商品名称不能为空');
        }
        if ($has_spec == 1) {
            if (empty($sku_info['specs'])){
                log_info('sku_info: ', $sku_info);
                $goodsId = empty($goods) ? '' : $goods['id'];
                throw new CommonResponseException(ResCode::param_invalid, "商品[ {$goodsId} ] $name :未识别到有效的规格信息");
            }
            if (empty($sku_info['skus'])) {
                throw new CommonResponseException(ResCode::param_invalid, '未失败到有效的SKU信息');
            }
            $stock = 0;
            foreach ($sku_info['skus'] as $inputSku) {
                if (empty($inputSku['sale_price'])) {
                    throw new CommonResponseException(ResCode::param_invalid, "请填写{$inputSku['key']}售价");
                }
                if (!empty($inputSku['stock']) && $inputSku['stock'] !== Goods::STOCK_UNLIMIT && $inputSku['stock'] < 0 ) {
                    throw new CommonResponseException(ResCode::param_invalid, "{$inputSku['key']}商品库存不能小于0");
                }
                // 计算总库存
                if (!isset($inputSku['stock']) || $inputSku['stock'] === Goods::STOCK_UNLIMIT || $inputSku['stock'] === '') {
                    $stock = Goods::STOCK_UNLIMIT;
                }
                if ($stock !== Goods::STOCK_UNLIMIT) {
                    $stock += $inputSku['stock'];
                }
            }
        }else {
            if (!empty($stock) && $stock !== Goods::STOCK_UNLIMIT && $stock < 0 ) {
                throw new CommonResponseException(ResCode::param_invalid, '商品库存不能小于0');
            }
            if (empty($sale_price)) {
                throw new CommonResponseException(ResCode::param_invalid, '请填写售价');
            }
        }
        // 团购商品
        if ($type == Goods::TYPE_GROUP) {
            if (empty($group_id)) {
                throw new CommonResponseException(ResCode::param_invalid, '商品所属团购ID不能为空');
            }
            if ($has_spec == 1) {
                foreach ($sku_info['skus'] as $item) {
                    if (empty($item['commission_rate']) && empty($item['commission_fee']) && empty($item['supply_price'])
                    ) { // 佣金模式
                        throw new CommonResponseException(ResCode::param_invalid, "请填写 {$item['key']} 商品佣金或供货价");
                    }
                }
            }else {
                if (empty($commission_rate) && empty($commission_fee) && empty($supply_price) ) { // 佣金模式
                    throw new CommonResponseException(ResCode::param_invalid, "请填写商品佣金或供货价");
                }
            }
        }

        Db::startTrans();
        try {
            // 如果是团购商品, 并且没有来源商品ID, 往商品库添加一个商品
            if ($type == Goods::TYPE_GROUP && empty($origin_goods_id)) {
                $libGoodsData = $goodsData;
                $libGoodsData['type'] = Goods::TYPE_LIBRARY;
                // 去掉商品库商品中的group_id/sort/supply_price/max_supply_price/commission_rate/commission_fee/max_commission_fee
                unset($libGoodsData['group_id']);
                unset($libGoodsData['sort']);
                unset($libGoodsData['supply_price']);
                unset($libGoodsData['max_supply_price']);
                unset($libGoodsData['commission_rate']);
                unset($libGoodsData['commission_fee']);
                unset($libGoodsData['max_commission_fee']);
                if ($libGoodsData['has_spec']) {
                    foreach ($libGoodsData['sku_info']['skus'] as &$item) {
                        unset($item['supply_price']);
                        unset($item['commission_rate']);
                        unset($item['commission_fee']);
                    }
                }
                $libGoods = self::saveGoodsWithRequestData($libGoodsData);
                $origin_goods_id = $libGoods->id;
                // 匹配sku的 origin_sku_id
                foreach ($libGoods->skus as $sku) {
                    foreach ($sku_info['skus'] as &$inputSku) {
                        if ($sku->key == $inputSku['key']) {
                            $inputSku['origin_sku_id'] = $sku->id;
                            unset($inputSku['id']);
                        }
                    }
                }
            }

            if (empty($goods)) {
                $goods = new Goods();
            }
            $cat_id = comma_str_to_arr($cat_id);
            $goods->cat_id_1 = $cat_id[0]??0;
            $goods->cat_id_2 = $cat_id[1]??0;
            $goods->cat_id_3 = $cat_id[2]??0;
            $goods->type = $type;
            $goods->name = $name;
            $goods->desc = $desc;
            $goods->pic = $pic;
            $goods->pics = $pics;
            $goods->video = $video;
            $goods->detail = $detail;

            $goods->has_spec = $has_spec;
            $goods->sale_price = $sale_price;
            $goods->max_sale_price = $max_sale_price;
            $goods->market_price = $market_price;
            $goods->cost_price = $cost_price;
            $goods->stock = $stock;
            $goods->status = $status;
            $goods->sale_count = $sale_count;

            // 运费模板处理
            $goods->shipping_template_id = $shipping_template_id;

            $goods->group_id = $group_id;
            $goods->origin_goods_id = $origin_goods_id;
            $goods->supply_price = $supply_price;
            $goods->max_supply_price = $max_supply_price;
            $goods->commission_rate = $commission_rate;
            $goods->commission_fee = $commission_fee;
            $goods->max_commission_fee = $max_commission_fee;
            $goods->sort = $sort;

            $goods->save();

            // 处理sku列表
            if($has_spec == 1){
                // 有规格商品, 处理规格及sku
                $skus = $sku_info['skus'];
                $specs = $sku_info['specs'];
                // 处理规格
                self::progressGoodsSpecs($goods, $specs);
                // 更新商品sku最低价
                $sku_sale_prices = array_column($skus, 'sale_price');
                $goods->sale_price = min($sku_sale_prices);
                $goods->max_sale_price = max($sku_sale_prices);
                $sku_market_price = array_column($skus, 'market_price');
                $goods->market_price = empty($sku_market_price) ? 0 : min($sku_market_price);
                $sku_cost_price = array_column($skus, 'cost_price');
                $goods->cost_price = empty($sku_cost_price) ? 0 : min($sku_cost_price);
                $sku_supply_price = array_column($skus, 'supply_price');
                $goods->supply_price = empty($sku_supply_price) ? 0 : min($sku_supply_price);
                $goods->max_supply_price = empty($sku_supply_price) ? 0 : max($sku_supply_price);
                $sku_commission_fees = array_column($skus, 'commission_fee');
                $goods->commission_fee = empty($sku_commission_fees) ? 0 : min($sku_commission_fees);
                $goods->max_commission_fee = empty($sku_commission_fees) ? 0 : max($sku_commission_fees);
                $goods->save();
                // 处理sku
                self::progressGoodsSkus($goods, $skus);
            }else if (!$isAdd) {
                // 无规格商品, 删除商品原有的规格以及sku信息
                $skus = GoodsSku::where('goods_id', $goods->id)->select();
                foreach ($skus as $sku) {
                    $sku->delete();
                }
                /** @var GoodsSpec $spec */
                $specs = GoodsSpec::where('goods_id', $goods->id)->select();
                foreach ($specs as $spec) {
                    $spec->together('values')->delete();
                }
            }

            Db::commit();
        }catch (\Exception $e) {
            Db::rollback();
            log_exception(($isAdd ? '添加' : '编辑') . '商品失败', $e);
            throw $e;
        }
        return $goods;
    }

    /**
     * 处理商品规格
     * @param $goods Goods
     * @param $specs
     * @return void
     * @throws \Exception
     */
    private static function progressGoodsSpecs(Goods $goods, $specs){
        $updateSpecIds = [];
        foreach ($specs as $spec) {
            if (!empty($spec['id'])) {
                $updateSpecIds[] = $spec['id'];
            }
        }
        $allSpecsIds = GoodsSpec::where('goods_id', $goods->id)->column('id');
        $deleteSpecIds = array_diff($allSpecsIds, $updateSpecIds);
        // 删除需要删除的规格
        if (!empty($deleteSpecIds)) {
            GoodsSpec::where('id', 'in', $deleteSpecIds)->delete();
            GoodsSpecValue::where('spec_id', 'in', $deleteSpecIds)->delete();
        }

        // 循环所有规格并更新或添加
        foreach ($specs as $item) {
            $spec = null;
            if (!empty($item['id'])) {
                $spec = GoodsSpec::where('goods_id', $goods->id)->where('id', $item['id'])->find();
            }
            if (empty($spec)) {
                $spec = new GoodsSpec();
                $spec->goods_id = $goods->id;
            }
            $spec->name = $item['name'];
            $spec->save();
            // 处理需要更新的规格值ID
            $updateValueIds = [];
            foreach ($item['values'] as $value) {
                if (empty($value['value'])) continue;
                if (!empty($value['id'])) {
                    $updateValueIds[] = $value['id'];
                }
            }
            $allSpecValueIds = GoodsSpecValue::where('spec_id', $spec->id)->column('id');
            // 计算需要删除的规格值ID
            $deleteValueIds = array_diff($allSpecValueIds, $updateValueIds);
            // 删除需要删除的规格值
            if (!empty($deleteValueIds)) {
                GoodsSpecValue::where('spec_id', $spec->id)->where('id', 'in', $deleteValueIds)->delete();
            }
            foreach ($item['values'] as $value) {
                // 过滤value为空的元素
                if (empty($value['value'])){
                    continue;
                }
                // 更新或添加
                $valueObj = null;
                if (!empty($value['id'])) {
                    $valueObj = GoodsSpecValue::where('spec_id', $spec->id)->where('id', $value['id'])->find();
                }
                if(empty($valueObj)) {
                    $valueObj = new GoodsSpecValue();
                }
                $valueObj->spec_id = $spec->id;
                $valueObj->value = $value['value'];
                $valueObj->save();
            }
        }
    }

    /**
     * 处理商品sku列表
     * @param Goods $goods
     * @param $skus
     * @return void
     */
    private static function progressGoodsSkus(Goods $goods, $skus)
    {
        // 删除需要删除的规格
        $allSkus = GoodsSku::where('goods_id', $goods->id)->select();
        /** @var GoodsSku $allSku */
        foreach ($allSkus as $allSku) {
            $willDelete = true;
            foreach ($skus as $item) {
                if (isset($item['id']) && $allSku['id'] == $item['id']) {
                    $willDelete = false;
                }
            }
            if ($willDelete) {
                $allSku->delete();
            }
        }

        // 更新或新增sku
        foreach ($skus as $item) {
            $sku = null;
            if (!empty($item['id'])) {
                $sku = GoodsSku::where('goods_id', $goods->id)->where('id', $item['id'])->find();
            }
            if (empty($sku)){
                $sku = new GoodsSku();
                $sku->goods_id = $goods->id;
            }

            $sku->key = $item['key'];
            if (empty($item['specs'])) {
                $sku->name = $item['name'];
            }else {
                $name = [];
                foreach ($item['specs'] as $spec) {
                    $name[] = $spec['name'] . ':' . $spec['value'];
                }
                $sku->name = implode(' ', $name);
            }
            $sku->sale_price = $item['sale_price'];
            $sku->market_price = $item['market_price'] ?? 0;
            $sku->cost_price = $item['cost_price'] ?? 0;
            $sku->supply_price = $item['supply_price'] ?? 0;
            $sku->commission_rate = $item['commission_rate'] ?? 0;
            $sku->commission_fee = $item['commission_fee'] ?? 0;
            $stock = $item['stock'] ?? Goods::STOCK_UNLIMIT;
            if ($stock === '') $stock = Goods::STOCK_UNLIMIT;
            $sku->stock = $stock;
            $sku->save();
        }
    }

}