<?php

namespace app\mall\service;

use app\common\exception\CommonResponseException;
use app\common\ResCode;
use app\mall\model\User;
use Exception;
use think\Db;
use think\facade\Cache;
use think\facade\Hook;
use think\helper\Str;

class UserService
{
    protected static $defaultNamePrefix = 'U';

    /**
     * 使用邮箱登录
     * @throws Exception
     */
    public static function loginByEmail($email, $password, $deviceNo = '')
    {
        $user = User::where('email', $email)->find();
        if(empty($user)) {
            throw new CommonResponseException(ResCode::un_user);
        }
        if(User::hashPassword($password, $user->salt) != $user->password) {
            throw new CommonResponseException(ResCode::password_error);
        }
        return self::login($user, User::login_type_email, $deviceNo);
    }

    /**
     * 使用邮箱注册
     * @param $email
     * @param $password
     * @param $params array {device_no,device_type,nickname, avatar}
     * @return User
     */
    public static function registerByEmail($email, $password, $params = [])
    {
        $user = User::where('email', $email)->find();
        if (!empty($user)) {
            throw new CommonResponseException(ResCode::user_exists);
        }

        // 创建用户信息
        $salt = Str::random();
        $password = User::hashPassword($password, $salt);

        // 用户昵称
        $nickname = $params['nickname'] ?? '';
        if (empty($nickname)) {
            $nickname = substr($email, 0, strpos($email, '@'));
        }

        $user = User::create([
            'email' => $email,
            'password' => $password,
            'salt' => $salt,
            'nickname' => $nickname,
            'avatar' => $params['avatar'] ?? '',
            'gender' => $params['gender'] ?? '',
            'birthday' => $params['birthday'] ?? null,
            'is_visitor' => 0,
            'device_no' => $params['device_no'] ?? '',
            'device_type' => $params['device_type'] ?? 0,
            'last_login_time' => time(),
            'last_login_type' => User::login_type_email,
            'last_launch_time' => time(),
            'last_launch_ip' => request()->ip(),
        ]);
        // 重新获取一次用户信息, 补全用户字段
        $user = User::get($user->id);

        // 用户注册钩子
        Hook::listen('mall_user_register', $user);
        Hook::listen('mall_user_login', $user);

        // 过滤用户输出字段
        $user->hidden(['password', 'salt']);
        return $user;
    }

    /**
     * 使用邮箱登录或注册
     * @param $email
     * @param $password
     * @param $params
     * @return User|mixed
     * @throws Exception
     */
    public static function loginOrRegisterByEmail($email, $password, $params = [])
    {
        $user = User::where('email', $email)->find();
        if(empty($user)) {
            return self::registerByEmail($email, $password, $params);
        }else {
            return self::loginByEmail($email, $password, $params['device_no'] ?? '');
        }
    }

    /**
     * 手机号登录
     * @throws Exception
     */
    public static function loginByMobile($mobile, $password, $deviceNo = '')
    {
        $user = User::where('mobile', $mobile)->find();
        if(empty($user)) {
            throw new CommonResponseException(ResCode::un_user);
        }
        if(User::hashPassword($password, $user->salt) != $user->password) {
            throw new CommonResponseException(ResCode::password_error);
        }
        return self::login($user, User::login_type_mobile, $deviceNo);
    }

    /**
     * 通用登录方法
     * @throws Exception
     */
    public static function login(User $user, $loginType, $deviceNo = '', $deviceType = '')
    {
        Db::startTrans();
        try {
            // 更新用户最后登录时间
            $user->last_login_time = time();
            $user->last_login_type = $loginType;
            $user->last_launch_ip = request()->ip();

            if (!empty($deviceNo)) {
                $user->device_no = $deviceNo;
            }
            if (!empty($deviceType)) {
                $user->device_type = $deviceType;
            }
            $user->save();

            Db::commit();
        }catch (Exception $e) {
            Db::rollback();
            throw $e;
        }
        // 用户登录钩子
        Hook::listen('mall_user_login', $user);

        // 过滤用户输出字段
        $user->hidden(['password', 'salt']);
        return $user;
    }

    /**
     * firebase 登录
     * @param $firebase_uid
     * @param $params
     * @return User
     * @throws Exception
     */
    public static function loginByFirebase($firebase_uid, $params = [])
    {
        $user = User::where('firebase_uid', $firebase_uid)->find();
        if (empty($user)) {
            return self::registerByFirebase($firebase_uid, $params);
        }else {
            // 更新用户最后登录时间
            return self::login($user, User::login_type_firebase, $params['device_no'], $params['device_type']);
        }
    }

    /**
     * firebase 注册
     */
    public static function registerByFirebase($firebase_uid, $params = [])
    {
        return self::registerByThird(User::login_type_firebase, 'firebase_uid', $firebase_uid, $params);
    }

    /**
     * Apple 登录
     * @param $apple_openid
     * @param $params
     * @return User
     * @throws Exception
     */
    public static function loginByApple($apple_openid, $params = [])
    {
        $user = User::where('apple_openid', $apple_openid)->find();
        if (empty($user)) {
            return self::registerByApple($apple_openid, $params);
        }else {
            // 更新用户最后登录时间
            return self::login($user, User::login_type_apple, $params['device_no'], $params['device_type']);
        }
    }

    /**
     * Apple 注册
     * @param $apple_openid
     * @param $params
     * @return User
     * @throws Exception
     */
    public static function registerByApple($apple_openid, $params = [])
    {
        return self::registerByThird(User::login_type_apple, 'apple_openid', $apple_openid, $params);
    }

    /**
     * 第三方授权登录
     * @param $loginType
     * @param $field
     * @param $openid
     * @param array $params
     * @return User
     */
    public static function registerByThird($loginType, $field, $openid, $params = [])
    {
        // 创建用户信息
        $salt = '';
        $password = '';
        if(isset($params['password']) && !empty($params['password'])) { // 密码存在时, 初始化salt并加密密码
            $salt = Str::random();
            $password = User::hashPassword($params['password'], $salt);
        }
        $nickname = $params['nickname'] ?? '';
        if(empty($nickname) ) { // 默认昵称
            if(!empty($params['email'])) {
                $nickname = substr($params['email'], 0, strpos($params['email'], '@'));
            }else {
                $nickname = self::$defaultNamePrefix . rand(10000000, 99999999);
            }
        }
        $user = User::create([
            'device_no' => $params['device_no'],
            'device_type' => $params['device_type'],
            $field => $openid,
            'salt' => $salt,
            'password' => $password,
            'email' => $params['email'] ?? '',
            'avatar' => $params['avatar'] ?? '',
            'nickname' => $nickname,
            'birthday' => $params['birthday'] ?? '2020-01-01',
            'gender' => $params['gender'] ?? 0,
            'last_login_time' => time(),
            'last_login_type' => $loginType,
            'last_launch_time' => time(),
            'last_launch_ip' => request()->ip(),
        ]);
        // 重新获取一次用户信息, 补全用户字段
        $user = User::get($user->id);

        // 用户注册钩子
        Hook::listen('mall_user_register', $user);
        Hook::listen('mall_user_login', $user);

        // 过滤用户输出字段
        $user->hidden(['password', 'salt']);
        return $user;
    }

    public static function saveToken(User $user)
    {
        // 生成token
        $token = Str::random(32);
        // 清楚其他登录信息
        $beforeToken = Cache::get('user_id_for_token:' . $user->id);
        Cache::rm('token_for_user:' . $beforeToken);
        // 缓存用户信息, 缓存两天
        $expire = config('user.login_expire_time', 3600 * 48);
        Cache::set('token_for_user:' . $token, $user, $expire);
        Cache::set('user_id_for_token:' . $user->id, $token, $expire);
        return $token;
    }

    public static function updateToken($token, User $user)
    {
        // 缓存用户信息, 缓存两天
        $expire = config('user.login_expire_time', 3600 * 48);
        Cache::set('token_for_user:' . $token, $user, $expire);
        Cache::set('user_id_for_token:' . $user->id, $token, $expire);
        return $token;
    }

    public static function logout($token)
    {
        $user = Cache::get('token_for_user:' . $token);
        if(!empty($user)) {
            Cache::rm('token_for_user:' . $token);
            Cache::rm('user_id_for_token:' . $user->id);
        }
    }


    public static function resetPasswordByEmail($email, $password)
    {
        $user = User::where('email', $email)->find();
        if(empty($user)) {
            throw new CommonResponseException(ResCode::un_user);
        }
        // 创建用户信息
        $salt = Str::random();
        $password = User::hashPassword($password, $salt);
        // 重设密码
        $user->salt = $salt;
        $user->password = $password;
        $user->save();
    }


}