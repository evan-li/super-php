<?php

namespace app\mall\admin;

use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;

class User extends Admin
{

    public function index()
    {
        $defaultField = input('field', 'id');
        $defaultKeyword = input('keyword', '');
        return SBuilder::makeTable()
            ->setPageTitle('用户列表')
            ->setNavTab([
//                ['user', '用户列表', url('index')],
//                ['user_stat', '用户统计', url('userStat')],
//                ['user_back', '用户回归统计', url('totalStat')],
            ])
//            ->setNavTabCurrent('user')
            ->addColumns([
                ['id', 'ID'],
                ['nickname', '昵称'],
//                ['account', '账号'],
                ['email', '邮箱'],
                ['sex', '性别', '', ['未知', '男', '女']],
//                ['birthday', '生日'],
                ['is_visitor', '访客', 'status', [1 => '访客:info', 0 => '正常:success']],
                ['device_type', '设备类型', '', [1 => '安卓', 2 => 'ios']],
//                ['user_extend.device_type', '设备类型', '', [1 => '安卓', 2 => 'ios']],
                ['user_extend.device_model', '设备型号'],
//                ['user_extend.idfa', 'IDFA'],
                ['user_extend.push_token', '推送token'],
                ['region_code', '地区码'],
//                ['origin_book_name', '来源书籍', 'link', [
//                    'type' => 'pop',
//                    'url' => url('book/detail', ['id' => '__origin_book_id__'])
//                ]],
                ['last_login_type', '登录方式', '', [0 => '', 1 => '谷歌', 2 => 'Facebook', 3 => '邮箱', 4 => 'Apple']],
                ['last_login_time', '登录时间', 'datetime'],
                ['last_launch_time', '启动时间', 'datetime'],
                ['last_launch_ip', '启动IP', ''],
                ['create_time', '注册时间', 'datetime'],
                ['__btn__', '', '', '', '', ['fixed' => 'right']],
            ])
            ->setColumnWidth('last_login_time,last_launch_time,create_time', 140)
            ->setColumnWidth('nickname,birthday', 100)
            ->setColumnWidth('region_code', 70)
            ->setColumnWidth('id,last_login_type', 80)
            ->setColumnWidth('last_launch_ip', 180)
            ->setColumnWidth('email,account', 150)
            ->setColumnWidth('__btn__', 150)
            ->addTopSelect('is_visitor', '是否访客', [0 => '正常用户', 1 => '访客'])
            ->addTopSelect('sex', '性别', [0 => '未知', 1 => '男', 2 => '女'])
            ->addTopSelect('device_type', '设备类型', [1 => '安卓', 2 => 'ios'])
            ->addTopSelect('last_login_type', '登录方式', [1 => '谷歌', 2 => 'Facebook', 3 => '邮箱', 4 => 'Apple'])
            ->setSearch('id|ID|=,nickname|昵称,email|邮箱|=,account|账号|=', '', [$defaultField => $defaultKeyword])
            ->addTimeFilter('last_login_time', '最后登录时间')
            ->addTimeFilter('last_launch_time', '最后启动时间')
            ->showPageSize()
            ->addRightButton('recharges', '充值记录', url('UserRecharge/index'), 'pop', ['extra_data' => ['user_id' => '__id__']])
            ->addRightButton('sendMsg', '发送消息', url('msg/SystemMsg/sendToUser'), 'pop', ['extra_data' => ['user_id' => '__id__']])
            ->addRightButton('push', '推送', url('msg/push/pushToUser'), 'pop', ['extra_data' => ['user_id' => '__id__']])
            ->addRightButton('readRecord', '阅读记录', url('readRecord'), 'pop')
            ->fetch();
    }

}