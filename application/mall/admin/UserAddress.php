<?php

namespace app\mall\admin;

use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;

class UserAddress extends Admin
{

    public function index()
    {
        return SBuilder::makeTable()
            ->setPageTitle('用户地址')
            ->setSearch('user_id|用户ID')
            ->addColumns([
                ['user_id', '用户ID'],
                ['country_code', '国家编码'],
                ['name', '收货人姓名'],
                ['mobile', '收货人手机号'],
                ['address', '地址'],
                ['is_default', '默认', 'yesno'],
                ['__btn__']
            ])
            ->setColumnWidth('user_id,country_code,name', 150)
            ->setColumnWidth('mobile', 200)
            ->setColumnWidth('is_default', 150)
            ->setColumnWidth('__btn__', 150)
            ->fetch();
    }

    public function getList()
    {
        $data = \app\mall\model\UserAddress::where($this->getWhere())
            ->order($this->getOrder())
            ->paginate(input('page_size'));
        foreach ($data as $item) {
            $item->address = "$item->province $item->city $item->district<br>$item->detail<br>$item->detail2";
        }
        return admin_data($data);
    }

}