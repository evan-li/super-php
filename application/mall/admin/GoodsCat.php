<?php


namespace app\mall\admin;


use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;

class GoodsCat extends Admin
{

    public function index()
    {
        return SBuilder::make('table')
            ->setPageTitle('商品分类')
            ->addTopButton('add', '', '', 'pop')
            ->addTopButton('enable', '', 'shop/goods_cat/batch')
            ->addTopButton('disable', '', 'shop/goods_cat/batch')
            ->addColumns([
                ['__checkbox__'],
                ['id', 'ID'],
                ['name', '名称'],
                ['status', '状态', 'status'],
                ['create_time', '创建时间', 'datetime'],
                ['__btn__']
            ])
            ->addRightButton('enable', '', 'shop/goods_cat/enable')
            ->addRightButton('disable', '', 'shop/goods_cat/disable')
            ->addRightButton('edit', '', '', 'pop')
            ->addRightButton('add_sub', '添加子分类', url('add'), 'pop', [
                'icon' => 'el-icon-plus',
                'extra_data' => ['pid' => '__id__'],
            ])
            ->setColumnWidth(['id' => '100'])
            ->fetch();
    }


    public function getList()
    {
        $list = \app\mall\model\GoodsCat::getTree();
        return admin_data($list);
    }

    /**
     * 添加分类
     * @return mixed
     * @throws \think\Exception
     */
    public function add()
    {
        if (is_page()) {
            $pid = $this->request->param('pid', '');
            $parentTree = \app\mall\model\GoodsCat::getTopLevel2Tree();
            return SBuilder::make('form')
                ->setPageTitle('新增分类')
                ->addFormItems([
                    ['pid', '父分类|顶级分类', 'cascader', $parentTree, '', $pid, ['select_all_levels' => true]],
                    ['name', '分类名'],
                    ['status', '状态', 'status'],
                ])
                ->fetch();
        }
        $result = $this->validate($this->request->param(), [
            'name|分类名' => 'require',
        ]);
        if ($result !== true) {
            return admin_error($result);
        }
        $pid = $this->request->param('pid', 0);
        $name = $this->request->param('name');
        $status = $this->request->param('status', 1);

        $tier = 1;
        if (!empty($pid)) {
            $parent = \app\mall\model\GoodsCat::get($pid);
            if (!empty($parent)) {
                $tier = $parent->tier + 1;
            }
        }
        $cat = new \app\mall\model\GoodsCat();
        $cat->pid = $pid;
        $cat->name = $name;
        $cat->tier = $tier;
        $cat->status = $status;
        $cat->save();
        return admin_success('添加成功', '__relist__');
    }

    /**
     * 编辑分类
     * @throws \think\Exception
     */
    public function edit()
    {
        $id = $this->request->param('id');
        if(empty($id)){
            return admin_error('缺少主键');
        }
        $cat = \app\mall\model\GoodsCat::get($id);
        if (empty($cat)) {
            return admin_error('数据不存在，主键ID：' . $id);
        }
        if (is_page()) {
            $parentTree = \app\mall\model\GoodsCat::getTopLevel2Tree();
            return SBuilder::make('form')
                ->setPageTitle('编辑分类')
                ->addFormItems([
                    ['pid', '父分类|顶级分类', 'cascader', $parentTree, '', '', ['select_all_levels' => true]],
                    ['name', '分类名'],
                    ['status', '状态', 'status'],
                ])
                ->setFormData($cat)
                ->fetch();
        }

        $result = $this->validate($this->request->param(), [
            'name|分类名' => 'require',
        ]);
        if ($result !== true) {
            return admin_error($result);
        }
        $pid = $this->request->param('pid', 0);
        $name = $this->request->param('name');
        $status = $this->request->param('status', 1);

        $tier = 1;
        if (!empty($pid)) {
            $parent = \app\mall\model\GoodsCat::get($pid);
            if (!empty($parent)) {
                $tier = $parent->tier + 1;
            }
        }
        $cat->pid = $pid;
        $cat->name = $name;
        $cat->tier = $tier;
        $cat->status = $status;
        $cat->save();
        return admin_success('修改成功', '__relist__');
    }

    public function enable()
    {
        $id = $this->request->param('id');
        if (empty($id)) return admin_error('缺少主键');
        $cat = \app\mall\model\GoodsCat::get($id);
        if (empty($cat)) {
            return admin_error('数据不存在，主键ID：' . $id);
        }
        $cat->status = \app\mall\model\GoodsCat::statusYes;
        $cat->save();
        return admin_success('已启用', '__relist__');
    }

    public function disable()
    {
        $id = $this->request->param('id');
        if (empty($id)) return admin_error('缺少主键');
        $cat = \app\mall\model\GoodsCat::get($id);
        if (empty($cat)) {
            return admin_error('数据不存在，主键ID：' . $id);
        }
        $cat->status = \app\mall\model\GoodsCat::statusNo;
        $cat->save();
        return admin_success('已禁用', '__relist__');
    }

    public function batch()
    {
        $type = $this->request->param('type');
        $ids = $this->request->param('ids');
        foreach ($ids as $k => $v) {
            $cat = \app\mall\model\GoodsCat::find($v);
            if (empty($cat)) {
                return admin_error('数据不存在，主键ID：' . $v);
            }
            if ($type == 'enable') {
                $cat->status = \app\mall\model\GoodsCat::statusYes;
                $cat->save();
            } elseif ($type == 'disable'){
                $cat->status = \app\mall\model\GoodsCat::statusNo;
                $cat->save();
            }
        }
        return admin_success('批量修改成功', '__relist__');
    }
}


