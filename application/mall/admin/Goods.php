<?php


namespace app\mall\admin;


use app\mall\model\Goods as GoodsModel;
use app\mall\model\GoodsCat;
use app\mall\model\GoodsSpec;
use app\mall\model\GoodsSpecValue;
use app\mall\service\GoodsService;
use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;
use think\Db;
use think\db\Query;
use think\Exception;

class Goods extends Admin
{

    public function index()
    {
        return SBuilder::makeTable()
            ->setPageTitle('商品库')
            ->addTopButton('add', '添加商品', '', 'link')
            ->addTopButton('enable', '批量上架')
            ->addTopButton('disable', '批量下架')
            ->addColumns([
                ['__checkbox__'],
                ['id', 'ID'],
                ['name', '商品名'],
                ['cat_name', '所属分类',],
                ['pics', '主图', 'image'],
                ['sale_price', '售价',],
                ['market_price', '市场价',],
                ['cost_price', '成本价',],
                ['sale_count', '销量'],
                ['stock_text', '库存', '', ['-1' => '无限']],
                ['status', '状态', 'status', ['下架', '上架']],
                ['create_time', '添加时间', 'datetime'],
                ['__btn__'],
            ])
            ->setColumnWidth([
                'id,pics,sale_price,market_price,cost_price,sale_count,stock,status' => 80,
                'cat_name' => 100,
                'create_time,__btn__' => 150,
            ])
//            ->addTopSelect('type', '商品类型', [1 => '商品库', 2 => '团购商品'])
            ->setSearch('id|ID|=,name|商品名')
            ->setRowListUrl(url('getList'))
            ->addRightButton('edit', '', '', 'link')
            ->addRightButton('enable')
            ->addRightButton('disable')
            ->addRightButton('delete')
            ->fetch();
    }

    public function getList()
    {
        $type = input('type');
        $min_price = input('min_price');
        $max_price = input('max_price');
        $cat_id = input('cat_id');
        $keyword = input('keyword');
        $withSpec = input('with_spec');
        $query = GoodsModel::where($this->getWhere())->order($this->getOrder());
        $with = ['skus'];
        if ($withSpec){
            $with[] = 'specs.values';
        }
        $query->with($with);

        if (!empty($type)) {
            $query->where('type', $type);
        }
        if (!empty($min_price)) {
            $query->where('sale_price', '>=', $min_price);
        }
        if (!empty($max_price)) {
            $query->where('sale_price', '<=', $max_price);
        }
        if (!empty($cat_id)) {
            $query->where(function(Query $query) use ($cat_id) {
                $query->where('cat_id_1', $cat_id)
                    ->whereOr('cat_id_2', $cat_id)
                    ->whereOr('cat_id_3', $cat_id);
            });
        }
        if (!empty($keyword)) {
            $query->where('name', 'like', "%$keyword%");
        }
        $data = $query->paginate(input('page_size'));
        $list = $data->items();
        $catIds = array_merge(
            array_column($list, 'cat_id_1'),
            array_column($list, 'cat_id_2'),
            array_column($list, 'cat_id_3')
        );
        $catNameMapping = GoodsCat::where('id', 'in', $catIds)->column('name', 'id');
        /** @var GoodsModel $item */
        foreach ($data as $item) {
            $catNames = [];
            if ($item->cat_id_1) {
                $catNames[] = $catNameMapping[$item->cat_id_1];
            }
            if ($item->cat_id_2) {
                $catNames[] = $catNameMapping[$item->cat_id_2];
            }
            if ($item->cat_id_3) {
                $catNames[] = $catNameMapping[$item->cat_id_3];
            }
            $item->cat_name = implode('/', $catNames);
            $item->pics = GoodsModel::getPicsWithDefault($item);
            $item->stock = GoodsModel::getTotalStock($item);
            $item->stock_text = $item->stock === GoodsModel::STOCK_UNLIMIT ? '无限' : $item->stock;

        }
        return admin_data($data);
    }

    /**
     * 添加商品
     * @return mixed
     * @throws \Exception
     */
    public function add()
    {
        if(is_page()){
            $catTree = GoodsCat::getTree();
//            return SBuilder::make('goods_form', ['span' => 18])
            return SBuilder::makeForm(['span' => 24])
                ->setPageTitle('添加商品')
                ->addFormItems([
                    ['base_info', '基本信息', 'separate_label'],
                    ['name:require', '商品名称', '', '', '', ['max' => 120, 'show_word_limit' => true]],
                    ['cat_id', '分类', 'cascader', $catTree, '', '', ['need_all_levels' => true]],
//                    ['type', '类型', 'select', [1 => '普通商品'], '', '1'],
                    ['desc', '商品描述', 'textarea', '', '', ['max' => 2000]],
//                    ['pic', '商品主图', 'image', '', '', ['width' => 375, 'height' => 375]],
                    ['pics', '商品图片', 'images', '建议尺寸：750x750像素，开团默认显示第一张图片，最多可添加9张', '', ['drag' => true]],
                    ['video', '商品视频', 'video', ],
//                    ['detail', '商品详情|请输入商品详情', 'mp_editor', '', '', ['image_drag' => true]],
                    ['detail', '商品详情', 'ueditor'],

                    ['price_info', '价格与库存', 'separate_label'],
                    ['has_spec', '商品规格', 'switch', '', '', ['active_text' => '开启商品规格']],
                    ['sku_info', '商品规格', 'goods_sku'],
                    ['sale_price:require', '售价', '', '', '', [
                        'type' => 'number', 'prepend' => '$', 'width' => '200px'
                    ]],
                    ['market_price', '市场价', '', '', '', [
                        'type' => 'number', 'prepend' => '$', 'width' => '200px'
                    ]],
                    ['cost_price', '成本价', '', '用于利润核算, 仅后台可见', '', [
                        'type' => 'number', 'prepend' => '$', 'width' => '200px'
                    ]],
                    ['stock', '库存|库存不填默认不限', '', '', '', [
                        'type' => 'number', 'width' => '200px', 'min' => '0',
                    ]],
                    ['shipping_template_id', '运费模板', 'goods_shipping_template'],
                    ['status', '状态', 'status'],
                    ['sale_count', '已售数量', 'number', '已售数量的起始值, 后续销售数量将在此基础上累加'],
                ])
                ->setTrigger('sku_info', 'has_spec', [1])
                ->setTrigger('sale_price,market_price,cost_price,supply_price,stock', 'has_spec', 2)
                ->fetch();
        }else {
            // 添加商品操作
            GoodsService::saveGoodsWithRequestData(input('param.'));

            return admin_success('商品添加成功');
        }
    }

    /**
     * 编辑商品
     * @return mixed
     * @throws Exception
     */
    public function edit()
    {
        $id = input('id');
        if(empty($id)) return admin_error('主键ID不能为空');

        if(is_page()){
            $goods = GoodsModel::where('id', $id)->find();
            if(empty($goods)) return admin_error('商品信息不存在');

            $goods->sku_info = [
                'skus' => $goods->skus,
                'specs' => $goods->specs()->with('values')->select(),
            ];
            $catTree = GoodsCat::getTree();
            return SBuilder::makeForm(['span' => 24])
                ->setPageTitle('修改商品')
                ->addFormItems([
                    ['base_info', '基本信息', 'separate_label'],
                    ['name:require', '商品名称', '', '', '', ['max' => 120, 'show_word_limit' => true]],
                    ['cat_id', '分类', 'cascader', $catTree, '', '', ['need_all_levels' => true]],
                    ['type', '类型', 'select', [1 => '普通商品'], '', '1'],
                    ['desc', '商品描述', 'textarea', '', '', ['max' => 2000]],
//                    ['pic', '商品主图', 'image', '', '', ['width' => 375, 'height' => 375]],
                    ['pics', '商品图片', 'images', '建议尺寸：750x750像素，开团默认显示第一张图片，最多可添加9张', '', ['drag' => true]],
                    ['video', '商品视频', 'video', ],
                    ['detail', '商品详情|请输入商品详情', 'ueditor', '', '', ['image_drag' => true]],

                    ['price_info', '价格与库存', 'separate_label'],

                    ['has_spec', '商品规格', 'switch', '', '', ['active_text' => '开启商品规格']],
                    ['sku_info', '商品规格', 'goods_sku'],
                    ['sale_price:require', '售价', '', '', '', [
                        'type' => 'number', 'prepend' => '$', 'width' => '200px'
                    ]],
                    ['market_price', '市场价', '', '', '', [
                        'type' => 'number', 'prepend' => '$', 'width' => '200px'
                    ]],
                    ['cost_price', '成本价', '', '用于利润核算, 仅后台可见', '', [
                        'type' => 'number', 'prepend' => '$', 'width' => '200px'
                    ]],
//                    ['supply_price', '供货价', 'number', '用于帮卖结算时, 以此价格为帮卖团长结算'],
                    ['stock', '库存|库存不填默认不限', '', '', '', [
                        'type' => 'number', 'width' => '200px', 'min' => '0',
                    ]],

                    ['shipping_template_id', '运费模板', 'goods_shipping_template'],
//                    ['contribution_type:require', '贡献值计算方式', 'select', [1 => '自动计算', 2 => '手动输入'], '设置为自动计算后, 将会根据商品利润自动计算贡献值', '2'],
//                    ['contribution', '贡献值', 'number', '用户购买商品后所获得的贡献值'],
                    ['status', '状态', 'status'],
                    ['sale_count', '已售数量', 'number', '已售数量的起始值, 后续销售数量将在此基础上累加'],
                ])
                ->setTrigger('sku_info', 'has_spec', 1)
                ->setTrigger('sale_price,market_price,cost_price,supply_price,stock', 'has_spec', 0)
                ->setFormData($goods)
                ->fetch();
        }else {
            $goods = GoodsModel::where('id', $id)->find();
            if(empty($goods)) return admin_error('商品信息不存在');
            // 修改商品操作
            GoodsService::saveGoodsWithRequestData(input('param.'), $goods);

            return admin_success('商品编辑成功');
        }
    }

    /**
     * 删除商品, 需要关联删除sku
     */
    public function delete()
    {
        $id = input('id');
        if (empty($id)){
            return admin_error('缺少主键');
        }
        $goods = GoodsModel::get($id);
        if(empty($goods)){
            return admin_error('商品信息不存在');
        }
        // 删除商品下的sku列表
        foreach ($goods->skus as $sku) {
            $sku->delete();
        }
        $goods->delete();
        $this->success('删除成功', '__relist__');
    }

    /**
     * 根据商品分类ID获取商品规格列表
     */
    public function getSpecsByCatId()
    {
        $catId = $this->request->param('catId', 0);
//        if(empty($catId)){
//            return admin_error('分类ID不能为空');
//        }
        $list = GoodsSpec::where('goods_cat_id', $catId)->with('values')->select();
        return admin_data(['list' => $list]);
    }

    /**
     * 根据分类ID保存商品规格
     */
    public function saveSpecByCatId()
    {
        $result = $this->validate($this->request->param(), [
            'id' => 'integer|min:1',
            'goods_cat_id' => 'integer',
            'name' => 'require'
        ]);
        if($result !== true){
            return admin_error($result);
        }
        $catId = $this->request->param('goods_cat_id', 0);
        $name = $this->request->param('name');
        $id = $this->request->param('id');
        // 如果ID为空,则为新增, 否则为更新
        if(empty($id)){
            $spec = new GoodsSpec();
        }else {
            $spec = GoodsSpec::getOrFail($id);
        }
        $spec->goods_cat_id = $catId;
        $spec->name = $name;
        $spec->save();
        return admin_data($spec);
    }

    /**
     * 删除规格
     * @throws \Exception
     */
    public function deleteSpec()
    {
        $result = $this->validate($this->request->param(), [
            'id' => 'require|integer|min:1',
        ]);
        if($result !== true){
            return admin_error($result);
        }
        $id = $this->request->param('id');
        $spec = GoodsSpec::get($id);
        $spec->together('values')->delete();
        return admin_success();
    }

    /**
     * 保存属性值
     */
    public function saveSpecValue()
    {
        $result = $this->validate($this->request->param(), [
            'id' => 'integer|min:1',
            'spec_id' => 'require|integer|min:1',
            'value' => 'require'
        ]);
        if($result !== true){
            return admin_error($result);
        }
        $id = $this->request->param('id');
        $specId = $this->request->param('spec_id');
        $value = $this->request->param('value');
        // 如果id为空, 则为新增, 否则为更新
        if(empty($id)){
            $specValue = new GoodsSpecValue();
        }else {
            $specValue = GoodsSpecValue::getOrFail($id);
        }
        $specValue->spec_id = $specId;
        $specValue->value = $value;
        $specValue->save();
        return admin_data($specValue);
    }

    /**
     * 删除属性值
     * @throws \Exception
     */
    public function deleteSpecValue()
    {
        $result = $this->validate($this->request->param(), [
            'id' => 'require|integer|min:1',
        ]);
        if($result !== true){
            return admin_error($result);
        }
        $id = $this->request->param('id');
        $specValue = GoodsSpecValue::get($id);
        $specValue->delete();
        return admin_success();
    }
}