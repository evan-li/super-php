<?php

namespace app\mall\admin;

use app\mall\model\GoodsCollectionItem;
use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;

class GoodsCollection extends Admin
{

    public function index()
    {
        $fields = [
            ['name', '名称'],
            ['code', '编码'],
            ['pic', '图片', 'image'],
            ['remark', '备注'],
            ['sort', '排序', 'number'],
            ['status', '状态', 'status'],
        ];
        return SBuilder::makeTable()
            ->setPageTitle('商品集合')
            ->addColumns([
                ['id', 'ID'],
                ['name', '名称'],
                ['code', '代码'],
                ['sort', '排序'],
                ['pic', '图片', 'image'],
                ['status', '状态', 'status'],
                ['remark', '备注'],
                ['create_time', '创建时间', 'datetime'],
                ['__btn__'],
            ])
            ->autoAdd($fields)
            ->autoEdit($fields)
            ->setSearch('name|名称')
            ->addRightButton('items', '商品列表', url('items', ['collectionId' => '__id__']))
            ->addRightButton('delete')
            ->setColumnWidth('id,sort', 60)
            ->setColumnWidth('pic,status', 100)
            ->fetch();
    }

    public function add()
    {
        if (is_page()) {
            return SBuilder::makeForm()
                ->addFormItems([
                    ['name', '名称'],
                    ['code', '编码'],
                    ['pic', '图片', 'image'],
                    ['remark', '备注'],
                    ['sort', '排序', 'number'],
                    ['status', '状态', 'status'],
                ])
                ->fetch();
        }else {
            $params = input('param.');
            $code = input('code', '');
            if (empty($code)) {
                $code = str_replace(' ', '-', strtolower(input('name', '')));
            }
            $params['code'] = $code;
            \app\mall\model\GoodsCollection::create($params);
            return admin_success('添加成功', '__relist__');
        }
    }

    public function getList()
    {
        $data = \app\mall\model\GoodsCollection::where($this->getWhere())->order($this->getOrder('sort asc'))->paginate(input('page_size'));
        return admin_data($data);
    }

    public function delete()
    {
        $id = input('id');
        $obj = \app\mall\model\GoodsCollection::find($id);
        if (empty($obj)) {
            return admin_error('数据不存在');
        }
        $goodsCount = GoodsCollectionItem::where('collection_id', $obj->id)->count();
        if ($goodsCount > 0) {
            return admin_error('此集合内存在商品, 不可删除');
        }
        $obj->delete();
        return admin_success('删除成功', '__relist__');
    }

    public function items()
    {
        $collectionId = input('collectionId');
        if (is_page()) {
            return SBuilder::makeTable()
                ->setPageTitle('商品列表')
                ->addColumns([
                    ['goods_id', '商品ID'],
                    ['goods_name', '商品名'],
                    ['goods_pic', '商品图片', 'images'],
                    ['goods_price', '价格'],
                    ['create_time', '创建时间', 'datetime'],
                ])
                ->addTopButton('add', '添加商品', url('addItem', ['collectionId' => $collectionId]))
                ->addRightButton('delete', '删除商品', url('deleteItem'))
                ->setRowListUrl(url('items', ['collectionId' => $collectionId]))
                ->fetch();
        }else {
            $data = GoodsCollectionItem::where('collection_id', $collectionId)->paginate();
            return admin_data($data);
        }
    }

    public function addItem()
    {
        $collectionId = input('collectionId');
        $collection = \app\mall\model\GoodsCollection::find($collectionId);
        if (is_page()) {
            return SBuilder::makeForm()
                ->setPageTitle('添加商品')
                ->addFormItems([
                    ['collectionName', '商品集合', 'static'],
                    ['collectionId', '', 'hidden'],
                    ['goods_id', '选择商品', 'goods_select'],
                    ['sort', '排序', 'number'],
                ])
                ->setFormData(['collectionName' => $collection->name])
                ->fetch();
        }else {
            $goodsId = input('goods_id');
            $goods = \app\mall\model\Goods::find($goodsId);
            if (empty($goods)) {
                return admin_error('商品信息不存在');
            }
            $sort = input('sort');
            GoodsCollectionItem::create([
                'collection_id' => $collection->id,
                'collection_type' => $collection->type,
                'sort' => $sort,
                'goods_id' => $goodsId,
                'goods_type' => $goods->type,
                'goods_name' => $goods->name,
                'goods_pic' => $goods->pic,
                'goods_price' => $goods->sale_price,
            ]);
            return admin_success('添加成功', '__relist__');
        }
    }

    public function deleteItem()
    {
        $id = input('id');
        $item = GoodsCollectionItem::find($id);
        if (empty($item)) {
            return admin_error('数据不存在');
        }
        $item->delete();
        return admin_success('删除成功', '__relist__');
    }

}