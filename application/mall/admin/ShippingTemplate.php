<?php

namespace app\mall\admin;

use evan\sbuilder\builder\SBuilder;
use evan\sbuilder\controller\Admin;

class ShippingTemplate extends Admin
{

    public function index()
    {

    }

    public function getList()
    {
        $data = \app\mall\model\ShippingTemplate::where($this->getWhere())
            ->order($this->getOrder())
            ->select();
        return admin_data(['list' => $data]);
    }

    public function add()
    {
        if (is_page()) {
            return SBuilder::makeForm()
                ->fetch();
        }else {
            $this->checkParam([
                'name' => 'require'
            ]);
            $shipping_rules = input('shipping_rules');
            $unshipping_areas = input('unshipping_areas');
            if (empty($shipping_rules)) {
                return admin_error('请设置运费规则');
            }

            $res = $this->checkShippingRuleAndUnshippingAreas($shipping_rules, $unshipping_areas);
            if ($res !== true) {
                return $res;
            }

            \app\mall\model\ShippingTemplate::create([
                'name' => input('name'),
                'shipping_rules' => $shipping_rules,
                'unshipping_areas' => $unshipping_areas,
            ]);
            return admin_success('添加成功');
        }
    }

    public function edit()
    {
        $id = input('id');
        if (empty($id)) return admin_error('主键不能为空');
        $obj = \app\mall\model\ShippingTemplate::where('id', $id)->find();
        if (empty($obj)) {
            return admin_error('数据不存在');
        }
        if (is_page()) {
            return SBuilder::makeForm()
                ->fetch();
        }else {

            $this->checkParam([
                'name' => 'require'
            ]);
            $shipping_rules = input('shipping_rules');
            $unshipping_areas = input('unshipping_areas');
            if (empty($shipping_rules)) {
                return admin_error('请设置运费规则');
            }

            $res = $this->checkShippingRuleAndUnshippingAreas($shipping_rules, $unshipping_areas);
            if ($res !== true) {
                return $res;
            }

            $obj->save([
                'name' => input('name'),
                'shipping_rules' => $shipping_rules,
                'unshipping_areas' => $unshipping_areas,
            ]);
            return admin_success('修改成功');
        }
    }

    protected function checkShippingRuleAndUnshippingAreas($shipping_rules, $unshipping_areas)
    {
        $shippingRulesContainerProvinceIds = [];
        $shippingRulesContainerCityIds = [];
        foreach ($shipping_rules as $index => $shipping_rule) {
            if (empty($shipping_rule['provinces'])) {
                return admin_error('请设置运费规则' . ($index + 1) . '的对应地区');
            }
            if (empty($shipping_rule['fee_type'])) {
                return admin_error('请设置规则' . ($index + 1) . '的对应地区');
            }
            if ($shipping_rule['fee_type'] == 1) {
                if (empty($shipping_rule['fee'])) {
                    return admin_error('请设置规则' . ($index + 1) . '的运费金额');
                }
            }else {
                if (empty($shipping_rule['start_num']) || empty($shipping_rule['start_fee'])) {
                    return admin_error('规则' . ($index + 1) . '配送运费不能为空');
                }
                if (empty($shipping_rule['plus_num']) || empty($shipping_rule['plus_fee'])) {
                    return admin_error('规则' . ($index + 1) . '增加运费不能为空');
                }
            }
            // 指定条件包邮判断
            if ($shipping_rule['has_free_condition']) {
                if ($shipping_rule['free_type'] != 1 && $shipping_rule['free_type'] != 2) {
                    return admin_error('指定条件包邮类型不合法');
                }
                if ($shipping_rule['free_num'] != 1 && $shipping_rule['free_num'] != 2) {
                    return admin_error('指定条件包邮数量不合法');
                }
                if ($shipping_rule['fee_type'] == 3 && $shipping_rule['free_type'] != 1) {
                    return admin_error('按重量计费不可选择指定件数包邮');
                }
            }

            // 判断省份与城市是否重复
            foreach ($shipping_rule['provinces'] as $province) {
                if (empty($province['cities'])) {
                    if (in_array($province['id'], $shippingRulesContainerProvinceIds)){
                        return admin_error('选择的地区' . $province['name'] . '重复');
                    }
                    $shippingRulesContainerProvinceIds[] = $province['id'];
                }else {
                    foreach ($province['cities'] as $city) {
                        if (in_array($city['id'], $shippingRulesContainerCityIds)){
                            return admin_error('选择的地区' . $city['name'] . '重复');
                        }
                        $shippingRulesContainerCityIds[] = $city['id'];
                    }
                }
            }
        }

        // 判断不发城市是否重复
        foreach ($unshipping_areas as $unshipping_area) {
            if (empty($unshipping_area['cities'])){
                if (in_array($unshipping_area['id'], $shippingRulesContainerProvinceIds)) {
                    return admin_error('选择的不配送地区' . $unshipping_area['name'] . '与配送规则中的地区重复');
                }
            }else {
                foreach ($unshipping_area['cities'] as $city) {
                    if (in_array($city['id'], $shippingRulesContainerCityIds)){
                        return admin_error('选择的不配送地区' . $city['name'] . '与配送规则中的地区重复');
                    }
                }
            }
        }

        return true;
    }

    public function delete()
    {
        $id = input('id');
        if(empty($id)) return admin_error('主键不能为空');
        $template = \app\mall\model\ShippingTemplate::where('id', $id)->find();
        if (empty($template)) {
            return admin_error('数据不存在');
        }
        $template->delete();
        return admin_success('删除成功', '');
    }

}