CREATE TABLE `__PREFIX__mall_user` (
    `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
    `mobile` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '手机号',
    `email` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '邮箱',
    `password` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '密码',
    `salt` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '盐值',
    `nickname` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '昵称',
    `avatar` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '头像',
    `gender` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '性别 0-未知, 1-男 2-女',
    `status` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '状态 1-正常 0-禁用',
    `create_time` INT(11) NOT NULL DEFAULT '0',
    `update_time` INT(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_unicode_ci' COMMENT='用户表';

CREATE TABLE `__PREFIX__mall_user_address` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` INT(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `is_default` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '是否默认 1-默认 0-非默认',
  `country_code` VARCHAR(5) NOT NULL DEFAULT 'CN' COMMENT '国家编码',
  `name` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `mobile` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '收货人手机号',
  `province_id` INT(11) NOT NULL DEFAULT '0' COMMENT '省份ID',
  `province` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '省份',
  `city_id` INT(11) NOT NULL DEFAULT '0' COMMENT '城市ID',
  `city` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '城市',
  `district_id` INT(11) NOT NULL DEFAULT '0' COMMENT '区县ID',
  `district` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '区/县',
  `detail` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '详细地址',
  `detail2` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '详细地址2',
  `create_time` INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id` (`user_id`) USING BTREE,
  INDEX `is_default` (`is_default`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_unicode_ci' COMMENT='用户收货地址';

CREATE TABLE `__PREFIX__mall_user_device` (
     `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '记录ID',
     `user_id` INT(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
     `device_type` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '设备类型 1-安卓 2-ios',
     `client_id` VARCHAR(50) NOT NULL DEFAULT '0' COMMENT '设备id, 由个推sdk生成',
     `device_token` VARCHAR(100) NOT NULL DEFAULT '0' COMMENT '设备token',
     `create_time` INT(11) NOT NULL DEFAULT '0',
     `update_time` INT(11) NOT NULL DEFAULT '0',
     PRIMARY KEY (`id`) USING BTREE,
     INDEX `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_unicode_ci' COMMENT='用户设备表';

CREATE TABLE `__PREFIX__mall_user_request_log` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `user_id` INT(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
    `date` DATE NOT NULL DEFAULT '0000-00-00' COMMENT '请求日期',
    `url` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '请求路径',
    `create_time` INT(11) NOT NULL DEFAULT '0' COMMENT '请求时间',
    `update_time` INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `user_id` (`user_id`) USING BTREE,
    INDEX `date` (`date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_unicode_ci' COMMENT='用户请求记录';

CREATE TABLE `__PREFIX__mall_user_verify_code` (
    `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '记录ID',
    `type` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '类型 1-手机验证码 2-邮箱验证码',
    `use_type` VARCHAR(32) NOT NULL DEFAULT '' COMMENT '使用类型, (自定义字符串), 如: login, register, reset_password等',
    `to` VARCHAR(15) NOT NULL DEFAULT '' COMMENT '接收人, 手机号或邮箱',
    `code` VARCHAR(15) NOT NULL DEFAULT '' COMMENT '验证码',
    `expire_time` INT(11) NOT NULL DEFAULT '0' COMMENT '过期时间 0表示永不过期',
    `status` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '状态 1-有效 0-无效',
    `create_time` INT(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
    `update_time` INT(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX `type` (`type`) USING BTREE,
    INDEX `use_type` (`use_type`) USING BTREE,
    INDEX `to` (`to`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_unicode_ci' COMMENT='用户验证码表';


