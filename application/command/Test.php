<?php

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;

class Test extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('test');
        // 设置参数
        
    }

    protected function execute(Input $input, Output $output)
    {
        echo 'test command ';
    }
}
