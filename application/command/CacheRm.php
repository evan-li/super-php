<?php
/**
 * Create By Evan.
 * Date: 2019/8/21
 * Time: 16:17
 */

namespace app\command;


use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\facade\Cache;

class CacheRm extends Command {

    protected function configure()
    {
        // 指令配置
        $this->setName('cache:rm')
            // 设置参数
            ->addArgument('key', null, Argument::REQUIRED, '缓存的key值');

    }


    protected function execute(Input $input, Output $output)
    {
        $key = $input->getArgument('key');
        if(!Cache::has($key)) {
            dd('key不存在');
        }
        Cache::rm($key);
        dd('success');
    }
}