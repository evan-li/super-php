<?php


namespace app\command;


use app\common\model\FailedJob;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\Queue;

class QueueRetry extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('queue:retry')
            ->addArgument('id', Argument::OPTIONAL, '要重试的ID, 支持的格式为: 2 或 1-5 或 1,3,6')
            ->setDescription('重试失败任务');
        // 设置参数

    }

    protected function execute(Input $input, Output $output)
    {
        if($input->hasArgument('id')) {
            $id = $input->getArgument('id');
            $query = new FailedJob();
            if(strpos($id, ',') !== false) {
                $query = $query->whereIn('id', explode(',', $id));
            }else if(strpos($id, '-') !== false) {
                $query = $query->whereBetween('id', explode('-', $id));
            }else {
                $query = $query->where('id', $id);
            }
            $list = $query->select();
            foreach ($list as $item) {
                $payload = json_decode($item->payload, 1);
                Queue::push($payload['job'], $payload['data'], $item->queue);
                $item->delete();
            }
        }else {
            FailedJob::chunk(500, function($list) {
                foreach ($list as $item) {
                    $payload = json_decode($item->payload, 1);
                    Queue::push($payload['job'], $payload['data'], $item->queue);
                    $item->delete();
                }
            });
        }
    }

}