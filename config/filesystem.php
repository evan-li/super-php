<?php

return [
    // 允许上传的图片后缀
    'upload_image_ext'       => 'gif,jpg,jpeg,bmp,png,webp',
    // 允许上传的视频后缀
    'upload_video_ext'       => 'mp4,m4v,mov,3gp,mpg,mpeg,mpe,rm,rmvb,wmv,asf,asx,avi,slv,avi,mkv,flv,vob',
    // 允许上传的图片后缀
    'upload_file_ext'        => 'doc,docx,xls,xlsx,ppt,pptx,pdf,wps,txt,rar,tar,zip,gz,bz2,7z,md,gif,jpg,jpeg,bmp,png,apk,wgt',
    // 是否开启文件秒传
    'flash' => true,
    // 文件存储驱动 local, qiniu, aliyun, tencent, aws
    'driver' => env('file_driver', 'local'),
    // 本地存储配置
    'local' => [
        'upload_path' => env('root_path') . 'public' . DIRECTORY_SEPARATOR . 'uploads',
    ],
    // 七牛文件存储
    'qiniu' => [
        'accessKey' => env('qiniu_access_key'),
        'secretKey' => env('qiniu_secret_key'),
        'bucket' => env('qiniu_bucket'),
        'domain' => env('qiniu_domain'),
    ],
    // 阿里云 oss 存储
    'aliyun' => [
        'accessKeyId' => env('aliyun_oss_id'),
        'accessKeySecret' => env('aliyun_oss_secret'),
        'endpoint' => env('aliyun_oss_endpoint'),
        'bucket' => env('aliyun_oss_bucket'),
        'domain' => env('aliyun_oss_domain'),
    ],
    // 腾讯云对象存储
    'tencent' => [
        'secretId' => env('tencent_secret_id'),
        'secretKey' => env('tencent_secret_key'),
        'region' => env('tencent_cos_region'),
        'schema' => env('tencent_cos_schema', 'https'),
        'bucket' => env('tencent_cos_bucket'),
        'domain' => env('tencent_cos_domain', ''),
    ],
    // aws s3 存储
    'aws' => [
        'key' => env('aws_s3_key'),
        'secret' => env('aws_s3_secret'),
        'version' => env('aws_s3_version', 'latest'),
        'region' => env('aws_s3_region'),
        'bucket' => env('aws_s3_bucket'),
        'domain' => env('aws_s3_domain', ''),
    ],
];