<?php

return [

    'paypal' => [
        'env' => env('paypal_env'),
        'client_id' => env('paypal_client_id'),
        'secret' => env('paypal_secret'),
        'currency' => 'USD', // paypal支付币种
        'notify_url' => "", // 登录超时时间, 单位:秒
        'return_url' => '', // 回调地址, 前端重定向到这个地址
    ],

];
