<?php

return [
    'host' => env('smtp_host'),
    'port' => env('smtp_port'),
    'from' => 'SuperPHP', // 发件人名称
    'username' => env('smtp_username'), // 发件人邮箱(smtp登录账号)
    'password' => env('smtp_password'), // 发件人邮箱密码
    'smtp_secure' => 'ssl', // 加密方式

];