<?php

return [

    // ------------ SBuilder相关配置
    'title' => 'SuperPHP管理后台', // 站点标题
    'short_title' => 'SB', // 站点短标题
    'description' => 'SPHP, 极速开发框架', // 站点描述
    'keyword' => 'SPHP,ThinkPHP,ThinkPHP5.1,极速,框架,开发,后台开发', // 站点关键字
    'system_config_url' => url('system/index/config'), // 系统核心配置获取地址
    'login_need_captcha' => true, // 登录是否需要验证码
    'clear_cache_url' => url('system/index/cache', ['opt' => 'clear']), // 清空缓存地址
    'clear_session_url' => url('system/index/session', ['opt' => 'clear']), // 清空session地址
    'init_data_url' => url('system/index/getInitData'), // 获取初始化数据地址(包含用户信息,菜单列表)
    'user_info_url' => url('system/index/currentUser'), // 获取用户信息地址
    'user_edit_url' => url('system/user/edit'), // 个人信息设置地址
    'menu_type' => 1, // 菜单渲染方式 1-顶部+左侧菜单 2-左侧菜单
    'menu_url' => url('system/index/menus'), // 获取菜单地址
    'login_url' => url('system/index/login'), // 登录
    'logout_url' => url('system/index/logout'), // 退出登录
    'modify_password_url' => url('system/index/modifyPassword'), // 获取用户信息地址
    'auth_rules_url' => '', // 获取权限列表地址
    'file_upload_url' => url('system/attachment/upload', ['dir' => 'files']), // 文件上传地址
    'big_file_upload_url' => url('system/attachment/bigupload', ['dir' => 'files']), // 文件上传地址
    'image_upload_url' => url('system/attachment/upload', ['dir' => 'images']), // 图片上传地址
    'video_upload_url' => url('system/attachment/upload', ['dir' => 'videos']), // 视频上传地址
    'file_path_url' => url('system/attachment/getFilesPath'), // 内部文件路径转换地址
    'index_page_path' => '/dashboard', // 首页路径
];
