<?php

namespace evan\sbuilder\util;

use think\helper\Str;

class Util
{

    /**
     * @param $url
     * @return array {type, target, url}
     */
    public static function parseOptionUrl($url, $defaultType='link'): array
    {
        if(empty($url)) return [];
        $target = null;
        if($url == '__back__'){
            $type = 'back';
        }else if($url == '__relist__'){ // 重新获取列表, 列表页才有效
            $type = 'relist';
        }else if($url == '__refresh__'){
            $type = 'refresh';
        }else if(Str::startsWith($url, 'pop:')){
            $type = 'pop';
            $url = Str::substr($url, 4);
        }else if(Str::startsWith($url, 'blank:')){
            $type = 'link';
            $target = '_blank';
            $url = Str::substr($url, 6);
        }else if(Str::startsWith($url, 'ajax:')){
            $type = 'ajax';
            $url = Str::substr($url, 5);
        }else if(Str::startsWith($url, 'link:')){
            $type = 'link';
            $url = Str::substr($url, 5);
        }else {
            $type = $defaultType;
        }
        return [
            'type' => $type, // 操作类型
            'url' => $url, // url(跳转或弹框)
            'target' => $target, // url跳转作用域
        ];
    }

}