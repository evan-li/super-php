<?php

namespace evan\sbuilder\builder;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;

class ModuleCreate extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('module:create')
            // 设置参数
            ->addArgument('name', null, Argument::REQUIRED, '模块名');

    }

    protected function execute(Input $input, Output $output)
    {
        $input->getArgument('name');
        // 检测模块名是否重复
        // 生成模块目录配置文件
        // 生成配置文件
        // todo
    }

}