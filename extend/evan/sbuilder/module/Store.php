<?php

namespace evan\sbuilder\module;

use app\common\exception\CommonResponseException;
use app\common\ResCode;
use app\system\model\Module;
use GuzzleHttp\Client;
use think\facade\App;
use think\facade\Log;

/**
 * 模块市场操作
 */
class Store
{

    private static $apiUrl = 'http://api.super-php.niucha.ren';
    private static $client;

    public static function getListByNames($names)
    {
        if (empty($names)) return [];
        $result = self::get('/store/module/getListByNames', [
            'names' => implode(',', $names)
        ]);
        if ($result['code'] != ResCode::success) {
            throw new CommonResponseException($result['code'], $result['msg']);
        }
        return $result['data']['list'];
    }

    public static function getList($page, $pageSize)
    {
        $result = self::get('/store/module/getList', [
            'page' => $page,
            'page_size' => $pageSize,
        ]);
        if ($result['code'] != ResCode::success) {
            throw new CommonResponseException($result['code'], $result['msg']);
        }
        return [
            'list' => $result['data']['list'],
            'total' => $result['data']['total'],
            'page' => $page,
            'page_size' => $pageSize,
        ];
    }

    public static function getInstalledList()
    {
        return Module::select();
    }
    public static function getInstalledListByNames($names)
    {
        return Module::where('name', 'in', $names)->select();
    }


    /**
     * 获取本地所有的模块列表(包含已安装和本地添加的模块)
     * @return array
     */
    public static function getLocalModules()
    {
        $appPath = App::getAppPath();
        $files = scandir($appPath);
        // 获取模块路径列表
        $modules = [];
        $excludedModuleList = config('deny_module_list') ?: [];
        foreach ($files as $file) {
            // 是否是正常的目录
            if ($file == '.' || $file == '..' || !is_dir($appPath . $file)) continue;
            // 排除common和system
            if ($file == 'common' || $file == 'system') continue;
            // 排除禁止访问的模块
            if (in_array($file, $excludedModuleList)) continue;
            // 检测模块配置文件
            $infoFile = $appPath . $file . DIRECTORY_SEPARATOR . 'info.json';
            if (!is_file($infoFile)) continue;
            $info = json_decode(file_get_contents($infoFile), 1);
            if ($info['name'] != $file) {
                throw new CommonResponseException("检测到模块 $file 配置文件中的模块名称不正确");
            }
            $modules[] = [
                'name' => $info['name'],
                'title' => $info['title'],
                'author' => $info['author'],
                'desc' => $info['desc'],
                'version' => $info['version'],
            ];
        }
        return $modules;
    }

    public static function download($module, $version)
    {
        $result = self::get('/store/module/download', ['name' => $module, 'version' => $version]);

        if ($result['code'] != ResCode::success) {
            throw new CommonResponseException($result['code'], $result['msg']);
        }
        $url = $result['data']['url'];
//        $url = self::$apiUrl . "/uploads/modules/zip/$module/$version/$module.zip";
        $contents = file_get_contents($url);
        $moduleZipPath = App::getRootPath() . 'modules' . DIRECTORY_SEPARATOR . 'zip' . DIRECTORY_SEPARATOR . $module . '.zip';
        @mkdir(pathinfo($moduleZipPath)['dirname'], 0777, true);
        file_put_contents($moduleZipPath, $contents);
        return $moduleZipPath;
    }

    /**
     * 修改
     * @param $zipFile
     * @param $params
     * @return bool
     */
    public static function publish($zipFile, $params)
    {
        $response = self::getClient()->post(self::$apiUrl . '/store/module/publish', [
            'multipart' => [
                ['name' => 'file', 'contents' => fopen($zipFile, 'r')],
                ['name' => 'name', 'contents' => $params['name']],
                ['name' => 'version', 'contents' => $params['version']],
                ['name' => 'title', 'contents' => $params['title'] ?? ''],
                ['name' => 'desc', 'contents' => $params['desc'] ?? ''],
            ],
        ]);
        if($response->getStatusCode() != 200){
            Log::error('http请求发送失败, 返回: [ ' . $response->getStatusCode() . ' ] ' . $response->getBody()->getContents());
            throw new CommonResponseException(ResCode::http_request_error);
        }else {
            $result = json_decode($response->getBody()->getContents(), 1);
        }
        if($result['code'] != 200) {
            throw new CommonResponseException($result['code'], $result['msg'], $result['data']);
        }
        return true;
    }

    /**
     * post 请求
     * @param $url
     * @param array $params
     * @return mixed
     */
    public static function post($url, array $params = []){
        $response = self::getClient()->post($url, [
            'form_params' => $params,
        ]);
        if($response->getStatusCode() != 200){
            Log::error('http请求发送失败, 返回: [ ' . $response->getStatusCode() . ' ] ' . $response->getBody()->getContents());
            throw new CommonResponseException(ResCode::http_request_error);
        }else {
            return json_decode($response->getBody()->getContents(), 1);
        }
    }

    /**
     * get 请求
     * @param $url
     * @param array $params
     * @return mixed
     */
    public static function get($url, array $params = []){
        $response = self::getClient()->get($url, [
            'query' => $params,
        ]);
        if($response->getStatusCode() != 200){
            Log::error('http请求发送失败, 返回: [ ' . $response->getStatusCode() . ' ] ' . $response->getBody()->getContents());
            throw new CommonResponseException(ResCode::http_request_error);
        }else {
            return json_decode($response->getBody()->getContents(), 1);
        }
    }

    /**
     * 获取http客户端
     * @return Client
     */
    protected static function getClient(){
        if(self::$client){
            return self::$client;
    }
        self::$client = new Client([
            'base_uri' => self::$apiUrl
        ]);
        return self::$client;
    }

}