<?php
namespace evan\sbuilder\module;

use app\system\model\Module;
use think\Db;
use think\facade\App;
use think\helper\Str;

/**
 * 模块管理
 */
class Manager
{

    /**
     * 发布模块
     * @param $module
     * @param $version
     * @param $verTitle
     * @param $verDesc
     * @return bool
     * @throws ModuleException
     */
    public static function publish($module, $version, $verTitle, $verDesc)
    {
        $filePath = self::package($module);

        return Store::publish($filePath, [
            'name' => $module,
            'version' => $version,
            'title' => $verTitle,
            'desc' => $verDesc,
        ]);
    }

    /**
     * @throws ModuleException
     */
    public static function install($module, $version)
    {
        // 是否已安装
        $exits = Module::where('name', $module)->find();
        if ($exits) {
            throw new ModuleException('已安装');
        }
        Store::download($module, $version);
        self::unzip($module);
        // 获取配置信息
        $info = self::getInfo($module);
        // 检查配置信息
        if ($info['name'] !== $module) {
            throw new ModuleException('模块名配置信息错误');
        }

        // 检查模块依赖
        if (!empty($info['require-modules'])) {
            foreach ($info['require-modules'] as $key => $value) {
                $installed = Module::where('name', $key)->find();
                if (empty($installed)) {
                    throw new ModuleException("依赖模块 $key:$value 未安装");
                }
                // 对比版本
                if ($installed['version'] < $value) {
                    throw new  ModuleException("依赖模块 $key:$value 版本不匹配, 当前安装版本: {$installed['version']}");
                }
            }
        }
        // todo composer require 检查
        // todo composer require-dev 检查

        // 执行sql
        $installSql = $info['sql']['install'] ?? '';
        $installSql = comma_str_to_arr($installSql ?: 'sql/install.sql');
        $config = config('database.');
        foreach ($installSql as $item) {
            $sqlFile = App::getAppPath() . $module . DIRECTORY_SEPARATOR . $item;
            if (!file_exists($sqlFile)) continue;
            self::importSql($sqlFile, $config['prefix']);
        }

        $hooks = self::getHooks($info);
        // 按顺序执行hook
        foreach ($hooks as $hook) {
            $hookarr = explode('::', $hook);
            $class = $hookarr[0];
            $method = $hookarr[1] ?? 'install';
            if (class_exists($class) && method_exists($class, $method)) {
                $res = call_user_func([new $class(), $method]);
            }
        }

        // 添加记录到已安装模块表
        if (!$exits) {
            $exits = Module::create([
                'name' => $module,
                'title' => $info['title'] ?? '',
                'author' => $info['author'] ?? '',
                'desc' => $info['desc'] ?? '',
                'cover' => $info['cover'] ?? '',
                'pics' => $info['pics'] ?? '',
                'version' => $info['version'] ?? '',
            ]);
        }
        self::enable($module);
        $exits->status = 1;
        $exits->save();
    }

    public static function enable($module)
    {
        // 获取配置信息
        $info = self::getInfo($module);
        $hooks = self::getHooks($info);

        // 按顺序执行hook
        foreach ($hooks as $hook) {
            $hookarr = explode('::', $hook);
            $class = $hookarr[0];
            $method = $hookarr[1] ?? 'enable';
            if (class_exists($class) && method_exists($class, $method)) {
                $res = call_user_func([new $class(), $method]);
            }
        }
        // 修改模块数据状态
        $exits = Module::where('name', $module)->find();
        if (!empty($exits)) {
            $exits->status = 1;
            $exits->save();
        }
    }

    public static function disable($module)
    {
        // 获取配置信息
        $info = self::getInfo($module);
        $hooks = self::getHooks($info);

        // 按顺序执行hook
        foreach ($hooks as $hook) {
            $hookarr = explode('::', $hook);
            $class = $hookarr[0];
            $method = $hookarr[1] ?? 'disable';
            if (class_exists($class) && method_exists($class, $method)) {
                $res = call_user_func([new $class(), $method]);
            }
        }
        // 修改模块数据状态
        $exits = Module::where('name', $module)->find();
        if (!empty($exits)) {
            $exits->status = 2;
            $exits->save();
        }
    }

    public static function uninstall($module)
    {
        self::disable($module);
        // 获取配置信息
        $info = self::getInfo($module);

        $moduleDir =  App::getAppPath() . $module;
        // 执行卸载sql
        $installSql = $info['sql']['uninstall'] ?? '';
        $installSql = comma_str_to_arr($installSql ?: 'sql/uninstall.sql');
        $config = config('database.');
        foreach ($installSql as $item) {
            $sqlFile = $moduleDir . DIRECTORY_SEPARATOR . $item;
            if (!file_exists($sqlFile)) continue;
            self::importSql($sqlFile, $config['prefix']);
        }

        $hooks = self::getHooks($info);
        // 按顺序执行hook
        foreach ($hooks as $hook) {
            $hookarr = explode('::', $hook);
            $class = $hookarr[0];
            $method = $hookarr[1] ?? 'uninstall';
            if (class_exists($class) && method_exists($class, $method)) {
                $res = call_user_func([new $class(), $method]);
            }
        }
        // 删除已安装模块记录
        $exits = Module::where('name', $module)->find();
        if (!empty($exits)) {
            $exits->delete();
        }
        // 删除模块目录
        deldir($moduleDir);
        // todo composer require 检查
        // todo composer require-dev 检查
    }


    /**
     * 获取模块配置信息
     */
    public static function getInfo($module, $absolute = false)
    {
        if ($absolute) {
            $infoFile = $module;
        }else {
            $infoFile = App::getAppPath() . $module . DIRECTORY_SEPARATOR . 'info.json';
        }
        if (!file_exists($infoFile)) {
            throw new ModuleException('模块配置信息不存在');
        }
        $info = json_decode(file_get_contents($infoFile), true);
        return $info;
    }

    /**
     * 获取模块配置的钩子信息
     */
    private static function getHooks($info, $module = '')
    {
        empty($module) && $module = $info['name'];
        $hook = $info['hook'] ?? null;
        // 获取钩子
        $hooks = [];

        if (empty($hook)) {
            $className = Str::studly($module);
            $defaultClass = "\\app\\$module\\" . $className;
            $hooks[] = $defaultClass;
        }else if (isset($hook['enable']) && !empty($hook['enable'])) {
            // 按方法配置
            if (is_array($hook['enable'])) {
                $hooks += $hook['enable'];
            }else {
                $hooks[] = $hook['enable'];
            }
        }else {
            // 按类名配置
            if (is_array($hook)) {
                $hooks += $hook;
            }else {
                $hooks[] = $hook;
            }
        }
        return $hooks;
    }

    /**
     * 解压
     */
    public static function unzip($module)
    {

        $zipPath = App::getRootPath() . 'modules' . DIRECTORY_SEPARATOR . 'zip' . DIRECTORY_SEPARATOR;
        $filename = $module . '.zip';
        if (!file_exists($zipPath . $filename)) {
            throw new ModuleException('模块包不存在');
        }
        $modulePath = App::getAppPath() . $module;
        if (!file_exists($modulePath)) {
            mkdir($modulePath, 0777, true);
        }

        self::unzipFile($zipPath . $filename, $modulePath);
    }

    /**
     * 打包模块
     * @throws ModuleException
     */
    public static function package($module)
    {
        $modulePath = App::getAppPath() . $module;
        if (!file_exists($modulePath)){
            throw new ModuleException('模块不存在');
        }

        $zipPath = App::getRootPath() . 'modules' . DIRECTORY_SEPARATOR . 'zip' . DIRECTORY_SEPARATOR;
        if (!file_exists($zipPath)) {
            mkdir($zipPath, 0777, true);
        }
        $zipFile = $zipPath . $module . '.zip';
        if (file_exists($zipFile)) { // 如果文件存在, 备份文件
            if (!file_exists($zipPath . 'back')) {
                @mkdir($zipPath . 'back');
            }
            rename($zipFile, $zipPath . 'back' . DIRECTORY_SEPARATOR . $module . '-' . time() . '.zip');
        }

        self::zipFolder($modulePath, $zipFile);
        return $zipFile;
    }


    /**
     * 导入sql
     * @param string $sqlFile
     * @param string $prefix 数据库前缀
     * @return bool
     */
    public static function importSql(string $sqlFile, $prefix): bool
    {
        $tempLine = '';
        if (is_file($sqlFile)) {
            $lines = file($sqlFile);
            foreach ($lines as $line) {
                if (substr($line, 0, 2) == '--' || $line == '' || substr($line, 0, 2) == '/*') {
                    continue;
                }

                $tempLine .= $line;
                if (substr(trim($line), -1, 1) == ';') {
                    $tempLine = str_ireplace('__PREFIX__', $prefix, $tempLine);
                    $tempLine = str_ireplace('INSERT INTO ', 'INSERT IGNORE INTO ', $tempLine);
                    Db::execute($tempLine);
                    $tempLine = '';
                }
            }
        }
        return true;
    }

    /**
     * 将目标文件夹下的内容压缩到zip中（zip包含文件夹目录）
     * @param $sourcePath *文件夹路径 例: /home/test
     * @param $outZipPath *zip文件名(包含路径) 例: /home/zip_file/test.zip
     * @return string
     */
    public static function zipFolder($sourcePath, $outZipPath)
    {
        // 将目录中的\统一替换为/
        $sourcePath = str_replace("\\", '/', $sourcePath);
        $outZipPath = str_replace("\\", '/', $outZipPath);
        $parentPath = rtrim(substr($sourcePath, 0, strrpos($sourcePath, '/')),"/")."/";
        $dirName = ltrim(substr($sourcePath, strrpos($sourcePath, '/')),"/");

        $sourcePath=$parentPath.'/'.$dirName;//防止传递'folder'文件夹产生bug

        $z = new \ZipArchive();
        $z->open($outZipPath, \ZIPARCHIVE::CREATE);//建立zip文件
//        $z->addEmptyDir($dirName);//建立文件夹
        self::folderToZip($sourcePath, $z, strlen("$sourcePath/"));
        $z->close();

        return $outZipPath;
    }

    private static function folderToZip($folder, &$zipFile, $exclusiveLength) {
        // 将目录中的\统一替换为/
        $handle = opendir($folder);
        while (false !== $f = readdir($handle)) {
            if ($f != '.' && $f != '..') {
                $filePath = "$folder/$f";
                // 在添加到zip之前从文件路径中删除前缀
                $localPath = substr($filePath, $exclusiveLength);
                if (is_file($filePath)) {
                    $zipFile->addFile($filePath, $localPath);
                } elseif (is_dir($filePath)) {
                    // 添加子文件夹
                    $zipFile->addEmptyDir($localPath);
                    self::folderToZip($filePath, $zipFile, $exclusiveLength);
                }
            }
        }
        closedir($handle);
    }

    /**
    解压
     * @param $zipFileName *zip文件名(包含路径) 例: /home/zip_file/test.zip
     * @param $unzipPath *解压路径 例: /home/test/
     * @return bool
     */
    public static function unzipFile($zipFileName, $unzipPath)
    {
        $zip = new \ZipArchive;

        if ($zip->open($zipFileName) === true) {
            if(!file_exists($unzipPath))
                @mkdir($unzipPath);

            //将压缩包文件解压到test目录下
            $zip->extractTo($unzipPath);

            // 关闭zip
            $zip->close();

            return true;
        }else{
            return false;
        }
    }
}