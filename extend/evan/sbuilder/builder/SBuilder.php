<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 * Date: 2019/4/23
 * Time: 16:02
 */

namespace evan\sbuilder\builder;


use evan\sbuilder\builder\block\CardList;
use evan\sbuilder\builder\block\Charts;
use evan\sbuilder\builder\block\Block;
use evan\sbuilder\builder\block\DataPanel;
use evan\sbuilder\builder\block\Form;
use evan\sbuilder\builder\block\Html;
use evan\sbuilder\builder\block\Iframe;
use evan\sbuilder\builder\block\Table;
use evan\sbuilder\builder\extend\GoodsForm;
use evan\sbuilder\builder\page\Builder;
use think\Exception;
use think\helper\Str;

class SBuilder
{
    /**
     * 构建器
     * @var
     */
    protected static $builder;

    /**
     * 构建参数
     * @var array
     */
    protected static $vars = [];

    /**
     * 创建Form类型的builder
     * @param array $layout 区块布局, 直接make区块构建器时使用
     * @return Form
     * @throws
     */
    public static function makeForm($layout = []):Form
    {
        return static::make('form', $layout);
    }

    /**
     * 创建Table类型的builder
     * @param array $layout 区块布局, 直接make区块构建器时使用
     * @return Table
     * @throws
     */
    public static function makeTable($layout = []):Table
    {
        return static::make('table', $layout);
    }

    /**
     * 创建一个图标
     * @param array $layout
     * @return Charts
     * @throws
     */
    public static function makeCharts($layout = []):Charts
    {
        $layout['height'] = $layout['height'] ?? 500;
        return static::make('charts', $layout);
    }

    /**
     * 创建一个支持html的区块
     * @param array $layout
     * @return Html
     * @throws
     */
    public static function makeHtml($layout = []):Html
    {
        return static::make('html', $layout);
    }

    /**
     * 创建一个iframe区块
     * @param array $layout
     * @return Iframe
     * @throws
     */
    public static function makeIframe($layout = []):Iframe
    {
        return static::make('iframe', $layout);
    }

    /**
     * 创建一个data-panel区块
     * @param array $layout
     * @return DataPanel
     * @throws
     */
    public static function makeDataPanel($layout = []):DataPanel
    {
        return static::make('data-panel', $layout);
    }

    /**
     * 创建一个card-list区块
     * @param array $layout
     * @return CardList
     * @throws
     */
    public static function makeCardList($layout = []):CardList
    {
        return static::make('card-list', $layout);
    }

    /**
     * 创建一个空的块
     * @param array $layout
     * @return Block
     * @throws
     */
    public static function makeBlock($layout = []):Block
    {
        return static::make('block', $layout);
    }

    /**
     * 创建各种builder的入口
     * @param string $type 构建器名称，'page', 'block', 'form', 'table', 'charts', 'html', 'iframe', data-panel, card-list 或其他自定义构建器
     * @param array $layout 区块布局, 直接make区块构建器时使用
     * @return page\Builder|Block|Form|Table|Charts|Iframe|Html|DataPanel|CardList|GoodsForm
     * @throws Exception
     */
    public static function make(string $type = '', array $layout = [])
    {
        if ($type == '') {
            throw new Exception('未指定构建器名称', 8001);
        } else {
            $type = Str::snake($type);
        }

        // 页面构建器
        $class = '\\evan\\sbuilder\\builder\\'. $type .'\\Builder';
        if (class_exists($class)) {
            return new $class;
        }
        // 区块构建器, 用于构建单区块页面
        $class = '\\evan\\sbuilder\\builder\\block\\' . Str::studly($type);
        if(class_exists($class)){
            $block = new $class;
            $page = new Builder();
            if($type == 'form' && !isset($layout['span'])){
                $layout['span'] = 24;
            }
            $page->addBlock('block1', $block, $layout);
            return $block;
        }
        // 自定义扩展构建器
        $class = '\\evan\\sbuilder\\builder\\extend\\' . Str::studly($type);
        if(class_exists($class)){
            $block = new $class;
            if($block instanceof Block){
                $page = new Builder();
                $page->addBlock('block1', $block, $layout);
            }
            return $block;
        }

        throw new Exception($type . '构建器不存在', 8002);
    }

    /**
     * 渲染页面数据
     * @param array $vars     模板输出变量
     * @return mixed
     */
    public function fetch(array $vars = [])
    {
        $vars = array_merge(self::$vars, $vars);
        $vars['page_title'] = $vars['page_title'] ?: '无标题';
        return $vars;
    }

}