<?php


namespace evan\sbuilder\builder\extend;


use evan\sbuilder\builder\block\Form;

class GoodsForm extends Form
{

    public function __construct()
    {
        parent::__construct();
        $this->_vars['type'] = 'goods_form';
    }

    /**
     * 设置分类树
     * @param $catTree
     * @return GoodsForm
     */
    public function setCatTree($catTree)
    {
        $this->_vars['cat_tree'] = $catTree;
        return $this;
    }

}