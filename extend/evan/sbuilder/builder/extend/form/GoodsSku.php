<?php


namespace evan\sbuilder\builder\extend\form;


class GoodsSku extends FormItem
{

    public function fetch($name, $title)
    {
        return [
            'type' => 'goods_sku',
            'name' => $name,
            'label' => $title,
        ];
    }
}