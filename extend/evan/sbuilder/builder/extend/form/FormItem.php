<?php


namespace evan\sbuilder\builder\extend\form;


use evan\sbuilder\builder\block\Form;

/**
 * 表单项类, 自定义的表单项需要可以继承此类, 并需要实现一个fetch方法, 此方法需要返回一个表单项配置数组
 * Class FormItem
 * @package evan\sbuilder\builder\extend\form
 */
class FormItem
{
    /**
     * @var Form
     */
    protected $builder;

    public function __construct(Form $builder)
    {
        $this->builder = $builder;
    }

}