<?php


namespace evan\sbuilder\builder\page;

use evan\sbuilder\builder\block\Block;
use evan\sbuilder\builder\SBuilder;
use think\helper\Str;

/**
 * 页面构建器, 构建一个基础页面
 * Class Builder
 * @package evan\sbuilder\builder\page
 */
class Builder extends SBuilder
{
    protected $_vars = [
        '_id' => '', // 页面id, 用于区分不同页面
        'page_title' => '', // 页面标题,
        'page_tips' => [], // 页面提示 {msg:提示内容, type:success|info|warning|error}
        'nav_tab_list' => [], // 页面tab按钮列表 {title, url, current: 1|null}
        'nav_tab_current' => '', // 当前选中的tab
        'closable_on_modal' => false, // 弹窗布局下是否点击遮罩关闭弹框
        'background' => '', // 页面背景色
        'blocks' => [], // 页面区块内容, 一般情况下是单个区块, 也可以是多个区块, 如表单、列表等
    ];

    public function __construct()
    {
        // 初始化页面唯一ID
        $this->_vars['_id'] = md5(request()->module() . '/' . request()->controller() . '/' . request()->action());
    }

    /**
     * 设置页面标题
     * @param string $pageTitle
     * @return $this
     */
    public function setPageTitle($pageTitle = '')
    {
        $this->_vars['page_title'] = $pageTitle;
        return $this;
    }

    /**
     * 设置页面背景色
     * @param $background
     * @return $this
     */
    public function setPageBackground($background = '')
    {
        if($background != ''){
            $this->_vars['background'] = $background;
        }
        return $this;
    }

    /**
     * 设置弹窗布局下是否点击遮罩关闭弹框
     * @return $this
     */
    public function closableOnModal($closableOnModal = true)
    {
        $this->_vars['closable_on_modal'] = $closableOnModal;
        return $this;
    }

    /**
     * 添加页面提示信息
     * @param string|array $msg 提示信息的内容，也可为提示信息数组
     * @param string $type 提示类型，可选值为 info success warning danger
     * @param string $desc 详细信息
     * @param bool $showIcon 是否展示图标
     * @return $this
     */
    public function setPageTips($msg = '', $type = 'info', $desc = '', $showIcon=false)
    {
        if(!empty($msg)){
            if(is_array($msg)) { // 如果是数组, 只接受二维数组
                foreach ($msg as $item) {
                    if (!empty($item) && is_array($item)) {
                        $this->_vars['page_tips'][] = [
                            'title' => $item[0],
                            'type' => isset($item[1]) && !empty($item[1]) ? $item[1] : 'info',
                            'desc' => isset($item[2]) ? $item[2] : '',
                            'showIcon' => isset($item[3]) ? $item[3] : false,
                        ];
                    }
                }
            }else {
                $this->_vars['page_tips'][] = [
                    'title' => $msg,
                    'type' => $type ?: 'info',
                    'desc' => $desc,
                    'showIcon' => $showIcon,
                ];
            }
        }
        return $this;
    }

    /**
     * 设置Tab按钮列表, 此操作会清空原来设置的值
     * @param string|array $name tab按钮名称，用于设置当前选中按钮时的标识，此参数也可以传Tab按钮列表
     * @param string $title tab按钮的标题
     * @param string $url tab按钮点击后的跳转路径, 支持pop:url
     * @return $this
     */
    public function setNavTab($name = '', $title = '', $url = '')
    {
        if(!empty($name)){
            if(is_array($name)){ // 如果是数组, 则以二维数组的方式添加
                $tab_list = [];
                foreach ($name as $item) {
                    if(!empty($item) && is_array($item)){
                        // 解析操作
                        $title = isset($item[1]) ? $item[1] : $item[0];
                        $url = isset($item[2]) ? $item[2] : '';
                        list($type, $url, $target) = self::parseOptionUrl($url);
                        $tab_list[] = [
                            'name' => $item[0],
                            'title' => $title,
                            'url' => $url,
                            'type' => $type,
                            'target' => $target,
                        ];
                    }
                }
            }else {
                // 解析操作
                list($type, $url, $target) = self::parseOptionUrl($url);
                $tab_list = [
                    ['name' => $name, 'title' => $title, 'url' => $url, 'type' => $type, 'target' => $target]
                ];
            }
            $this->_vars['nav_tab_list'] = $tab_list;
        }
        return $this;
    }

    /**
     * 添加 NavTab 按钮, 此操作会在原来的数组后追加
     * 这个方法意义不大, 计划去掉
     * @param string $name
     * @param string|array $title
     * @param string $url
     * @return $this
     * @deprecated
     */
    public function addNavTab($name = '', $title = '', $url = '')
    {
        if(!empty($name)){
            if(is_array($name)){
                foreach ($name as $item) {
                    if(!empty($item) && is_array($item)){
                        // 解析操作
                        $title = isset($item[1]) ? $item[1] : $item[0];
                        $url = isset($item[2]) ? $item[2] : '';
                        list($type, $url, $target) = self::parseOptionUrl($url);
                        $this->_vars['nav_tab_list'][] = [
                            'name' => $item[0],
                            'title' => $title,
                            'url' => $url,
                            'type' => $type,
                            'target' => $target,
                        ];
                    }
                }
            }else {
                // 解析操作
                list($type, $url, $target) = self::parseOptionUrl($url);
                $this->_vars['nav_tab_list'][] = [
                    'name' => $name, 'title' => $title, 'url' => $url, 'type' => $type, 'target' => $target
                ];
            }
        }
        return $this;
    }

    /**
     * 设置当前选中的nav_tab
     * @param string $name 当前选中的tab按钮名称
     * @return $this
     */
    public function setNavTabCurrent($name = '')
    {
        if($name != ''){
            $this->_vars['nav_tab_current'] = $name;
        }
        return $this;
    }

    /**
     * 添加一个区块
     * @param string $name
     * @param Block $block
     * @param array $layout 布局 {span: 宽度, 1-24, height: 高度, eg: 200px}
     * @return Builder
     */
    public function addBlock($name, $block, $layout = [])
    {
        if(!empty($block)){
            if(isset($layout['height']) && is_int($layout['height'])){
                $layout['height'] .= 'px';
            }
            if($block instanceof Block) {
                $block->setPage($this)->setName($name)->setLayout($layout);
            }else {
                !empty($name) && $block['name'] = $name;
                !empty($layout) && $block['layout'] = $layout;
            }
            $this->_vars['blocks'][] = $block;
        }
        return $this;
    }

    /**
     * 渲染页面数据
     * @param array $vars
     * @return mixed
     */
    public function fetch(array $vars = [])
    {
        $this->_vars = array_merge($this->_vars, $vars);
        // 设置各项默认值
        // 页面标题
        $this->_vars['page_title'] = $this->_vars['page_title'] ?: '无标题';
        // 当前选中tab页
        if(empty($this->_vars['nav_tab_current']) && count($this->_vars['nav_tab_list']) > 0){
            $this->_vars['nav_tab_current'] = $this->_vars['nav_tab_list'][0]['name'];
        }
        // 区块渲染
        foreach ($this->_vars['blocks'] as &$item) {
            if($item instanceof Block){
                $item = $item->fetch([], true);
            }
        }
        return parent::fetch($this->_vars);
    }


    public static function parseOptionUrl($url)
    {
        if($url == '__back__'){
            $type = 'back';
            $url = '';
        }else if($url == '__relist__'){ // 重新获取列表, 列表页才有效
            $type = 'relist';
            $url = '';
        }else if($url == '__refresh__'){
            $type = 'refresh';
            $url = '';
        }else if(Str::startsWith($url, 'pop:')){
            $type = 'pop';
            $url = Str::substr($url, 4);
        }else if(Str::startsWith($url, 'blank:')){
            $type = 'link';
            $target = '_blank';
            $url = Str::substr($url, 6);
        }else if(Str::startsWith($url, 'ajax:')){
            $type = 'ajax';
            $url = Str::substr($url, 5);
        }else {
            $type = 'link';
        }
        return [$type, $url, $target ?? ''];
    }
}