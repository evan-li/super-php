<?php

namespace evan\sbuilder\builder\block;

class Html extends Block
{

    protected $_vars = [
        'name' => '',
        'type' => 'html',
        'layout' => [], // 布局参数: {span, height}
        'content' => null, // html内容
    ];

    public function setContent($content = null){
        if ($content) {
            $this->_vars['content'] = $content;
        }
        return $this;
    }

}