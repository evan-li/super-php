<?php


namespace evan\sbuilder\builder\block;


use evan\sbuilder\builder\page\Builder;
use think\Exception;

/**
 * Class Block
 * @package evan\sbuilder\builder\block
 * @see Builder
 *
 * @method $this setPageTitle($pageTitle = '')
 * @method $this setPageBackground($background = '')
 * @method $this setPageTips($msg = '', $type = 'info', $desc = '', $showIcon=false)
 * @method $this setNavTab($name = '', $title = '', $url = '')
 * @method $this addNavTab($name = '', $title = '', $url = '')
 * @method $this setNavTabCurrent($name = '')
 * @method $this closableOnModal($closableOnModal = true)
 * @method $this addBlock($name, Block $block, $layout = [])
 *
 */
class Block
{

    /**
     * @var array 区块元素, 内容自定义
     */
    protected $_vars = [];

    /**
     * @var Builder
     */
    protected $page; // 当前页面构建器的引用

    /**
     * @return static
     */
    public static function create(){
        return new static();
    }

    /**
     * 设置当前页面构建器
     * @param Builder $page
     * @return $this
     */
    public function setPage(Builder $page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * 设置区块名
     * @param $name string 区块名称
     * @return $this
     */
    public function setName($name)
    {
        if(!empty($name)) {
            $this->_vars['name'] = $name;
        }
        return $this;
    }

    /**
     * 设置区块布局
     * @param $layout array 布局参数, 通用: {span: 区块宽度(1-24) }, 另外各个类型的区块可以扩充自己的布局参数, 如图表额外支持的字段: {width: 图表宽度(px), height: 图表高度(px), padding: 图表边距(px) }
     * @return $this
     */
    public function setLayout($layout)
    {
        if(!empty($layout)) {
            $this->_vars['layout'] = $layout;
        }
        return $this;
    }

    /**
     * @param $name
     * @param $arguments
     * @return $this
     * @throws Exception
     */
    public function __call($name, $arguments)
   {
       // page 构建器方法调用处理
       if(in_array($name, [
           'setPageTitle',
           'setPageBackground',
           'setPageTips',
           'setNavTab',
           'addNavTab',
           'setNavTabCurrent',
           'closableOnModal',
           'addBlock',
       ])){
           $this->page->{$name}(...$arguments);
           return $this;
       }
       throw new Exception("方法 $name 不存在");
   }

    /**
     * 渲染页面
     * @param array $vars
     * @param bool $getVars
     * @return mixed
     */
    public function fetch($vars = [], $getVars = false)
    {
        if($getVars){
            return $this->_vars;
        }
        return $this->page->fetch($vars);
    }

}