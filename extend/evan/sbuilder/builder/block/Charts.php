<?php


namespace evan\sbuilder\builder\block;


use stdClass;

class Charts extends Block
{
    protected $_vars = [
        'name' => '',
        'title' => '',
        'type' => 'charts',
        'xColumn' => '', // x轴的列名key
        'option' => [ // 图标参数
            'title' => '', // 图标的标题文字
            'tooltip' => [ // tooltip必须是一个对象
                'show' => true,
            ],
            'xAxis' => [
                'show' => true,
                'data' => [], // x轴列表
                'type' => 'category',
            ],
            'yAxis' => [ // yAxis必须是一个对象
                'show' => true,
            ],
            'dataset' => [ // 数据源
                'source' => [],
            ],
            'series' => [],
        ],
        'layout' => [ // 布局参数
            'height' => '500px',
            'padding' => '0',
        ],
    ];

    public function __construct()
    {
    }

    /**
     * 设置标题
     * @param string|array $title @see https://echarts.apache.org/zh/option.html#title
     * @return $this
     */
    public function setTitle($title = '')
    {
        if(!empty($title)) {
            $this->_vars['title'] = $title;
        }
        return $this;
    }

    /**
     * 设置图表option中的标题, 此标题渲染在图表中
     * @param string|array $title @see https://echarts.apache.org/zh/option.html#title
     * @return $this
     */
    public function setChartsTitle($title = '')
    {
        if(!empty($title)) {
            $this->_vars['option']['title'] = is_string($title) ? ['text' => $title] : $title;
        }
        return $this;
    }

    /**
     * 设置是否显示tooltip
     * @param bool $show
     * @return $this
     */
    public function setToolTip($show = true)
    {
        $this->_vars['tooltip']['show'] = $show;
        return $this;
    }

    /**
     * 设置x轴显示的列
     * @param string $column
     * @return $this
     */
    public function setXColumn($column = '')
    {
        if(!empty($column)) {
            $this->_vars['xColumn'] = $column;
        }
        return $this;
    }

    /**
     * 设置x轴类型
     * @param string $type value-数值轴, category-类目轴 time-时间轴, log-对数轴
     * @return $this
     */
    public function setXType($type = 'category')
    {
        if(!empty($type)) {
            $this->_vars['option']['xAxis']['type'] = $type;
        }
        return $this;
    }

    /**
     * 添加柱状图
     * @param string $yColumn y轴显示的列
     * @param string $name 展示名称, 用于在tooltip中展示
     * @return Charts
     */
    public function addBar($yColumn = '', $name = '')
    {
        if(!empty($yColumn)) {
            $this->addSeries([
                'name' => $name,
                'type' => 'bar',
                'encode' => [
                    'x' => $this->_vars['xColumn'],
                    'y' => $yColumn,
                ],
            ]);
        }
        return $this;
    }

    /**
     * 添加一个折线图
     * @param string $yColumn
     * @param string $name
     * @param bool $smooth 是否平滑曲线
     * @param bool|array $areaStyle 是否填充
     * @return $this
     */
    public function addLine($yColumn = '', $name = '', $smooth = false, $areaStyle = false)
    {
        if(!empty($yColumn)) {
            $series = [
                'name' => $name,
                'type' => 'line',
                'smooth' => $smooth,
                'encode' => [
                    'x' => $this->_vars['xColumn'],
                    'y' => $yColumn,
                ],
            ];
            if($areaStyle === true) {
                $series['areaStyle'] = new stdClass();
            }else if(is_array($areaStyle)) {
                $series['areaStyle'] = $areaStyle;
            }
            $this->addSeries($series);
        }
        return $this;
    }

    /**
     * 设置数据
     * @param array $data
     * @param string $xColumn
     * @param string $xType
     * @return $this
     */
    public function setData($data = [], $xColumn = '', $xType = 'category')
    {
        if(!empty($data)) {
            // 设置x轴列名
            if(!empty($xColumn)) {
                $this->setXColumn($xColumn);
            }
            // 设置x轴类型
            if(!empty($xType)) {
                $this->setXType($xType);
            }
            // 处理x轴数据
            $xData = array_column($data, $this->_vars['xColumn']);
            $this->_vars['option']['xAxis']['data'] = $xData;
            // 设置数据
            $this->_vars['option']['dataset']['source'] = $data;
        }
        return $this;
    }

    /**
     * 设置x轴原始数据, 数据结构参考echarts文档 @see https://echarts.apache.org/zh/option.html#xAxis
     * @param array $xAxis
     * @return $this
     */
    public function setXAxis($xAxis = [])
    {
        if(!empty($xAxis)) {
            $this->_vars['option']['xAxis'] = $xAxis;
        }
        return $this;
    }

    /**
     * 添加一个图表
     * @param $series
     * @return $this
     */
    public function addSeries($series)
    {
        if(!empty($series)) {
            $this->_vars['option']['series'][] = $series;
        }
        return $this;
    }

    /**
     * 设置图标参数, 此参数设置后其他设置被覆盖
     * @param array $option
     * @return $this
     */
    public function setOption($option = [])
    {
        if(!empty($option)) {
            $this->_vars['option'] = $option;
        }
        return $this;
    }

}