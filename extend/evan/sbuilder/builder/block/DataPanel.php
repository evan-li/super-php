<?php


namespace evan\sbuilder\builder\block;


use stdClass;

class DataPanel extends Block
{
    protected $_vars = [
        'name' => '',
        'type' => 'data-panel',
        'layout' => [], // 布局参数: {span, height}
        'props' => null, // 属性名定义 {title: title, value: value, second_value: second_value, icon: icon, icon_color: icon_color}
        'data' => null, // 数据, 格式为: [{title, value, second_value, icon, icon_color}]
        'ajax_data_url' => false, // ajax获取数据的url, 非ajax动态获取数据时为false
    ];

    public function __construct()
    {
    }

    public function setProps($props = null)
    {
        $this->_vars['props'] = $props;
        return $this;

    }

    /**
     * 设置当前数据面版内的数据
     * @param $data array 数据格式为: [{title, value, second_value, icon, icon_color}, ...] 其中title与value为必填参数
     * @return $this
     */
    public function setData($data = null): DataPanel
    {
        $this->_vars['data'] = $data;
        return $this;
    }

    /**
     * 设置ajax获取数据
     * @param $url string|false ajax获取数据的url, 非ajax动态获取数据时为false
     * @return $this
     */
    public function setAjaxDataUrl($url = false): DataPanel
    {
        $this->_vars['ajax_data_url'] = $url;
        return $this;
    }
}