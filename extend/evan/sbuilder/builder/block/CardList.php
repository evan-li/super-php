<?php

namespace evan\sbuilder\builder\block;

use evan\sbuilder\util\Util;

class CardList extends Block
{
    protected $_vars = [
        'name' => '',
        'type' => 'card-list',
        'layout' => [], // 布局参数: {span, height}
        'props' => null, // 属性名定义 {cover: cover, name: name, tags: tags, count: count, count_icon: count_icon, price: price, price_prefix: price_prefix, price_unit: price_unit, crossed_price: crossed_price}
        // 数据列表, 格式为: [{cover:string|array, name, tags: [{title, type}], count, count_icon, price, crossed_price, price_prefix, price_unit, html: 若有此字段则渲染为html,其他字段全部无效}]
        // 若需要分页, 则分页数据格式: {total, per_page, current_page, list: []}
        'data' => null,
        'ajax_data_url' => false, // ajax获取数据的url, 非ajax动态获取数据时为false
        'detail_link' => [ // 点击跳转到详情的链接
            'type' => 'pop', // 详情打开方式 pop-弹框 link-链接跳转
            'target' => null, // 链接跳转打开方式  _blank : 新窗口打开
            'url' => '', // 点击跳转详情url
        ],

    ];

    public function __construct()
    {
    }

    /**
     * @param $props array 属性名定义 {cover: cover, name: name, tags: tags, count: count, count_icon: count_icon, price: price, price_prefix: price_prefix, price_unit: price_unit, show_price: show_price, crossed_price: crossed_price}
     * @return $this
     */
    public function setProps($props = null): CardList
    {
        $this->_vars['props'] = $props;
        return $this;

    }

    /**
     * 设置当前数据面版内的数据
     * @param $data array 数据格式为: [{cover:string|array, name, count, count_icon, price, crossed_price, show_price, price_prefix, price_unit, show_btn, btn: {type, text} , tags: [{title, type}], html: 若有此字段则渲染为html,其他字段全部无效}]
     * @return $this
     */
    public function setData($data = null): CardList
    {
        $this->_vars['data'] = $data;
        return $this;
    }

    /**
     * 设置ajax获取数据
     * @param $url string|false ajax获取数据的url, 非ajax动态获取数据时为false
     * @return $this
     */
    public function setAjaxDataUrl($url = false): CardList
    {
        $this->_vars['ajax_data_url'] = $url;
        return $this;
    }

    /**
     * 设置详情跳转链接
     * @param $url string 示例: pop:url('detail')  blank:url('detail')
     * @return $this
     */
    public function setDetailLink($url = ''): CardList
    {
        if ($url) {
            $this->_vars['detail_link'] = Util::parseOptionUrl($url, 'pop');
        }
        return $this;
    }
}