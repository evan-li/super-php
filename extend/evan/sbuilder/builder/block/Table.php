<?php


namespace evan\sbuilder\builder\block;


use evan\sbuilder\builder\button\Button;
use evan\sbuilder\util\Util;
use stdClass;
use think\Collection;
use think\facade\Cache;
use think\Paginator;

class Table extends Block
{

    // 页面配置信息
    protected $_vars = [
        //---------- 表配置 -----------
        'name' => '', // 区块名, 用于识别区块
        'type' => 'table',
        'layout' => [], // 表格布局参数
        'table_name' => '', // 数据库表名
        'pk' => 'id', // 主键名
        'lazy' => false, // 懒加载, 树形表格支持
        'lazy_load_url' => '', // 懒加载数据地址
        'size' => 'default', // 表格大小

        //---------- 表格操作配置 -----------
        /**
         * 操作按钮
         * @param array $attr 按钮属性: { type:default|primary|success|info|warning|danger|text,round:true|false,plain:true|false,circle:true|false, icon, disabled}
         * @param array $option 操作: { type:submit提交表单|link页面跳转|pop打开弹窗|null无操作, url, target link类型有效:_blank新页面打开, confirm操作前弹框确认: false|true|{title, msg}, extra_data额外数据: []}
         */
        'top_buttons' => [], // 顶部按钮列表, 一般用于添加新数据或批量操作
        'search' => false, // 搜索栏配置 {columns: {name|字段名: title|字段标题}, tips: 请输入关键字, url: 搜索提交地址, btn_text: 提交按钮文字|true}
        'search_ext' => [], // 扩展搜索字段 [{columns: {name|字段名: title|字段标题}, tips: 请输入关键字, url: 搜索提交地址, btn_text: 提交按钮文字|true}]
        'time_filters' => [], // 时间段筛选列 [{name: 字段名, type:类型(time|date|datetime), tips: [起始时间标题, 结束时间标题], default: [默认起始时间, 默认结束时间]}, ]
        'top_selects' => [], // 顶部筛选框列表

        //---------- 表格内容配置 ----------
        'default_expand_all' => false, // 是否默认展开所有行(针对树形表格及可展开行表格)
        'show_checkbox' => false, // 是否显示第一列多选框
        'show_index' => false, // 数量索引 false|true|表头名, 为true时表头默认为'#'
        'show_index_paginate' => true, // 数量索引分页时是否根据页面递增显示
        'columns' => [], // 表格的列
        'row_list' => [], // 列表数据, 支持树形数据
        'ajax_get_list' => true, // 是否通过ajax方式获取列表
        'row_list_url' => '', // 列表数据获取地址
        'row_list_paginate' => true, // 列表数据是否分页(用于自动获取列表数据)
        'paginate' => [
            'show' => false, // 是否开启分页
            'total' => 0, // 总记录数
            'last_page' => 0, // 最后一页的页码
            'page_size' => 10, // 每页大小
            'show_sizes' => true, // 显示页大小调整器
        ], // 分页相关配置
        'quick_edit_url' => '', // 快捷编辑url地址
        'children_key' => 'children', // 树形数据中子元素的键名, 默认为 children
        'sortable_columns' => [], // 可排序的字段列表
        'filter_columns' => [], // 表头筛选的字段
        'column_widths' => [], // 列宽
        'right_buttons' => [], // 右侧按钮列表, 用于表格中每列数据的操作
    ];

    // 添加分组数据
    protected $isGroup = false;

    // 自动新增配置
    protected $autoAdd = null;
    // 自动编辑配置
    protected $autoEdit = null;

    // 是否隐藏列表中的刷新按钮
    protected $hideRefreshBtn = false;

    /**
     * Builder constructor.
     */
    public function __construct()
    {

    }

    /**
     * 设置表名, 默认表名为当前控制器名转下划线格式
     * @param string $tableName 表名
     * @return $this
     */
    public function setTableName($tableName = '')
    {
        if(!empty($tableName)){
            $this->_vars['table_name'] = $tableName;
        }
        return $this;
    }

    /**
     * 设置主键
     * @param $pk
     * @return Table
     */
    public function setPk($pk) {
        $this->_vars['pk'] = $pk;
        return $this;
    }

    /**
     * 设置表格树形结构懒加载
     * @param bool $lazy
     * @return $this
     */
    public function setLazyLoad($lazy = true)
    {
        $this->_vars['lazy'] = $lazy;
        return $this;
    }

    public function setLazyLoadUrl($url = '')
    {
        if($url) {
            $this->setLazyLoad();
            $this->_vars['lazy_load_url'] = $url;
        }else {
            $this->setLazyLoad(false);
        }
        return $this;
    }

    /**
     * 设置树形结构中的子元素键名
     * @param string $childrenKey
     * @return $this
     */
    public function setChildrenKey($childrenKey = '')
    {
        if($childrenKey) {
            $this->_vars['children_key'] = $childrenKey;
        }
        return $this;
    }

    /**
     * 添加顶部按钮
     * @param string $type 按钮类型：add/enable/disable/delete/custom
     * @param string $title 按钮标题
     * @param string $url 按钮操作链接
     * @param string $linkType 链接类型 link-普通跳转 blank|_blank-新页面打开 ajax-ajax请求 pop-打开弹窗 null-无操作
     * @param array $attr {
     *      tooltip: 是否显示tooltip, 为false时不显示, 为true时显示title, 值为字符串时显示指定的字符串, 默认为true, 即显示title
     *      type: default|primary|success|info|warning|danger, // 按钮类型，默认 primary
     *      text: 是否为文本按钮, 默认为false,
     *      round:true|false, // 按钮是否圆角，默认 false
     *      plain:true|false, // 是否朴素按钮，默认 false
     *      circle:true|false, // 是否圆形按钮，默认 false
     *      icon, // 按钮图标，可选择的图标见icon表单项
     *      disabled, // 是否禁用，默认 false
     *      target:  按钮链接跳转类型(linkType为link时有效, _blank表示新页面打开链接),
     *      confirm: false|true|{title, tips, type:warning}, 按钮点击时是否需要确认框,
     *      extra_data: 附加数据, 表单提交时会把附加数据提交,
     *      batch: 是否批量操作(批量操作时会附带当前列表中选中的数据ID), 默认为true
     *      with_search_form: 是否鞋带查询表单数据, 默认为false
     * }
     * @return $this
     */
    public function addTopButton($type = '', $title = '', $url = '', $linkType = 'pop', $attr = [])
    {
        if(!empty($type)){
            if ($type instanceof Button) {
                $button = $type->fetch();
            }else {
                $target = $attr['target'] ?? '';
                if ($linkType == 'blank' || $linkType == '_blank') {
                    $linkType = 'link';
                    $target = '_blank';
                }
                $button = [
                    // 按钮文字
                    'title' => $title,
                    'tooltip' => $attr['tooltip'] ?? true,
                    // 按钮属性 { type:default|primary|success|info|warning|danger|text,round:true|false,plain:true|false,circle:true|false, icon, disabled}
                    'attr' => [
                        'type' => $attr['type'] ?? 'primary',
                        'text' => $attr['text'] ?? false,
                        'round' => $attr['round'] ?? false,
                        'plain' => $attr['plain'] ?? false,
                        'circle' => $attr['circle'] ?? false,
                        'icon' => $attr['icon'] ?? '',
                        'disabled' => $attr['disabled'] ?? false,
                    ],
                    // 操作参数 { type:ajax提交表单|link页面跳转|pop打开弹窗|null无操作, url, target link类型有效:_blank新页面打开, confirm操作前弹框确认: false|true|{title, msg}, extra_data额外数据: [], batch: 是否批量操作(会附带当前选中的数据ID), with_search_form: 是否携带表单数据}
                    'option' => [
                        'type' => $linkType ?: 'link',
                        'url' => $url,
                        'target' => $target,
                        'confirm' => $attr['confirm'] ?? false,
                        'extra_data' => $attr['extra_data'] ?? null,
                        'batch' => $attr['batch'] ?? true,
                        'with_search_form' => $attr['with_search_form'] ?? false,
                    ],
                    // 按钮类型
                    'flag' => $type, // 按钮标记, 用于前端对特殊的按钮做特殊处理
                ];
                switch ($type) {
                    // 添加按钮
                    case 'add':
                        $button['title'] = $title ?: '添加';
                        $button['attr']['icon'] = $button['attr']['icon'] ?: 'el-icon-Plus';
                        $button['attr']['type'] = $attr['type'] ?? 'primary';
                        $button['option']['url'] = $url ?: url('add');
                        $button['option']['batch'] = false;
                        break;//
                    // 批量启用
                    case 'enable':
                        $button['title'] = $title ?: '批量启用';
                        $button['attr']['icon'] = $button['attr']['icon'] ?: 'el-icon-Check';
                        $button['option']['type'] = 'ajax';
                        $button['option']['url'] = $url ?: url('batch');
                        $button['option']['extra_data'] = ['type' => $type];
                        $button['option']['batch'] = true;
                        break;//
                    // 批量禁用
                    case 'disable':
                        $button['title'] = $title ?: '批量禁用';
                        $button['attr']['icon'] = $button['attr']['icon'] ?: 'el-icon-Remove';
                        $button['attr']['type'] = $attr['type'] ?? 'warning';
                        $button['option']['type'] = 'ajax';
                        $button['option']['url'] = $url ?: url('batch');
                        $button['option']['extra_data'] = ['type' => $type];
                        $button['option']['batch'] = true;
                        break;//
                    // 批量删除操作
                    case 'delete':
                        $button['title'] = $title ?: '批量删除';
                        $button['attr']['icon'] = $button['attr']['icon'] ?: 'el-icon-Delete';
                        $button['attr']['type'] = $attr['type'] ?? 'danger';
                        $button['option']['type'] = 'ajax';
                        $button['option']['url'] = $url ?: url('batch');
                        $button['option']['extra_data'] = ['type' => $type];
                        $button['option']['confirm'] = ['title' => '确定要删除所选择的数据吗?', 'tips' => '删除后将不可恢复!', 'type' => 'error'];
                        $button['option']['batch'] = true;
                        break;//
                    // 自定义按钮
                    default:
                        break;
                }
            }
            $this->_vars['top_buttons'][] = $button;
        }
        return $this;
    }

    /**
     * 添加时间段筛选
     * @param string $name 要筛选的字段名
     * @param array|string $placeholder 开始时间与结束时间的placeholder,使用数组或逗号分隔的字符串
     * @param string $type date|datetime 类型，默认为date
     * @param array|string $default 默认值
     * @return $this
     */
    public function addTimeFilter($name = '', $placeholder = [], $type = 'date', $default = [])
    {
        if(!empty($name)){
            if(is_array($name)){
                foreach ($name as $item) {
                    $this->addTimeFilter(...$item);
                }
            }else {
                if(is_string($placeholder)){
                    $placeholder = explode(',', $placeholder);
                }
                if(is_string($default)){
                    $default = explode(',', $default);
                }
                $this->_vars['time_filters'][] = [
                    'name' => $name,
                    'placeholder' => $placeholder,
                    'type' => $type,
                    'default' => $default,
                ];
            }
        }
        return $this;
    }

    /**
     * 设置搜索栏
     * @param array|string $columns 'id|ID|=,name|名称|like' 或 ['id|ID|=', 'name|名称|like']
     * @param string $tips
     * @param array $default
     * @param string $url
     * @param string|bool $btnText
     * @return $this
     */
    public function setSearch($columns = [], $tips = '请输入关键字', $default = [], $url = '', $btnText = false)
    {
        if(!empty($columns)){
            // 处理列
            if(is_string($columns)){
                $columns = explode(',', $columns);
            }
            $columnsArr = [];
            foreach ($columns as $key => $val) {
                $column = explode('|', $val);
                $field = $column[0];
                $label = $column[1] ?? $field;
                $op = $column[2] ?? 'like';
                $columnsArr[] = [
                    'field' => trim($field),
                    'label' => trim($label),
                    'op' => trim($op),
                ];
            }
            if(is_array($default) && array_key_exists(0, $default) && array_key_exists(1, $default)){
                $defaultField = $default[0];
                $defaultKeyword = $default[1];
            }else if(is_array($default) && count($default) > 0){
                $defaultField = array_keys($default)[0];
                $defaultKeyword = $default[$defaultField];
            }else {
                $defaultField = null;
                $defaultKeyword = null;
            }

            // 最后fetch时处理url
            $this->_vars['search'] = [
                'columns' => $columnsArr,
                'tips' => $tips,
                'url' => $url,
                'btn_text' => $btnText,
                'default_field' => $defaultField,
                'default_keyword' => $defaultKeyword,
            ];
        }
        return $this;
    }

    /**
     * 添加顶部搜索字段
     * @param array|string $columns
     * @param string $tips
     * @param array $default
     * @param string $url
     * @param bool $btnText
     * @return $this
     */
    public function addTopSearch($columns = [], $tips = '请输入关键字', $default = [], $url = '', $btnText = false) {
        if(!empty($columns)){
            // 处理列
            if(is_string($columns)){
                $columns = explode(',', $columns);
            }
            $columnsArr = [];
            foreach ($columns as $val) {
                $column = explode('|', $val);
                $field = $column[0];
                $label = $column[1] ?? $field;
                $op = $column[2] ?? 'like';
                $columnsArr[] = [
                    'field' => trim($field),
                    'label' => trim($label),
                    'op' => trim($op),
                ];
            }
            if(is_array($default) && array_key_exists(0, $default) && array_key_exists(1, $default)){
                $defaultField = $default[0];
                $defaultValue = $default[1];
            }else if(is_array($default) && count($default) > 0){
                $defaultField = array_keys($default)[0];
                $defaultValue = $default[$defaultField];
            }else {
                $defaultField = null;
                $defaultValue = null;
            }
            // 最后fetch时处理url
            $this->_vars['search_ext'][] = [
                'columns' => $columnsArr,
                'tips' => $tips,
                'url' => $url,
                'btn_text' => $btnText,
                'default_field' => $defaultField,
                'default_value' => $defaultValue,
            ];
        }
        return $this;
    }

    /**
     * 添加禁用按钮
     * @param int $disableStatus
     * @param string $icon
     * @return $this
     */
    public function addRightDisableBtn($disableStatus = 0, $icon = 'el-icon-sicon sicon-disabled')
    {
        $this->addRightButton('disable', '禁用', url('disable'), 'ajax', [
//            'icon' => $icon,
            'confirm' => ['title' => '确定要禁用吗?', 'type' => 'warning'],
            'hide' => ['status' => $disableStatus]
        ]);
        return $this;
    }

    /**
     * 列表右侧操作按钮
     * @param string $type 按钮类型：edit/enable/disable/delete/custom
     * @param string $title 按钮标题
     * @param string $url 按钮操作链接
     * @param string $linkType 链接类型 link-普通跳转 blank|_blank-新页面打开 ajax-ajax请求 pop-打开弹窗 null-无操作
     * @param array $attr {
     *      tooltip: 是否显示tooltip, 为false时不显示, 为true时显示title, 值为字符串时显示指定的字符串, 默认为true, 即显示title
     *      type: default|primary|success|info|warning|danger, // 按钮类型，默认 primary
     *      text: 是否为文本按钮,
     *      round: 是否圆角,
     *      plain: 是否朴素按钮(默认true),
     *      circle: 是否圆形按钮,
     *      icon: 按钮图标,
     *      disabled: 按钮是否禁用,
     *      text: 是否为文字按钮,
     *      target: 按钮链接跳转类型(linkType为link时有效),
     *      confirm: false|true|{title, tips, type:warning}-按钮点击时是否需要确认框,
     *      extra_data: 附加数据, 表单提交时会把附加数据提交,
     *      batch: 是否批量操作(会附带当前选中的数据ID),
     *      hide: true|{field: value}，按钮隐藏规则，当指定的字段值与指定的值相等时隐藏该按钮
     *      show: false|{field: value}，按钮展示规则，当指定的字段值与指定的值相等时展示该按钮, 此值配置后hide配置无效
     * }
     * @return $this
     */
    public function addRightButton($type = '', $title = '', $url = '', $linkType = 'pop', $attr = [])
    {
        if($type instanceof Button) {
            $button = $type->fetch();
        }else {
            if ($linkType == 'blank' || $linkType == '_blank') {
                $linkType = 'link';
                $attr['target'] = '_blank';
            }
            $button = [
                // 按钮文字
                'title' => $title,
                'tooltip' => $attr['tooltip'] ?? true,
                // 按钮属性 { type:default|primary|success|info|warning|danger|text,round:true|false,plain:true|false,circle:true|false, icon, disabled}
                'attr' => [
                    'type' => $attr['type'] ?? 'primary',
                    'text' => $attr['text'] ?? true,
                    'round' => $attr['round'] ?? false,
                    'plain' => $attr['plain'] ?? false,
                    'circle' => $attr['circle'] ?? false,
                    'icon' => $attr['icon'] ?? '',
                    'disabled' => $attr['disabled'] ?? false,
                ],
                // 操作参数 { type:ajax提交表单|link页面跳转|pop打开弹窗|null无操作, url, target link类型有效:_blank新页面打开, confirm操作前弹框确认: false|true|{title, msg}, extra_data额外数据: [], batch: 是否批量操作(会附带当前选中的数据ID)}
                'option' => [
                    'type' => $linkType ?: 'link',
                    'url' => $url,
                    'target' => $attr['target'] ?? '',
                    'confirm' => $attr['confirm'] ?? false,
                    'extra_data' => $attr['extra_data'] ?? null,
                ],
                // 按钮类型
                'flag' => $type, // 按钮标记, 用于前端对特殊的按钮做特殊处理
                'hide' => $attr['hide'] ?? false, // 按钮隐藏触发器
                'show' => $attr['show'] ?? true, // 按钮显示触发器, 有show时其他值都为隐藏
            ];
            // 右侧按钮若无附加数据, 统一添加ID字段
            $extra_data = $button['option']['extra_data'] ?: ['id' => '__id__'];
            $button['option']['extra_data'] = $extra_data;
            switch ($type) {
                case 'edit': // 编辑按钮
                    $button['title'] = $title ?: '编辑';
//                    $button['attr']['icon'] = $attr['icon'] ?? 'el-icon-Edit';
                    $button['option']['url'] = $url ?: url('edit');
                    break;
                case 'delete': // 删除按钮
                    $button['title'] = $title ?: '删除';
//                    $button['attr']['icon'] = $attr['icon'] ?? 'el-icon-Delete';
                    $button['option']['type'] = 'ajax';
                    $button['option']['url'] = $url ?: url('delete');
                    $button['option']['confirm'] = $attr['confirm'] ?? ['title' => '确定要删除这条信息吗?', 'tips' => '删除后将不可恢复!', 'type' => 'error'];
                    break;
                case 'enable': // 启用按钮
                    $button['title'] = $title ?: '启用';
//                    $button['attr']['icon'] = $attr['icon'] ?? 'el-icon-Check';
                    $button['option']['type'] = 'ajax';
                    $button['option']['url'] = $url ?: url('enable');
                    $button['option']['confirm'] = $attr['confirm'] ?? ['title' => '确定要启用吗?', 'type' => 'info'];
                    $button['hide'] = $attr['hide'] ?? ['status' => 1];
                    break;
                case 'disable': // 禁用按钮
                    $button['title'] = $title ?: '禁用';
//                    $button['attr']['icon'] = $attr['icon'] ?? 'el-icon-Remove';
                    $button['option']['type'] = 'ajax';
                    $button['option']['url'] = $url ?: url('disable');
                    $button['option']['confirm'] = $attr['confirm'] ?? ['title' => '确定要禁用吗?', 'type' => 'warning'];
                    $button['hide'] = $attr['hide'] ?? ['status' => 2];
                    break;
                default:
                    break;
            }
        }
        $this->_vars['right_buttons'][] = $button;
        return $this;
    }

    /**
     * 批量添加按钮
     * @param array $buttons
     * @return $this
     */
    public function addRightButtons($buttons = [])
    {
        if(!empty($buttons)){
            foreach ($buttons as $button) {
                $this->addRightButton(...$button);
            }
        }
        return $this;
    }

    /**
     * 设置列表中是否显示选择框
     * @param bool $show
     * @return $this
     */
    public function showCheckbox($show = true)
    {
        $this->_vars['show_checkbox'] = $show;
        return $this;
    }

    /**
     * 设置是否显示数量索引, 可以传字符串指定数量索引的标题
     * @param bool|string $show
     * @param bool $showIndexPaginate 分页时是否根据页面连续显示序号
     * @return $this
     */
    public function showIndex($show = true, $showIndexPaginate = true)
    {
        $this->_vars['show_index'] = $show === true ? '#' : $show;
        $this->_vars['show_index_paginate'] = $showIndexPaginate;
        return $this;
    }

    /**
     * 设置树形结构或可展开表格是否默认展开所有行
     * @param bool $expand
     * @return $this
     */
    public function defaultExpandAll($expand = true)
    {
        $this->_vars['default_expand_all'] = $expand;
        return $this;
    }

    /**
     * 添加列
     * @param string $name 字段名
     * @param string $title 标题
     * @param string $type 类型, 为空时默认为text类型
     * @param string|array $option 选项
     * @param string $default 值为空时的默认值
     * @param array $attr 其他属性, 如触发器等 {
     *      trigger: {key: value}, // 用于控制当前列是否显示, value可以是一个数组, 若传了trigger参数, 只有当前行数据中出现在value中时才显示
     *      fixed: 是否固定列,
     *      show_tooltip: 是否在长度超出后显示tooltip
     * }
     * @return $this|array
     */
    public function addColumn($name = '', $title = '', $type = '', $option = '', $default = '', $attr = [])
    {
        if(!empty($name)){
            // name 特殊处理
            if($name == '__index__'){ // 数量索引
                $title = $title == '' ? true : $title;
                $showIndexPaginate = $type === false ? false : true;
                $this->showIndex($title, $showIndexPaginate);
                return $this;
            }else if($name == '__checkbox__'){ // 多选
                $this->showCheckbox();
                return $this;
            }else if($name == '__btn__'){ // 操作按钮
                $type = 'option';
            }
            $type = $type ?: 'text';
            $column = [
                'name' => $name,
                'title' => $title,
                'type' => $type,
                'option' => $option,
                'default' => $default,
                'trigger' => $attr['trigger'] ?? null,
                'fixed' => $attr['fixed'] ?? false,
                'show_tooltip' => $attr['show_tooltip'] ?? true,
                'attr' => $attr,
            ];
            switch ($type){
                case 'status':
                    $column['option'] = $option ?: [1 => '启用', 2 => '禁用'];
                    $column['default'] = 1;
                    break;
                case 'yesno':
                    $column['yes_value'] = $option ?: 1;
                    break;
                case 'datetime':
                    $format = $option ?: 'Y-m-d H:i';
                    // 转换为前端的格式
                    $format = str_replace('Y', 'yyyy', $format);
                    $format = str_replace('m', 'MM', $format);
                    $format = str_replace('d', 'dd', $format);
                    $format = str_replace('H', 'hh', $format);
                    $format = str_replace('i', 'mm', $format);
                    $format = str_replace('s', 'ss', $format);
                    $column['format'] = $format;
                    break;
                case 'image':
                    $column['limit'] = 1;
                    $column['show_tooltip'] = false;
                    break;
                case 'images':
                    $column['type'] = 'image';
                    $column['limit'] = $option ?: 0;
                    $column['show_tooltip'] = false;
                    break;
                case 'image_external':
                    $column['type'] = 'image';
                    $column['limit'] = 1;
                    $column['external'] = true;
                    $column['show_tooltip'] = false;
                    break;
                case 'images_external':
                    $column['type'] = 'image';
                    $column['limit'] = $option ?: 0;
                    $column['external'] = true;
                    $column['show_tooltip'] = false;
                    break;
                case 'option':
                    $column['title'] = $title ?: '操作';
                    $column['show_tooltip'] = false;
                    break;
                case 'link':
                    if(is_string($option)){
                        $option = Util::parseOptionUrl($option);
                    }
                    $column['option'] = $option;
                    break;
                case 'tags':
                    $column['show_tooltip'] = false;
                    break;
                // 添加组
                case 'group':
                    $this->isGroup = true;
                    $columns = $this->addColumns($option);
                    $column['columns'] = $columns;
                    $this->isGroup = false;
                    break;
            }
            if($this->isGroup) {
                return $column;
            }
            $this->_vars['columns'][] = $column;
        }
        return $this;
    }

    /**
     * 添加多列
     * @param $columns array columns参数中每个元素是一个对应添加单列中的参数的数组。
     * @return $this|array
     */
    public function addColumns($columns)
    {
        $results = [];
        foreach ($columns as $column) {
            $results[] = $this->addColumn(...$column);
        }
        if($this->isGroup) {
            return $results;
        }
        return $this;
    }

    /**
     * 设置快捷编辑地址
     * @param string $url
     * @return $this
     */
    public function setQuickEditUrl($url = '')
    {
        if(!empty($url)) {
            $this->_vars['quick_edit_url'] = $url;
        }
        return $this;
    }

    /**
     * 设置列表数据获取地址
     * @param string|false $url 数据获取地址
     * @param string $tableName 自动获取列表时，使用的表名，设置此参数时将会自动调用setTableName方法表名，如果没设置过，则操作的表名默认为控制器的名称。
     * @param bool $paginate 自动获取列表时，列表数据是否分页，默认为true
     * @return $this
     */
    public function setRowListUrl($url = '', $tableName = '', $paginate = true)
    {
        if($url !== ''){
            $this->_vars['row_list_url'] = $url;
            $this->_vars['row_list_paginate'] = $paginate;
        }
        $this->setTableName($tableName);
        return $this;
    }

    /**
     * 设置数据
     * @param array|Collection|Paginator $data 列表数据，可以是一个列表、集合、或分页对象，如果是一个分页对象，则会自动分页。
     * @return $this
     * @deprecated
     */
    public function setRowList($data = [])
    {
        if(!empty($data)){
            // 分页自动处理
            if($data instanceof Paginator){
                $list = $data->items();
                $total = $data->total();
                $currentPage = $data->currentPage();
                $pageSize = $data->listRows();
                $this->setPaginate($total, $pageSize, $currentPage);
            }else {
                $list = $data;
            }
            $this->_vars['row_list'] = $list;
            $this->setAjaxGetList(false);
        }
        return $this;
    }

    /**
     * 设置是否以ajax方式获取数据列表
     * @param bool $ajax
     * @return $this
     */
    public function setAjaxGetList($ajax = false)
    {
        $this->_vars['ajax_get_list'] = $ajax;
        return $this;
    }

    /**
     * 设置分页
     * @param int $total
     * @param int $pageSize
     * @param int $currentPage
     * @return $this
     */
    public function setPaginate(int $total, int $pageSize, int $currentPage)
    {
        $this->_vars['paginate'] = array_merge(
            $this->_vars['paginate'],
            [
                'show' => true,
                'total' => $total,
                'page_size' => $pageSize,
                'current_page' => $currentPage,
            ]
        );
        return $this;
    }

    /**
     * 展示分页大小调整器
     * @return $this
     */
    public function showPageSize()
    {
        $this->_vars['paginate']['show_sizes'] = true;
        return $this;
    }

    /**
     * 设置可排序的字段
     * @param string|array $columns
     * @return $this
     */
    public function setOrder($columns = '')
    {
        if(!empty($columns)){
            if(is_string($columns)){
                $columns = array_map('trim', explode(',', $columns));
            }
            $this->_vars['sortable_columns'] = $columns;
        }
        return $this;
    }

    /**
     * 表头筛选
     * @param string|array $column
     * @param array $option
     * @param string|array $default
     * @return $this
     */
    public function setFilters($column = '', $option = [], $default = '')
    {
        if(!empty($column)){
            if(is_array($column)){
                foreach ($column as $item) {
                    $this->setFilters(...$item);
                }
            }else {
                if(!empty($default) && is_string($default)){
                    $default = explode(',', $default);
                }
                $this->_vars['filter_columns'][$column] = [
                    'name' => $column,
                    'option' => $option,
                    'default' => $default,
                ];
            }
        }
        return $this;
    }

    /**
     * 顶部筛选框
     * @param string $name
     * @param string $title
     * @param array $option
     * @param string $default
     * @param array $attr
     * @return $this
     */
    public function addTopSelect($name = '', $title = '', $option = [], $default = '', $attr = [])
    {
        if(!empty($name)){
            if(is_array($name)){
                foreach ($name as $item) {
                    $this->addTopSelect(...$item);
                }
            }else {
                $multiple = $attr['multiple'] ?? false;
                if($multiple && is_string($default)){
                    $default = $default ? explode(',', $default) : [];
                }
                $this->_vars['top_selects'][] = [
                    'type' => 'select',
                    'name' => $name,
                    'title' => $title,
                    'option' => $option,
                    'default' => $default,
                    'multiple' => $multiple,
                    'remote' => $attr['remote'] ?? false,
                    'remote_empty' => $attr['remote_empty'] ?? false,
                    'attr' => $attr,
                ];
            }
        }
        return $this;
    }

    /**
     * 添加顶部级联选择器
     * @param string $name
     * @param string $title
     * @param array $option
     * @param array $default
     * @param array $attr 组件属性 {
     *      need_all_levels: 设置为true时, show_all_levels与select_all_levels默认为true, 并在表单提交时将以数组形式提交所有层级的数据
     *      show_all_levels: 是否展示所有层级，默认为true
     *      select_all_levels: 是否可选择所有层级，默认为false
     *      props: option的属性名定义 {
     *          value: option中value的键名，默认为id,
     *          label: option中label的键名，默认为name，
     *          disabled: option中disabled的键名，默认为disabled,
     *          children： option中children的键名，默认为children,
     *      }
     * }
     * @return $this
     */
    public function addTopCascader($name = '', $title = '', $option = [], $default = [], $attr = []) {

        if(!empty($name)){
            if(is_array($name)){
                foreach ($name as $item) {
                    $this->addTopSelect(...$item);
                }
            }else {
                $multiple = $attr['multiple'] ?? false;
                if($multiple && is_string($default)){
                    $default = $default ? explode(',', $default) : [];
                }

                // 处理props
                $props = [
                    'value' => 'id',
                    'label' => 'name',
                    'children' => 'children',
                    'disabled' => 'disabled',
                    'multiple' => $attr['multiple'] ?? false,
                ];
                if(isset($attr['props']) && is_array($attr['props'])){
                    $props = array_merge($props, $attr['props']);
                }

                $needAllLevels = $attr['need_all_levels'] ?? false;

                $this->_vars['top_selects'][] = [
                    'type' => 'cascader',
                    'name' => $name,
                    'title' => $title,
                    'option' => $option,
                    'default' => $default,
                    'props' => $props,
                    'remote' => $attr['remote'] ?? false,
                    'remote_empty' => $attr['remote_empty'] ?? false,
                    'need_all_levels' => $needAllLevels,
                    'select_all_levels' => $attr['select_all_levels'] ?? ($needAllLevels),
                    'show_all_levels' => $attr['show_all_levels'] ?? true,
                    'attr' => $attr,
                ];
            }
        }
        return $this;
    }

    /**
     * 设置列宽
     * @param string|array $column
     * @param null $width
     * @return $this
     */
    public function setColumnWidth($column = '', $width = null)
    {
        if(!empty($column)){
            if(is_array($column)){
                foreach ($column as $key => $value) {
                    $this->setColumnWidth($key, $value);
                }
            }else {
                $columns = explode(',', $column);
                foreach ($columns as $v) {
                    $this->_vars['column_widths'][$v] = $width;
                }

            }
        }
        return $this;
    }

    /**
     * 自动新增
     * @param array $fields 新增页面的表单项，参数内容同 addFromItems 方法
     * @param string|array $validate 验证器，字符串或验证数组，参见thinkphp表单验证一节
     * @param bool $pop 是否弹窗形式打开页面
     * @param string $btnTitle 新增按钮的标题，默认为'新增'
     * @param string|bool $autoTime 自动写入时间戳，在找不到对应模型时，会根据此字段对创建时间进行操作。能找到对应模型时此字段无效，将会使用模型的设置。
     *     默认为create_time和update_time
     * @return $this
     */
    public function autoAdd($fields = [], $validate = null, $pop = true, $btnTitle = '', $autoTime = true)
    {
        if(!empty($autoTime)){
            $autoTime = $autoTime === true ? ['create_time', 'update_time'] : explode(',', $autoTime);
        }
        $this->autoAdd = [
            'fields' => $fields,
            'validate' => $validate,
            'autoTime' => $autoTime,
            'pop' => $pop,
        ];
        $this->addTopButton('add', $btnTitle ?: '新增', '', $pop ? 'pop' : 'link');
        return $this;
    }

    /**
     * 自动编辑
     * @param array $fields 编辑页面的表单项，参数内容同 addFromItems 方法
     * @param string|array $validate 验证器，字符串或验证数组，参见thinkphp表单验证一节
     * @param bool $pop 是否弹窗形式打开页面
     * @param string $btnTitle 编辑按钮的标题，默认为'编辑'
     * @param string|bool $autoTime 自动写入时间戳，在找不到对应模型时，会根据此字段对创建时间进行操作。能找到对应模型时此字段无效，将会使用模型的设置。
     *     默认为update_time
     * @return $this
     */
    public function autoEdit($fields = [], $validate = null, $pop = true, $btnTitle = '', $autoTime = true)
    {
        if(!empty($autoTime)){
            $autoTime = $autoTime === true ? ['update_time'] : explode(',', $autoTime);
        }
        $this->autoEdit = [
            'fields' => $fields,
            'validate' => $validate,
            'autoTime' => $autoTime,
            'pop' => $pop,
        ];
        $this->addRightButton('edit', $btnTitle ?: '编辑', '', $pop ? 'pop' : 'link');
        return $this;
    }

    /**
     * 隐藏表格顶部的刷新按钮
     * @param $hide
     * @return $this
     */
    public function hideRefreshBtn($hide = true)
    {
        $this->hideRefreshBtn = $hide;
        return $this;
    }

    /**
     * 渲染页面数据
     * @param array $vars
     * @param bool $getVars
     * @return mixed
     */
    public function fetch($vars = [], $getVars = false)
    {
        // 防止重复fetch
        if ($getVars) {
            // 设置各项默认值
            // 获取列表接口
            if(empty($this->_vars['row_list_url'])) {
                $this->_vars['row_list_url'] = url('getList');
            }
            // 顶部按钮
            if(!$this->hideRefreshBtn) {
                array_unshift($this->_vars['top_buttons'], Button::create([])
                    ->setFlag('refresh')
                    ->setTooltip('刷新')
                    ->setAttrIcon('el-icon-Refresh')
                    ->setAttrType('info')
                    ->setOptionType('refresh')
                    ->fetch()
                );
            }
            // 搜索数据地址
            if($this->_vars['search'] && empty($this->_vars['search']['url'])){
                $this->_vars['search']['url'] = $this->_vars['row_list_url'];
            }
            if(empty($this->_vars['pk'])) {
                $this->_vars['pk'] = 'id';
            }
            // 快捷编辑接口地址
            if(empty($this->_vars['quick_edit_url'])) {
                $this->_vars['quick_edit_url'] = url('quickedit');
            }
            // 防止返回的column_widths为一个空数组, 前端解析为一个数据后, 会取到js数据的sort等方法
            if (empty($this->_vars['column_widths'])) {
                $this->_vars['column_widths'] = new stdClass();
            }
            // 缓存页面配置
            $cacheName =  SITE_NAME . ':' .request()->module() . ':' . request()->controller() . ':';
            Cache::set($cacheName . 'tablePageConfig', $this->_vars);

            // 缓存自动新增配置
            if($this->autoAdd){
                $this->autoAdd['table_name'] = $this->_vars['table_name'];
                Cache::set($cacheName . 'autoAdd', $this->autoAdd);
            }
            // 缓存自动新增配置
            if($this->autoEdit){
                $this->autoEdit['table_name'] = $this->_vars['table_name'];
                Cache::set($cacheName . 'autoEdit', $this->autoEdit);
            }
        }
        return parent::fetch($vars, $getVars);
    }
}