<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 * Date: 2019/4/23
 * Time: 16:06
 */

namespace evan\sbuilder\builder\block;

use evan\sbuilder\builder\button\Button;
use think\helper\Str;

class Form extends Block
{
    /**
     * @var int 组合分组标识, 大于0表示正在组合分组, 组合分组时添加表单项将直接返回表单项结构数据
     * 支持分组嵌套, 嵌套时对应嵌套的层数
     */
    private $_group_flag = 0;

    private function _isGroup(){
        return $this->_group_flag > 0;
    }

    /**
     * @var array 页面配置
     */
    protected $_vars = [
        'name' => '', // 区块名, 用于识别区块
        'type' => 'form', // 区块类型: form, table
        'form_items' => [], // 表单项
        'btn_title' => [], // 表单页按钮标题, submit和cancel按钮
        'btn_hide' => [], // 要隐藏的按钮, submit和cancel按钮
        /**
         *  // 额外按钮
         * {
         *  title,
         *  attr: { type:default|primary|success|info|warning|danger,round:true|false,plain:true|false,circle:true|false, icon, disabled},
         *  option: { type:ajax提交表单|jump页面跳转|pop打开弹窗|null无操作, url, confirm操作前弹框确认: false|{title, msg} }
         * }
         */
        'btn_extra' => [],
        'form_data' => [], // 表单数据
        'triggers' => [], // 触发器数组
        'layout' => [], // 表单布局参数
        'label_width' => '120px', // 表单项标题宽度, 为空时标题独占一行
    ];

    /**
     * Builder constructor.
     */
    public function __construct()
    {
        // 初始化添加默认按钮
        $this->setBtnExtra('提交', request()->baseUrl(), 'ajax', [
            'type' => 'primary',
            'flag' => 'submit',
        ]);
        $this->setBtnExtra('取消', '', 'back', ['type' => 'default', 'flag' => 'cancel']);
    }

    /**
     * 设置表单提交地址, 不设置时默认为当前地址
     * @param string $post_url 表单提交地址
     * @return $this
     */
    public function setUrl($post_url)
    {
        if ($post_url != '') {
            foreach ($this->_vars['btn_extra'] as &$item) {
                if($item['flag'] == 'submit'){
                    $item['option']['url'] = $post_url;
                }
            }
        }
        return $this;
    }

    /**
     * 设置默认按钮标题, 按钮分别为 submit 及cancel
     * @param string|array $btnFlag 按钮标识，或按钮标识与标题的数组
     * @param string $title 按钮标题
     * @return $this
     */
    public function setBtnTitle($btnFlag = '', $title = '')
    {
        if (!empty($btnFlag)) {
            if (is_array($btnFlag)) {
                foreach ($btnFlag as $flag => $title) {
                    foreach ($this->_vars['btn_extra'] as &$item) {
                        if($item['flag'] == $flag){
                            $item['title'] = $title;
                        }
                    }
                }
            } else {
                foreach ($this->_vars['btn_extra'] as &$item) {
                    if($item['flag'] == $btnFlag){
                        $item['title'] = $title;
                    }
                }
            }
        }
        return $this;
    }

    /**
     * 隐藏默认按钮
     * @param array|string $btn 隐藏的按钮列表，逗号分隔的字符串或数组
     * @return $this
     */
    public function hideBtn($btn = [])
    {
        if (!empty($btn)) {
            $btn = is_array($btn) ? $btn : explode(',', $btn);
            foreach ($btn as $b){
                foreach ($this->_vars['btn_extra'] as &$item) {
                    if($item['flag'] == trim($b)){
                        $item['hide'] = true;
                    }
                }
            }
        }
        return $this;
    }

    /**
     * 设置提交表单时是否显示确认框
     * @param bool|string $title 提示框标题
     * @param string $tips 提示框内容
     * @return $this
     */
    public function submitConfirm($title = true, $tips = '')
    {
        foreach ($this->_vars['btn_extra'] as &$item) {
            if($item['flag'] == 'submit'){
                if(is_string($title)){
                    $confirm = ['title' => $title, 'tips' => $tips];
                }else {
                    $confirm = $title;
                }
                $item['option']['confirm'] = $confirm;
            }
        }
        return $this;
    }

    /**
     * 添加底部额外按钮
     * @param string $title 按钮标题或按钮数组
     * @param string $url 按钮地址，用于link跳转、pop弹框或ajax提交的地址
     * @param string $linkType 跳转类型 ajax-提交表单，link-页面跳转，pop-打开弹窗，null无操作，默认为link
     * @param array $attr 按钮属性 {
     *         type:default|primary|success|info|warning|danger|text, // 按钮类型，默认 primary
     *         round:true|false, // 按钮是否圆角，默认 false
     *         plain:true|false, // 是否朴素按钮，默认 false
     *         circle:true|false, // 是否圆形按钮，默认 false
     *         icon, // 按钮图标，可选择的图标见icon表单项
     *         disabled, // 是否禁用，默认 false
     *         flag: 按钮标记， 用于对按钮进行操作,
     *         target:  按钮链接跳转类型(linkType为link时有效, _blank表示新页面打开链接),
     *         confirm: false|true|{title, tips, type:warning}, 按钮点击时是否需要确认框,
     *         extra_data: 附加数据, 表单提交时会把附加数据提交,
     * }
     * @return $this
     */
    public function setBtnExtra($title = '', $url = '', $linkType = 'link', $attr = [])
    {
        if(is_array($title)){
            foreach ($title as $item) {
                $this->setBtnExtra(...$item);
            }
        }else if($title instanceof Button){
            $this->_vars['btn_extra'][] = $title->fetch();
        }else {
            $this->_vars['btn_extra'][] = [
                // 按钮文字
                'title' => $title,
                // 按钮属性 { type:default|primary|success|info|warning|danger|text,round:true|false,plain:true|false,circle:true|false, icon, disabled}
                'attr' => [
                    'type' => $attr['type'] ?? 'primary',
                    'round' => $attr['round'] ?? false,
                    'plain' => $attr['plain'] ?? false,
                    'circle' => $attr['circle'] ?? false,
                    'icon' => $attr['icon'] ?? '',
                    'disabled' => $attr['disabled'] ?? false,
                ],
                // 操作参数 { type:ajax提交表单|link页面跳转|pop打开弹窗|null无操作, url, target link类型有效:_blank新页面打开, confirm操作前弹框确认: false|true|{title, msg}, extra_data额外数据: [], }
                'option' => [
                    'type' => $linkType ?: 'link',
                    'url' => $url,
                    'target' => $attr['target'] ?? '',
                    'confirm' => $attr['confirm'] ?? false,
                    'extra_data' => $attr['extra_data'] ?? null,
                ],
                // 按钮类型
                'flag' => $attr['flag'] ?? '', // 按钮标记, 用于前端对特殊的按钮做特殊处理
            ];
        }
        return $this;
    }

    /**
     * 设置表单label_width
     * @param string $labelWith label宽度，数字或带px的字符串
     * @return $this
     */
    public function setLabelWith($labelWith = '')
    {
        if(is_null($labelWith)){
            $this->_vars['label_width'] = null;
        }else if($labelWith !== ''){
            if(!Str::endsWith($labelWith, 'px')){
                $labelWith .= 'px';
            }
            $this->_vars['label_width'] = $labelWith;
        }
        return $this;
    }

    /**
     * 添加表单项通用方法
     * @param string $name
     * @param string $title
     * @param string $type
     * @param string $tips
     * @param string $default
     * @param array $attr
     * @return Form|array
     */
    public function addFormItem($name = '', $title = '', $type = 'text', $tips = '', $default = '', $attr = [])
    {
        if($name != ''){
            if(empty($type)) $type = 'text';
            $method = 'add' . ucfirst($type);
            if(method_exists($this, $method)){
                $args = func_get_args();
                array_splice($args, 2, 1);
                return call_user_func_array([$this, $method], $args);
            }else {
                // 自定义表单组件
                $class = '\\evan\\sbuilder\\builder\\extend\\form\\' . Str::studly($type);
                if(class_exists($class)){
                    $args = func_get_args();
                    array_splice($args, 2, 1);
                    $builder = new $class($this);
                    $item = $builder->fetch(...$args);
                }else {
                    list($name, $require) = $this->handleFormItemName($name);
                    list($title, $placeholder) = $this->getPlaceholderFromTitle($title);
                    $item = [
                        'type' => $type,
                        'name' => $name,
                        'require' => $require,
                        'label' => $title,
                        'placeholder' => $placeholder,
                        'tips' => $tips,
                        'default' => $default,
                    ];
                    foreach ($attr as $key => $value) {
                        $item[$key] = $value;
                    }
                }

                if ($this->_isGroup()) {
                    return $item;
                }
                $this->_vars['form_items'][] = $item;
            }
        }
        return $this;
    }

    /**
     * 批量添加表单项
     * @param array $formItems
     * @return Form|array
     */
    public function addFormItems($formItems = [])
    {
        if ($this->_isGroup()) {
            $items = [];
            foreach ($formItems as $item) {
                $items[] = $this->addFormItem(...$item);
            }
            return $items;
        }else {
            foreach ($formItems as $item) {
                $this->addFormItem(...$item);
            }
        }
        return $this;
    }

    /**
     * 单行文本框
     * @param string $name 字段名及是否必填 格式: name:require 或 name
     * @param string $title 标题及占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示信息
     * @param string $default 默认值
     * @param array|string $attr 输入框属性，完整格式:
     *     {
     *         prefix, 输入框内部前缀文字
     *         suffix, 输入框内部后缀文字
     *         prepend, 输入框外部前缀文字
     *         append, 输入框外部后缀文字
     *         password: 是否是密码框
     *         width: 输入框宽度, 如: 200px 或 20%
     *         type: input原生属性, 可以是number, 为number时
     *     }
     *     其中，前后缀文字可以显示为图标, 只需要以'i.'开头即可
     * @return Form|array
     */
    public function addText($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        if($name != ''){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            // 处理placeholder
            list($title, $placeholder) = $this->getPlaceholderFromTitle($title);

            if (is_string($attr)) {
                $attr = ['prepend' => $attr];
            }
            if (isset($attr['password']) && $attr['password']) {
                $attr['password_encrypt'] = $attr['password_encrypt'] ?? false;
            }
            $attr['placeholder'] = $placeholder;
            $item = [
                'type' => 'text',
                'name' => $name,
                'label' => $title,
                'placeholder' => $placeholder,
                'tips' => $tips,
                'default' => $default,
                'require' => $require === true ? $require : ($attr['require'] ?? false),
//                'prefix' => $attr['prefix'] ?? '',
//                'suffix' => $attr['suffix'] ?? '',
//                'prepend' => is_string($attr) ? $attr : ($attr['prepend'] ?? ''),
//                'append' => $attr['append'] ?? '',
//                'min' => $attr['min'] ?? '',
//                'max' => $attr['max'] ?? '',
//                'password' => $attr['password'] ?? false,
//                'password_encrypt' => $attr['password_encrypt'] ?? false,
                'attr' => $attr,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 数字输入框
     * @param string $name 字段名及是否必填 格式: name:require 或 name
     * @param string $title 标题及占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示信息
     * @param int|float $default 默认值
     * @param array $attr 组件属性 {min: 最小值, max: 最大值, step: 步长, precision: 精读位数}
     * @return Form|array
     */
    public function addNumber($name = '', $title = '', $tips = '', $default = null, $attr = [])
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);

            $item = [
                'type' => 'number',
                'name' => $name,
                'label' => $title,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
                'min' => $attr['min'] ?? '',
                'max' => $attr['max'] ?? '',
                'step' => $attr['step'] ?? 1,
                'precision' => $attr['precision'] ?? '',
                'prepend' => $attr['prepend'] ?? '',
                'append' => $attr['append'] ?? '',
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 密码框
     * @param string $name 字段名及是否必填 格式: name:require 或 name
     * @param string $title 标题及占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示信息
     * @param string $default 默认值
     * @param false|string $encrypt 密码加密方式，目前支持 md5，默认不加密
     * @param bool $require 是否必填
     * @return Form|array
     */
    public function addPassword($name = '', $title = '', $tips = '', $default = '', $encrypt = false, $require = false)
    {
        $attr['password'] = true;
        $attr['password_encrypt'] = $encrypt;
        $attr['require'] = $require;
        return $this->addText($name, $title, $tips, $default, $attr);
    }

    /**
     * 多行文本框
     * @param string $name 字段名及是否必填 格式: name:require 或 name
     * @param string $title 标题及占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示信息
     * @param string $default 默认值
     * @param array $attr 输入框选项 {min_rows:最少行数, max_rows:最大行数, width: 输入框宽度, 如: 200px 或 20%}
     * @return Form|array
     */
    public function addTextarea($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        if($name != ''){
            if (is_string($attr)) {
                $attr = ['prepend' => $attr];
            }
            $attr['textarea'] = true;
            $attr['show_word_limit'] = true;
            return $this->addText($name, $title, $tips, $default, $attr);

        }
        return $this;
    }

    /**
     * 数组输入框, 支持一维数组与二维数组
     * @param string $name 字段名及是否必填 格式: name:require 或 name
     * @param string $title 标题及占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示信息
     * @param array $default 默认值
     * @param array $attr 输入框选项 {min_rows:最少行数, max_rows:最大行数}
     * @return Form|array
     */
    public function addArray($name = '', $title = '', $tips = '', $default = null, $attr = [])
    {
        if($name != ''){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            // 处理placeholder
            list($title, $placeholder) = $this->getPlaceholderFromTitle($title);

            $item = [
                'type' => 'array',
                'name' => $name,
                'label' => $title,
                'placeholder' => $placeholder,
                'tips' => $tips,
                'default' => $default,
                'textarea' => true,
                'require' => $attr['require'] ?? $require,
                'min' => $attr['min'] ?? '',
                'max' => $attr['max'] ?? '',
                'min_rows' => $attr['min_rows'] ?? 3,
                'max_rows' => $attr['max_rows'] ?? 6,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 单选框
     * @param string $name 字段名及是否必填 格式: name:require 或 name
     * @param string $title 标题及占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param array $option 选项 格式为：{value:title, }
     * @param string $tips 提示文字
     * @param string $default 默认值
     * @param array $attr 组件属性 {button: 是否按钮形式, border: 是否带边框, disabled: 禁用的选项列表}
     * @return Form|array
     */
    public function addRadio($name = '', $title = '', $option = [], $tips = '', $default = '', $attr = [])
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            // 处理禁用选项
            $disabled = [];
            if(isset($attr['disabled'])){
                $inputDisabled = $attr['disabled'];
                if(!is_array($inputDisabled)) $inputDisabled = explode(',', $inputDisabled);
                foreach ($inputDisabled as $index => $value) {
                    $disabled[$value] = true;
                }
            }
            $item = [
                'type' => 'radio',
                'name' => $name,
                'label' => $title,
                'tips' => $tips,
                'default' => $default,
                'option' => $option,
                'require' => $attr['require'] ?? $require,
                'button' => $attr['button'] ?? false,
                'border' => $attr['border'] ?? false,
                'disabled' => $disabled,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 状态选择器
     * @param string $name 字段名及是否必填 格式: name:require 或 name
     * @param string $title 标题及占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param array $option 选项 {value:title, }
     * @param string $tips 提示文字
     * @param int $default 默认值
     * @param array $attr 组件属性 {button: 按钮形式, border: 带边框, disabled: 禁用的选项列表}
     * @return Form|array
     */
    public function addStatus($name = '', $title = '', $option = [], $tips = '', $default = 1, $attr = [])
    {
        if(empty($option)) {
            $option = [1 => '启用', 2 => '禁用'];
        }
        return $this->addRadio($name, $title, $option, $tips, $default, $attr);
    }

    /**
     * 复选框
     * @param string $name 字段名及是否必填 格式: name:require 或 name
     * @param string $title 标题及占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param array $option 选项数据 格式为：{value:title, }
     * @param string $tips 提示信息
     * @param string|array $default 默认值
     * @param array $attr 多选框属性 {
     *     checkAll: 是否需要全选, 默认为true
     *     disabled: 禁用的选项数组,
     *     min: 最少勾选项目数量,
     *     max: 最大勾选项目的数量,
     *     button: 是否已按钮样式显示,
     *     border: 是否带边框(默认不带)
     * }
     * @return Form|array
     */
    public function addCheckbox($name = '', $title = '', $option = [], $tips = '', $default = [], $attr = [])
    {
        if($name != ''){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);

            // 处理禁用选项
            $disabled = [];
            if(isset($attr['disabled'])){
                $inputDisabled = $attr['disabled'];
                if(!is_array($inputDisabled)) $inputDisabled = explode(',', $inputDisabled);
                foreach ($inputDisabled as $index => $value) {
                    $disabled[$value] = true;
                }
            }
            // 处理默认值
            if(empty($default)){
                $default = [];
            }else if(is_string($default)){
                $default = explode(',', $default);
            }
            $item = [
                'type' => 'checkbox',
                'name' => $name,
                'label' => $title,
                'option' => $option,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
                'checkAll' => $attr['checkAll'] ?? true,
                'disabled' => $disabled,
                'min' => $attr['min'] ?? 0,
                'max' => $attr['max'] ?? count($option),
                'button' => $attr['button'] ?? false,
                'border' => $attr['border'] ?? false,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 下拉框
     * @param string $name 字段名及是否必填 格式: name:require 或 name
     * @param string $title 标题及占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param array $option 选项 格式为：{value:title, }
     * @param string $tips 提示文字
     * @param string|array $default 默认值，多选时为数组
     * @param array $attr 下拉框属性 {
     *      disabled: 禁用的选项数组,
     *      multiple: 是否多选(多选时选中的值为数组),
     *      clearable: 是否可清空选中内容，默认为true,
     *      filterable: 是否可筛选，默认为true,
     *      allow_create: 是否允许新增，默认为false, 新增的值与输入相同,
     *      remote: 远程加载地址, 默认为false, 不远程加载,
     *      remote_empty: select搜索框中内容为空时是否远程加载，默认为false,
     * }
     * @return Form|array
     */
    public function addSelect($name = '', $title = '', $option = [], $tips = '', $default = '', $attr = [])
    {
        if($name != ''){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            // 处理placeholder
            list($title, $placeholder) = $this->getPlaceholderFromTitle($title , '请选择');

            // 处理禁用选项
            $disabled = [];
            if(isset($attr['disabled'])){
                $inputDisabled = $attr['disabled'];
                if(!is_array($inputDisabled)) $inputDisabled = explode(',', $inputDisabled);
                foreach ($inputDisabled as $index => $value) {
                    $disabled[$value] = true;
                }
            }

            // 处理默认值
            if(isset($attr['multiple']) && $attr['multiple']){
                if(empty($default)){
                    $default = [];
                }else if(is_string($default)){
                    $default = explode(',', $default);
                }
            }

            $item = [
                'type' => 'select',
                'name' => $name,
                'label' => $title,
                'placeholder' => $placeholder,
                'option' => $option,
                'tips' => $tips,
                'default' => $default,
                'disabled' => $disabled,
                'remote' => $attr['remote'] ?? false, // 远程加载地址
                'remote_empty' => $attr['remote_empty'] ?? false, // 搜索项为空时是否远程加载
                'require' => $attr['require'] ?? $require,
                'multiple' => $attr['multiple'] ?? false,
                'clearable' => $attr['clearable'] ?? true,
                'filterable' => $attr['filterable'] ?? true,
                'allow_create' => $attr['allow_create'] ?? false,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 级联选择器
     * @param string $name 字段名及是否必填 格式: name:require 或 name
     * @param string $title 标题及占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param array $option 默认格式为: [{id,name,disabled,children},] 可通过attr中的props指定各个字段的键名
     * @param string $tips 提示信息
     * @param string $default 默认值
     * @param array $attr 组件属性 {
     *      need_all_levels: 设置为true时, show_all_levels与select_all_levels默认为true, 并在表单提交时将以数组形式提交所有层级的数据
     *      show_all_levels: 是否展示所有层级，默认为true
     *      select_all_levels: 是否可选择所有层级，默认为false
     *      props: option的属性名定义 {
     *          value: option中value的键名，默认为id,
     *          label: option中label的键名，默认为name，
     *          disabled: option中disabled的键名，默认为disabled,
     *          children： option中children的键名，默认为children,
     *      }
     * }
     * @return Form|array
     */
    public function addCascader($name = '', $title = '', $option = [], $tips = '', $default = '', $attr = [])
    {
        if($name != ''){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            // 处理placeholder
            list($title, $placeholder) = $this->getPlaceholderFromTitle($title , '请选择');

            // 处理props
            $props = [
                'value' => 'id',
                'label' => 'name',
                'children' => 'children',
                'disabled' => 'disabled',
            ];
            if(isset($attr['props']) && is_array($attr['props'])){
                $props = array_merge($props, $attr['props']);
            }

            $needAllLevels = $attr['need_all_levels'] ?? false;

            $item = [
                'type' => 'cascader',
                'name' => $name,
                'label' => $title,
                'placeholder' => $placeholder,
                'option' => $option,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
                'props' => $props,
                'remote' => $attr['remote'] ?? false,
                'need_all_levels' => $needAllLevels,
                'select_all_levels' => $attr['select_all_levels'] ?? ($needAllLevels),
                'show_all_levels' => $attr['show_all_levels'] ?? true,
                'clearable' => $attr['clearable'] ?? true,
                'filterable' => $attr['filterable'] ?? true,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 开关
     * @param string $name 字段名及是否必填 格式: name:require 或 name
     * @param string $title 标题
     * @param string $tips 提示文字
     * @param int $default 默认值2, 1为开, 2为关
     * @param array $attr 其他属性 {
     *      active_text: switch 打开时的文字描述，默认无,
     *      inactive_text: switch 关闭时的文字描述，默认无,
     *      active_color: switch 打开时的背景色，默认#409EFF,
     *      inactive_color: switch 关闭时的背景色，默认#C0CCDA,
     *  }
     * @return Form|array
     */
    public function addSwitch($name = '', $title = '', $tips = '', $default = 2, $attr = [])
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            $item = [
                'type' => 'switch',
                'name' => $name,
                'label' => $title,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
                'active_color' => $attr['active_color'] ?? '',
                'inactive_color' => $attr['inactive_color'] ?? '',
                'active_text' => $attr['active_text'] ?? '',
                'inactive_text' => $attr['inactive_text'] ?? '',
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 日期选择器
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题|占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示文字
     * @param string|array $default 默认值
     * @param array $attr 组件属性 {
     *      date_type:日期类型-year/month/week/date/datetime/datetimerange/daterange,
     *      min:最小日期,
     *      max:最大日期,
     *      default_time: 可选, 选中日期后的默认具体时刻,
     *      value_format: 可选，绑定值的格式。默认为 YYYY-MM-DD,
     *      shortcuts: 设置快捷选项, 格式为: [{text, value: Y-m-d H:i:s 格式日期字符串, range类型为两个元素的数组}, ...]
     *  }
     * @return Form|array
     */
    public function addDate($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            // 处理placeholder
            list($title, $placeholder) = $this->getPlaceholderFromTitle($title, '请选择');

            $attr['date_type'] = $attr['date_type'] ?? 'date';
            $attr['value_format'] = $attr['value_format'] ?? 'YYYY-MM-DD';
            $item = [
                'type' => 'date',
                'name' => $name,
                'placeholder' => $placeholder,
                'label' => $title,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
                'attr' => $attr,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 日期时间选择器
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题|占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示文字
     * @param string|array $default 默认值
     * @param array $attr 组件属性 {
     *      min:最小时间,
     *      max:最大时间,
     *      shortcuts: 设置快捷选项, 格式为: [{text, value: Y-m-d H:i:s 格式日期字符串, range类型为两个元素的数组}, ...]
     *  }
     * @return Form|array
     */
    public function addDatetime($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        $attr['date_type'] = 'datetime';
        $attr['value_format'] = 'YYYY-MM-DD HH:mm:ss';
        return $this->addDate($name, $title, $tips, $default, $attr);
    }

    /**
     * 日期范围选择器
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题|占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示文字
     * @param string|array $default 默认值
     * @param array $attr 组件属性 {
     *      min:最小日期,
     *      max:最大日期,
     *      default_time: 可选, 选中日期后的默认具体时刻,
     *      value_format: 可选，绑定值的格式。默认为 YYYY-MM-DD,
     *      shortcuts: 设置快捷选项, 格式为: [{text, value: Y-m-d H:i:s 格式日期字符串, range类型为两个元素的数组}, ...]
     *  }
     * @return Form|array
     */
    public function addDaterange($name = '', $title = '', $tips = '', $default = [], $attr = [])
    {
        $attr['date_type'] = 'daterange';
        return $this->addDate($name, $title, $tips, $default, $attr);
    }

    /**
     * 日期时间范围选择器
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题|占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示文字
     * @param string|array $default 默认值
     * @param array $attr 组件属性 {
     *      min:最小日期,
     *      max:最大日期,
     *      shortcuts: 设置快捷选项, 格式为: [{text, value: Y-m-d H:i:s 格式日期字符串, range类型为两个元素的数组}, ...]
     *  }
     * @return Form|array
     */
    public function addDatetimerange($name = '', $title = '', $tips = '', $default = [], $attr = [])
    {
        $attr['date_type'] = 'datetimerange';
        $attr['value_format'] = 'YYYY-MM-DD HH:mm:ss';
        return $this->addDate($name, $title, $tips, $default, $attr);
    }

    /**
     * 时间选择器
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题|占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示文字
     * @param string|array $default 默认值
     * @param array $attr 组件属性 {
     *      start:起始时间,
     *      end:结束时间,
     *      step:步长,
     *      min:最小时间,
     *      max:最大时间,
     *      selectable_range:可选时间段，例如'18:30:00 - 20:30:00'或者传入数组['09:30:00 - 12:00:00', '14:30:00 - 18:30:00']，todo 由于ElementUI的bug，非范围选择时，设置可选时间段会报错，所以非范围选择暂时不支持可选时间段
     *      is_range:是否是时间范围选择
     *  }
     * @return Form|array
     */
    public function addTime($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            // 处理placeholder
            list($title, $placeholder) = $this->getPlaceholderFromTitle($title, '请选择');

            $item = [
                'type' => 'time',
                'name' => $name,
                'placeholder' => $placeholder,
                'label' => $title,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
                'start' => $attr['start'] ?? null,
                'end' => $attr['end'] ?? null,
                'step' => $attr['step'] ?? '00:15',
                'min' => $attr['min'] ?? '00:00',
                'max' => $attr['max'] ?? null,
                'selectable_range' => (isset($attr['is_range']) && $attr['is_range'] && isset($attr['selectable_range'])) ? $attr['selectable_range'] : null,
                'is_range' => $attr['is_range'] ?? false,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 时间范围选择器
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题|占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示文字
     * @param string|array $default 默认值
     * @param array $attr 组件属性 {
     *      selectable_range:可选时间段，例如'18:30:00 - 20:30:00'或者传入数组['09:30:00 - 12:00:00', '14:30:00 - 18:30:00']
     *  }
     * todo selectable_range可选时间段配置后无效，待解决
     * @return Form|array
     */
    public function addTimerange($name = '', $title = '', $tips = '', $default = null, $attr = [])
    {
        $attr['is_range'] = true;
        return $this->addTime($name, $title, $tips, $default, $attr);
    }

    /**
     * 树形选择器
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题
     * @param array $option 格式：[{id,label,children: []}]
     * @param string $tips 提示文字
     * @param null $default 默认值
     * @param array $attr {
     *      node_key：节点的唯一键，默认为id，
     *      disabled: 要禁用的选项key列表，
     *      props: option的属性名定义 {
     *          label: option中label的键名，默认为name，
     *          children： option中children的键名，默认为children,
     *      }
     * 	}
     * @return Form|array
     */
    public function addTree($name = '', $title = '', $option = [], $tips = '', $default = null, $attr = [])
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);

            $item = [
                'type' => 'tree',
                'name' => $name,
                'label' => $title,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
                'option' => $option,
                'filterable' => $attr['filterable'] ?? true,
                'node_key' => $attr['node_key'] ?? 'id',
                'props' => [
                    'label' => isset($attr['props']) && isset($attr['props']['label']) ? $attr['props']['label'] : 'name',
                    'children' => isset($attr['props']) && isset($attr['props']['children']) ? $attr['props']['children'] : 'children',
                ],
            ];
            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 图标选择器
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题|占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示文字
     * @param string $default 默认值
     * @return Form|array
     */
    public function addIcon($name = '', $title = '', $tips = '', $default = '')
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            list($title, $placeholder) = $this->getPlaceholderFromTitle($title, '请选择');
            $item = [
                'type' => 'icon',
                'name' => $name,
                'label' => $title,
                'placeholder' => $placeholder,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 颜色选择器
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题
     * @param string $tips 提示文字
     * @param string $default 默认值
     * @return Form|array
     */
    public function addColor($name = '', $title = '', $tips = '', $default = '')
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            $item = [
                'type' => 'color',
                'name' => $name,
                'label' => $title,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 单图片上传
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题
     * @param string $tips 提示
     * @param string $default 默认值
     * @param array $attr 组件属性 {
     *      value: id/url, 值使用的附件字段, id-保存附件ID, url-保存附件url, 默认为url,
     *      width: 图片宽度限制，int类型,
     *      height: 图片高度限制，int类型,
     *      size_type: 尺寸限制类型, absolute: 绝对值, ratio: 按比例,
     *      format: 格式限制(逗号分隔的字符串或数组,默认为 ['image/png', 'image/jpeg', 'image/gif', 'image/webp']),
     *      size: 文件大小限制，int类型，单位为字节,
     *      limit: 图片数量限制，用于多图片上传,
     *      disabled: 是否禁用上传功能, 即只做展示
     *      long_image: 长图模式,每行显示一张,依次往下显示, bool
     *      long_image_width: 长图模式时图片的宽度,不传时不限制大小
     *      draggable: 图片是否可拖拽排序
     *      drag: 是否开启拖拽上传
     *      list_type: 图片列表显示类型, 默认picture-card, 可传值text
     *      multiple: 图片是否支持多选
     *      big: 是否为大图模式
     *  }
     * @return Form|array
     */
    public function addImage($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            $format = $attr['format'] ?? ['image/png', 'image/jpeg', 'image/gif', 'image/webp'];
            if(is_string($format)){
                $format = explode(',', $format);
            }
            $item = [
                'type' => 'image',
                'name' => $name,
                'label' => $title,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
                'limit' => $attr['limit'] ?? 1,
                'size' => $attr['size'] ?? 0,
                'width' => $attr['width'] ?? 0,
                'height' => $attr['height'] ?? 0,
                'size_type' => $attr['size_type'] ?? false,
                'disabled' => $attr['disabled'] ?? false,
                'long_image' => $attr['long_image'] ?? false,
                'long_image_width' => $attr['long_image_width'] ?? 'auto',
                'format' => $format,
                'draggable' => $attr['draggable'] ?? false,
                'drag' => $attr['drag'] ?? false,
                'list_type' => $attr['list_type'] ?? '',
                'multiple' => $attr['multiple'] ?? false,
                'big' => $attr['big'] ?? false,
                'value' => $attr['value'] ?? 'url',
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 多图片上传
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题
     * @param string $tips 提示
     * @param string $default 默认值
     * @param array $attr 组件属性 {
     *      value: id/url, 值使用的附件字段, id-保存附件ID, url-保存附件url, 默认为url,
     *      width: 图片宽度限制，int类型,
     *      height: 图片高度限制，int类型,
     *      size_type: 尺寸限制类型, absolute: 绝对值, ratio: 按比例,
     *      format: 格式限制(逗号分隔的字符串或数组,默认为 ['image/png', 'image/jpeg', 'image/gif']),
     *      size: 文件大小限制，int类型，单位为字节,
     *      limit: 图片数量限制，用于多图片上传,
     *      disabled: 是否禁用上传功能, 即只做展示
     *      long_image: 长图模式,每行显示一张,依次往下显示, bool
     *      long_image_width: 长图模式时图片的宽度,不传时不限制大小
     *      draggable: 图片是否可拖拽排序
     *      drag: 是否开启拖拽上传
     *      list_type: 图片列表显示类型, 默认picture-card, 可传值text
     *      multiple: 图片是否支持多选
     *      big: 是否为大图模式
     *  }
     * @return Form|array
     */
    public function addImages($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        $attr['limit'] = $attr['limit'] ?? 99;
        $attr['multiple'] = true;
        return $this->addImage($name, $title, $tips, $default, $attr);
    }

    /**
     * 视频上传
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题
     * @param string $tips 提示
     * @param string $default 默认值
     * @param array $attr 组件属性 {
     *      value: id/url, 值使用的附件字段, id-保存附件ID, url-保存附件url, 默认为url,
     *      size: 文件大小限制，int类型，单位为字节,
     *      limit: 图片数量限制，用于多视频上传,
     *      disabled: 是否禁用上传功能, 即只做展示
     *      format: 支持的视频格式列表, 需要mine-type, 如: video/mp4
     *  }
     * @return Form|array
     */
    public function addVideo($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            $item = [
                'type' => 'video',
                'name' => $name,
                'label' => $title,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
                'limit' => $attr['limit'] ?? 1,
                'size' => $attr['size'] ?? 0,
                'disabled' => $attr['disabled'] ?? false,
                'format' => $attr['format'] ?? null,
                'value' => $attr['value'] ?? 'url',
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 单文件上传
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题
     * @param string $tips 提示
     * @param string $default 默认值
     * @param array $attr 组件属性 {
     *      value: id/url, 值使用的附件字段, id-保存附件ID, url-保存附件url, 默认为url,
     *      format: 格式限制, mime type(逗号分隔的字符串或数组,默认不限制),不确定文件类型时, 可在上传时通过浏览器控制台查看
     *      size: 文件大小限制，int类型，单位为字节,
     *      limit: 文件数量限制，用于多文件上传,
     *  }
     * @return Form|array
     */
    public function addFile($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            $format = $attr['format'] ?? false;
            if(is_string($format)){
                $format = explode(',', $format);
            }
            $item = [
                'type' => 'file',
                'name' => $name,
                'label' => $title,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
                'limit' => $attr['limit'] ?? 1,
                'size' => $attr['size'] ?? 1024*1024*5,
                'value' => $attr['value'] ?? 'url',
                'format' => $format,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 多文件上传
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题|占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示
     * @param string $default 默认值
     * @param array $attr 组件属性 {
     *      value: id/url, 值使用的附件字段, id-保存附件ID, url-保存附件url, 默认为url,
     *      format: 格式限制, mime type(逗号分隔的字符串或数组,默认不限制),不确定文件类型时, 可在上传时通过浏览器控制台查看
     *      size: 文件大小限制，int类型，单位为字节,
     *      limit: 图片数量限制，用于多文件上传,
     *  }
     * @return $this|array
     */
    public function addFiles($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        $attr['limit'] = $attr['limit'] ?? 99;
        return $this->addFile($name, $title, $tips, $default, $attr);
    }

    /**
     * 大文件上传
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题
     * @param string $tips 提示
     * @param string $default 默认值
     * @param array $attr 组件属性 {
     *      value: id/url, 值使用的附件字段, id-保存附件ID, url-保存附件url, 默认为url,
     *      format: 格式限制, mime type(逗号分隔的字符串或数组,默认不限制),不确定文件类型时, 可在上传时通过浏览器控制台查看
     *      size: 文件大小限制，int类型，单位为字节,
     *      limit: 文件数量限制，用于多文件上传,
     *  }
     * @return Form|array
     */
    public function addBigfile($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            $format = $attr['format'] ?? false;
            if(is_string($format)){
                $format = explode(',', $format);
            }
            $item = [
                'type' => 'big_file',
                'name' => $name,
                'label' => $title,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
                'limit' => $attr['limit'] ?? 1,
                'size' => $attr['size'] ?? 1024*1024*1024,
                'value' => $attr['value'] ?? 'url',
                'format' => $format,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 大文件多文件上传
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题|占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示
     * @param string $default 默认值
     * @param array $attr 组件属性 {
     *      value: id/url, 值使用的附件字段, id-保存附件ID, url-保存附件url, 默认为url,
     *      format: 格式限制, mime type(逗号分隔的字符串或数组,默认不限制),不确定文件类型时, 可在上传时通过浏览器控制台查看
     *      size: 文件大小限制，int类型，单位为字节,
     *      limit: 图片数量限制，用于多文件上传,
     *  }
     * @return $this|array
     */
    public function addBigfiles($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        $attr['limit'] = $attr['limit'] ?? 99;
        return $this->addBigfile($name, $title, $tips, $default, $attr);
    }

    /**
     * 富文本
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题
     * @param string $tips 提示
     * @param string $default 默认值
     * @return $this|array
     */
    public function addEditor($name = '', $title = '', $tips = '', $default = '')
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            $item = [
                'type' => 'editor',
                'name' => $name,
                'label' => $title,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
            ];
            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 富文本
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题
     * @param string $tips 提示
     * @param string $default 默认值
     * @param array $attr 组件属性
     * @return $this|array
     */
    public function addUEditor($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            $item = [
                'type' => 'ueditor',
                'name' => $name,
                'label' => $title,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
            ];
            foreach ($attr as $key => $value) {
                $item[$key] = $value;
            }
            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 小程序富文本
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题|占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param string $tips 提示
     * @param string $default 默认值
     * @param array $attr 组件属性 {image_drag: 图片是否可拖拽上传}
     * @return $this|array
     */
    public function addMpEditor($name = '', $title = '', $tips = '', $default = '', $attr = [])
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            list($title, $placeholder) = $this->getPlaceholderFromTitle($title, '请选择');
            $item = [
                'type' => 'mp_editor',
                'name' => $name,
                'label' => $title,
                'placeholder' => $placeholder,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
                'image_drag' => $attr['image_drag'] ?? false,
            ];
            foreach ($attr as $key => $value) {
                $item[$key] = $value;
            }
            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 静态文本
     * @param string $name 字段名:是否必填 格式: name:require 或 name
     * @param string $title 标题|占位符 格式: title|placeholder，无竖线分隔则为标题
     * @param array $option 需要转换的文字, 如: ['1' => '男'], 则前端渲染时在遇到1时将自动显示男
     * @param string $tips 提示
     * @param string $default 默认值
     * @param string $extra_class 样式类名，一般用于设置颜色
     * @return Form|array
     */
    public function addStatic($name = '', $title = '', $option = [], $tips = '', $default = '', $extra_class = '')
    {
        if(!empty($name)){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            if(is_string($option)){
                list($tips, $default, $extra_class) = [$option, $tips, $default];
            }
            if(!is_string($extra_class)) {
                $attr = $extra_class;
                $extra_class = $attr['extra_class'] ?? '';
            }

            $item = [
                'type' => 'static',
                'name' => $name,
                'label' => $title,
                'option' => $option,
                'tips' => $tips,
                'default' => $default,
                'require' => $attr['require'] ?? $require,
                'extra_class' => $extra_class,
                'extra_style' => $attr['extra_style'] ?? '',
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }
    /**
     * 隐藏表单项
     * @param string $name 字段名
     * @param string $default 默认值
     * @return Form|array
     */
    public function addHidden($name = '', $default = '')
    {
        if(!empty($name)){
            $item = [
                'type' => 'hidden',
                'name' => $name,
                'default' => $default,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 表格数据输入框
     * @param string $name 字段名
     * @param string $title 标题
     * @param string $tips 提示信息
     * @param array $default 默认值
     * @param array $attr 输入框选项 {rows_editable: 行数是否可增减, min_rows:最少行数, max_rows:最大行数}
     * @return Form|array
     */
    public function addTable($name = '', $title = '', $columns = [], $tips = '', $default = null, $attr = [])
    {
        if($name != ''){
            // 从表单名中处理是否必填
            list($name, $require) = $this->handleFormItemName($name);
            // 处理placeholder
            list($title, $placeholder) = $this->getPlaceholderFromTitle($title);

            // 获取列字段列表
            $this->_group_flag ++;
            $columns = $this->addFormItems($columns);
            $this->_group_flag --;


            $item = [
                'type' => 'table',
                'name' => $name,
                'label' => $title,
                'placeholder' => $placeholder,
                'tips' => $tips,
                'default' => $default,
                'textarea' => true,
                'columns' => $columns,
                'require' => $attr['require'] ?? $require,
                'rows_editable' => $attr['rows_editable'] ?? true,
                'min_rows' => $attr['min_rows'] ?? 0,
                'max_rows' => $attr['max_rows'] ?? 0,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }
        return $this;
    }

    /**
     * 分组
     * @param string $name 字段名
     * @param array $groups 分组列表，格式：{
     *      分组名: [ // 分组内的表单项列表
     *          表单项，同 addFormItem 方法的参数列表：
     *          [name, title, type, tips, default, attr]
     *      ]
     * }
     *
     * @return Form|array
     */
    public function addGroup($name = '', $groups = [])
    {
        if(!empty($name)){
            if(is_array($groups) && !empty($groups)){
                $this->_group_flag ++;
                foreach ($groups as &$group) {
                    foreach ($group as $key => $item) {
                        $group[$key] = $this->addFormItem(...$item);
                    }
                }
                $this->_group_flag --;
            }

            $item = [
                'type'    => 'group',
                'name' => $name,
                'option' => $groups,
            ];

            if ($this->_isGroup()) {
                return $item;
            }
            $this->_vars['form_items'][] = $item;
        }

        return $this;
    }

    /**
     * 设置触发器, 只有当target的值为value时,才会显示field
     * @param string|array $fields 要触发显示的字段
     * @param string $target 目标字段
     * @param string|array $values 目标字段的值
     * @param bool $show 是否触发器为真时显示字段, 设置为false则触发器为真时隐藏字段
     * @return $this
     */
    public function setTrigger($fields = '', $target = '', $values = '', $show = true)
    {
        if(!empty($fields)){
            if (!is_array($fields)){
                $fields = explode(',', $fields);
            }
            if (!is_array($values)){
                $values = explode(',', $values);
            }
            foreach ($fields as $value) {
                if(!isset($this->_vars['triggers'][$value])){
                    $this->_vars['triggers'][$value] = [];
                }
                $this->_vars['triggers'][$value][] = ['target' => $target, 'values' => $values, 'show' => $show];
            }
        }
        return $this;
    }

    /**
     * 从title中解析placeholder
     * @param $title
     * @param string $prefix
     * @return array
     */
    public function getPlaceholderFromTitle($title, $prefix = '请输入')
    {

        if(strpos($title, '|')){
            $arr = explode('|', $title);
            $title = $arr[0];
            $placeholder = $arr[1];
        }else {
            $placeholder = $prefix . $title;
        }
        return [$title, $placeholder];
    }

    /**
     * 处理表单字段名, 从名称中获取是否必填
     * @param string $name
     * @return array
     */
    public function handleFormItemName(string $name)
    {
        $require = false;
        if(strpos($name, ':')){
            $rule = substr($name, strpos($name, ':') + 1);
            $name = substr($name, 0, strpos($name, ':'));
            if($rule == 'require'){
                $require = true;
            }
        }
        return [$name, $require];
    }

    public function setFormData($data = [])
    {
        if(!empty($data)){
            $this->_vars['form_data'] = $data;
        }
        return $this;
    }

    /**
     * 渲染页面数据
     * @param array $vars
     * @param bool $getVars
     * @return mixed
     */
    public function fetch($vars = [], $getVars = false)
    {
        return parent::fetch($vars, $getVars);
    }

}