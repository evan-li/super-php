<?php


namespace evan\sbuilder\builder\button;

/**
 * 表格顶部新增按钮
 * Class TableTopButton
 * @package evan\sbuilder\builder\button
 */
class TableTopAddButton extends TableTopButton
{

    public function __construct()
    {
        parent::__construct();
        // 初始化默认值
        $this->setTitle('添加');
        $this->setAttrType('success');
        $this->setAttrIcon('el-icon-plus');
        $this->setOptionBatch(false);
    }
}