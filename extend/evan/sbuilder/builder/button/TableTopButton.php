<?php


namespace evan\sbuilder\builder\button;


class TableTopButton extends Button
{
    public function __construct()
    {
        parent::__construct();
        $this->setOptionBatch(true); // 顶部按钮默认为批量操作
    }

}