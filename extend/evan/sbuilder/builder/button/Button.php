<?php


namespace evan\sbuilder\builder\button;


class Button
{
    /**
     * @var array 按钮属性
     */
    protected array $_vars = [
        // 按钮文字
        'title' => '',
        // 按钮标识
        'flag' => '', // 按钮标记, 用于前端对特殊的按钮做特殊处理
        'tooltip' => true, // 是否显示tooltip, 为false时不显示, 为true时显示title, 值为字符串时显示指定的字符串, 默认为true, 即显示title
        /*
         * 按钮样式属性 {
         *      type:default|primary|success|info|warning|danger|text,
         *      round:true|false,
         *      plain:true|false,
         *      circle:true|false,
         *      icon,
         *      disabled: true|false,
         *      text:true|false,
         * }
         */
        'attr' => [
            'type' => 'primary',
            'text' => false,
            'round' => false,
            'plain' => false,
            'circle' => false,
            'icon' => '',
            'disabled' => false,
        ],
        /*
         * 操作参数 {
         *     type:refresh刷新页面|back返回上一页|link页面跳转|pop打开弹窗|ajax提交表单|relist刷新列表(在table中有效)|null无操作,
         *     url,
         *     target link类型有效:_blank新页面打开,
         *     confirm 操作前弹框确认: false|true|{title, msg},
         *     extra_data 额外数据: [],
         *     batch: 是否批量处理按钮(table中有效),
         *     with_search_form: 是否携带搜索表单数据(table中有效),
         *     hide: true|{field: value}，按钮隐藏规则，当指定的字段值与指定的值相等时隐藏该按钮(用于table中的行数据)
         *     show: false|{field: value}，按钮展示规则，当指定的字段值与指定的值相等时展示该按钮, 此值配置后hide配置无效(用于table中的行数据)
         * }
         */
        'option' => [
            'type' => 'link',
            'url' => '',
            'target' => '',
            'confirm' => false,
            'extra_data' => null,
            'batch' => false,
            'with_search_form' => false,
            'hide' => false,
            'show' => true,
        ],
    ];

//    public static function create($title, $flag, $url, $linkType, $option, $attr): Button
    public static function create($_vars): Button
    {
        return (new static())->setVars($_vars);
    }

    public function __construct()
    {
    }

    public function setVars($vars = []): Button
    {
        if(!empty($vars)) {
            // 合并属性
            if(isset($_vars['title'])) {
                $this->setTitle($vars['title']);
            }
            if(isset($_vars['attr'])) {
                $this->setAttr($vars['attr']);
            }
            if(isset($_vars['option'])) {
                $this->setOption($vars['option']);
            }
        }
        return $this;
    }

    /**
     * 批量设置按钮的样式属性
     * @param array $attr
     * @return $this
     */
    public function setAttr($attr = [])
    {
        $this->_vars['attr'] = array_merge($this->_vars['attr'], $attr);
        return $this;
    }

    /**
     * 批量设置按钮的操作属性
     * @param array $option
     * @return $this
     */
    public function setOption($option = [])
    {
        $this->_vars['option'] = array_merge($this->_vars['option'], $option);
        return $this;
    }

    // 设置按钮标题
    public function setTitle($title = '')
    {
        if($title) {
            $this->_vars['title'] = $title;
        }
        return $this;
    }

    public function setFlag($flag = '')
    {
        if($flag) {
            $this->_vars['flag'] = $flag;
        }
        return $this;
    }

    /**
     * 设置是否显示tooltip
     * @param bool|string $tooltip
     * @return $this
     */
    public function setTooltip($tooltip = true): Button
    {
        $this->_vars['tooltip'] = $tooltip;
        return $this;
    }

    /**
     * 按钮类型(样式)
     * @param string $type
     * @return $this
     */
    public function setAttrType(string $type = ''): Button
    {
        if($type) {
            $this->_vars['attr']['type'] = $type;
        }
        return $this;
    }

    /**
     * 按钮是否圆角
     * @param bool $round
     * @return $this
     */
    public function setAttrRound(bool $round = true): Button
    {
        $this->_vars['attr']['round'] = $round;
        return $this;
    }

    /**
     * 按钮是否镂空
     * @param bool $plain
     * @return $this
     */
    public function setAttrPlain(bool $plain = true): Button
    {
        $this->_vars['attr']['plain'] = $plain;
        return $this;
    }

    /**
     * 是否圆形按钮
     * @param bool $circle
     * @return $this
     */
    public function setAttrCircle($circle = true)
    {
        $this->_vars['attr']['circle'] = $circle;
        return $this;
    }

    /**
     * 是否圆形按钮
     * @param string $icon
     * @return $this
     */
    public function setAttrIcon(string $icon = ''): Button
    {
        if($icon) {
            $this->_vars['attr']['icon'] = $icon;
        }
        return $this;
    }

    /**
     * 是否圆形按钮
     * @param bool $disabled
     * @return $this
     */
    public function setAttrDisabled(bool $disabled = true): Button
    {
        $this->_vars['attr']['disabled'] = $disabled;
        return $this;
    }

    /**
     * 设置是否显示文本
     * @param bool $text
     * @return $this
     */
    public function setAttrText($text = true)
    {
        $this->_vars['attr']['text'] = $text;
        return $this;
    }

    /**
     * 设置按钮操作类型
     * @param string $type
     * @return $this
     */
    public function setOptionType($type = 'link')
    {
        $this->_vars['option']['type'] = $type;
        return $this;
    }

    /**
     * 设置按钮操作url
     * @param string $url
     * @return $this
     */
    public function setOptionUrl($url = '')
    {
        if($url) {
            $this->_vars['option']['url'] = $url;
        }
        return $this;
    }

    /**
     * 设置操作target, type为link类型有效:_blank新页面打开
     * @param string $target
     * @return $this
     */
    public function setOptionTarget($target = '_blank')
    {
        if($target) {
            $this->_vars['option']['target'] = $target;
        }
        return $this;
    }

    /**
     * 设置操作前弹框确认信息
     * @param bool|array $confirm false|true|{title, msg}
     * @return $this
     */
    public function setOptionConfirm($confirm = true)
    {
        $this->_vars['option']['confirm'] = $confirm;
        return $this;
    }

    /**
     * 设置按钮操作附加数据
     * @param array $data
     * @return $this
     */
    public function setOptionExtraData($data = null)
    {
        if(!empty($data)) {
            $this->_vars['option']['extra_data'] = $data;
        }
        return $this;
    }

    /**
     * 设置是否批量操作
     * @param bool $batch
     * @return $this
     */
    public function setOptionBatch($batch = true)
    {
        $this->_vars['option']['batch'] = $batch;
        return $this;
    }

    /**
     * 设置是否携带搜索表单
     * @param bool $withSearchForm
     * @return $this
     */
    public function setOptionWithSearchForm($withSearchForm = true)
    {
        $this->_vars['option']['with_search_form'] = $withSearchForm;
        return $this;
    }

    /**
     * 设置是否隐藏
     * @param bool|array|string $hide
     * @return $this
     */
    public function setOptionHide($hide = true)
    {
        $this->_vars['option']['hide'] = $hide;
        return $this;
    }

    /**
     * 设置是否显示
     * @param bool|array|string $show
     * @return $this
     */
    public function setOptionShow($show = true)
    {
        $this->_vars['option']['show'] = $show;
        return $this;
    }

    /**
     * 获取按钮的属性
     * @return array
     */
    public function fetch()
    {
        return $this->_vars;
    }

}