<?php


namespace evan\sbuilder\builder\button;


class TableRightButton extends Button
{
    public function __construct()
    {
        parent::__construct();
        $this->setAttrText() // 默认为text类型的按钮
            ->setAttrIcon('el-icon-Operation') // 默认图标
            ->setOptionExtraData(['id' => '__id__']) // 默认带当前行ID
        ;
    }
}