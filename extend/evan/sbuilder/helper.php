<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

use app\common\ResCode;
use app\system\model\Attachment;
use app\system\model\Icon;
use app\system\service\SettingService;
use evan\sbuilder\builder\ModuleCreate;
use evan\sbuilder\util\Util;
use think\Console;
use think\facade\Env;
use think\facade\Log;
use think\helper\Str;
use think\Response;
use think\response\Json;

Console::addDefaultCommands([
    ModuleCreate::class,
]);

if(! function_exists('dd')) {
    function dd(...$args){
        foreach ($args as $arg) {
            dump($arg);
        }
        die;
    }
}

if(! function_exists('price_format')) {
    function price_format($price, $decimal = 2){
        return number_format($price, $decimal, '.', '');
    }
}

/**
 * 将逗号分隔的字符串转换为数组
 */
if (!function_exists('comma_str_to_arr')) {
    function comma_str_to_arr($value)
    {
        return empty($value) ? [] : (is_string($value) ? explode(',', $value) : $value);
    }
}

/**
 * 将数组转为逗号分隔的字符串(仅支持一维数组)
 */
if (!function_exists('arr_to_comma_str')) {
    function arr_to_comma_str($value)
    {
        return is_array($value) ? implode(',', $value) : $value;
    }
}

/**
 * 将逗号分隔的字符串转换为数组
 */
if (!function_exists('json_str_to_arr')) {
    function json_str_to_arr($value, $defaultReturn = null)
    {
        return empty($value) ? $defaultReturn : (is_string($value) ? json_decode($value, 1) : $value);
    }
}

/**
 * 将数组转为逗号分隔的字符串(仅支持一维数组)
 */
if (!function_exists('arr_to_json_str')) {
    function arr_to_json_str($value, $defaultReturn = null)
    {
        return empty($value) ? $defaultReturn : (is_array($value) ? json_encode($value, JSON_UNESCAPED_UNICODE) : $value);
    }
}

if(! function_exists('log_exception')){
    function log_exception($msg, Throwable $e, $type = 'error'){
        switch ($type) {
            case 'info':
                Log::info($msg . ': ' . $e->getMessage() . "\r\n" . $e->getTraceAsString());
                break;
            case 'warning':
                Log::warning($msg . ': ' . $e->getMessage() . "\r\n" . $e->getTraceAsString());
                break;
            default:
                Log::error($msg . ': ' . $e->getMessage() . "\r\n" . $e->getTraceAsString());
                break;
        }
    }
}

if(! function_exists('log_exception_warning')){
    function log_exception_warning($msg, Throwable $e){
        log_exception($msg, $e, 'warning');
    }
}

if(! function_exists('log_think')){
    /**
     * @param string|array $msg
     * @param array $data
     * @param string $level
     */
    function log_think($msg, $data = null, $level = 'info'){
        if(is_array($msg)) {
            $msg = json_encode($msg, JSON_UNESCAPED_UNICODE);
        }
        if(!is_null($data)) {
            $msg .= ' ' . json_encode($data, JSON_UNESCAPED_UNICODE);
        }
        Log::log($level, $msg);
    }
}

if(! function_exists('log_debug')){
    /**
     * @param string|array $msg
     * @param array $data
     */
    function log_debug($msg, $data = null){
        log_think($msg, $data, 'debug');
    }
}

if(! function_exists('log_info')){
    /**
     * @param string|array $msg
     * @param array $data
     */
    function log_info($msg, $data = null){
        log_think($msg, $data, 'info');
    }
}

if(! function_exists('log_warning')){
    /**
     * @param string|array $msg
     * @param array $data
     */
    function log_warning($msg, $data = null){
        log_think($msg, $data, 'warning');
    }
}

if(! function_exists('log_error')){
    /**
     * @param string|array $msg
     * @param array $data
     */
    function log_error($msg, $data = null){
        log_think($msg, $data, 'error');
    }
}

if(! function_exists('public_path')) {
    /**
     * 获取public目录地址
     * @param $path
     * @return string
     */
    function public_path($path = ''){
        return Env::get('root_path') . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . ltrim($path, DIRECTORY_SEPARATOR);
    }
}

if(! function_exists('mix')) {
    /**
     * 从mix文件中获取带版本号的静态资源地址
     *
     * @param  string $path
     * @param  string $manifestDirectory
     * @return array
     *
     * @throws \Exception
     */
    function mix($path, $manifestDirectory = '')
    {
        static $manifests = [];

//        if (!Str::startsWith($path, '/')) {
//            $path = "/{$path}";
//        }

        if ($manifestDirectory && !Str::startsWith($manifestDirectory, '/')) {
            $manifestDirectory = "/{$manifestDirectory}";
        }

        $manifestPath = public_path($manifestDirectory . 'manifest.json');

        if (!isset($manifests[$manifestPath])) {
            if (!file_exists($manifestPath)) {
                throw new Exception('The Mix manifest does not exist.');
            }

            $manifests[$manifestPath] = json_decode(file_get_contents($manifestPath), true);
        }

        $manifest = $manifests[$manifestPath];

        if (!isset($manifest[$path])) {
            throw new Exception("Unable to locate Manifest file: {$path}.");

        }

        return $manifest[$path];
    }
}

if(! function_exists('is_page')) {
    /**
     * 判断是否为页面布局的请求
     */
    function is_page(){
        return request()->header('x-page') == '1';
    }
}

if(! function_exists('icons_map')){
    /**
     * 获取图标集合
     * @throws
     */
    function icons_map(){
        $icons = Icon::getEnableIcons();
        foreach ($icons as $item) {
            $item->icons = explode(',', $item['icons']);
        }
        return $icons;
    }
}

if(! function_exists('get_file_path')){
    /**
     * 根据ID获取文件路径
     * @param int $id
     * @return bool|string
     * @throws
     */
    function get_file_path($id = 0){
        return Attachment::getFilePath($id);
    }
}
if(! function_exists('get_files_path')){
    /**
     * 根据ID获取文件路径
     * @param array $id
     * @return array
     * @throws
     */
    function get_files_path($id = []){
        if(is_string($id)){
            $id = explode(',', $id);
        }
        return Attachment::getFilePath($id);
    }
}

if(! function_exists('convert_list_to_tree')){
    function convert_list_to_tree($list, $pid=0, $tier = null, $pidKey = 'pid', $idKey = 'id', $subKey = 'children'){
        if(!is_null($tier)) $tier--;
        $tree = [];
        foreach ($list as &$item) {
            if($item->{$pidKey} == $pid){
                if(is_null($tier) || $tier > 0){
                    $children = convert_list_to_tree($list, $item->{$idKey}, $tier, $pidKey, $idKey);
                    if(count($children) > 0){
                        $item[$subKey] = $children;
                    }
                }
                $tree[] = $item;
            }
        }
        return $tree;
    }
}

if(! function_exists('module_config')){
    /**
     * 获取模块配置
     * @param $key
     * @param null $value
     * @param $module
     * @return mixed
     * @throws
     */
    function module_config($module = null, $key = null, $value = null){
        if(is_null($key)){
            $key = $module;
            $module = request()->module();
        }
        if(is_null($value)){
            return SettingService::get($module, $key);
        }else {
            return SettingService::set($module, $key, $value);
        }
    }
}

if(! function_exists('is_wechat')){
    /**
     * 是否微信浏览器(公众号)
     */
    function is_wechat(){
        if (
            (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) || (env('app_env') == 'local' && session('mock_wechat'))) {
            return true;
        }
        return false;
    }
}

if(! function_exists('is_miniprogram')){
    /**
     * 是否微信小程序浏览器环境
     */
    function is_miniprogram(){
        if (
            strpos($_SERVER['HTTP_USER_AGENT'], 'miniProgram') !== false // 小程序中webview发出的请求
            || get_device_type() == 5 // 小程序原生请求
        ) {
            return true;
        }
        return false;
    }
}

if(! function_exists('is_h5')){
    /**
     * 是否H5请求
     */
    function is_h5(){
        if (
            strpos($_SERVER['HTTP_USER_AGENT'], 'miniProgram') !== false // 小程序中webview发出的请求
            || get_device_type() == 3 // H5请求
        ) {
            return true;
        }
        return false;
    }
}

if (! function_exists('is_android')){
    /**
     * 是否安卓环境
     */
    function is_android(){
        return get_device_type() == 1;
    }
}

if (! function_exists('is_ios')){
    /**
     * 是否安卓环境
     */
    function is_ios(){
        return get_device_type() == 2;
    }
}

if (! function_exists('is_ios_webview')){
    /**
     * 是否为ios浏览器环境
     */
    function is_ios_webview(){
        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        if(strpos($agent,'iphone') || strpos($agent,'ipad'))
        {
            return true;
        }
        return false;
    }
}
if (! function_exists('is_android_webview')){
    /**
     * 是否为安卓浏览器环境
     */
    function is_android_webview(){
        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        if(strpos($agent,'android'))
        {
            return true;
        }
        return false;
    }
}

if (! function_exists('is_app')){
    /**
     * 是否安卓环境
     */
    function is_app(){
        return is_android() || is_ios();
    }
}

if (!function_exists('get_device_type')) {
    function get_device_type(){
        return input('device_type');
    }
}

if (!function_exists('get_common_param')) {
    function get_common_param($name){
        return input($name) || request()->header($name);
    }
}

if (! function_exists('array_get')){
    function array_get($arr, $key, $default = null){
        return $arr[$key] ?? $default;
    }
}

if (! function_exists('string_to_kv_array')){
    /**
     * 字符串解析为键值对数组, 支持的格式为: a=1,b=2 或 a=1\nb=2 或 a=1|b=2 或 a=1&b=2
     * @param $str
     * @return array
     */
    function string_to_kv_array($str){

        // 解析参数
        $str = preg_replace("/[,\n|]/", '&', $str);
        $strArr = explode("&", $str);
        $array = [];
        foreach ($strArr as $param) {
            if(!empty($param) && strpos($param, '=') !== false) {
                $kv = explode('=', $param);
                $array[$kv[0]] = $kv[1];
            }
        }
        return $array;
    }
}

if( !function_exists('admin_success')) {
    /**
     * admin返回操作成功
     * @param string $msg
     * @param string $url
     * @param string $extra_data
     * @param int $wait
     * @param array $header
     * @param int $code
     * @return Response
     */
    function admin_success($msg = '', $url = '__back__', $extra_data = '', $wait = 1, array $header = [], $code = ResCode::success){
        $option = Util::parseOptionUrl($url);
        // action 操作数据封装
        $data = [
            'type' => $option['type'] ?? '', // 操作类型
            'url' => $option['url'] ?? '', // url(跳转或弹框)
            'target' => $option['target'] ?? '', // url跳转作用域
            'wait' => $wait,
            'extra_data' => $extra_data,
        ];

        return res_error($code, $msg, $data);
    }
}

if( !function_exists('admin_error') ){
    /**
     * admin返回操作失败
     * @param string $msg
     * @param string $url
     * @param string $extra_data
     * @param int $wait
     * @param array $header
     * @return Response
     */
    function admin_error($msg = '', $url = null, $extra_data = '', $wait = 1, array $header = []){
        return admin_success($msg, $url, $extra_data, $wait, $header, ResCode::unknow_error);
    }
}

if( !function_exists('admin_data') ) {
    /**
     * 后台返回数据
     * @param array $data
     * @param string $msg
     * @return Json
     */
    function admin_data($data = [], $msg = '') {
        return res_ok($data, $msg);
    }
}


if (!function_exists('deldir')) {

    /**
     * 删除文件夹
     * @param string $dirname 目录
     * @param bool   $delself 是否删除自身
     * @return boolean
     */
    function deldir(string $dirname, bool $delself = true): bool
    {
        if (!is_dir($dirname)) {
            return false;
        }
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dirname, FilesystemIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($files as $fileinfo) {
            if ($fileinfo->isDir()) {
                deldir($fileinfo->getRealPath());
            } else {
                @unlink($fileinfo->getRealPath());
            }
        }
        if ($delself) {
            @rmdir($dirname);
        }
        return true;
    }
}

