<?php
/**
 * Created by PhpStorm.
 * User: EvanLee
 * Date: 2019/4/23
 * Time: 16:32
 */

namespace evan\sbuilder\controller;


use app\common\exception\CommonResponseException;
use app\common\ResCode;
use think\Controller;

class Base extends Controller
{

    protected function checkParam($rule, $message = [])
    {
        $params = $this->request->param();
        $res = $this->validate($params, $rule, $message);
        if ($res !== true) {
            throw new CommonResponseException(ResCode::param_invalid, $res);
        }
        return $params;
    }

}