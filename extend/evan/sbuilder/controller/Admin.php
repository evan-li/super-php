<?php
/**
 * 后台通用控制器
 * 包含了后台核心功能(部分功能可通过继承覆盖的方式自定义):
 *      1. 请求验证 (支持自定义)
 *      2. 登录验证 (支持自定义)
 *      3. 权限验证 (支持自定义)
 *      4. 自动获取数据列表
 *      5. 自动添加
 *      6. 自动编辑
 *      7. 批量操作
 *      8. 启用禁用
 * User: EvanLee
 * Date: 2019/4/23
 * Time: 17:06
 */

namespace evan\sbuilder\controller;

use app\common\exception\CommonResponseException;
use app\common\ResCode;
use evan\sbuilder\builder\SBuilder;
use app\system\model\User;
use think\Db;
use think\db\Query;
use think\Exception;
use think\exception\ClassNotFoundException;
use think\exception\DbException;
use think\exception\PDOException;
use think\facade\Cache;
use think\helper\Str;
use think\Model;
use think\model\Collection;
use think\response\Json;

class Admin extends Base
{
    /** @var User */
    protected $currentUser;
    private $queryWhere = null; // 查询条件

    // 统一缓存前缀
    protected $cachePrefix = SITE_NAME . ':';

    /**
     * 可以不通过ajax请求的url地址, 如果不是ajax请求, 只有此数组内的请求不会转发到前段首页
     * @var string[] 模块名/控制器名称/方法名
     */
    protected $notAjaxUrls = [
        'index/index/index',
        'system/index/index',
        'system/attachment/upload',
        'system/attachment/download',
    ];

    /**
     * 未登录即可请求的url列表
     * @var string[] 模块名/控制器名称/方法名
     */
    protected $notLoginUrls = [
        '/index/index/index',
        '/system/index/index',
        '/system/index/config',
        '/system/index/login',
        '/system/index/logout',
    ];

    /**
     * 排除权限验证的地址
     * @var string[] 模块名/控制器名称/方法名
     */
    protected $authExcludeUrls = [
        '/',
        '/index/index/index',
        '/system/index/config',
        '/system/index/login',
        '/system/index/logout',
        '/system/index/getinitdata',
        '/system/index/currentuser',
        '/system/index/modifypassword',
        '/system/index/menus',
        '/system/attachment/getfilespath',
        '/system/attachment/upload',
        '/system/attachment/download',
    ];

    protected function tablePageCacheKey(){
        return $this->cachePrefix . $this->request->module() . ':' . $this->request->controller() . ':tablePageConfig';
    }

    protected function autoAddCacheKey(){
        return $this->cachePrefix . $this->request->module() . ':' . $this->request->controller() . ':autoAdd';
    }

    protected function autoEditCacheKey(){
        return $this->cachePrefix . $this->request->module() . ':' . $this->request->controller() . ':autoEdit';
    }

    /**
     */
    protected function initialize()
    {
        parent::initialize();
        // 后台只接受ajax请求, 如果不是ajax请求, 直接抛一个404, 由异常处理重定向到前端页面
        $this->checkAjax();
        // 验证登陆,并注入当前登陆的用户信息, 未登录需返回10003错误码
        $this->checkUser();
        // 权限验证
//        $this->checkAuth();

    }

    /**
     * 后台只接受ajax请求, 如果不是ajax请求, 直接抛一个404, 由异常处理重定向到前端页面
     */
    protected function checkAjax()
    {
        if(!request()->isAjax()){
            $path =  $this->request->module() . '/' . Str::snake($this->request->controller()) . '/' . $this->request->action();
            if(! in_array($path, $this->notAjaxUrls) && !$this->request->param('not_ajax') && !$this->request->param('_debug')){
                abort(404);
            }
        }
    }

    /**
     * 初始化当前请求的用户信息,并验证登陆
     */
    protected function checkUser()
    {
        // 注入当前登陆的用户信息
        $user = session('current_user');
        if(!empty($user)){
            $this->currentUser = $user;
        }else {
            // 判断是否为无需验证登陆的操作
            $path = '/' . $this->request->module() . '/' . Str::snake($this->request->controller()) . '/' . $this->request->action();
            if(!in_array($path, $this->notLoginUrls)){
                throw new CommonResponseException(ResCode::un_login, '未登录', ['url' => '/login', 'type' => 'link']);
            }
        }
    }

    /**
     * 验证权限
     */
    protected function checkAuth()
    {
        if($this->currentUser){
            $path =  '/' . $this->request->module() . '/' . Str::snake($this->request->controller()) . '/' . $this->request->action();
            if(in_array($path, $this->authExcludeUrls)){
                return ;
            }
            $rules = $this->getAuthRules();
            if($rules instanceof Collection){
                $paths = $rules->column('url');
            }else {
                $paths = array_column($rules, 'url');
            }
            if (!in_array($path, $paths)){
                throw new CommonResponseException(ResCode::permission_denied, '无权限操作:' . $path);
            }
        }
    }

    /**
     * 获取当前用户的权限列表
     * @return array|Collection
     */
    protected function getAuthRules()
    {
        return User::getAuthRules($this->currentUser);
    }

    /**
     * 从请求中获取排序
     * @param string $default 默认排序
     * @return string
     */
    protected function getOrder($default = null)
    {
        $order = $default ?: 'id desc';
        $sort = $this->request->param('sort');
        if(is_string($sort)){
            $sort = json_decode($sort, 1);
        }
        if($sort && isset($sort['field']) && isset($sort['orderType'])){
            $order = $sort['field'] . ' ' . $sort['orderType'];
        }
        return $order;
    }

    /**
     * 从请求中解析where条件
     * @param string|array $fields 指定字段列表
     * @param bool $except 是否排除指定字段
     * @return array
     */
    protected function getWhere($fields = null, $except = false)
    {
        $where = $this->queryWhere;
        if($where === null) {
            // 搜索处理
            $keyword = $this->request->param('keyword');
            $searchField = $this->request->param('field');
            $where = [];
            $config = Cache::get($this->tablePageCacheKey(), []);
            if(!empty($keyword) && !empty($searchField)){ // 如果有传字段, 则只搜索指定字段, 否则不搜索
                $searchColumns = $config['search']['columns'];
                foreach ($searchColumns as $item) {
                    if($item['field'] == $searchField){
                        if($item['op'] == 'like'){
                            $keyword = "%$keyword%";
                        }
                        $where[] = [$item['field'], $item['op'], $keyword];
                    }
                }
            }
            // 扩展搜索字段
            $searchExt = $this->request->param('searchExt');
            if(is_string($searchExt)){
                $searchExt = json_decode($searchExt, 1);
            }
            if(!empty($config['search_ext']) && $searchExt && is_array($searchExt)){
                foreach ($searchExt as $field => $value){
                    foreach ($config['search_ext'] as $searchItemConfig) {
                        foreach ($searchItemConfig['columns'] as $item) {
                            if($item['field'] == $field){
                                if($item['op'] == 'like'){
                                    $value = "%$value%";
                                }
                                $where[] = [$item['field'], $item['op'], $value];
                            }
                        }
                    }
                }
            }

            // 顶部下拉框筛选
            $topSelects = $this->request->param('topSelects');
            if(is_string($topSelects)){
                $topSelects = json_decode($topSelects, 1);
            }

            if(!empty($config['top_selects']) && $topSelects && is_array($topSelects)){
                $topSelectsFields = array_column($config['top_selects'], 'name');
                foreach ($topSelects as $key => $value) {
                    if(($value || $value === '0' || $value === 0) && in_array($key, $topSelectsFields)){
                        if(is_array($value)){
                            $where[] = [$key, 'in', $value];
                        }else {
                            $where[] = [$key, '=', $value];
                        }
                    }
                }
            }
            // 时间筛选
            $timeFilters = $this->request->param('time_filter');
            if(!empty($timeFilters)){
                if(is_string($timeFilters)){
                    $timeFilters = json_decode($timeFilters, 1);
                }
                foreach ($timeFilters as $key => $times) {
                    $times = explode(',', $times);
                    foreach ($times as $index => $time) {
                        $times[$index] = strtotime($time);
                    }
                    $where[] = [$key, 'between', $times];
                }
            }
            // 表头筛选
            if(!empty($filters = $this->request->param('filters'))){
                if(is_string($filters)){
                    $filters = json_decode($filters, 1);
                }
                foreach ($filters as $key => $value) {
                    if(($value || $value === 0) && is_array($value) && count($value) > 0){
                        $where[] = [$key, 'in', $value];
                    }
                }
            }
            $this->queryWhere = $where;
        }

        // 指定字段
        if(!empty($fields)) {
            $fields = is_string($fields) ? explode(',', $fields) : $fields;
            foreach ($where as $key => $item) {
                if($except && in_array($item[0], $fields)) {
                    unset($where[$key]);
                }else if(!$except && !in_array($item[0], $fields)) {
                    unset($where[$key]);
                }
            }
            $where = array_values($where);
        }
        return $where;
    }

    /**
     * 获取键值对形式的where条件, (忽略条件判断方式, 如:=,like等)
     * @param string|array $fields
     * @param bool $except
     * @return array
     */
    public function getWhereKV($fields = null, $except = false) {
        $where = $this->getWhere($fields, $except);
        $result = [];
        foreach ($where as $item) {
            $result[$item[0]] = $item[2];
        }
        return $result;
    }

    /**
     * 获取where条件中指定字段的值
     * @param string $field
     * @param mixed $default
     * @return mixed
     */
    public function getWhereValue($field, $default = null) {
        return $this->getWhereKV($field)[$field] ?? $default;
    }

    /**
     * 自动获取数据列表
     * @throws DbException
     */
    public function getList()
    {
        $where = $this->getWhere();
        $config = Cache::get($this->tablePageCacheKey());
//        $columns = $config['columns'];
//        $fields = [];
//        foreach ($columns as $column) {
//            if(!in_array($column['name'], ['__btn__'])){
//                $fields[] = $column['name'];
//            }
//        }
        // 获取排序
        $order = $this->getOrder();

        // 获取模型
        $model = $this->getModel($config);
        $query = $model->order($order)->where($where);
        $data = $config && $config['row_list_paginate'] ? $query->paginate(input('page_size')) : $query->select();
        return admin_data($data);
    }

    /**
     * 自动新增
     * @throws Exception
     */
    public function add()
    {
        $config = Cache::get($this->autoAddCacheKey());
        if(empty($config)){
            return admin_error('自动新增不存在, 请刷新重试');
        }
        if(is_page()){
            return SBuilder::make('form')
                ->setPageTitle('新增')
                ->addFormItems($config['fields'])
                ->setLabelWith(150)
                ->fetch();
        }else {
            $result = $this->validate($this->request->param(), $config['validate']);
            if($result !== true){
                return admin_error($result);
            }
            $params = $this->request->param();
            // 过滤输出
            $data = [];
            foreach ($config['fields'] as $field) {
                if(isset($field[3]) && $field[3] == 'static') continue;
                $fieldName = $field[0];
                if(strpos($fieldName, ':')){
                    $fieldName = substr($fieldName, 0, strpos($fieldName, ':'));
                }
                $data[$fieldName] = $params[$fieldName] ?? '';
            }
            // 获取模型
            $model = $this->getModel($config);
            // 添加数据
            if($model instanceof Model){
                $model->save($data);
            }else {
                // 自动更新时间戳支持
                if($config['autoTime']){
                    foreach ($config['autoTime'] as $item) {
                        $nowTime = $this->request->time();
                        if (strpos($item, '|')) {
                            list($item, $format) = explode('|', $item);
                            $data[$item] = date($format, $nowTime);
                        } else {
                            $data[$item] = $nowTime;
                        }
                    }
                }
                $model->insert($data);
            }
            // 如果是在弹框中,则关闭弹框并刷新列表
            if($config['pop']){
                return admin_success('添加数据成功', '__relist__');
            }else {
                return admin_success('添加数据成功', '__back__');
            }
        }
    }

    /**
     * 自动编辑
     * @throws Exception
     */
    public function edit()
    {
        $config = Cache::get($this->autoEditCacheKey());
        if(empty($config)){
            return admin_error('自动编辑不存在, 请刷新重试');
        }
        $id = $this->request->param('id');
        if (empty($id)) return admin_error('缺少主键');

        // 获取模型
        $model = $this->getModel($config);
        if(is_page()){
            $obj = $model->where($model->getPk(), $id)->find();
            if(empty($obj)){
                return admin_error('数据不存在，主键ID：' . $id);
            }
            return SBuilder::make('form')
                ->setPageTitle('编辑')
                ->addFormItems($config['fields'])
                ->setFormData($obj)
                ->setLabelWith(150)
                ->fetch();
        }else {
            $result = $this->validate($this->request->param(), $config['validate']);
            if($result !== true){
                return admin_error($result);
            }
            $params = $this->request->param();
            // 过滤输出
            $data = [];
            foreach ($config['fields'] as $field) {
                if(isset($field[3]) && $field[3] == 'static') continue;
                $fieldName = $field[0];
                if(strpos($fieldName, ':')){
                    $fieldName = substr($fieldName, 0, strpos($fieldName, ':'));
                }
                $data[$fieldName] = $params[$fieldName] ?? '';
            }

            // 修改数据
            if($model instanceof Model){
                // 使用模型修改数据
                $obj = $model->find($id);
                $obj->save($data);
            }else {
                // 如果是Db类,自动更新时间戳支持
                if($config['autoTime']){
                    foreach ($config['autoTime'] as $item) {
                        $nowTime = $this->request->time();
                        if (strpos($item, '|')) {
                            list($item, $format) = explode('|', $item);
                            $data[$item] = date($format, $nowTime);
                        } else {
                            $data[$item] = $nowTime;
                        }
                    }
                }
                $model->where($model->getPk(), $id)->update($data);
            }
            // 如果是在弹框中,则关闭弹框并刷新列表
            if($config['pop']){
                return admin_success('修改数据成功', '__relist__');
            }else {
                return admin_success('修改数据成功', '__back__');
            }
        }
    }

    /**
     * 批量操作
     *  type: enable disable delete
     * @throws PDOException
     * @throws Exception
     */
    public function batch()
    {
        $type = $this->request->param('type');
        $ids = $this->request->param('ids');
        $config = Cache::get($this->tablePageCacheKey());
        if(empty($config)){
            return admin_error('批量操作配置不存在');
        }
        if (empty($ids)) return admin_error('缺少主键');
        if(is_string($ids)) $ids = explode(',', $ids);

        // 获取模型
        $model = $this->getModel($config);
        switch ($type){
            case 'enable':
                // 批量启用
                $model->where($model->getPk(), 'in', $ids)->update(['status' => 1]);
                return admin_success('批量启用成功', '__relist__');
            case 'disable':
                // 批量禁用
                $model->where($model->getPk(), 'in', $ids)->update(['status' => 2]);
                return admin_success('批量禁用成功', '__relist__');
            case 'delete':
                // 批量删除
                // 删除
                if($model instanceof Model){
                    // 先查询,再删除,以便于使用模型时可以使用软删除
                    $data = $model->where($model->getPk(), 'in', $ids)->select();
                    foreach ($data as $item) {
                        $item->delete();
                    }
                }else {
                    $model->where($model->getPk(), 'in', $ids)->delete();
                }
                return admin_success('批量删除成功', '__relist__');
            default:
                return admin_error('不允许的操作: ' . $type);
        }
    }

    /**
     * 启用
     * @throws PDOException
     * @throws Exception
     */
    public function enable()
    {
        $config = Cache::get($this->tablePageCacheKey());
        if(empty($config)){
            return admin_error('页面配置信息不存在');
        }
        $id = $this->request->param('id');
        if (empty($id)) return admin_error('缺少主键');

        // 获取模型
        $model = $this->getModel($config);
        // 启用
        $model->where($model->getPk(), $id)->update(['status' => 1]);
        return admin_success('启用成功', '__relist__');
    }

    /**
     * 自动禁用
     * @throws PDOException
     * @throws Exception
     */
    public function disable()
    {
        $config = Cache::get($this->tablePageCacheKey());
        if(empty($config)){
            return admin_error('页面配置信息不存在');
        }
        $id = $this->request->param('id');
        if (empty($id)) return admin_error('缺少主键');

        // 获取模型
        $model = $this->getModel($config);
        // 禁用
        $model->where($model->getPk(), $id)->update(['status' => 2]);
        return admin_success('禁用成功', '__relist__');
    }

    /**
     * 自动删除
     * @throws PDOException
     * @throws Exception
     * @throws \Exception
     */
    public function delete()
    {
        $config = Cache::get($this->tablePageCacheKey());
        if(empty($config)){
            return admin_error('页面配置信息不存在');
        }
        $id = $this->request->param('id');
        if (empty($id)) return admin_error('缺少主键');

        // 获取模型
        $model = $this->getModel($config);
        // 删除
        if($model instanceof Model){
            // 先查询,再删除,以便于使用模型时可以使用软删除
            $model->where($model->getPk(), $id)->find()->delete();
        }else {
            $model->where($model->getPk(), $id)->delete();
        }
        return admin_success('删除成功', '__relist__');
    }

    /**
     * 快捷编辑
     */
    public function quickEdit()
    {
        $config = Cache::get($this->tablePageCacheKey());
        if(empty($config)){
            return admin_error('页面配置信息不存在');
        }
        $_id = $this->request->param('_id');
        if (empty($_id)) return admin_error('缺少主键');

        $field = input('field');
        $value = input('value');

        // 获取模型
        $model = $this->getModel($config);
        // 禁用
        $model->where($model->getPk(), $_id)->update([$field => $value]);
        return admin_success('修改成功', '__relist__');
    }

    /**
     * 获取模型对象或query对象
     * @param $pageConfig array 页面配置
     * @return Model|Query
     */
    private function getModel($pageConfig){
        $modelName = $pageConfig && $pageConfig['table_name'] ?: Str::snake($this->request->controller());
        $tableName = $pageConfig && $pageConfig['table_name'] ?: Str::snake($this->request->module()) . '_' . Str::snake($this->request->controller());
        try {
            // 尝试使用模型修改数据
            $model = model($modelName);
        }catch (ClassNotFoundException $e) {
            // 如果模型不存在,则直接使用Db类
            $model = Db::name($tableName);
        }
        return $model;
    }

    /**
     * 获取系统配置
     * @param bool $getAsArray
     * @return Json|array
     */
    public function systemConfig($getAsArray = false)
    {
        $config = config((defined('SITE_NAME') ? SITE_NAME : 'admin') . '.');
        // 合并默认配置信息
        $config = array_merge([
            'captcha_url' => captcha_src(), // 登录验证码地址
            'icons' => icons_map(), // 图标库列表
            'debug' => config('app_debug'),
        ], $config);
        if($getAsArray){
            return $config;
        }
        return admin_data([
            'title' => $config['title'], // 站点标题
            'short_title' => $config['short_title'], // 站点短标题
            'description' => $config['description'], // 站点描述
            'keyword' => $config['keyword'], // 站点关键字
            'system_config_url' => $config['system_config_url'], // 系统核心配置获取地址
            'login_need_captcha' => $config['login_need_captcha'], // 登录是否需要验证码
            'captcha_url' => $config['captcha_url'], // 登录验证码地址
            'clear_cache_url' => $config['clear_cache_url'], // 清空缓存地址
            'clear_session_url' => $config['clear_session_url'], // 清空session地址
            'init_data_url' => $config['init_data_url'], // 获取用户信息地址
            'user_info_url' => $config['user_info_url'], // 获取用户信息地址
            'user_edit_url' => $config['user_edit_url'], // 获取用户信息地址
            'menu_type' => $config['menu_type'], // 菜单渲染方式 1-顶部+左侧菜单 2-左侧菜单
            'menu_url' => $config['menu_url'], // 获取菜单地址
            'login_url' => $config['login_url'], // 获取用户信息地址
            'logout_url' => $config['logout_url'], // 获取用户信息地址
            'modify_password_url' => $config['modify_password_url'], // 获取菜单地址
            'auth_rules_url' => $config['auth_rules_url'], // 获取权限列表地址
            'file_upload_url' => $config['file_upload_url'], // 文件上传地址
            'big_file_upload_url' => $config['big_file_upload_url'], // 文件上传地址
            'image_upload_url' => $config['image_upload_url'], // 图片上传地址
            'video_upload_url' => $config['video_upload_url'], // 视频上传地址
            'file_path_url' => $config['file_path_url'], // 内部文件路径转换地址
            'icons' => $config['icons'], // 图标库列表
            'debug' => $config['debug'],
            'index_page_path' => $config['index_page_path'], // 首页路径
        ]);
    }

    /**
     * 管理后台页面
     * @return \think\response\View
     */
    public function homePage($template = 'admin/index')
    {
        if(is_page()){ // 如果是请求页面配置, 返回404页面
            abort(404);
        }
        $config = $this->systemConfig(true);
        $icons = icons_map();
        return view($template, ['config' => $config, 'icons' => $icons]);
    }
}