import './bootstrap'

import { createApp } from 'vue'
import pinia from './store/index'
import router from './router'


import App from './App.vue'
// ElementPlus
import ElementPlus from 'element-plus'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/display.css'
import './style/index.scss'
// v-viewer
import 'viewerjs/dist/viewer.css'
import VueViewer from 'v-viewer'
import VueUeditorWrap from 'vue-ueditor-wrap'

import {registerIcons} from "@/util/icon.js";
import {initLang} from "@/lang/index.js";
import {registerEventBus} from "@/util/eventBus.js";
import CountTo from '@/components/common/count-to/count-to.vue'


async function init() {
    const app = createApp(App)

    // app.use(VueViewer)
    app.use(pinia)
    app.use(router)
    app.use(VueViewer)
    app.use(VueUeditorWrap)
    app.component(CountTo.name, CountTo)

    const i18n = await initLang(app)

    app.use(ElementPlus, {
        locale: zhCn,
        i18n: i18n.global.t
    })
    /**
     * 全局注册图标,包括element Plus的图标以及自定义图标, elementPlus图标统一注册名字为: el-icon-Menu
     */
    registerIcons(app)

    registerEventBus(app)

    app.mount('#app')

}

init()