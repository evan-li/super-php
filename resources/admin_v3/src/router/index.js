import {createRouter, createWebHistory} from 'vue-router'
import NProgress from 'nprogress'
import {loading} from "@/util/loading.js";

import Layout from "@/layout/index.vue";
import SContent from "@/components/core/s-content.vue";
import Login from "@/views/login.vue";
import Dashboard from "@/views/dashboard.vue";
import E404 from '@/views/common/404.vue'
import E401 from '@/views/common/401.vue'
import AuthRule from '@/views/auth/rule/index.vue'
import StoreIndex from '@/views/store/index.vue'
import Refresh from '@/views/refresh.vue'
import {usePageConfig} from "@/store/pageConfig.js";
import api from '@/util/api'
import {ElMessage} from "element-plus";


const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        { path: '/', redirect: '/dashboard'},
        { path: '/index.html', redirect: '/dashboard'},
        // 登陆页面
        { path: '/login', name: 'login', component: Login },

        // 基础页面
        {
            path: '/',
            name: 'common',
            component: Layout,
            children: [
                // 内容页面
                {path: 'dashboard', component: Dashboard, name: 'Dashboard'},
                {path: '404', component: E404, name: '404'},
                {path: '401', component: E401, name: '401'},
                // 页面刷新组件
                {path: 'refresh', component: Refresh, name: 'refresh'},
            ]
        },

        // 模块市场
        {
            path: '/system',
            name: 'store',
            component: Layout,
            children: [
                // 内容页面
                {path: 'module/index', component: StoreIndex, name: 'StoreIndex'},
            ]
        },

        // 所有未匹配的页面路由到内容页面, 由内容页面获取响应的页面配置并渲染实际页面
        {
            path: '/',
            name: 'matchContent',
            component: Layout,
            children: [
                // 内容页面
                {path: ':pathMatch(.*)', component: SContent, name: 'SContent'},
            ]
        }
    ]
})

router.beforeEach(async (to, from) => {
    NProgress.configure({ showSpinner: false })
    NProgress.start()
    // console.log('router.beforeEach: to, from', to, from)
    if(!window.existLoading) {
        window.existLoading = true
        loading.show()
    }

    // 处理服务器重定向到指定页面时在浏览器返回页面为空的问题
    if(to.query.__to){
        NProgress.done()
        return {path: to.query.__to}
    }

    let pageConfig = usePageConfig()
    pageConfig.globalLoading = true

    // 如果是匹配到SContent路由, 拦截请求, 获取页面结构
    if((to.matched[0] && to.matched[0].name == 'matchContent') || to.query.refresh) {
        // 开启页面loading
        pageConfig.pageLoading = true
        // 路由跳转前拦截, 获取页面配置
        const res = await api.get(to.path, to.query, false, {'x-page': 1})
        // 关闭页面loading
        pageConfig.pageLoading = false
        let currentPageConfig = res;
        if(res.code == api.CODE_UN_LOGIN){
            // 未登录
            ElMessage.error('您尚未登陆')
            return {path: '/login'}
        }else if(res.code == 404){
            currentPageConfig = {
                type: 'error',
                page_title: '错误页面',
                blocks: [{
                    layout: {},
                    name: 'error',
                    type: 'error',
                    error: {code: 404, msg: '啊哦, 页面走丢了~'},
                }],
            }
        }else if(res.code === 0 || res.code > 0){
            currentPageConfig = {
                type: 'error',
                page_title: '错误页面',
                blocks: [{
                    layout: {},
                    name: 'error',
                    type: 'error',
                    error: {code: res.code || '500', msg: res.msg || '服务器错误'},
                }],
            };
        }
        pageConfig.currentPage = currentPageConfig
    }
})

// 路由加载后
router.afterEach(() => {
    NProgress.done()
    if (window.existLoading) {
        loading.hide()
    }
})

export default router
