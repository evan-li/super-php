import mitt from "mitt";


let bus = false
if(!bus) {
    bus = mitt()
}

export const registerEventBus = (app) => {
    app.config.globalProperties.$bus = bus
}

export const useEventBus = () => {
    return bus
}

export default bus