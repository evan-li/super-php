import axios from 'axios';

import {ElMessage} from "element-plus";
import {useAuth} from "@/store/auth.js";
import router from '@/router'

window.baseApiUrl = import.meta.env.VITE_AXIOS_BASE_URL || '/';
const CODE_OK = 200;
const CODE_OK_ALIAS = 0;
const CODE_UN_LOGIN = 10003;

let auth

class ResponseError {
    constructor(response){
        this.response = response;
    }
}

function handlerRes(res) {
    if (res && (res.code === CODE_OK || res.code === CODE_OK_ALIAS)) {
        return res.data;
    } else {
        return Promise.reject(new ResponseError(res));
    }
}

function handlerError(error) {
    if(!auth) {
        auth = useAuth()
    }
    if(error instanceof ResponseError){
        let res = error.response;
        if(res){
            switch (res.code) {
                case CODE_UN_LOGIN:
                    // store.commit('setUser', null)
                    // store.commit('setMenus', [])
                    auth.user = null
                    auth.menus = []
                    router.push('/login')

                    ElMessage.error('您的登录信息已失效, 请先登录');
                    break;
                default:
                    console.log('接口返回错误信息:', res, error);
                    if(!res.disableErrorMessage){
                        ElMessage.error(res.msg)
                    }
                    break;
            }
        }else {
            console.log('未知错误:', res);
        }
    }else {
        console.error('network error: ', error);
        ElMessage.error('请求超时，请检查网络')
    }
}

function getRealUrl(url) {
    if(url.indexOf(window.baseApiUrl) === 0 || url.indexOf('http://') === 0 || url.indexOf('https://') === 0){
        return url;
    }
    if(url.indexOf('/') === 0){
        url = url.substr(1);
    }
    return window.baseApiUrl + url
}

function get(url, params = {}, defaultHandlerRes=true, headers = {}) {
    headers['X-Requested-With'] = 'XMLHttpRequest';
    let options = {
        headers: headers,
        params: params,
    };
    url = getRealUrl(url);
    let promise = axios.get(url, options).then(res => {
        // console.log(url, res)
        let result = res.data;
        if(defaultHandlerRes){
            return handlerRes(result);
        }else {
            return result;
        }
    });
    promise.catch(handlerError);
    return promise;
}

function post(url, params, defaultHandlerRes=true, headers = {}) {
    headers['X-Requested-With'] = 'XMLHttpRequest';
    let options = {
        headers: headers,
        timeout: 1000 * 30,
    };
    url = getRealUrl(url);
    let promise = axios.post(url, params, options).then(res => {
        let result = res.data;
        if(defaultHandlerRes){
            return handlerRes(result);
        }else {
            return result;
        }
    });
    promise.catch(handlerError);
    return promise;
}

function mockData(data) {
    return new Promise((resolve, reject) => {
        resolve(data);
    })
}

const api = {
    get,
    post,
    getRealUrl,
    mockData,
    handlerRes,
    CODE_OK,
    CODE_UN_LOGIN,
}
export default api