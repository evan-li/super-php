import {isNavigationFailure, NavigationFailureType, useRoute} from "vue-router";
import qs from 'qs';
import {ElAlert, ElLoading, ElMessage, ElMessageBox, ElNotification} from "element-plus";
import router from "@/router/index.js";
import api from "@/util/api.js";
import _ from "lodash";
import bus from './eventBus'
import {usePageConfig} from "@/store/pageConfig.js";

export const btnProcessOption = (option, inDialog = false) => {
    // 按钮实际操作的方法
    const pageConfig = usePageConfig()
    let optionFun;
    switch (option?.type) {
        case 'refresh': // 刷新页面
            optionFun = () => {
                // 在弹窗中则刷新当前弹窗页面
                if(inDialog){
                    pageConfig.refreshLastDialog()
                }else {
                    let route = router.currentRoute.value
                    router.push({
                        path: '/refresh',
                        query: {
                            from: JSON.stringify({
                                path: route.path,
                                query: route.query,
                            })
                        }
                    })
                }
            }
            break;
        case 'back':
            optionFun = () => {
                // 在弹窗中则关闭弹窗，否则返回
                if(inDialog){
                    pageConfig.closeLastDialog()
                }else {
                    router.go(-1)
                }
            }
            break;
        case 'link': // 打开连接
            optionFun = () => {
                if(option.target === '_blank'){
                    window.open(buildUrlWithQuery(option.url, option.extra_data));
                }else {
                    // 判断是否站内链接, 如果是站内链接, 则直接使用router跳转
                    if(option.url.startsWith('http://') || option.url.startsWith('https://') || option.url.startsWith('//')){
                        location.href = buildUrlWithQuery(option.url, option.extra_data);
                    }else {
                        // 在弹窗中则关闭弹窗
                        if(inDialog){
                            pageConfig.clearDialog()
                        }
                        router.push({
                            path: option.url,
                            query: option.extra_data,
                        })
                    }
                }
            }
            break;
        case 'pop': // 打开弹窗
            optionFun = () => {
                pageConfig.showDialog(buildUrlWithQuery(option.url, option.extra_data))
            }
            break;
        case 'ajax': // ajax提交表单
            optionFun = async () => {
                let method = option.method || 'post';
                if(typeof option.before_ajax_submit == 'function'){
                    let res = await option.before_ajax_submit(option.extra_data)
                    if(res === false){ // 阻止表单提交
                        return ;
                    }
                    option.extra_data = res;
                }
                // ajax操作超过300毫秒设置全局loading
                let loading = null;
                let timeout = setTimeout(() => {
                    loading = ElLoading.service({
                        lock: true,
                        text: 'Loading',
                        spinner: 'el-icon-Loading',
                        background: 'rgba(0, 0, 0, 0.7)'
                    });
                }, 300)
                api[method](option.url, option.extra_data, false).then(res => {
                    if(res.code == 200){
                        let msg = res.msg || '';
                        if(msg){ // 如果返回消息才提示
                            let msgType = 'message'
                            if(msg.startsWith('alert:')) {
                                msgType = 'alert'
                                msg = msg.substr(6)
                            }
                            if(res.data.type == 'back'){
                                msg += ', 页面即将返回'
                            }else if(res.data.type == 'refresh'){
                                msg += ', 页面即将刷新'
                            }else if(res.data.type == 'link'){
                                msg += ', 页面即将跳转'
                            }else if(res.data.type == 'pop'){
                                msg += ', 页面即将跳转'
                            }
                            if (msgType == 'alert') {
                                ElAlert(msg)
                            }else {
                                ElMessage.success(msg)
                            }
                        }
                    }else {
                        ElMessage.error(res.msg || '操作失败')
                    }
                    let wait = res.data?.wait || 0;
                    if(wait > 0) wait = wait * 1000;
                    else wait = 0;
                    setTimeout(() => {
                        btnProcessOption(res.data, inDialog)
                    }, wait)
                }).finally(() => {
                    // 清除定时器及加载框
                    clearTimeout(timeout)
                    if(loading){
                        loading.close();
                        loading = null;
                    }
                })
            }
            break;
        case 'relist':
            // 在弹窗中则关闭弹窗
            if(inDialog){
                pageConfig.clearDialog()
            }

            bus.emit('table-relist')
            break;
        case 'event': // 表单数据通过事件发送
            optionFun = async () => {
                if (option.with_form_data) {
                    if(typeof option.before_ajax_submit == 'function'){
                        let res = await option.before_ajax_submit(option.extra_data)
                        if(res === false){ // 阻止表单提交
                            return ;
                        }
                        option.extra_data = res;
                    }
                }
                bus.emit(option.url, option.extra_data)
            }
            break;
        default:
            console.log('process option 无操作: ', option)
    }

    // 是否需要弹出确认框
    if(optionFun && typeof optionFun == 'function') {
        if(option.confirm){
            ElMessageBox.confirm(option.confirm.tips || '确定要做此操作吗?', option.confirm.title || '提示', {type: option.confirm.type}).then(function(){
                optionFun();
            })
        }else {
            optionFun();
        }
    }
}
export const buildUrlWithQuery = function(url, params){
    if (!params) return url
    let prefix = '?'
    if (url.match(/\?/g)) {
        prefix = '&'
    }
    return url + prefix + qs.stringify(params)
}
export const parseUrlByData = (url, data) => {
    let matches = url.match(/__[\w]+__/g);
    if(matches) {
        matches.forEach(match => {
            let val = _.at(data, match.substring(2, match.length - 2))[0]
            // 需要有效的值才进行替换
            if(typeof val !== 'undefined' && val !== null) {
                url = url.replace(match, val)
            }
        })
    }
    return url;
}

/**
 * 导航失败有错误消息的路由push
 * @param to — 导航位置，同 router.push
 */
export const routePush = async (to) => {
    try {
        const failure = await router.push(to)
        if (isNavigationFailure(failure, NavigationFailureType.aborted)) {
            ElNotification({
                message: '导航失败，导航守卫拦截！',
                type: 'error',
            })
        } else if (isNavigationFailure(failure, NavigationFailureType.duplicated)) {
            ElNotification({
                message: '导航失败，已在导航目标位置！',
                type: 'warning',
            })
        }
    } catch (error) {
        ElNotification.error('导航失败，路由无效！')
        console.error(error)
    }
}

/**
 * 加载网络css文件
 * @param url css资源url
 */
export function loadCss(url) {
    const link = document.createElement('link')
    link.rel = 'stylesheet'
    link.href = url
    link.crossOrigin = 'anonymous'
    document.getElementsByTagName('head')[0].appendChild(link)
}

/**
 * 加载网络js文件
 * @param url js资源url
 */
export function loadJs(url) {
    const link = document.createElement('script')
    link.src = url
    document.body.appendChild(link)
}

/**
 * 是否是外部链接
 * @param path
 */
export function isExternal(path) {
    return /^(https?|ftp|mailto|tel):/.test(path)
}

const hexList = []
for (let i = 0; i <= 15; i++) {
    hexList[i] = i.toString(16)
}
/**
 * 生成全球唯一标识
 * @returns {string} uuid
 */
export function uuid() {
    let uuid = ''
    for (let i = 1; i <= 36; i++) {
        if (i === 9 || i === 14 || i === 19 || i === 24) {
            uuid += '-'
        } else if (i === 15) {
            uuid += 4
        } else if (i === 20) {
            uuid += hexList[(Math.random() * 4) | 8]
        } else {
            uuid += hexList[(Math.random() * 16) | 0]
        }
    }
    return uuid
}

export function isMobile(){
    // 检查 userAgent 是否包含手机端的特征字符串
    const userAgent = navigator.userAgent || navigator.vendor || window.opera;

    // 移动端设备常见的userAgent关键词
    return /android|iphone|ipod|ipad|windows phone/i.test(userAgent);
}

export default {
    routePush,
    loadCss,
    loadJs,
    isExternal,
    uuid,
    parseUrlByData,
    buildUrlWithQuery,
    btnProcessOption,
    isMobile,
}