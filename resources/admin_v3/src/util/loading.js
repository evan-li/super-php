import { nextTick } from 'vue'
import '@/style/loading.scss'

/**
 * 页面加载效果
 * @type {{hide: loading.hide, show: loading.show}}
 */
export const loading = {
    show: () => {
        const bodys = document.body
        const div = document.createElement('div')
        div.className = 'block-loading'
        div.innerHTML = `
            <div class="block-loading-box">
                <div class="block-loading-box-warp">
                    <div class="block-loading-box-item"></div>
                    <div class="block-loading-box-item"></div>
                    <div class="block-loading-box-item"></div>
                    <div class="block-loading-box-item"></div>
                    <div class="block-loading-box-item"></div>
                    <div class="block-loading-box-item"></div>
                    <div class="block-loading-box-item"></div>
                    <div class="block-loading-box-item"></div>
                    <div class="block-loading-box-item"></div>
                </div>
            </div>
        `
        bodys.insertBefore(div, bodys.childNodes[0])
    },
    hide: () => {
        nextTick(() => {
            setTimeout(() => {
                const el = document.querySelector('.block-loading')
                el && el.parentNode?.removeChild(el)
            }, 300)
        })
    },
}
