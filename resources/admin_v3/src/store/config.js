import {defineStore} from "pinia";
import {uuid} from "@/util/util.js";
import api from "@/util/api.js";
import {ElLoading, ElMessage} from "element-plus";

export const useConfig = defineStore('config', {
    persist: { key: 'storeConfig', },
    state: () => {
        return {
            layout: {
                // 后台布局方式，可选值<Default|Classic|Streamline|Double>
                mode: 'Default',
                // 是否暗黑模式
                isDark: false,
                // 是否收缩布局(小屏设备)
                shrink: false,
                // 后台主页面切换动画，可选值<slide-right|slide-left|el-fade-in-linear|el-fade-in|el-zoom-in-center|el-zoom-in-top|el-zoom-in-bottom>
                mainAnimation: 'slide-right',
                // 是否显示设置面板
                showSettingPanel: false,
                // 是否水平折叠收起菜单
                menuCollapse: false,
                // 侧边菜单宽度(展开时)，单位px
                menuWidth: 260,
                // 侧边菜单项默认图标
                menuDefaultIcon: 'el-icon-Menu',
                // 是否只保持一个子菜单的展开(手风琴)
                menuUniqueOpened: false,
                // 显示菜单栏顶栏(LOGO)
                menuShowTopBar: true,
                // 是否全屏当前显示的tab页
                tabFullScreen: false,

                themes: {
                    // 默认主题
                    default: {
                        /* 侧边菜单 */
                        // 侧边菜单背景色
                        menuBackground: '#ffffff',
                        // 侧边菜单文字颜色
                        menuColor: '#303133',
                        // 侧边菜单激活项背景色
                        menuActiveBackground: '#ffffff',
                        // 侧边菜单激活项文字色
                        menuActiveColor: '#409eff',
                        // 侧边菜单顶栏背景色
                        menuTopBarBackground: '#fcfcfc',
                        /* 顶栏 */
                        // 顶栏文字色
                        headerBarTabColor: '#000000',
                        // 顶栏激活项背景色
                        headerBarTabActiveBackground: '#ffffff',
                        // 顶栏激活项文字色
                        headerBarTabActiveColor: '#000000',
                        // 顶栏背景色
                        headerBarBackground: '#ffffff',
                        // 顶栏悬停时背景色
                        headerBarHoverBackground: '#f5f5f5',
                    },
                    // 暗黑模式
                    dark: {
                        /* 侧边菜单 */
                        // 侧边菜单背景色
                        menuBackground: '#1d1e1f',
                        // 侧边菜单文字颜色
                        menuColor: '#CFD3DC',
                        // 侧边菜单激活项背景色
                        menuActiveBackground: '#1d1e1f',
                        // 侧边菜单激活项文字色
                        menuActiveColor: '#3375b9',
                        // 侧边菜单顶栏背景色
                        menuTopBarBackground: '#1d1e1f',
                        /* 顶栏 */
                        // 顶栏文字色
                        headerBarTabColor: '#CFD3DC',
                        // 顶栏激活项背景色
                        headerBarTabActiveBackground: '#1d1e1f',
                        // 顶栏激活项文字色
                        headerBarTabActiveColor: '#409EFF',
                        // 顶栏背景色
                        headerBarBackground: '#1d1e1f',
                        // 顶栏悬停时背景色
                        headerBarHoverBackground: '#18222c',
                    }
                },
            },
            // 多语言
            lang: {
                // 默认语言，可选值<zh-cn|en>
                defaultLang: 'zh-cn',
                // 当在默认语言包找不到翻译时，继续在 fallbackLang 语言包内查找翻译
                fallbackLang: 'zh-cn',
                // 支持的语言列表
                langArray: [
                    { name: 'zh-cn', value: '中文简体' },
                    { name: 'en', value: 'English' },
                ],
            },
            system: { // 系统配置信息
                title: 'Loading...', // 站点标题
                description: '', // 站点描述
                keyword: '', // 站点关键字
                login_need_captcha: false, // 登录时是否需要验证码
                captcha_url: '', // 验证码地址

                clear_cache_url: '', // 清空缓存操作地址
                clear_session_url: '', // 清空session操作地址
                init_data_url: '', // 获取初始化数据地址(包含用户信息/菜单列表)
                user_info_url: '', // 获取用户信息地址
                menu_url: '', // 获取菜单列表地址
                login_url: '', // 登陆地址
                logout_url: '', // 退出登陆地址
                modify_password_url: '', // 修改密码地址
                index_page_path: '/dashboard',
                file_upload_url: '/system/attachment/upload/dir/files', // 文件上传地址
                big_file_upload_url: '/system/attachment/bigUpload/dir/files', // 大文件上传地址
                image_upload_url: '/system/attachment/upload/dir/images', // 图片上传地址
                video_upload_url: '/system/attachment/upload/dir/videos', // 图片上传地址
                file_path_url: '/system/attachment/getFilesPath', // 图片上传地址
                store_base_url: '', // 模块市场地址
                icons: [], // 图标库列表
                debug: false, // 是否调试模式
            },
            mobile: {
                isMobile: false,
                showMenu: false,
            }

        }
    },
    getters: {
        currentTheme(state){
            return state.layout.isDark ? state.layout.themes.dark : state.layout.themes.default
        }
    },
    actions: {
        // 内容窗体高度
        getMainHeight(extra = 0){
            const headerHeight = 70;
            let height = extra
            if(!this.layout.tabFullScreen) {
                height += headerHeight
            }
            return {
                height: 'calc(100vh - ' + height.toString() + 'px)',
            }
        },
        setLayoutMode(mode){
            this.layout.mode = mode
            this.onSetLayoutColor()
        },
        onSetLayoutColor(mode = this.layout.mode){
            const tempValue = this.layout.isDark ? { idx: 1, color: '#1d1e1f', newColor: '#141414' } : { idx: 0, color: '#ffffff', newColor: '#f5f5f5' }
            let theme = this.layout.isDark ? this.layout.themes.dark : this.layout.themes.default

            if (mode === 'Classic' && theme.headerBarTabActiveBackground === tempValue.color) {
                theme.headerBarTabActiveBackground = tempValue.newColor
            }else if(mode === 'Default' && theme.headerBarTabActiveBackground === tempValue.newColor) {
                theme.headerBarTabActiveBackground[tempValue.idx] = tempValue.color
            }
        },
        setCurrentThemeValue(field, value){
            let currentTheme = this.layout.isDark ? this.layout.themes.dark : this.layout.themes.default
            currentTheme[field] = value
        },

    }
})