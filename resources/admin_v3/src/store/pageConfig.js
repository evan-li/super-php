import {defineStore} from "pinia";
import {ElLoading, ElMessage} from "element-plus";
import api from "@/util/api.js";
import {uuid} from "@/util/util.js";
import router from "@/router/index.js";

export const usePageConfig = defineStore('pageConfig', {
    // persist: { key: 'storePageConfig', },
    state: () => {
        return {
            // 全局loading
            globalLoading: false,
            // 页面加载中状态
            pageLoading: false,
            // 当前页面配置
            currentPage: {},
            // 当前显示的弹框列表, 关闭弹框时, 从弹框列表移除
            dialogs: [],
        }
    },
    actions: {
        showDialog(url){
            // 根据url获取页面配置信息
            const loadingInstance = ElLoading.service({ fullscreen: true })
            api.get(url, {}, false, {'x-page': 1}).then(res => {
                // 开启页面loading
                let pageConfig = res;
                if(res.code == api.CODE_UN_LOGIN){
                    // 未登录
                    ElMessage.error('您尚未登陆')
                    return router.push({path: '/login'})
                }else if(res.code == 404){
                    pageConfig = {
                        type: 'error',
                        page_title: '错误页面',
                        blocks: [{
                            layout: {},
                            name: 'error',
                            type: 'error',
                            error: {code: 404, msg: '啊哦, 页面走丢了~'},
                        }],
                    }
                }else if(res.code === 0 || res.code > 0){
                    pageConfig = {
                        type: 'error',
                        page_title: '错误页面',
                        blocks: [{
                            layout: {},
                            name: 'error',
                            type: 'error',
                            error: {code: res.code || '500', msg: res.msg || '服务器错误'},
                        }],
                    };
                }
                this.addDialog(url, pageConfig)
            }).finally(() => {
                loadingInstance.close()
            })
        },
        addDialog(url, pageConfig){
            this.dialogs.push({
                url,
                pageConfig,
                uuid: uuid(),
                show: true,
            })
        },
        closeDialog(dialog) {
            if(!dialog) return this.closeLastDialog()

            let index = this.dialogs.findIndex(item => item.uuid == dialog.uuid)
            this.dialogs[index].show = false
            return this.dialogs.splice(index, 1)
        },
        closeLastDialog() {
            return this.dialogs.splice(this.dialogs.length - 1, 1)
        },
        clearDialog() {
            this.dialogs.forEach(item => item.show = false)
            this.dialogs = []
        },
        // 刷新最上层的弹窗
        refreshLastDialog(){
            let dialog = this.dialogs[this.dialogs.length - 1]
            this.closeLastDialog()
            this.showDialog(dialog.url)
        }
    }
})