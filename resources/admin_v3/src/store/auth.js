import {defineStore} from "pinia";
import _ from "lodash";


// import menus from '@/mock/menus'

export const useAuth = defineStore('auth', {
    persist: { key: 'storeAuth', },
    state: () => {
        return {
            menus: [],
            currentMenuIndex: '',

            navTabs: [],

            // 当前登录用户
            user: {
                username: 'admin',
                nickname: 'Admin',
                lastlogintime: '2023-3-18 15:49:18',
                avatar: '',
                super: true,
                accessToken: null,
            }
        }
    },
    getters: {
        // 激活的tab
        activeTab(state) {
            return state.navTabs[this.activeTabIndex]
        },
        // 当前激活tab的index
        activeTabIndex(state){
            return _.findIndex(state.navTabs, function (item) {return item.active})
        }
    },
    actions: {
        addNavTab: function (menu) {
            // if (!menu.addtab) return
            for (const key in this.navTabs) {
                if (this.navTabs[key].id === menu.id) {
                    this.navTabs[key].params = !_.isEmpty(menu.params) ? menu.params : this.navTabs[key].params
                    this.navTabs[key].query = !_.isEmpty(menu.query) ? menu.query : this.navTabs[key].query
                    return this.navTabs[key]
                }
            }
            // if (typeof menu.title == 'string') {
            //     menu.title = menu.title.indexOf('pagesTitle.') === -1 ? menu.title : i18n.global.t(menu.title)
            // }

            let tab = JSON.parse(JSON.stringify(menu))
            tab.active = true
            this.navTabs.push(tab)
            return tab
        },
        setActiveNavTab: function (tab) {
            if(this.activeTab) this.activeTab.active = false
            const currentRouteIndex = this.navTabs.findIndex((item) => {
                return item.id === tab.id
            })
            this.navTabs[currentRouteIndex].active = true
        },
        closeNavTab: function (menu) {
            this.navTabs.map((v, k) => {
                if (v.id === menu.id) {
                    if (v.active) {
                        // 如果当前删除的tab是激活状态的, 激活相邻的标签页
                        if (this.navTabs.length > k+1) {
                            this.navTabs[k+1].active = true
                        }else {
                            this.navTabs[k-1] && (this.navTabs[k-1].active = true)
                        }
                    }
                    this.navTabs.splice(k, 1)
                }
            })
        },
        /**
         * 关闭多个标签
         * @param retainMenu 需要保留的标签，否则关闭全部标签
         */
        closeNavTabs: function (retainMenu = false) {
            if (retainMenu) {
                this.navTabs = [retainMenu]
            } else {
                this.navTabs = []
            }
        },

        getFirstMenu: function () {
            let find = false
            for (const key in this.menus) {
                if (this.menus[key].type !== 'menu_dir') {
                    return this.menus[key]
                } else if (this.menus[key].children && this.menus[key].children?.length) {
                    find = this.getFirstMenu(this.menus[key].children)
                    if (find) return find
                }
            }
            return find
        },
        getMenuByPath: function (path, menus = null) {
            if(!menus) menus = this.menus
            for (const key in menus) {
                if (menus[key].url === path) {
                    return menus[key]
                } else if (menus[key].children && menus[key].children?.length) {
                    let find = this.getMenuByPath(path, menus[key].children)
                    if (find) return find
                }
            }
            return false
        },

        checkLogin(){
            return this.user && !!this.user.accessToken
        }
    }
})