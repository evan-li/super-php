export default {

    Loading: '加载中...',
    Reload: '重新加载',
    comma: '，',
    'Welcome back!': '欢迎回来！',
    'It is late at night. Please tack care of your body!': '夜深了，注意身体哦！',
    'good morning!': '早上好！',
    'Good morning!': '上午好！',
    'Good noon!': '中午好！',
    'Good afternoon': '下午好！',
    'Good evening': '晚上好！',
    'Hello!': '您好！',
    open: '开启',
    close: '关闭',
}