import {useConfig} from "@/store/config.js";
import {createI18n} from "vue-i18n";

export let i18n
export async function initLang (app) {

    const config = useConfig()
    let locale = config.lang.defaultLang
    let message = {}
    if (locale == 'zh-cn') {
        // import.meta.glob 路径不能传变量
        message = (await import('./zh-cn')).default ?? {}
        // 加载模块语言包
        Object.assign(message, getLangFileMessage(import.meta.glob('./zh-cn/**/*.js', { eager: true }), locale))
    }else if (locale == 'en') {
        // import.meta.glob 路径不能传变量
        message = (await import('./en')).default ?? {}
        // 加载模块语言包
        Object.assign(message, getLangFileMessage(import.meta.glob('./en/**/*.js', { eager: true }), locale))
    }

    const messages = {
        [locale]: message
    }
    i18n = createI18n({
        locale,
        legacy: false, // 组合式api
        globalInjection: true, // 挂载$t,$d等到全局
        fallbackLocale: config.lang.fallbackLang,
        messages,
    })
    app.use(i18n)
    return i18n
}

export function changeDefaultLang(lang) {
    const config = useConfig()
    config.lang.defaultLang = lang

    /*
     * 语言包是按需加载的,比如默认语言为中文,则只在app实例内加载了中文语言包,所以切换语言需要进行 reload
     */
    location.reload()
}

function getLangFileMessage(mList, locale) {
    let msg = {}
    locale = '/' + locale
    for (const path in mList) {
        if (mList[path].default) {
            //  获取文件名
            const pathName = path.slice(path.lastIndexOf(locale) + (locale.length + 1), path.lastIndexOf('.'))
            if (pathName.indexOf('/') > 0) {
                msg = handleMsglist(msg, mList[path].default, pathName)
            } else {
                msg[pathName] = mList[path].default
            }
        }
    }
    return msg
}

export function handleMsglist(msg, mList, pathName) {
    const pathNameTmp = pathName.split('/')
    let obj = {}
    for (let i = pathNameTmp.length - 1; i >= 0; i--) {
        if (i == pathNameTmp.length - 1) {
            obj = {
                [pathNameTmp[i]]: mList,
            }
        } else {
            obj = {
                [pathNameTmp[i]]: obj,
            }
        }
    }
    return mergeMsg(msg, obj)
}

export function mergeMsg(msg, obj) {
    for (const key in obj) {
        if (typeof msg[key] == 'undefined') {
            msg[key] = obj[key]
        } else if (typeof msg[key] == 'object') {
            msg[key] = mergeMsg(msg[key], obj[key])
        }
    }
    return msg
}