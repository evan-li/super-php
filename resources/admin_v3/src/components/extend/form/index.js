/*
 * 请在此处引入自定义组件, 如: import demo from './demo'
 */
import goods_sku from './goods-sku.vue'
import goods_shipping_template from './goods_shipping_template.vue'
import ad_items from './ad-items.vue'
import cps_goods_select from './cps-goods-select.vue'
import goods_material_select from './goods-material-select.vue'
import group_goods_select from './group-goods-select.vue'
import group_sale_setting from './group_sale_setting.vue'
import mp_tabbar from './mp-tabbar.vue'

/**
 * 自定义form组件扩展, 需要在form目录中加入自定义的组件, 并在export中导出
 */
export default {
    /*
     * 请在此处导出自定义组件, 如: demo,
     */
    goods_sku,
    goods_shipping_template,
    ad_items,
    cps_goods_select,
    goods_material_select,
    group_goods_select,
    group_sale_setting,
    mp_tabbar,
}