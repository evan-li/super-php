/*
 * 请在此处引入自定义组件, 如: import demo from './demo'
 */
import goods_form from './goods-form.vue'
/**
 * 自定义区块组件扩展, 需要在此目录中加入自定义的组件, 并在export中导出
 */
export default {
    /*
     * 请在此处导出自定义组件, 如: demo,
     */
    goods_form,
}