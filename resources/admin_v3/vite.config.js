import { fileURLToPath, URL } from 'node:url'
import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import { svgBuilder } from './src/components/common/icon/svg/index'

// https://vitejs.dev/config/
export default defineConfig(({command, mode}) => {
  const {VITE_AXIOS_BASE_URL} = loadEnv(mode, process.cwd(), '')
  return {
    plugins: [vue(), svgBuilder('./src/assets/icons/')],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url)),
      }
    },
    build: {
      manifest: true,
      // rollupOptions: {
      //   // 覆盖默认的 .html 入口
      //   input: './src/main.js'
      // },
      outDir: '../../public'
    },
    server: {
      open: true,
      proxy: {
        // 带选项写法：http://localhost:5173/api/bar -> http://jsonplaceholder.typicode.com/bar
        [VITE_AXIOS_BASE_URL]: {
          target: 'http://admin.sphp.local.niucha.ren/',
          changeOrigin: true,
          rewrite: (path) => path.replace(VITE_AXIOS_BASE_URL, ''),
        },
        // 静态资源访问
        '/static': {
          target: 'http://admin.sphp.local.niucha.ren/',
          changeOrigin: true,
        },
        // ueditor上传文件访问
        '/ueditor/php/upload': {
          target: 'http://admin.sphp.local.niucha.ren/',
          changeOrigin: true,
        },
        // 上传文件访问
        '/uploads': {
          target: 'http://admin.sphp.local.niucha.ren/',
          changeOrigin: true,
        },
        // iframe测试
        '/iframe-demo.html': {
          target: 'http://admin.sphp.local.niucha.ren/',
          changeOrigin: true,
        },
      },
    },
  }

})
